CREATE TABLE cms_supplier_price_export (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL COMMENT 'Наименование',
  provider_id varchar(255) DEFAULT NULL COMMENT 'Поставщики',
  good_type varchar(255) DEFAULT NULL COMMENT 'Тип товаров',
  columns varchar(1000) DEFAULT NULL COMMENT 'Колонки (заголовки)',
  is_cron tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Импорт по крону',
  cron_periodicity varchar(255) NOT NULL COMMENT 'Периодичность импортa по крону',
  url varchar(255) DEFAULT NULL COMMENT 'Ссылка файла',
  min_size int(11) DEFAULT NULL COMMENT 'От какого количества импортировать',
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Экспорты таваров в файл';