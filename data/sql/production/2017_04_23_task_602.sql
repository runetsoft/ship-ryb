-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.0.493.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 23.04.2017 15:29:40
-- Версия сервера: 5.5.29-0ubuntu0.12.04.1-log
-- Версия клиента: 4.1

SET NAMES 'utf8';

CREATE TABLE cms_supplier_price_import (
  id INT NOT NULL AUTO_INCREMENT,
  supplier_id INT DEFAULT NULL COMMENT 'id поставщика',
  import_type SMALLINT DEFAULT NULL COMMENT '(1 - url, 2 - почта)',
  import_info VARCHAR(255) DEFAULT NULL COMMENT 'url файла для загрузки или почтовый ящик',
  encoding varchar(255) DEFAULT NULL,
  is_cron BOOLEAN NOT NULL DEFAULT '0' COMMENT 'Импорт по крону',
  cron_periodicity VARCHAR(255) NOT NULL COMMENT 'Периодичность импортa по крону',
  PRIMARY KEY (id)
)
ENGINE = INNODB
COMMENT = 'Способы импорта остатков';

CREATE TABLE cms_import_file_structure (
  id int(11) NOT NULL AUTO_INCREMENT,
  supplier_price_import_id int(11) DEFAULT NULL,
  book_name varchar(50) DEFAULT NULL COMMENT 'Имя книги excel',
  entity varchar(50) DEFAULT NULL COMMENT 'Класс данных',
  fields_match text DEFAULT NULL COMMENT 'Cериализованная информация о соответствии полей книги и таблицы',
  default_data text DEFAULT NULL COMMENT 'Значения поумодлчанию',
  preparer_class varchar(255) DEFAULT NULL COMMENT 'Класс обработчика строк (имя сервиса)',
  primary_field varchar(255) DEFAULT NULL COMMENT 'Колонка по которой идет сравнение',
  start_row int(11) DEFAULT NULL COMMENT 'Стартовая строка (с заголовком)',
  PRIMARY KEY (id),
  UNIQUE INDEX UK_cms_import_file_structure (supplier_price_import_id, book_name)
)
ENGINE = INNODB
COMMENT = 'Структура файла: импорта описывает одну книгу в файле';

--
-- Изменить таблицу "cms_price"
--
ALTER TABLE cms_price
  CHANGE COLUMN zarticle zarticle VARCHAR(255) DEFAULT NULL;

