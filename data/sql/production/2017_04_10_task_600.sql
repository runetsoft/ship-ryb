
CREATE TABLE cms_dictionary_tyre (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = MYISAM
  AUTO_INCREMENT = 1
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  ROW_FORMAT = fixed;

CREATE TABLE cms_dictionary_disk (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = MYISAM
  AUTO_INCREMENT = 1
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  ROW_FORMAT = fixed;

ALTER TABLE cms_good
  ADD COLUMN type INT DEFAULT NULL AFTER not_load;

CREATE TABLE cms_good_tyre (
  good_id int(11) NOT NULL COMMENT 'id товара',
  brand varchar(255) DEFAULT NULL COMMENT 'бренд',
  model varchar(50) DEFAULT NULL COMMENT 'модель',
  width decimal(3, 2) DEFAULT NULL COMMENT 'ширина',
  height decimal(3, 2) DEFAULT NULL COMMENT 'высота',
  construction varchar(255) DEFAULT NULL COMMENT 'конструкция',
  diameter decimal(3, 2) DEFAULT NULL COMMENT 'диаметр',
  index_load decimal(3, 0) DEFAULT NULL COMMENT 'индекс нагрузки',
  index_speed varchar(255) DEFAULT NULL COMMENT 'индекс скорости',
  features varchar(255) DEFAULT NULL COMMENT 'характеризующие аббревиатуры',
  runflat tinyint(1) DEFAULT NULL COMMENT 'ранфлэт',
  camers varchar(255) DEFAULT NULL COMMENT 'камерность',
  season int(11) DEFAULT NULL COMMENT 'сезон',
  tyre_type tinyint(4) DEFAULT NULL COMMENT 'тип (1 - авто, 2 - мото )',
  axis tinyint(4) DEFAULT NULL COMMENT 'Ось (1 - перед, 2 - зад)',
  PRIMARY KEY (good_id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE cms_good_disk (
  good_id int(11) NOT NULL COMMENT 'id товара',
  brand varchar(255) DEFAULT NULL COMMENT 'бренд',
  model varchar(50) DEFAULT NULL COMMENT 'модель',
  width decimal(5, 2) DEFAULT NULL COMMENT 'ширина',
  diameter decimal(5, 2) DEFAULT NULL COMMENT 'диаметр',
  hole_num tinyint(4) DEFAULT NULL COMMENT 'количество отверстий',
  diameter_hole decimal(5, 2) DEFAULT NULL COMMENT 'диаметр отверстий',
  overhand tinyint(4) DEFAULT NULL COMMENT 'вылет',
  diameter_nave decimal(5, 2) DEFAULT NULL COMMENT 'диаметр ступичного отверстия',
  color varchar(255) DEFAULT NULL COMMENT 'цвет',
  disk_type tinyint(1) DEFAULT NULL COMMENT 'Тип диска (1 -Штампованные 2 - Литые 3 - Кованые)',
  PRIMARY KEY (good_id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;