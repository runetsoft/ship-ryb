DELETE FROM `cms_supplier_price_import_file` WHERE `deleted` = 1;
ALTER TABLE cms_import_file_structure
  ADD COLUMN no_upload TINYINT(1) DEFAULT 0 AFTER no_headers;
ALTER TABLE `cms_supplier_price_import_file` CHANGE COLUMN `deleted` `no_upload` smallint(6) DEFAULT 0;
ALTER TABLE `cms_price` MODIFY `rprice` mediumint(9) DEFAULT 0;