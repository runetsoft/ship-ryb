CREATE TABLE cms_dictionary_word_to_number (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

SET NAMES 'utf8';

INSERT INTO cms_dictionary_word_to_number(code, name) VALUES
('Много', '20');