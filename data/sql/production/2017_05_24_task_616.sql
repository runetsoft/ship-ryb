
DROP TABLE IF EXISTS `cms_dictionary_tyre_model`;
CREATE TABLE `cms_dictionary_tyre_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `cms_dictionary_tyre_model` (`name`)  SELECT DISTINCT `cms_good_tyre`.`model` FROM `cms_good_tyre`;


DROP TABLE IF EXISTS `cms_dictionary_disk_model`;
CREATE TABLE `cms_dictionary_disk_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `cms_dictionary_disk_model` (`name`)  SELECT DISTINCT `cms_good_disk`.`model` FROM `cms_good_disk`;