INSERT INTO cms_good_tyre_moto 
SELECT  good_id,
    brand,
    model,
    width,
    height,
    construction,
    diameter,
    index_load,
    index_speed,
    features,
    camers,
    axis 
FROM cms_good_tyre WHERE cms_good_tyre.tyre_type = 2;

DELETE FROM cms_good_tyre WHERE cms_good_tyre.tyre_type = 2;

UPDATE cms_good SET type = 3 WHERE id IN (SELECT good_id FROM cms_good_tyre_moto);
 