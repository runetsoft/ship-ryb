SET NAMES 'utf8';

CREATE TABLE cms_dictionary_good_type_category (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Категории типов товаров';

CREATE TABLE cms_dictionary_good_type (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Тип товара';

INSERT INTO cms_dictionary_good_type_category(id, code, name, parent_id) VALUES
(1, 'tyre_disk', 'Шины и диски ', NULL);

INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(1, 'disk', 'Диски', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(2, 'tyre', 'Шины', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(3, 'tyre_moto', 'Шины мото', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(4, 'good', 'Без типа', NULL);

CREATE TABLE ship_ryb.cms_dictionary_tyre_moto_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  code varchar(100) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE ship_ryb.cms_dictionary_tyre_moto_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  code varchar(100) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE cms_good_type_structure (
  id int(11) NOT NULL AUTO_INCREMENT,
  good_type_id int(11) DEFAULT NULL,
  structure text DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (good_type_id) REFERENCES cms_dictionary_good_type (id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Структура формы типа товара, в формате extJs/json';

INSERT INTO ship_ryb.cms_good_type_structure(id, good_type_id, structure) VALUES
(1, 1, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"27%","margin":"5 0 0 0","labelWidth":100},{"xtype":"textfield","fieldLabel":"количество отверстий","name":"hole_num","width":"24%","labelWidth":100},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"23%","labelWidth":70,"margin":"5 0 0 0"},{"xtype":"textfieldnumber","fieldLabel":"диаметр отверстий","name":"diameter_hole","width":"25%","labelWidth":80}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"вылет","name":"overhand","width":"26%"},{"xtype":"textfieldnumber","fieldLabel":"Ø ступичного отверстия","name":"diameter_nave","width":"40%","labelWidth":170},{"xtype":"textfield","fieldLabel":"цвет","name":"color","width":"33%","labelWidth":40}]},{"xtype":"dictionary","fieldLabel":"Тип диска","name":"disk_type","dictionaryName":"DiskType","valueField":"code"}]');
INSERT INTO ship_ryb.cms_good_type_structure(id, good_type_id, structure) VALUES
(2, 2, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"Construction","fieldLabel":"Конструкция","valueField":"code","name":"construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"Runflat","fieldLabel":"Ранфлэт","valueField":"code","displayField":"name","name":"runflat","width":"30%"},{"xtype":"dictionary","dictionaryName":"Camers","fieldLabel":"Камерность","valueField":"code","name":"camers","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","fieldLabel":"Сезон","name":"season","dictionaryName":"TyreSeason","valueField":"code","width":"30%"},{"xtype":"dictionary","dictionaryName":"Xl","fieldLabel":"XL","valueField":"code","name":"xl","width":"30%"},{"xtype":"checkbox","fieldLabel":"Карго","name":"cargo","width":"30%"}]}]');
INSERT INTO ship_ryb.cms_good_type_structure(id, good_type_id, structure) VALUES
(3, 3, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"Construction","fieldLabel":"Конструкция","valueField":"code","name":"construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"Camers","fieldLabel":"Камерность","valueField":"code","name":"camers","width":"26%"},{"xtype":"dictionary","fieldLabel":"Ось","name":"axis","dictionaryName":"TyreAxisMoto","reference":"axis","valueField":"code","width":"34%"}]}]');
INSERT INTO ship_ryb.cms_good_type_structure(id, good_type_id, structure) VALUES
(4, 4, '[]');