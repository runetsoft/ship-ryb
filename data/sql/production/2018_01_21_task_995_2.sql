SET NAMES 'utf8';

ALTER DATABASE shipryb_db
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

INSERT INTO cms_dictionary_good_type_category(id, code, name, parent_id) VALUES
(1, 'tyre_disk', 'Шины и диски ', NULL);

INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(1, 'disk', 'Диски', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(2, 'tyre', 'Шины', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(3, 'tyre_moto', 'Шины мото', 1);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(4, 'good', 'Без типа', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(6, 'coil', 'Катушка', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(7, 'rod', 'Удилище', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(8, 'minnow', 'Блесна', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(9, 'wobbler', 'Воблер', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(10, 'softbait', 'Мягкая приманка', NULL);

CREATE TABLE cms_dictionary_tyre_moto_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  code varchar(100) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE cms_dictionary_tyre_moto_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  code varchar(100) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE cms_good_type_structure (
  id int(11) NOT NULL AUTO_INCREMENT,
  good_type_id int(11) DEFAULT NULL,
  structure text DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (good_type_id) REFERENCES cms_dictionary_good_type (id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Структура формы типа товара, в формате extJs/json';

INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(1, 1, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"27%","margin":"5 0 0 0","labelWidth":100},{"xtype":"textfield","fieldLabel":"количество отверстий","name":"hole_num","width":"24%","labelWidth":100},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"23%","labelWidth":70,"margin":"5 0 0 0"},{"xtype":"textfieldnumber","fieldLabel":"диаметр отверстий","name":"diameter_hole","width":"25%","labelWidth":80}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"вылет","name":"overhand","width":"26%"},{"xtype":"textfieldnumber","fieldLabel":"Ø ступичного отверстия","name":"diameter_nave","width":"40%","labelWidth":170},{"xtype":"textfield","fieldLabel":"цвет","name":"color","width":"33%","labelWidth":40}]},{"xtype":"dictionary","fieldLabel":"Тип диска","name":"disk_type","dictionaryName":"DiskType","valueField":"code"}]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(2, 2, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"Construction","fieldLabel":"Конструкция","valueField":"code","name":"construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"Runflat","fieldLabel":"Ранфлэт","valueField":"code","displayField":"name","name":"runflat","width":"30%"},{"xtype":"dictionary","dictionaryName":"Camers","fieldLabel":"Камерность","valueField":"code","name":"camers","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","fieldLabel":"Сезон","name":"season","dictionaryName":"TyreSeason","valueField":"code","width":"30%"},{"xtype":"dictionary","dictionaryName":"Xl","fieldLabel":"XL","valueField":"code","name":"xl","width":"30%"},{"xtype":"checkbox","fieldLabel":"Карго","name":"cargo","width":"30%"}]}]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(3, 3, '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"Construction","fieldLabel":"Конструкция","valueField":"code","name":"construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"Camers","fieldLabel":"Камерность","valueField":"code","name":"camers","width":"26%"},{"xtype":"dictionary","fieldLabel":"Ось","name":"axis","dictionaryName":"TyreAxisMoto","reference":"axis","valueField":"code","width":"34%"}]}]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(4, 4, '[]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(5, 6, '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_coil_type", "xtype":"dictionary", "dictionaryName":"TypeCoil" }, { "fieldLabel":"Передаточное число", "name":"ratio" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Лесоемкость", "name":"intensity" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Наличие запасной шпули", "name":"spool" }, { "fieldLabel":"Количество подшипников", "name":"bearings" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Байтраннер", "name":"bytraner", "checked":false, "xtype":"checkboxfield" }, { "boxLabel":"Счетчик лески", "name":"line_counter", "checked":false, "xtype":"checkboxfield" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Защита от соленой воды", "name":"protection_water", "checked":false, "xtype":"checkboxfield" }, { "fieldLabel":"Рука", "name":"dictionary_coil_hand", "xtype":"dictionary", "dictionaryName":"HandCoil" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Наличие электропривода", "name":"electric_drive", "checked":false, "xtype":"checkboxfield" }, { "boxLabel":"Наличие трещетки", "name":"ratchets", "checked":false, "xtype":"checkboxfield" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Материал", "name":"dictionary_coil_material", "xtype":"dictionary", "dictionaryName":"MaterialCoil" } ] } ]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(6, 7, '[ { "xtype":"fieldset", "items":[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_rod_type", "xtype":"dictionary", "dictionaryName":"TypeRod" }, { "fieldLabel":"Тест", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Длина", "name":"length" }, { "fieldLabel":"Транспортировочная длина", "name":"shipping_length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество секций", "name":"sections_count" }, { "fieldLabel":"Вес", "name":"weight" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип соединения", "name":"dictionary_rod_connection_type", "xtype":"dictionary", "dictionaryName":"ConnectionTypeRod" }, { "fieldLabel":"Kоличество колец", "name":"rings_count" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Диаметр коннектора", "name":"connector_diameter" } ] } ] } ]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(7, 8, '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_minnow_type", "xtype":"dictionary", "dictionaryName":"TypeMinnow" }, { "fieldLabel":"Цвет", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"В упаковке", "name":"amount_package" }, { "fieldLabel":"Производитель", "name":"dictionary_minnow_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerMinnow" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Кратность упаковки", "name":"packing_ratio" } ] } ]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(8, 9, '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_wobbler_type", "xtype":"dictionary", "dictionaryName":"TypeWobbler" }, { "fieldLabel":"Цвет", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Заглубление", "name":"recess" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Производитель", "name":"dictionary_wobbler_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerWobbler" }, { "fieldLabel":"Плавучесть", "name":"dictionary_wobbler_buoyancy", "xtype":"dictionary", "dictionaryName":"BuoyancyWobbler" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Крючки", "name":"hooks" } ] } ]');
INSERT INTO cms_good_type_structure(id, good_type_id, structure) VALUES
(9, 10, '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_softbait_type", "xtype":"dictionary", "dictionaryName":"TypeSoftbait" }, { "fieldLabel":"Цвет", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество в упаковке", "name":"amount_package" }, { "boxLabel":"Плавучесть", "name":"dictionary_softbait_buoyancy", "checked":false, "xtype":"checkboxfield" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Наличие погремушки", "name":"line_counter", "checked":false, "xtype":"checkboxfield" } ] } ]');

CREATE TABLE cms_good_coil (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_coil_type int(11) DEFAULT NULL COMMENT 'Тип',
  ratio decimal(5, 2) DEFAULT 0 COMMENT 'Передаточное число',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  intensity varchar(255) DEFAULT NULL COMMENT 'Лесоемкость',
  spool varchar(50) DEFAULT NULL COMMENT 'Наличие запасной шпули',
  bearings decimal(5, 2) DEFAULT 0 COMMENT 'Количество подшипников',
  bytraner tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Байтраннер',
  line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Счетчик лески',
  protection_water tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Защита от соленой воды',
  dictionary_coil_hand int(11) DEFAULT NULL COMMENT 'Рука',
  electric_drive tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие электропривода',
  ratchets tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие трещетки',
  dictionary_coil_material int(11) DEFAULT NULL COMMENT 'Материал'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_rod (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_rod_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Тест',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  shipping_length decimal(5, 2) DEFAULT 0 COMMENT 'Транспортировочная длина',
  sections_count decimal(5, 2) DEFAULT 0 COMMENT 'Количество секций',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  dictionary_rod_connection_type int(11) DEFAULT NULL COMMENT 'Тип соединения',
  rings_count decimal(5, 2) DEFAULT 0 COMMENT 'Kоличество колец',
  connector_diameter decimal(5, 2) DEFAULT 0 COMMENT 'Диаметр коннектора'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_minnow (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_minnow_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  amount_package varchar(255) DEFAULT NULL COMMENT 'В упаковке',
  dictionary_minnow_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель',
  packing_ratio varchar(255) DEFAULT NULL COMMENT 'Кратность упаковки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_wobbler (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_wobbler_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  recess varchar(255) DEFAULT NULL COMMENT 'Заглубление',
  dictionary_wobbler_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель',
  dictionary_wobbler_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть',
  hooks varchar(255) DEFAULT NULL COMMENT 'Крючки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_softbait (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_softbait_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  amount_package varchar(255) DEFAULT NULL COMMENT 'Количество в упаковке',
  dictionary_softbait_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть',
  line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие погремушки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_dictionary_coil_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Бренд катушка';

CREATE TABLE cms_dictionary_coil_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Модел катушка';

CREATE TABLE cms_dictionary_rod_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Бренд удилище';

CREATE TABLE cms_dictionary_rod_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Модел удилище';


CREATE TABLE cms_dictionary_minnow_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Бренд блесна';

CREATE TABLE cms_dictionary_minnow_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Модел блесна';


CREATE TABLE cms_dictionary_wobbler_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Бренд воблер';

CREATE TABLE cms_dictionary_wobbler_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Модел воблер';

CREATE TABLE cms_dictionary_softbait_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Бренд мягкая приманка';

CREATE TABLE cms_dictionary_softbait_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Модел мягкая приманка';

UPDATE cms_good cg
SET cg.type = IF(cg.type = 0, 4, cg.type);

CREATE TABLE cms_dictionary_buoyancy_wobbler	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_camers	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_connection_type_rod	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_construction	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_disk_type	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_hand_coil	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_manufacturer_minnow	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_manufacturer_wobbler	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_material_coil	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_runflat	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_softbait_brand	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_softbait_model	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_type_coil	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_type_minnow	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_type_rod	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_type_softbait	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_type_wobbler	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_tyre_axis_moto	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_tyre_season	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;
CREATE TABLE cms_dictionary_xl	( id int(11) NOT NULL AUTO_INCREMENT, code varchar(255) DEFAULT NULL, name varchar(55) DEFAULT NULL, parent_id int(11) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8
COLLATE utf8_general_ci;

INSERT INTO cms_dictionary_buoyancy_wobbler(id, code, name, parent_id) VALUES
(1, NULL, 'test', NULL);
INSERT INTO cms_dictionary_buoyancy_wobbler(id, code, name, parent_id) VALUES
(2, NULL, 'тест', NULL);

INSERT INTO cms_dictionary_camers(id, code, name, parent_id) VALUES
(3, NULL, 'TL', NULL);
INSERT INTO cms_dictionary_camers(id, code, name, parent_id) VALUES
(4, NULL, 'TT', NULL);


INSERT INTO cms_dictionary_construction(id, code, name, parent_id) VALUES
(1, '1', 'R', NULL);
INSERT INTO cms_dictionary_construction(id, code, name, parent_id) VALUES
(2, '2', 'B', NULL);

INSERT INTO cms_dictionary_disk_type(id, code, name, parent_id) VALUES
(1, NULL, 'Штампованные', NULL);
INSERT INTO cms_dictionary_disk_type(id, code, name, parent_id) VALUES
(2, NULL, 'Литые', NULL);
INSERT INTO cms_dictionary_disk_type(id, code, name, parent_id) VALUES
(3, NULL, 'Кованые', NULL);
INSERT INTO cms_dictionary_disk_type(id, code, name, parent_id) VALUES
(4, NULL, 'Магниевые', NULL);

INSERT INTO cms_dictionary_manufacturer_minnow(id, code, name, parent_id) VALUES
(1, NULL, 'test', NULL);
INSERT INTO cms_dictionary_manufacturer_minnow(id, code, name, parent_id) VALUES
(2, NULL, 'тест', NULL);
INSERT INTO cms_dictionary_manufacturer_minnow(id, code, name, parent_id) VALUES
(3, NULL, 'тесттест', NULL);
INSERT INTO cms_dictionary_manufacturer_minnow(id, code, name, parent_id) VALUES
(4, NULL, 'тест', NULL);



INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(1, 'RunFlat', 'RunFlat', NULL);
INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(2, 'ZP', 'ZP', NULL);
INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(3, 'ZPS', 'ZPS', NULL);
INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(4, 'SSR', 'SSR', NULL);
INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(5, 'ROF', 'ROF', NULL);
INSERT INTO cms_dictionary_runflat(id, code, name, parent_id) VALUES
(6, 'RFT', 'RFT', NULL);

INSERT INTO cms_dictionary_softbait_brand(id, code, name, parent_id) VALUES
(1, 'test_1', 'softbait brand', NULL);

INSERT INTO cms_dictionary_softbait_model(id, code, name, parent_id) VALUES
(1, 'test_1', 'softbait model', 1);
INSERT INTO cms_dictionary_softbait_model(id, code, name, parent_id) VALUES
(2, NULL, 'тест', NULL);




INSERT INTO cms_dictionary_type_softbait(id, code, name, parent_id) VALUES
(1, 'test_code', 'softbait test', NULL);
INSERT INTO cms_dictionary_type_softbait(id, code, name, parent_id) VALUES
(2, '000', 'test', 1);
INSERT INTO cms_dictionary_type_softbait(id, code, name, parent_id) VALUES
(3, 'mnnmn', 'hjghg', NULL);


INSERT INTO cms_dictionary_tyre_axis_moto(id, code, name, parent_id) VALUES
(1, '1', 'Мото перед', NULL);
INSERT INTO cms_dictionary_tyre_axis_moto(id, code, name, parent_id) VALUES
(2, NULL, 'Мото зад', NULL);
INSERT INTO cms_dictionary_tyre_axis_moto(id, code, name, parent_id) VALUES
(3, NULL, 'Мото F/R', NULL);

INSERT INTO cms_dictionary_tyre_season(id, code, name, parent_id) VALUES
(1, '1', 'Зимние (нешипованные)', NULL);
INSERT INTO cms_dictionary_tyre_season(id, code, name, parent_id) VALUES
(2, NULL, 'Зимние (шипованные)', NULL);
INSERT INTO cms_dictionary_tyre_season(id, code, name, parent_id) VALUES
(3, NULL, 'Летние', NULL);
INSERT INTO cms_dictionary_tyre_season(id, code, name, parent_id) VALUES
(4, NULL, 'Внедорожные', NULL);
INSERT INTO cms_dictionary_tyre_season(id, code, name, parent_id) VALUES
(5, NULL, 'Внесезонные', NULL);

INSERT INTO cms_dictionary_xl(id, code, name, parent_id) VALUES
(1, NULL, 'XL', NULL);