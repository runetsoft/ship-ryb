--
ALTER TABLE cms_import_file_structure
  ADD COLUMN no_headers TINYINT(1) DEFAULT 0 AFTER start_row;

ALTER TABLE cms_supplier_price_export
  ADD COLUMN all_good TINYINT(1) DEFAULT 0 COMMENT 'Выгружать все товары даже без соответствий' AFTER min_size;

ALTER TABLE cms_import_log
  ADD COLUMN supplier_price_import_file_id INT DEFAULT NULL COMMENT 'supplier_price_import_id' AFTER min_size;

ALTER TABLE cms_supplier_price_import
  ADD COLUMN date_import DATETIME DEFAULT NULL COMMENT 'Дата последнего импорта' AFTER cron_periodicity;

