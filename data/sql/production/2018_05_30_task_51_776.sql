SET NAMES 'utf8';

ALTER TABLE cms_good
  CHANGE COLUMN id id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id товара',
  CHANGE COLUMN dt_insert dt_insert DATETIME NOT NULL COMMENT 'Дата создания товара',
  CHANGE COLUMN dt_update dt_update DATETIME NOT NULL COMMENT 'Дата обновления товара',
  CHANGE COLUMN dt_update_photo dt_update_photo DATETIME DEFAULT '1970-01-01 00:00:00' COMMENT 'Дата обновления фото товара',
  CHANGE COLUMN rprice rprice decimal(9, 2) DEFAULT NULL COMMENT 'МРЦ товара',
  CHANGE COLUMN adm_id adm_id tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Adm id',
  CHANGE COLUMN good good VARCHAR(255) NOT NULL COMMENT 'Названия товара',
  CHANGE COLUMN not_load not_load TINYINT(4) DEFAULT NULL COMMENT 'Не загружать',
  CHANGE COLUMN zarticle zarticle VARCHAR(255) DEFAULT NULL COMMENT 'Артикул',
  CHANGE COLUMN type type INT(11) DEFAULT NULL COMMENT 'Тип товара',
  CHANGE COLUMN photo photo VARCHAR(255) DEFAULT NULL COMMENT 'Фотография';

ALTER TABLE cms_price
  CHANGE COLUMN id id MEDIUMINT(9) NOT NULL AUTO_INCREMENT COMMENT 'Id предложения',
  CHANGE COLUMN good_id good_id MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT 'Id товара',
  CHANGE COLUMN dt_insert dt_insert DATETIME NOT NULL COMMENT 'Дата создания предложения',
  CHANGE COLUMN dt_update dt_update DATETIME NOT NULL COMMENT 'Дата обновления предложения',
  CHANGE COLUMN dt_price dt_price DATETIME NOT NULL COMMENT 'Дата обновления цены',
  CHANGE COLUMN provider_id provider_id SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Id поставщика',
  CHANGE COLUMN good good VARCHAR(255) NOT NULL COMMENT 'Названия предложения',
  CHANGE COLUMN photo photo VARCHAR(255) DEFAULT NULL COMMENT 'Фото предложения',
  CHANGE COLUMN price price MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'Цена',
  CHANGE COLUMN rprice rprice MEDIUMINT(9) DEFAULT 0 COMMENT 'МРЦ товара',
  CHANGE COLUMN zarticle zarticle VARCHAR(255) DEFAULT NULL COMMENT 'Артикул',
  CHANGE COLUMN sarticle sarticle VARCHAR(255) DEFAULT NULL COMMENT 'Артикул поставщика',
  CHANGE COLUMN sklad sklad SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Остаток',
  CHANGE COLUMN pass pass tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Пропуск',
  CHANGE COLUMN status status tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Статус';

UPDATE cms_good_disk cgd set cgd.diameter_hole = NULL WHERE cgd.diameter_hole = '0.00' OR cgd.diameter_hole = '';
ALTER TABLE cms_good_disk
  CHANGE COLUMN good_id good_id int(11) NOT NULL  COMMENT 'Id товара Диски',
  CHANGE COLUMN brand brand varchar(255) DEFAULT NULL COMMENT 'Бренд Диски',
  CHANGE COLUMN model model varchar(50) DEFAULT NULL COMMENT 'Модель Диски',
  CHANGE COLUMN width width decimal(5, 2) DEFAULT NULL COMMENT 'Ширина Диски',
  CHANGE COLUMN diameter diameter decimal(5, 2) DEFAULT NULL COMMENT 'Диаметр Диски',
  CHANGE COLUMN hole_num hole_num tinyint(4) DEFAULT NULL COMMENT 'Количество отверстий Диски',
  CHANGE COLUMN diameter_hole diameter_hole decimal(5, 2) DEFAULT NULL COMMENT 'Диаметр отверстий Диски',
  CHANGE COLUMN overhand overhand decimal(5, 1) DEFAULT NULL COMMENT 'Вылет Диски',
  CHANGE COLUMN diameter_nave diameter_nave decimal(5, 2) DEFAULT NULL COMMENT 'Диаметр ступичного отверстия Диски',
  CHANGE COLUMN color color varchar(255) DEFAULT NULL COMMENT 'Цвет Диски',
  CHANGE COLUMN disk_type dictionary_disk_type tinyint(4) DEFAULT NULL COMMENT 'Тип Диски';

UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = '', NULL, cgt.camers);
UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = 'TL', 3, cgt.camers);
UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = 'TT', 4, cgt.camers);

UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = '', NULL, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = '-', NULL, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = 'R', 1, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = 'B', 2, cgt.construction);

UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = '', NULL, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'RunFlat', 1, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ZP', 2, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ZPS', 3, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'SSR', 4, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ROF', 5, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'RFT', 6, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'EMT', 7, cgt.runflat);

UPDATE cms_good_tyre cgt set cgt.xl = IF(cgt.xl = '', NULL, cgt.xl);
UPDATE cms_good_tyre cgt set cgt.xl = IF(cgt.xl = 'XL', 1, cgt.xl);

ALTER TABLE cms_good_tyre
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'id товара Шины',
  CHANGE COLUMN brand brand varchar(255) DEFAULT NULL COMMENT 'Бренд Шины',
  CHANGE COLUMN model model varchar(50) DEFAULT NULL COMMENT 'Модель Шины',
  CHANGE COLUMN width width decimal(5, 2) DEFAULT NULL COMMENT 'Ширина Шины',
  CHANGE COLUMN height height decimal(5, 2) DEFAULT NULL COMMENT 'Высота Шины',
  CHANGE COLUMN diameter diameter decimal(5, 2) DEFAULT NULL COMMENT 'Диаметр Шины',
  CHANGE COLUMN index_load index_load varchar(255) DEFAULT NULL COMMENT 'Индекс нагрузки Шины',
  CHANGE COLUMN index_speed index_speed varchar(255) DEFAULT NULL COMMENT 'Индекс скорости Шины',
  CHANGE COLUMN features features varchar(255) DEFAULT NULL COMMENT 'Характеризующие аббревиатуры Шины',
  CHANGE COLUMN axis axis tinyint(4) DEFAULT NULL COMMENT 'Ось Шины',
  CHANGE COLUMN cargo cargo tinyint(1) DEFAULT NULL COMMENT 'Карго Шины',
  CHANGE COLUMN runflat dictionary_tyre_runflat tinyint(4) DEFAULT NULL COMMENT 'Ранфлэтп Шины',
  CHANGE COLUMN camers dictionary_tyre_camers tinyint(4) DEFAULT NULL COMMENT 'Камерность Шины',
  CHANGE COLUMN season dictionary_tyre_season tinyint(4) DEFAULT NULL COMMENT 'Сезон Шины',
  CHANGE COLUMN tyre_type dictionary_tyre_type tinyint(4) DEFAULT NULL COMMENT 'Тип Шины',
  CHANGE COLUMN construction dictionary_tyre_construction tinyint(4) DEFAULT NULL COMMENT 'Кoнструкция Шины',
  CHANGE COLUMN xl dictionary_tyre_xl tinyint(4) DEFAULT NULL COMMENT 'XL Шины';

UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = '', NULL, cgt.camers);
UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = 'TL', 3, cgt.camers);
UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = 'TT', 4, cgt.camers);

UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = '', NULL, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = '-', NULL, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = 'R', 1, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = 'B', 2, cgt.construction);

ALTER TABLE cms_good_tyre_moto
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Мотошины',
  CHANGE COLUMN brand brand varchar(255) DEFAULT NULL COMMENT 'Бренд Мотошины',
  CHANGE COLUMN model model varchar(50) DEFAULT NULL COMMENT 'Модель Мотошины',
  CHANGE COLUMN width width decimal(5, 2) DEFAULT NULL COMMENT 'Ширина Мотошины',
  CHANGE COLUMN height height decimal(5, 2) DEFAULT NULL COMMENT 'Высота Мотошины',
  CHANGE COLUMN diameter diameter decimal(5, 2) DEFAULT NULL COMMENT 'Диаметр Мотошины',
  CHANGE COLUMN index_load index_load varchar(255) DEFAULT NULL COMMENT 'Индекс нагрузки Мотошины',
  CHANGE COLUMN index_speed index_speed varchar(255) DEFAULT NULL COMMENT 'Индекс скорости Мотошины',
  CHANGE COLUMN features features varchar(255) DEFAULT NULL COMMENT 'Характеризующие аббревиатуры Мотошины',
  CHANGE COLUMN construction dictionary_tyremoto_construction tinyint(4) DEFAULT NULL COMMENT 'Кoнструкция Мотошины',
  CHANGE COLUMN camers dictionary_tyremoto_camers tinyint(4) DEFAULT NULL COMMENT 'Камерность Мотошины',
  CHANGE COLUMN axis dictionary_tyremoto_axis tinyint(4) DEFAULT NULL COMMENT 'Ось Мотошины';

RENAME TABLE `cms_good_tyre_moto` TO `cms_good_tyremoto`;

ALTER TABLE cms_good_coil
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Катушка',
  CHANGE COLUMN brand brand int(11) DEFAULT NULL COMMENT 'Бренд Катушка',
  CHANGE COLUMN model model int(11) DEFAULT NULL COMMENT 'Модель Катушка',
  CHANGE COLUMN ratio ratio varchar(255) DEFAULT NULL COMMENT 'Передаточное число Катушка',
  CHANGE COLUMN weight weight decimal(5, 2) DEFAULT 0.00 COMMENT 'Вес Катушка',
  CHANGE COLUMN intensity intensity varchar(255) DEFAULT NULL COMMENT 'Лесоемкость Катушка',
  CHANGE COLUMN spool spool tinyint(1) DEFAULT NULL COMMENT 'Наличие запасной шпули Катушка',
  CHANGE COLUMN bearings bearings varchar(255) DEFAULT NULL COMMENT 'Количество подшипников Катушка',
  CHANGE COLUMN bytraner bytraner tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Байтраннер Катушка',
  CHANGE COLUMN line_counter line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Счетчик лески Катушка',
  CHANGE COLUMN protection_water protection_water tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Защита от соленой воды Катушка',
  CHANGE COLUMN electric_drive electric_drive tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие электропривода Катушка',
  CHANGE COLUMN ratchets ratchets tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие трещетки Катушка',
  CHANGE COLUMN other other varchar(255) DEFAULT NULL COMMENT 'Прочее Катушка',
  CHANGE COLUMN spool_size spool_size varchar(255) DEFAULT NULL COMMENT 'Размер шпули Катушка',
  CHANGE COLUMN friction_load friction_load decimal(5, 2) DEFAULT NULL COMMENT 'Нагрузка на фрикцион Катушка',
  CHANGE COLUMN dictionary_coil_hand dictionary_coil_hand INT(11) DEFAULT NULL COMMENT 'Рука Катушка',
  CHANGE COLUMN dictionary_coil_type dictionary_coil_type INT(11) DEFAULT NULL COMMENT 'Тип Катушка',
  CHANGE COLUMN dictionary_coil_material dictionary_coil_material INT(11) DEFAULT NULL COMMENT 'Материал Катушка';

ALTER TABLE cms_good_rod
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Удилище',
  CHANGE COLUMN brand brand int(11) DEFAULT NULL COMMENT 'Бренд Удилище',
  CHANGE COLUMN model model int(11) DEFAULT NULL COMMENT 'Модель Удилище',
  CHANGE COLUMN dictionary_rod_type dictionary_rod_type int(11) DEFAULT NULL COMMENT 'Тип Удилище',
  CHANGE COLUMN test test varchar(255) DEFAULT NULL COMMENT 'Тест Удилище',
  CHANGE COLUMN length length decimal(6, 2) DEFAULT 0.00 COMMENT 'Длина Удилище',
  CHANGE COLUMN shipping_length shipping_length decimal(6, 2) DEFAULT 0.00 COMMENT 'Транспортировочная длина Удилище',
  CHANGE COLUMN sections_count sections_count varchar(255) DEFAULT NULL COMMENT 'Количество секций Удилище',
  CHANGE COLUMN weight weight decimal(6, 2) DEFAULT 0.00 COMMENT 'Вес Удилище',
  CHANGE COLUMN dictionary_rod_connection_type dictionary_rod_connection_type int(11) DEFAULT NULL COMMENT 'Тип соединения Удилище',
  CHANGE COLUMN rings_count rings_count decimal(6, 2) DEFAULT 0.00 COMMENT 'Kоличество колец Удилище',
  CHANGE COLUMN connector_diameter connector_diameter decimal(6, 2) DEFAULT 0.00 COMMENT 'Диаметр коннектора Удилище',
  CHANGE COLUMN other other varchar(255) DEFAULT NULL COMMENT 'Прочее Удилище',
  CHANGE COLUMN dictionary_rod_story dictionary_rod_story int(11) DEFAULT NULL COMMENT 'Удилища  - Строй Удилище';

ALTER TABLE cms_good_minnow
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Блесна',
  CHANGE COLUMN brand brand int(11) DEFAULT NULL COMMENT 'Бренд Блесна',
  CHANGE COLUMN model model int(11) DEFAULT NULL COMMENT 'Модель Блесна',
  CHANGE COLUMN dictionary_minnow_type dictionary_minnow_type int(11) DEFAULT NULL COMMENT 'Тип Блесна',
  CHANGE COLUMN color color varchar(255) DEFAULT NULL COMMENT 'Цвет Блесна',
  CHANGE COLUMN weight weight decimal(6, 2) DEFAULT 0.00 COMMENT 'Вес Блесна',
  CHANGE COLUMN length length decimal(5, 2) DEFAULT 0.00 COMMENT 'Длина Блесна',
  CHANGE COLUMN amount_package amount_package varchar(255) DEFAULT NULL COMMENT 'В упаковке Блесна',
  CHANGE COLUMN dictionary_minnow_manufacturer dictionary_minnow_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель Блесна',
  CHANGE COLUMN packing_ratio packing_ratio varchar(255) DEFAULT NULL COMMENT 'Кратность упаковки Блесна',
  CHANGE COLUMN other other varchar(255) DEFAULT NULL COMMENT 'Прочее Блесна';

ALTER TABLE cms_good_softbait
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Мягкая приманка',
  CHANGE COLUMN brand brand int(11) DEFAULT NULL COMMENT 'Бренд Мягкая приманка',
  CHANGE COLUMN model model int(11) DEFAULT NULL COMMENT 'Модель Мягкая приманка',
  CHANGE COLUMN dictionary_softbait_type dictionary_softbait_type int(11) DEFAULT NULL COMMENT 'Тип Мягкая приманка',
  CHANGE COLUMN test test varchar(255) DEFAULT NULL COMMENT 'Цвет Мягкая приманка',
  CHANGE COLUMN weight weight decimal(5, 2) DEFAULT 0.00 COMMENT 'Вес Мягкая приманка',
  CHANGE COLUMN length length decimal(5, 2) DEFAULT 0.00 COMMENT 'Длина Мягкая приманка',
  CHANGE COLUMN amount_package amount_package varchar(255) DEFAULT NULL COMMENT 'Количество в упаковке Мягкая приманка',
  CHANGE COLUMN dictionary_softbait_buoyancy dictionary_softbait_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть Мягкая приманка',
  CHANGE COLUMN line_counter line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие погремушки Мягкая приманка',
  CHANGE COLUMN other other varchar(255) DEFAULT NULL COMMENT 'Прочее Мягкая приманка';

ALTER TABLE cms_good_wobbler
  CHANGE COLUMN good_id good_id int(11) NOT NULL COMMENT 'Id товара Воблер',
  CHANGE COLUMN brand brand int(11) DEFAULT NULL COMMENT 'Бренд Воблер',
  CHANGE COLUMN model model int(11) DEFAULT NULL COMMENT 'Модель Воблер',
  CHANGE COLUMN dictionary_wobbler_type dictionary_wobbler_type int(11) DEFAULT NULL COMMENT 'Тип Воблер',
  CHANGE COLUMN test test varchar(255) DEFAULT NULL COMMENT 'Цвет Воблер',
  CHANGE COLUMN weight weight decimal(5, 2) DEFAULT 0.00 COMMENT 'Вес Воблер',
  CHANGE COLUMN recess_from  recess_from decimal(5, 2) DEFAULT NULL COMMENT 'Заглубление от Воблер  Воблер',
  CHANGE COLUMN dictionary_wobbler_manufacturer dictionary_wobbler_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель Воблер',
  CHANGE COLUMN dictionary_wobbler_buoyancy dictionary_wobbler_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть Воблер',
  CHANGE COLUMN hooks hooks varchar(255) DEFAULT NULL COMMENT 'Крючки Воблер',
  CHANGE COLUMN other other varchar(255) DEFAULT NULL COMMENT 'Прочее Воблер',
  CHANGE COLUMN length length decimal(5, 2) DEFAULT NULL COMMENT 'Длина Воблер',
  CHANGE COLUMN recess_to recess_to decimal(5, 2) DEFAULT NULL COMMENT 'Заглубление до Воблер';

CREATE TABLE cms_dictionary_tyre_type (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Шина - тип'
  ROW_FORMAT = DYNAMIC;

INSERT INTO cms_dictionary_tyre_type (id, code, name, parent_id) VALUES
  (1, '1', 'Авто', NULL);
INSERT INTO cms_dictionary_tyre_type (id, code, name, parent_id) VALUES
  (2, '2', 'Мото', NULL);

CREATE TABLE cms_dictionary_tyremoto_construction (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Конструкция Мотошин'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE cms_dictionary_tyremoto_camers (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Камерность Мотошин'
  ROW_FORMAT = DYNAMIC;

UPDATE cms_dictionary_good_type SET code = 'tyremoto' WHERE id = 3;

RENAME TABLE `cms_dictionary_xl` TO `cms_dictionary_tyre_xl`;
RENAME TABLE `cms_dictionary_construction` TO `cms_dictionary_tyre_construction`;
RENAME TABLE `cms_dictionary_runflat` TO `cms_dictionary_tyre_runflat`;
RENAME TABLE `cms_dictionary_camers` TO `cms_dictionary_tyre_camers`;

RENAME TABLE `cms_dictionary_tyre_moto_brand` TO `cms_dictionary_tyremoto_brand`;
RENAME TABLE `cms_dictionary_tyre_moto_model` TO `cms_dictionary_tyremoto_model`;
RENAME TABLE `cms_dictionary_tyre_axis_moto` TO `cms_dictionary_tyremoto_axis`;

RENAME TABLE `cms_dictionary_type_minnow` TO `cms_dictionary_minnow_type`;
RENAME TABLE `cms_dictionary_manufacturer_minnow` TO `cms_dictionary_minnow_manufacturer`;

RENAME TABLE `cms_dictionary_type_rod` TO `cms_dictionary_rod_type`;
RENAME TABLE `cms_dictionary_connection_type_rod` TO `cms_dictionary_rod_connection_type`;

RENAME TABLE `cms_dictionary_type_softbait` TO `cms_dictionary_softbait_type`;

RENAME TABLE `cms_dictionary_type_wobbler` TO `cms_dictionary_wobbler_type`;
RENAME TABLE `cms_dictionary_manufacturer_wobbler` TO `cms_dictionary_wobbler_manufacturer`;
RENAME TABLE `cms_dictionary_buoyancy_wobbler` TO `cms_dictionary_wobbler_buoyancy`;

## disk 1
UPDATE cms_good_type_structure SET good_type_id = 1, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"27%","margin":"5 0 0 0","labelWidth":100},{"xtype":"textfield","fieldLabel":"количество отверстий","name":"hole_num","width":"24%","labelWidth":100},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"23%","labelWidth":70,"margin":"5 0 0 0"},{"xtype":"textfield","fieldLabel":"диаметр отверстий","name":"diameter_hole","width":"25%","labelWidth":80}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"вылет","name":"overhand","width":"26%"},{"xtype":"textfieldnumber","fieldLabel":"Ø ступичного отверстия","name":"diameter_nave","width":"40%","labelWidth":170},{"xtype":"textfield","fieldLabel":"цвет","name":"color","width":"33%","labelWidth":40}]},{"xtype":"dictionary","fieldLabel":"Тип диска","name":"dictionary_disk_type","dictionaryName":"DiskType","valueField":"id"}]' WHERE id = 1;
## tyer 2
UPDATE cms_good_type_structure SET good_type_id = 2, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"TyreConstruction","fieldLabel":"Конструкция","valueField":"name","name":"dictionary_tyre_construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreRunflat","fieldLabel":"Ранфлэт","valueField":"name","displayField":"name","name":"dictionary_tyre_runflat","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreCamers","fieldLabel":"Камерность","valueField":"name","name":"dictionary_tyre_camers","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","fieldLabel":"Сезон","name":"dictionary_tyre_season","dictionaryName":"TyreSeason","valueField":"id","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreXl","fieldLabel":"XL","valueField":"name","name":"dictionary_tyre_xl","width":"30%"},{"xtype":"checkbox","fieldLabel":"Карго","name":"cargo","width":"30%"}]}]' WHERE id = 2;
## tyermoto 3
UPDATE cms_good_type_structure SET good_type_id = 3, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"TyremotoConstruction","fieldLabel":"Конструкция","valueField":"id","name":"dictionary_tyremoto_construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyremotoCamers","fieldLabel":"Камерность","valueField":"id","name":"dictionary_tyremoto_camers","width":"26%"},{"xtype":"dictionary","fieldLabel":"Ось","name":"dictionary_tyremoto_axis","dictionaryName":"TyremotoAxis","reference":"axis","valueField":"id","width":"34%"}]}]' WHERE id = 3;
## coil 6
UPDATE cms_good_type_structure SET good_type_id = 6, structure = '[ { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Тип", "name": "dictionary_coil_type", "xtype": "dictionary", "dictionaryName": "CoilType" }, {  "fieldLabel": "Передаточное число", "name": "ratio" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "xtype": "textfieldnumber", "fieldLabel": "Вес", "name": "weight" }, { "fieldLabel": "Лесоемкость", "name": "intensity" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "xtype": "textfieldnumber", "fieldLabel": "Нагрузка на фрикцион", "name": "friction_load" }, {"fieldLabel": "Количество подшипников", "name": "bearings" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Наличие запасной шпули", "name": "spool", "checked": false, "xtype": "checkboxfield" }, { "fieldLabel": "Размер шпули", "name": "spool_size" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Байтраннер", "name": "bytraner", "checked": false, "xtype": "checkboxfield" }, { "boxLabel": "Счетчик лески", "name": "line_counter", "checked": false, "xtype": "checkboxfield" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Защита от соленой воды", "name": "protection_water", "checked": false, "xtype": "checkboxfield" }, { "fieldLabel": "Рука", "name": "dictionary_coil_hand", "xtype": "dictionary", "dictionaryName": "CoilHand" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Наличие электропривода", "name": "electric_drive", "checked": false, "xtype": "checkboxfield" }, { "boxLabel": "Наличие трещетки", "name": "ratchets", "checked": false, "xtype": "checkboxfield" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Материал", "name": "dictionary_coil_material", "xtype": "dictionary", "dictionaryName": "CoilMaterial" }, { "fieldLabel": "Прочее", "name": "other" } ] } ]' WHERE id = 6;
## rod 7
UPDATE cms_good_type_structure SET good_type_id = 7, structure = '[ { "xtype":"fieldset", "items":[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_rod_type", "xtype":"dictionary", "dictionaryName":"TypeRod" }, { "fieldLabel":"Тест", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Длина", "name":"length" }, { "fieldLabel":"Транспортировочная длина", "name":"shipping_length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество секций", "name":"sections_count" }, { "fieldLabel":"Вес", "name":"weight" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип соединения", "name":"dictionary_rod_connection_type", "xtype":"dictionary", "dictionaryName":"ConnectionTypeRod" }, { "fieldLabel":"Kоличество колец", "name":"rings_count" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Диаметр коннектора", "name":"connector_diameter" }, { "fieldLabel":"Прочее", "name":"other" } ] },{ "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Удилище - строй", "name":"dictionary_rod_story", "xtype":"dictionary", "dictionaryName":"RodStory" } ] } ] } ]' WHERE id = 7;
## minnow 8
UPDATE cms_good_type_structure SET good_type_id = 8, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_minnow_type", "xtype":"dictionary", "dictionaryName":"TypeMinnow" }, { "fieldLabel":"Цвет", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"В упаковке", "name":"amount_package" }, { "fieldLabel":"Производитель", "name":"dictionary_minnow_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerMinnow" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Кратность упаковки", "name":"packing_ratio" }, { "fieldLabel":"Прочее", "name":"other" } ] } ]' WHERE id = 8;
## wobbler 9
UPDATE cms_good_type_structure SET good_type_id = 9, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_wobbler_type", "xtype":"dictionary", "dictionaryName":"TypeWobbler" }, { "fieldLabel":"Цвет", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Крючки", "name":"hooks" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Производитель", "name":"dictionary_wobbler_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerWobbler" }, { "fieldLabel":"Плавучесть", "name":"dictionary_wobbler_buoyancy", "xtype":"dictionary", "dictionaryName":"BuoyancyWobbler" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Заглубление от", "name":"recess_from" }, { "fieldLabel":"Заглубление до", "name":"recess_to" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Прочее", "name":"other" },{ "fieldLabel":"Длина", "name":"length" } ] } ]' WHERE id = 9;
## softbait 10
UPDATE cms_good_type_structure SET good_type_id = 10, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_softbait_type", "xtype":"dictionary", "dictionaryName":"TypeSoftbait" }, { "fieldLabel":"Цвет", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество в упаковке", "name":"amount_package" }, { "boxLabel":"Плавучесть", "name":"dictionary_softbait_buoyancy", "checked":false, "xtype":"checkboxfield" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Наличие погремушки", "name":"line_counter", "checked":false, "xtype":"checkboxfield" }, { "fieldLabel":"Прочее", "name":"other" } ] } ]' WHERE id = 10;
