SET NAMES 'utf8';
ALTER DATABASE shipryb_db CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE cms_good
  CHANGE COLUMN id id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Товар Id',
  CHANGE COLUMN dt_insert dt_insert DATETIME NOT NULL COMMENT 'Товар Дата создания',
  CHANGE COLUMN dt_update dt_update DATETIME NOT NULL COMMENT 'Товар Дата обновления',
  CHANGE COLUMN dt_update_photo dt_update_photo DATETIME DEFAULT '1970-01-01 00:00:00' COMMENT 'Товар Дата обновления фото',
  CHANGE COLUMN rprice rprice DECIMAL(9, 2) DEFAULT NULL COMMENT 'Товар МРЦ',
  CHANGE COLUMN adm_id adm_id TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Adm id',
  CHANGE COLUMN good good VARCHAR(255) NOT NULL COMMENT 'Товар Название',
  CHANGE COLUMN not_load not_load INT(11) DEFAULT NULL COMMENT 'Товар Не загружать',
  CHANGE COLUMN zarticle zarticle VARCHAR(255) DEFAULT NULL COMMENT 'Товар Артикул',
  CHANGE COLUMN type type INT(11) DEFAULT NULL COMMENT 'Товар Тип',
  CHANGE COLUMN photo photo VARCHAR(255) DEFAULT NULL COMMENT 'Товар Фотография';

ALTER TABLE cms_price
  CHANGE COLUMN id id MEDIUMINT(9) NOT NULL AUTO_INCREMENT COMMENT 'Предложение Id ',
  CHANGE COLUMN good_id good_id MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT 'Предложение Товар Id',
  CHANGE COLUMN dt_insert dt_insert DATETIME NOT NULL COMMENT 'Предложение Дата создания',
  CHANGE COLUMN dt_update dt_update DATETIME NOT NULL COMMENT 'Предложение Дата обновления',
  CHANGE COLUMN dt_price dt_price DATETIME NOT NULL COMMENT 'Предложение Дата обновления цены',
  CHANGE COLUMN provider_id provider_id SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Предложение Поставщик',
  CHANGE COLUMN good good VARCHAR(255) NOT NULL COMMENT 'Предложение Названия',
  CHANGE COLUMN photo photo VARCHAR(255) DEFAULT NULL COMMENT 'Предложение Фото',
  CHANGE COLUMN price price MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'Предложение Цена',
  CHANGE COLUMN rprice rprice MEDIUMINT(9) DEFAULT NULL COMMENT 'Предложение МРЦ товара',
  CHANGE COLUMN zarticle zarticle VARCHAR(255) DEFAULT NULL COMMENT 'Предложение Артикул',
  CHANGE COLUMN sarticle sarticle VARCHAR(255) DEFAULT NULL COMMENT 'Предложение Артикул поставщика',
  CHANGE COLUMN sklad sklad SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Предложение Остаток',
  CHANGE COLUMN pass pass TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Предложение Пропуск',
  CHANGE COLUMN status status TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Предложение Статус';

UPDATE cms_good_disk cgd set cgd.diameter_hole = NULL WHERE cgd.diameter_hole = '0.00' OR cgd.diameter_hole = '';
ALTER TABLE cms_good_disk
  CHANGE COLUMN good_id good_id INT(11) NOT NULL  COMMENT 'Товар Id',
  CHANGE COLUMN brand brand VARCHAR(255) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model VARCHAR(50) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN width width DECIMAL(8, 2) DEFAULT NULL COMMENT 'Ширина',
  CHANGE COLUMN diameter diameter DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр',
  CHANGE COLUMN hole_num hole_num INT(11) DEFAULT NULL COMMENT 'Количество отверстий',
  CHANGE COLUMN diameter_hole diameter_hole DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр отверстий',
  CHANGE COLUMN overhand overhand DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вылет',
  CHANGE COLUMN diameter_nave diameter_nave DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр ступичного отверстия',
  CHANGE COLUMN color color VARCHAR(255) DEFAULT NULL COMMENT 'Цвет',
  CHANGE COLUMN disk_type dictionary_disk_type INT(11) DEFAULT NULL COMMENT 'Тип';

UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = '', NULL, cgt.camers);
UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = 'TL', 3, cgt.camers);
UPDATE cms_good_tyre cgt set cgt.camers = IF(cgt.camers = 'TT', 4, cgt.camers);

UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = '', NULL, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = '-', NULL, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = 'R', 1, cgt.construction);
UPDATE cms_good_tyre cgt set cgt.construction = IF(cgt.construction = 'B', 2, cgt.construction);

UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = '', NULL, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'RunFlat', 1, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ZP', 2, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ZPS', 3, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'SSR', 4, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'ROF', 5, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'RFT', 6, cgt.runflat);
UPDATE cms_good_tyre cgt set cgt.runflat = IF(cgt.runflat = 'EMT', 7, cgt.runflat);

UPDATE cms_good_tyre cgt set cgt.xl = IF(cgt.xl = '', NULL, cgt.xl);
UPDATE cms_good_tyre cgt set cgt.xl = IF(cgt.xl = 'XL', 1, cgt.xl);

UPDATE cms_good_tyre cgt set cgt.cargo = 0 WHERE cgt.cargo IS NULL;

ALTER TABLE cms_good_tyre
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand VARCHAR(255) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model VARCHAR(50) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN width width DECIMAL(8, 2) DEFAULT NULL COMMENT 'Ширина',
  CHANGE COLUMN height height DECIMAL(8, 2) DEFAULT NULL COMMENT 'Высота',
  CHANGE COLUMN diameter diameter DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр',
  CHANGE COLUMN index_load index_load VARCHAR(255) DEFAULT NULL COMMENT 'Индекс нагрузки',
  CHANGE COLUMN index_speed index_speed VARCHAR(255) DEFAULT NULL COMMENT 'Индекс скорости',
  CHANGE COLUMN features features VARCHAR(255) DEFAULT NULL COMMENT 'Характеризующие аббревиатуры',
  DROP COLUMN axis,
  CHANGE COLUMN cargo cargo TINYINT(1) DEFAULT NULL COMMENT 'Карго',
  CHANGE COLUMN runflat dictionary_tyre_runflat INT(11) DEFAULT NULL COMMENT 'Ранфлэтп',
  CHANGE COLUMN camers dictionary_tyre_camers INT(11) DEFAULT NULL COMMENT 'Камерность',
  CHANGE COLUMN season dictionary_tyre_season INT(11) DEFAULT NULL COMMENT 'Сезон',
  DROP COLUMN tyre_type,
  CHANGE COLUMN construction dictionary_tyre_construction INT(11) DEFAULT NULL COMMENT 'Кoнструкция',
  CHANGE COLUMN xl dictionary_tyre_xl INT(11) DEFAULT NULL COMMENT 'XL';

UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = '', NULL, cgt.camers);
UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = 'TL', 3, cgt.camers);
UPDATE cms_good_tyre_moto cgt set cgt.camers = IF(cgt.camers = 'TT', 4, cgt.camers);

UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = '', NULL, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = '-', NULL, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = 'R', 1, cgt.construction);
UPDATE cms_good_tyre_moto cgt set cgt.construction = IF(cgt.construction = 'B', 2, cgt.construction);

ALTER TABLE cms_good_tyre_moto
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand VARCHAR(255) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model VARCHAR(50) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN width width DECIMAL(8, 2) DEFAULT NULL COMMENT 'Ширина',
  CHANGE COLUMN height height DECIMAL(8, 2) DEFAULT NULL COMMENT 'Высота',
  CHANGE COLUMN diameter diameter DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр',
  CHANGE COLUMN index_load index_load VARCHAR(255) DEFAULT NULL COMMENT 'Индекс нагрузки',
  CHANGE COLUMN index_speed index_speed VARCHAR(255) DEFAULT NULL COMMENT 'Индекс скорости',
  CHANGE COLUMN features features VARCHAR(255) DEFAULT NULL COMMENT 'Характеризующие аббревиатуры',
  CHANGE COLUMN construction dictionary_tyremoto_construction INT(11) DEFAULT NULL COMMENT 'Кoнструкция',
  CHANGE COLUMN camers dictionary_tyremoto_camers INT(11) DEFAULT NULL COMMENT 'Камерность',
  CHANGE COLUMN axis dictionary_tyremoto_axis INT(11) DEFAULT NULL COMMENT 'Ось';

RENAME TABLE `cms_good_tyre_moto` TO `cms_good_tyremoto`;

ALTER TABLE cms_good_coil
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand INT(11) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model INT(11) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN ratio ratio VARCHAR(255) DEFAULT NULL COMMENT 'Передаточное число',
  CHANGE COLUMN weight weight DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вес',
  CHANGE COLUMN intensity intensity VARCHAR(255) DEFAULT NULL COMMENT 'Лесоемкость',
  CHANGE COLUMN spool spool TINYINT(1) DEFAULT NULL COMMENT 'Наличие запасной шпули',
  CHANGE COLUMN bearings bearings VARCHAR(255) DEFAULT NULL COMMENT 'Количество подшипников',
  CHANGE COLUMN bytraner bytraner TINYINT(1) DEFAULT NULL COMMENT 'Байтраннер',
  CHANGE COLUMN line_counter line_counter TINYINT(1) DEFAULT NULL COMMENT 'Счетчик лески',
  CHANGE COLUMN protection_water protection_water TINYINT(1) DEFAULT NULL COMMENT 'Защита от соленой воды',
  CHANGE COLUMN electric_drive electric_drive TINYINT(1) DEFAULT NULL COMMENT 'Наличие электропривода',
  CHANGE COLUMN ratchets ratchets TINYINT(1) DEFAULT NULL COMMENT 'Наличие трещетки',
  CHANGE COLUMN other other VARCHAR(255) DEFAULT NULL COMMENT 'Прочее',
  CHANGE COLUMN spool_size spool_size VARCHAR(255) DEFAULT NULL COMMENT 'Размер шпули',
  CHANGE COLUMN friction_load friction_load DECIMAL(8, 2) DEFAULT NULL COMMENT 'Нагрузка на фрикцион',
  CHANGE COLUMN dictionary_coil_hand dictionary_coil_hand INT(11) DEFAULT NULL COMMENT 'Рука',
  CHANGE COLUMN dictionary_coil_type dictionary_coil_type INT(11) DEFAULT NULL COMMENT 'Тип',
  CHANGE COLUMN dictionary_coil_material dictionary_coil_material INT(11) DEFAULT NULL COMMENT 'Материал';

ALTER TABLE cms_good_rod
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand INT(11) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model INT(11) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN dictionary_rod_type dictionary_rod_type INT(11) DEFAULT NULL COMMENT 'Тип',
  CHANGE COLUMN test test VARCHAR(255) DEFAULT NULL COMMENT 'Тест',
  CHANGE COLUMN length length DECIMAL(8, 2) DEFAULT NULL COMMENT 'Длина',
  CHANGE COLUMN shipping_length shipping_length DECIMAL(8, 2) DEFAULT NULL COMMENT 'Транспортировочная длина',
  CHANGE COLUMN sections_count sections_count VARCHAR(255) DEFAULT NULL COMMENT 'Количество секций',
  CHANGE COLUMN weight weight DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вес',
  CHANGE COLUMN dictionary_rod_connection_type dictionary_rod_connection_type INT(11) DEFAULT NULL COMMENT 'Тип соединения',
  CHANGE COLUMN rings_count rings_count DECIMAL(8, 2) DEFAULT NULL COMMENT 'Kоличество колец',
  CHANGE COLUMN connector_diameter connector_diameter DECIMAL(8, 2) DEFAULT NULL COMMENT 'Диаметр коннектора',
  CHANGE COLUMN other other VARCHAR(255) DEFAULT NULL COMMENT 'Прочее',
  CHANGE COLUMN dictionary_rod_story dictionary_rod_story INT(11) DEFAULT NULL COMMENT 'Удилища  - Строй';

ALTER TABLE cms_good_minnow
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand INT(11) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model INT(11) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN dictionary_minnow_type dictionary_minnow_type INT(11) DEFAULT NULL COMMENT 'Тип',
  CHANGE COLUMN color color VARCHAR(255) DEFAULT NULL COMMENT 'Цвет',
  CHANGE COLUMN weight weight DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вес',
  CHANGE COLUMN length length DECIMAL(8, 2) DEFAULT NULL COMMENT 'Длина',
  CHANGE COLUMN amount_package amount_package VARCHAR(255) DEFAULT NULL COMMENT 'В упаковке',
  CHANGE COLUMN dictionary_minnow_manufacturer dictionary_minnow_manufacturer INT(11) DEFAULT NULL COMMENT 'Производитель',
  CHANGE COLUMN packing_ratio packing_ratio VARCHAR(255) DEFAULT NULL COMMENT 'Кратность упаковки',
  CHANGE COLUMN other other VARCHAR(255) DEFAULT NULL COMMENT 'Прочее';

ALTER TABLE cms_good_softbait
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand INT(11) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model INT(11) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN dictionary_softbait_type dictionary_softbait_type INT(11) DEFAULT NULL COMMENT 'Тип',
  CHANGE COLUMN test test VARCHAR(255) DEFAULT NULL COMMENT 'Цвет',
  CHANGE COLUMN weight weight DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вес',
  CHANGE COLUMN length length DECIMAL(8, 2) DEFAULT NULL COMMENT 'Длина',
  CHANGE COLUMN amount_package amount_package VARCHAR(255) DEFAULT NULL COMMENT 'Количество в упаковке',
  CHANGE COLUMN dictionary_softbait_buoyancy dictionary_softbait_buoyancy INT(11) DEFAULT NULL COMMENT 'Плавучесть',
  CHANGE COLUMN line_counter line_counter TINYINT(1) DEFAULT NULL COMMENT 'Наличие погремушки',
  CHANGE COLUMN other other VARCHAR(255) DEFAULT NULL COMMENT 'Прочее';

ALTER TABLE cms_good_wobbler
  CHANGE COLUMN good_id good_id INT(11) NOT NULL COMMENT 'Товар Id',
  CHANGE COLUMN brand brand INT(11) DEFAULT NULL COMMENT 'Бренд',
  CHANGE COLUMN model model INT(11) DEFAULT NULL COMMENT 'Модель',
  CHANGE COLUMN dictionary_wobbler_type dictionary_wobbler_type INT(11) DEFAULT NULL COMMENT 'Тип',
  CHANGE COLUMN test test VARCHAR(255) DEFAULT NULL COMMENT 'Цвет',
  CHANGE COLUMN weight weight DECIMAL(8, 2) DEFAULT NULL COMMENT 'Вес',
  CHANGE COLUMN recess_from  recess_from DECIMAL(8, 2) DEFAULT NULL COMMENT 'Заглубление' ,
  CHANGE COLUMN dictionary_wobbler_manufacturer dictionary_wobbler_manufacturer INT(11) DEFAULT NULL COMMENT 'Производитель',
  CHANGE COLUMN dictionary_wobbler_buoyancy dictionary_wobbler_buoyancy INT(11) DEFAULT NULL COMMENT 'Плавучесть',
  CHANGE COLUMN hooks hooks VARCHAR(255) DEFAULT NULL COMMENT 'Крючки',
  CHANGE COLUMN other other VARCHAR(255) DEFAULT NULL COMMENT 'Прочее',
  CHANGE COLUMN length length DECIMAL(8, 2) DEFAULT NULL COMMENT 'Длина',
  CHANGE COLUMN recess_to recess_to DECIMAL(8, 2) DEFAULT NULL COMMENT 'Заглубление до';

CREATE TABLE cms_dictionary_tyremoto_construction (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Конструкция Мотошин'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE cms_dictionary_tyremoto_camers (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Камерность Мотошин'
  ROW_FORMAT = DYNAMIC;

UPDATE cms_dictionary_good_type SET code = 'tyremoto' WHERE id = 3;

CREATE TABLE cms_dictionary_softbait_buoyancy (
  id        INT(11) NOT NULL AUTO_INCREMENT,
  code      VARCHAR(255)     DEFAULT NULL,
  name      VARCHAR(55)      DEFAULT NULL,
  parent_id INT(11)          DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci
  COMMENT = 'Мягкая приманка - Плавучесть'
  ROW_FORMAT = DYNAMIC;

RENAME TABLE `cms_dictionary_xl` TO `cms_dictionary_tyre_xl`;
RENAME TABLE `cms_dictionary_construction` TO `cms_dictionary_tyre_construction`;
RENAME TABLE `cms_dictionary_runflat` TO `cms_dictionary_tyre_runflat`;
RENAME TABLE `cms_dictionary_camers` TO `cms_dictionary_tyre_camers`;

RENAME TABLE `cms_dictionary_tyre_moto_brand` TO `cms_dictionary_tyremoto_brand`;
RENAME TABLE `cms_dictionary_tyre_moto_model` TO `cms_dictionary_tyremoto_model`;
RENAME TABLE `cms_dictionary_tyre_axis_moto` TO `cms_dictionary_tyremoto_axis`;

RENAME TABLE `cms_dictionary_type_minnow` TO `cms_dictionary_minnow_type`;
RENAME TABLE `cms_dictionary_manufacturer_minnow` TO `cms_dictionary_minnow_manufacturer`;

RENAME TABLE `cms_dictionary_type_rod` TO `cms_dictionary_rod_type`;
RENAME TABLE `cms_dictionary_connection_type_rod` TO `cms_dictionary_rod_connection_type`;

RENAME TABLE `cms_dictionary_type_softbait` TO `cms_dictionary_softbait_type`;

RENAME TABLE `cms_dictionary_type_wobbler` TO `cms_dictionary_wobbler_type`;
RENAME TABLE `cms_dictionary_manufacturer_wobbler` TO `cms_dictionary_wobbler_manufacturer`;
RENAME TABLE `cms_dictionary_buoyancy_wobbler` TO `cms_dictionary_wobbler_buoyancy`;

## disk 1
UPDATE cms_good_type_structure SET good_type_id = 1, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"27%","margin":"5 0 0 0","labelWidth":100},{"xtype":"textfield","fieldLabel":"количество отверстий","name":"hole_num","width":"24%","labelWidth":100},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"23%","labelWidth":70,"margin":"5 0 0 0"},{"xtype":"textfield","fieldLabel":"диаметр отверстий","name":"diameter_hole","width":"25%","labelWidth":80}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"вылет","name":"overhand","width":"26%"},{"xtype":"textfieldnumber","fieldLabel":"Ø ступичного отверстия","name":"diameter_nave","width":"40%","labelWidth":170},{"xtype":"textfield","fieldLabel":"цвет","name":"color","width":"33%","labelWidth":40}]},{"xtype":"dictionary","fieldLabel":"Тип диска","name":"dictionary_disk_type","dictionaryName":"DiskType","valueField":"id"}]' WHERE id = 1;
## tyer 2
UPDATE cms_good_type_structure SET good_type_id = 2, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"TyreConstruction","fieldLabel":"Конструкция","valueField":"id","name":"dictionary_tyre_construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreRunflat","fieldLabel":"Ранфлэт","valueField":"id","displayField":"name","name":"dictionary_tyre_runflat","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreCamers","fieldLabel":"Камерность","valueField":"id","name":"dictionary_tyre_camers","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","fieldLabel":"Сезон","name":"dictionary_tyre_season","dictionaryName":"TyreSeason","valueField":"id","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyreXl","fieldLabel":"XL","valueField":"id","name":"dictionary_tyre_xl","width":"30%"},{"xtype":"checkbox","fieldLabel":"Карго","name":"cargo","width":"30%"}]}]' WHERE id = 2;
## tyermoto 3
UPDATE cms_good_type_structure SET good_type_id = 3, structure = '[{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfieldnumber","fieldLabel":"Ширина","name":"width","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Высота","name":"height","width":"30%"},{"xtype":"textfieldnumber","fieldLabel":"Диаметр","name":"diameter","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"dictionary","dictionaryName":"TyremotoConstruction","fieldLabel":"Конструкция","valueField":"id","name":"dictionary_tyremoto_construction","width":"30%"},{"xtype":"textfield","fieldLabel":"Нагрузка","name":"index_load","width":"30%"},{"xtype":"textfield","fieldLabel":"Скорость","name":"index_speed","width":"30%"}]},{"xtype":"container","layout":"hbox","defaultType":"textfield","margin":"0 0 5 0","items":[{"xtype":"textfield","fieldLabel":"Хар. Аббр.","name":"features","width":"30%"},{"xtype":"dictionary","dictionaryName":"TyremotoCamers","fieldLabel":"Камерность","valueField":"id","name":"dictionary_tyremoto_camers","width":"26%"},{"xtype":"dictionary","fieldLabel":"Ось","name":"dictionary_tyremoto_axis","dictionaryName":"TyremotoAxis","reference":"axis","valueField":"id","width":"34%"}]}]' WHERE id = 3;
## coil 6
UPDATE cms_good_type_structure SET good_type_id = 6, structure = '[ { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Тип", "name": "dictionary_coil_type", "xtype": "dictionary", "dictionaryName": "CoilType" }, {  "fieldLabel": "Передаточное число", "name": "ratio" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "xtype": "textfieldnumber", "fieldLabel": "Вес", "name": "weight" }, { "fieldLabel": "Лесоемкость", "name": "intensity" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "xtype": "textfieldnumber", "fieldLabel": "Нагрузка на фрикцион", "name": "friction_load" }, {"fieldLabel": "Количество подшипников", "name": "bearings" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Наличие запасной шпули", "name": "spool", "checked": false, "xtype": "checkboxfield" }, { "fieldLabel": "Размер шпули", "name": "spool_size" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Байтраннер", "name": "bytraner", "checked": false, "xtype": "checkboxfield" }, { "boxLabel": "Счетчик лески", "name": "line_counter", "checked": false, "xtype": "checkboxfield" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Защита от соленой воды", "name": "protection_water", "checked": false, "xtype": "checkboxfield" }, { "fieldLabel": "Рука", "name": "dictionary_coil_hand", "xtype": "dictionary", "dictionaryName": "CoilHand" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "boxLabel": "Наличие электропривода", "name": "electric_drive", "checked": false, "xtype": "checkboxfield" }, { "boxLabel": "Наличие трещетки", "name": "ratchets", "checked": false, "xtype": "checkboxfield" } ] }, { "xtype": "container", "layout": "hbox", "defaultType": "textfield", "margin": "0 0 5 0", "items": [ { "fieldLabel": "Материал", "name": "dictionary_coil_material", "xtype": "dictionary", "dictionaryName": "CoilMaterial" }, { "fieldLabel": "Прочее", "name": "other" } ] } ]' WHERE id = 6;
## rod 7
UPDATE cms_good_type_structure SET good_type_id = 7, structure = '[ { "xtype":"fieldset", "items":[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_rod_type", "xtype":"dictionary", "dictionaryName":"TypeRod" }, { "fieldLabel":"Тест", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Длина", "name":"length" }, { "fieldLabel":"Транспортировочная длина", "name":"shipping_length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество секций", "name":"sections_count" }, { "fieldLabel":"Вес", "name":"weight" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип соединения", "name":"dictionary_rod_connection_type", "xtype":"dictionary", "dictionaryName":"ConnectionTypeRod" }, { "fieldLabel":"Kоличество колец", "name":"rings_count" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Диаметр коннектора", "name":"connector_diameter" }, { "fieldLabel":"Прочее", "name":"other" } ] },{ "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Удилище - строй", "name":"dictionary_rod_story", "xtype":"dictionary", "dictionaryName":"RodStory" } ] } ] } ]' WHERE id = 7;
## minnow 8
UPDATE cms_good_type_structure SET good_type_id = 8, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_minnow_type", "xtype":"dictionary", "dictionaryName":"TypeMinnow" }, { "fieldLabel":"Цвет", "name":"color" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"В упаковке", "name":"amount_package" }, { "fieldLabel":"Производитель", "name":"dictionary_minnow_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerMinnow" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Кратность упаковки", "name":"packing_ratio" }, { "fieldLabel":"Прочее", "name":"other" } ] } ]' WHERE id = 8;
## wobbler 9
UPDATE cms_good_type_structure SET good_type_id = 9, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_wobbler_type", "xtype":"dictionary", "dictionaryName":"TypeWobbler" }, { "fieldLabel":"Цвет", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Крючки", "name":"hooks" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Производитель", "name":"dictionary_wobbler_manufacturer", "xtype":"dictionary", "dictionaryName":"ManufacturerWobbler" }, { "fieldLabel":"Плавучесть", "name":"dictionary_wobbler_buoyancy", "xtype":"dictionary", "dictionaryName":"BuoyancyWobbler" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Заглубление от", "name":"recess_from" }, { "fieldLabel":"Заглубление до", "name":"recess_to" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Прочее", "name":"other" },{ "fieldLabel":"Длина", "name":"length" } ] } ]' WHERE id = 9;
## softbait 10
UPDATE cms_good_type_structure SET good_type_id = 10, structure = '[ { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Тип", "name":"dictionary_softbait_type", "xtype":"dictionary", "dictionaryName":"TypeSoftbait" }, { "fieldLabel":"Цвет", "name":"test" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Вес", "name":"weight" }, { "fieldLabel":"Длина", "name":"length" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "fieldLabel":"Количество в упаковке", "name":"amount_package" }, { "boxLabel":"Плавучесть", "name":"dictionary_softbait_buoyancy", "checked":false, "xtype":"checkboxfield" } ] }, { "xtype":"container", "layout":"hbox", "defaultType":"textfield", "margin":"0 0 5 0", "items":[ { "boxLabel":"Наличие погремушки", "name":"line_counter", "checked":false, "xtype":"checkboxfield" }, { "fieldLabel":"Прочее", "name":"other" } ] } ]' WHERE id = 10;

UPDATE cms_price cp SET cp.rprice = NULL WHERE cp.rprice = 0;
UPDATE cms_good cg SET cg.rprice = NULL WHERE cg.rprice = 0;
UPDATE cms_good cg SET cg.photo = NULL WHERE cg.photo LIKE 'C:%';

UPDATE cms_good_disk cgd set cgd.width = NULL WHERE cgd.width = 0;
UPDATE cms_good_disk cgd set cgd.diameter = NULL WHERE cgd.diameter = 0;
UPDATE cms_good_disk cgd set cgd.hole_num = NULL WHERE cgd.hole_num = 0;
UPDATE cms_good_disk cgd set cgd.diameter_hole = NULL WHERE cgd.diameter_hole = 0;
UPDATE cms_good_disk cgd set cgd.overhand = NULL WHERE cgd.overhand = 0;
UPDATE cms_good_disk cgd set cgd.diameter_nave = NULL WHERE cgd.diameter_nave = 0;
UPDATE cms_good_disk cgd set cgd.dictionary_disk_type = NULL WHERE cgd.dictionary_disk_type = 0;

UPDATE cms_good_coil cgd set cgd.dictionary_coil_type = NULL WHERE cgd.dictionary_coil_type = 0;
UPDATE cms_good_coil cgd set cgd.weight = NULL WHERE cgd.weight = 0;
UPDATE cms_good_coil cgd set cgd.spool = 0 WHERE cgd.spool = NULL;
UPDATE cms_good_coil cgd set cgd.bytraner = 0 WHERE cgd.bytraner = NULL;
UPDATE cms_good_coil cgd set cgd.line_counter = 0 WHERE cgd.line_counter = NULL;
UPDATE cms_good_coil cgd set cgd.protection_water = 0 WHERE cgd.protection_water = NULL;
UPDATE cms_good_coil cgd set cgd.dictionary_coil_hand = NULL WHERE cgd.dictionary_coil_hand = 0;
UPDATE cms_good_coil cgd set cgd.electric_drive = 0 WHERE cgd.electric_drive = NULL;
UPDATE cms_good_coil cgd set cgd.ratchets = 0 WHERE cgd.ratchets = NULL;
UPDATE cms_good_coil cgd set cgd.dictionary_coil_material = NULL WHERE cgd.dictionary_coil_material = 0;
UPDATE cms_good_coil cgd set cgd.friction_load = NULL WHERE cgd.friction_load = 0;

UPDATE cms_good_kruchki cgd set cgd.dictionary_kruchki_type = NULL WHERE cgd.dictionary_kruchki_type = 0;
UPDATE cms_good_kruchki cgd set cgd.nezaceplayka = NULL WHERE cgd.nezaceplayka = 0;
UPDATE cms_good_kruchki cgd set cgd.v_upakovke = NULL WHERE cgd.v_upakovke = 0;
UPDATE cms_good_kruchki cgd set cgd.kratnost_upakovki = NULL WHERE cgd.kratnost_upakovki = 0;
UPDATE cms_good_kruchki cgd set cgd.ves = NULL WHERE cgd.ves = 0;

UPDATE cms_good_leska cgd set cgd.dictionary_leska_type = NULL WHERE cgd.dictionary_leska_type = 0;
UPDATE cms_good_leska cgd set cgd.diametr = NULL WHERE cgd.diametr = 0;
UPDATE cms_good_leska cgd set cgd.razmotka = NULL WHERE cgd.razmotka = 0;
UPDATE cms_good_leska cgd set cgd.razryvnaya_nagruzka = NULL WHERE cgd.razryvnaya_nagruzka = 0;
UPDATE cms_good_leska cgd set cgd.kolichestvo_zhil = NULL WHERE cgd.kolichestvo_zhil = 0;
UPDATE cms_good_leska cgd set cgd.in_package = NULL WHERE cgd.in_package = 0;

UPDATE cms_good_minnow cgd set cgd.dictionary_minnow_type = NULL WHERE cgd.dictionary_minnow_type = 0;
UPDATE cms_good_minnow cgd set cgd.weight = NULL WHERE cgd.weight = 0;
UPDATE cms_good_minnow cgd set cgd.length = NULL WHERE cgd.length = 0;
UPDATE cms_good_minnow cgd set cgd.dictionary_minnow_manufacturer = NULL WHERE cgd.dictionary_minnow_manufacturer = 0;

UPDATE cms_good_rod cgd set cgd.dictionary_rod_type = NULL WHERE cgd.dictionary_rod_type = 0;
UPDATE cms_good_rod cgd set cgd.length = NULL WHERE cgd.length = 0;
UPDATE cms_good_rod cgd set cgd.shipping_length = NULL WHERE cgd.shipping_length = 0;
UPDATE cms_good_rod cgd set cgd.weight = NULL WHERE cgd.weight = 0;
UPDATE cms_good_rod cgd set cgd.dictionary_rod_connection_type = NULL WHERE cgd.dictionary_rod_connection_type = 0;
UPDATE cms_good_rod cgd set cgd.rings_count = NULL WHERE cgd.rings_count = 0;
UPDATE cms_good_rod cgd set cgd.connector_diameter = NULL WHERE cgd.connector_diameter = 0;
UPDATE cms_good_rod cgd set cgd.dictionary_rod_story = NULL WHERE cgd.dictionary_rod_story = 0;

UPDATE cms_good_softbait cgd set cgd.dictionary_softbait_type = NULL WHERE cgd.dictionary_softbait_type = 0;
UPDATE cms_good_softbait cgd set cgd.weight = NULL WHERE cgd.weight = 0;
UPDATE cms_good_softbait cgd set cgd.length = NULL WHERE cgd.length = 0;
UPDATE cms_good_softbait cgd set cgd.dictionary_softbait_buoyancy = NULL WHERE cgd.dictionary_softbait_buoyancy = 0;
UPDATE cms_good_softbait cgd set cgd.line_counter = 0 WHERE cgd.line_counter = NULL;

UPDATE cms_good_tyre cgd set cgd.width = NULL WHERE cgd.width = 0;
UPDATE cms_good_tyre cgd set cgd.height = NULL WHERE cgd.height = 0;
UPDATE cms_good_tyre cgd set cgd.dictionary_tyre_construction = NULL WHERE cgd.dictionary_tyre_construction = 0;
UPDATE cms_good_tyre cgd set cgd.diameter = NULL WHERE cgd.diameter = 0;
UPDATE cms_good_tyre cgd set cgd.dictionary_tyre_runflat = NULL WHERE cgd.dictionary_tyre_runflat = 0;
UPDATE cms_good_tyre cgd set cgd.dictionary_tyre_camers = NULL WHERE cgd.dictionary_tyre_camers = 0;
UPDATE cms_good_tyre cgd set cgd.dictionary_tyre_season = NULL WHERE cgd.dictionary_tyre_season = 0;
UPDATE cms_good_tyre cgd set cgd.cargo = 0 WHERE cgd.cargo = NULL;
UPDATE cms_good_tyre cgd set cgd.dictionary_tyre_xl = NULL WHERE cgd.dictionary_tyre_xl = 0;

UPDATE cms_good_tyremoto cgd set cgd.width = NULL WHERE cgd.width = 0;
UPDATE cms_good_tyremoto cgd set cgd.height = NULL WHERE cgd.height = 0;
UPDATE cms_good_tyremoto cgd set cgd.dictionary_tyremoto_construction = NULL WHERE cgd.dictionary_tyremoto_construction = 0;
UPDATE cms_good_tyremoto cgd set cgd.diameter = NULL WHERE cgd.diameter = 0;
UPDATE cms_good_tyremoto cgd set cgd.dictionary_tyremoto_camers = NULL WHERE cgd.dictionary_tyremoto_camers = 0;
UPDATE cms_good_tyremoto cgd set cgd.dictionary_tyremoto_axis = NULL WHERE cgd.dictionary_tyremoto_axis = 0;

UPDATE cms_good_wobbler cgd set cgd.dictionary_wobbler_type = NULL WHERE cgd.dictionary_wobbler_type = 0;
UPDATE cms_good_wobbler cgd set cgd.weight = NULL WHERE cgd.weight = 0;
UPDATE cms_good_wobbler cgd set cgd.recess_from = NULL WHERE cgd.recess_from = 0;
UPDATE cms_good_wobbler cgd set cgd.dictionary_wobbler_manufacturer = NULL WHERE cgd.dictionary_wobbler_manufacturer = 0;
UPDATE cms_good_wobbler cgd set cgd.dictionary_wobbler_buoyancy = NULL WHERE cgd.dictionary_wobbler_buoyancy = 0;
UPDATE cms_good_wobbler cgd set cgd.length = NULL WHERE cgd.length = 0;
UPDATE cms_good_wobbler cgd set cgd.recess_to = NULL WHERE cgd.recess_to = 0;
