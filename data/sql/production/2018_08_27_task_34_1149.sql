DELETE n1 FROM cms_dictionary_tyre_model n1, cms_dictionary_tyre_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_tyre_brand n1, cms_dictionary_tyre_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_tyremoto_model n1, cms_dictionary_tyremoto_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_tyremoto_brand n1, cms_dictionary_tyremoto_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_disk_model n1, cms_dictionary_disk_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_disk_brand n1, cms_dictionary_disk_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
 
DELETE n1 FROM cms_dictionary_coil_model n1, cms_dictionary_coil_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_coil_brand n1, cms_dictionary_coil_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
 
DELETE n1 FROM cms_dictionary_rod_model n1, cms_dictionary_rod_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_rod_brand n1, cms_dictionary_rod_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_minnow_model n1, cms_dictionary_minnow_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_minnow_brand n1, cms_dictionary_minnow_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_wobbler_model n1, cms_dictionary_wobbler_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_wobbler_brand n1, cms_dictionary_wobbler_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_wobbler_model n1, cms_dictionary_wobbler_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_wobbler_brand n1, cms_dictionary_wobbler_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_leska_model n1, cms_dictionary_leska_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_leska_brand n1, cms_dictionary_leska_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_kruchki_model n1, cms_dictionary_kruchki_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_kruchki_brand n1, cms_dictionary_kruchki_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_secretki_model n1, cms_dictionary_secretki_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_secretki_brand n1, cms_dictionary_secretki_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_secretkib_model n1, cms_dictionary_secretkib_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_secretkib_brand n1, cms_dictionary_secretkib_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;

DELETE n1 FROM cms_dictionary_tyrecvadro_model n1, cms_dictionary_tyrecvadro_model n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;
DELETE n1 FROM cms_dictionary_tyrecvadro_brand n1, cms_dictionary_tyrecvadro_brand n2 WHERE (n1.id > n2.id AND n1.name = n2.name) OR n1.name IS NULL;