ALTER TABLE `cms_price`
ADD `good_id` mediumint(8) UNSIGNED DEFAULT NULL AFTER `id`,
ADD INDEX good_id (good_id);
UPDATE cms_price cp
INNER JOIN cms_good_price cgp ON cgp.price_id = cp.id
SET cp.good_id = IF(cgp.good_id > 0, cgp.good_id, cp.good_id);

CREATE INDEX photo ON cms_good (photo);
CREATE INDEX type  ON cms_good (type);