CREATE TABLE cms_cron (
  id int(11) NOT NULL AUTO_INCREMENT,
  method varchar(255) DEFAULT NULL,
  start_date datetime DEFAULT NULL,
  period varchar(255) DEFAULT NULL,
  run_date datetime DEFAULT NULL,
  service varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = MYISAM
CHARACTER SET utf8
COLLATE utf8_general_ci;

INSERT INTO cms_cron (`id`, `method`, `start_date`, `period`, `run_date`, `service`)
VALUES (NULL, 'run', '2017-05-27 00:00:00', '1 day', NULL, 'Import\\Service\\Cron');