CREATE TABLE cms_import_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  supplier_price_import_id int(11) DEFAULT NULL,
  date_import datetime DEFAULT NULL,
  success smallint(1) DEFAULT NULL,
  detail text DEFAULT NULL,
  warning smallint(1) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;