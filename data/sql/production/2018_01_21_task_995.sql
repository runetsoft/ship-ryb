CREATE TABLE cms_good_coil (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_coil_type int(11) DEFAULT NULL COMMENT 'Тип',
  ratio decimal(5, 2) DEFAULT 0 COMMENT 'Передаточное число',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  intensity varchar(255) DEFAULT NULL COMMENT 'Лесоемкость',
  spool varchar(50) DEFAULT NULL COMMENT 'Наличие запасной шпули',
  bearings decimal(5, 2) DEFAULT 0 COMMENT 'Количество подшипников',
  bytraner tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Байтраннер',
  line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Счетчик лески',
  protection_water tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Защита от соленой воды',
  dictionary_coil_hand int(11) DEFAULT NULL COMMENT 'Рука',
  electric_drive tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие электропривода',
  ratchets tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие трещетки',
  dictionary_coil_material int(11) DEFAULT NULL COMMENT 'Материал'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_rod (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_rod_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Тест',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  shipping_length decimal(5, 2) DEFAULT 0 COMMENT 'Транспортировочная длина',
  sections_count decimal(5, 2) DEFAULT 0 COMMENT 'Количество секций',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  dictionary_rod_connection_type int(11) DEFAULT NULL COMMENT 'Тип соединения',
  rings_count decimal(5, 2) DEFAULT 0 COMMENT 'Kоличество колец',
  connector_diameter decimal(5, 2) DEFAULT 0 COMMENT 'Диаметр коннектора'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_minnow (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_minnow_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  amount_package varchar(255) DEFAULT NULL COMMENT 'В упаковке',
  dictionary_minnow_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель',
  packing_ratio varchar(255) DEFAULT NULL COMMENT 'Кратность упаковки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_wobbler (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_wobbler_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  recess varchar(255) DEFAULT NULL COMMENT 'Заглубление',
  dictionary_wobbler_manufacturer int(11) DEFAULT NULL COMMENT 'Производитель',
  dictionary_wobbler_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть',
  hooks varchar(255) DEFAULT NULL COMMENT 'Крючки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cms_good_softbait (
  good_id int(11) NOT NULL COMMENT 'Id товара',
  brand int(11) DEFAULT NULL COMMENT 'Бренд',
  model int(11) DEFAULT NULL COMMENT 'Модель',
  dictionary_softbait_type int(11) DEFAULT NULL COMMENT 'Тип',
  test varchar(255) DEFAULT NULL COMMENT 'Цвет',
  weight decimal(5, 2) DEFAULT 0 COMMENT 'Вес',
  length decimal(5, 2) DEFAULT 0 COMMENT 'Длина',
  amount_package varchar(255) DEFAULT NULL COMMENT 'Количество в упаковке',
  dictionary_softbait_buoyancy int(11) DEFAULT NULL COMMENT 'Плавучесть',
  line_counter tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Наличие погремушки'
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

########################################################################################################################

SET NAMES 'utf8';

INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(6, 'coil', 'Катушка', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(7, 'rod', 'Удилище', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(8, 'minnow', 'Блесна', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(9, 'wobbler', 'Воблер', NULL);
INSERT INTO cms_dictionary_good_type(id, code, name, parent_id) VALUES
(10, 'softbait', 'Мягкая приманка', NULL);

########################################################################################################################

CREATE TABLE cms_dictionary_coil_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Бренд катушка';

CREATE TABLE cms_dictionary_coil_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Модел катушка';


CREATE TABLE cms_dictionary_rod_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Бренд удилище';

CREATE TABLE cms_dictionary_rod_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Модел удилище';


CREATE TABLE cms_dictionary_minnow_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Бренд блесна';

CREATE TABLE cms_dictionary_minnow_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Модел блесна';


CREATE TABLE cms_dictionary_wobbler_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Бренд воблер';

CREATE TABLE cms_dictionary_wobbler_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Модел воблер';

CREATE TABLE cms_dictionary_softbait_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Бренд мягкая приманка';

CREATE TABLE cms_dictionary_softbait_model (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(55) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci
COMMENT = 'Модел мягкая приманка';

UPDATE cms_good cg
SET cg.type = IF(cg.type = 0, 4, cg.type);