--
-- Изменить таблицу "cms_provider"
--
ALTER TABLE cms_provider
  ADD COLUMN comments VARCHAR(128) DEFAULT NULL AFTER uuid,
  ADD COLUMN payment_method VARCHAR(128) DEFAULT NULL AFTER comments,
  ADD COLUMN email_balance VARCHAR(128) DEFAULT NULL AFTER payment_method,
  ADD COLUMN email_order VARCHAR(128) DEFAULT NULL AFTER email_balance,
  ADD COLUMN messenger VARCHAR(128) DEFAULT NULL AFTER email_order,
  ADD COLUMN phone VARCHAR(128) DEFAULT NULL AFTER messenger;


INSERT INTO cms_provider_group (id, provider_group) VALUES ('2', 'Шины и Диски');
INSERT INTO cms_provider_group (id, provider_group) VALUES ('3', 'Рыбалка');