ALTER TABLE `ship_ryb`.`cms_price` DROP INDEX `provider_good`, ADD INDEX `provider_good` (`provider_id`, `good`) USING BTREE;
ALTER TABLE `cms_price` ADD `sarticle` VARCHAR(255) NULL DEFAULT NULL COMMENT 'артикул поставщика' AFTER `zarticle`;
ALTER TABLE `ship_ryb`.`cms_price` ADD INDEX `sarticle` (`sarticle`) USING BTREE;