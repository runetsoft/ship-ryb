CREATE TABLE cms_supplier_price_import_file (
  id int(11) NOT NULL AUTO_INCREMENT,
  supplier_price_import_id int(11) DEFAULT NULL COMMENT 'id импорта',
  filename_mask varchar(255) DEFAULT NULL COMMENT 'Маска  названия файла',
  encoding varchar(255) DEFAULT NULL
  PRIMARY KEY (id)
)
ENGINE = MYISAM
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Файлы импорта остатоков';

ALTER TABLE cms_import_file_structure
  ADD COLUMN supplier_price_import_file_id INT DEFAULT NULL AFTER id;