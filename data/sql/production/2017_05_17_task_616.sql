CREATE TABLE cms_dictionary_brand (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;


