UPDATE cms_provider cp 
  SET cp.dt_insert = '1970.01.01 00:00:00',
  cp.dt_update = '1970.01.01 00:00:00',
  cp.dt_price_load = '1970.01.01'
  WHERE cp.id >= 97 AND cp.id <=103 ;

UPDATE cms_provider cp 
  SET cp.dt_price_load = '2017.04.11'
  WHERE cp.id = 95;

UPDATE cms_provider cp 
  SET cp.dt_update = '2017.04.21 00:00:00',
  cp.dt_price_load = '2017.04.21'
  WHERE cp.id = 96;