CREATE TABLE cms_good_tyre_moto (
  good_id int(11) NOT NULL COMMENT 'id товара',
  brand varchar(255) DEFAULT NULL COMMENT 'бренд',
  model varchar(50) DEFAULT NULL COMMENT 'модель',
  width decimal(5, 2) DEFAULT NULL COMMENT 'ширина',
  height decimal(5, 2) DEFAULT NULL COMMENT 'высота',
  construction varchar(255) DEFAULT NULL COMMENT 'конструкция',
  diameter decimal(5, 2) DEFAULT NULL COMMENT 'диаметр',
  index_load decimal(3, 0) DEFAULT NULL COMMENT 'индекс нагрузки',
  index_speed varchar(255) DEFAULT NULL COMMENT 'индекс скорости',
  features varchar(255) DEFAULT NULL COMMENT 'характеризующие аббревиатуры',
  camers varchar(255) DEFAULT NULL COMMENT 'камерность',
  axis tinyint(4) DEFAULT NULL COMMENT 'Ось (1 - перед, 2 - зад)',
  PRIMARY KEY (good_id)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 175
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;