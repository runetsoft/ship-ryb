CREATE TABLE cms_dictionarymultiselect_coil_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_coil_hand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_coil_material
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_coil_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_coil_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_disk_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_disk_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_disk_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_good_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_good_type_category
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_kruchki_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_kruchki_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_kruchki_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_leska_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_leska_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_leska_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_minnow_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_minnow_manufacturer
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_minnow_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_minnow_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_rod_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_rod_connection_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_rod_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_rod_story
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_rod_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_kluch
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_posadochoe_mesto
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_rezba
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretki_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretkib_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretkib_kluch
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretkib_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretkib_posadochoe_mesto
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_secretkib_rezba
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_softbait_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_softbait_buoyancy
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_softbait_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_softbait_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_camers
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_construction
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_runflat
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_season
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyre_xl
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyrecvadro_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyrecvadro_camers
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyrecvadro_constr
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyrecvadro_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyrecvadro_os
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyremoto_axis
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyremoto_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyremoto_camers
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyremoto_construction
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_tyremoto_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_wobbler_brand
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_wobbler_buoyancy
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_wobbler_manufacturer
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_wobbler_model
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_wobbler_type
 (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
CREATE TABLE cms_dictionarymultiselect_word_to_number
  (   id int(11) NOT NULL AUTO_INCREMENT,
   good_id int(11) NOT NULL,
   dictionary_id int(11) NOT NULL,
   PRIMARY KEY (id) ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT = DYNAMIC;
