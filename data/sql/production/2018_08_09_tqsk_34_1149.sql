UPDATE cms_good set type = 0 WHERE type IS NULL;
ALTER TABLE cms_good
  CHANGE COLUMN type type INT(11) DEFAULT 0 NOT NULL COMMENT 'Товар Тип';