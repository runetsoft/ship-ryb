ALTER TABLE `cms_dictionary_tyre` RENAME `cms_dictionary_tyre_brand`;
ALTER TABLE `cms_dictionary_disk` RENAME `cms_dictionary_disk_brand`;

ALTER TABLE `cms_dictionary_disk_brand` ADD `code` VARCHAR(100);
ALTER TABLE `cms_dictionary_tyre_brand` ADD `code` VARCHAR(100);