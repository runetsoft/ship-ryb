CREATE TABLE `cms_binder_dictionary` ( `table_name` VARCHAR(255) NOT NULL , `parent_table_name` VARCHAR(255) NULL DEFAULT NULL , PRIMARY KEY (`table_name`)) ENGINE = InnoDB;

ALTER TABLE `cms_binder_dictionary` ADD UNIQUE `UNIQE` (`table_name`, `parent_table_name`);

ALTER TABLE `cms_dictionary_brand` ADD `parent_id` INT(11) NULL DEFAULT NULL AFTER `name`;

ALTER TABLE `cms_dictionary_disk_brand` ADD `parent_id` INT(11) NULL DEFAULT NULL AFTER `code`;

ALTER TABLE `cms_dictionary_disk_model` CHANGE `brand_id` `parent_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `cms_dictionary_runflat` ADD `parent_id` INT(11) NULL DEFAULT NULL AFTER `name`;

ALTER TABLE `cms_dictionary_tyre_brand` ADD `parent_id` INT(11) NULL DEFAULT NULL AFTER `code`;

ALTER TABLE `cms_dictionary_tyre_model` CHANGE `brand_id` `parent_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `cms_dictionary_word_to_number` ADD `parent_id` INT(11) NULL DEFAULT NULL AFTER `name`;

ALTER TABLE `shipryb_db`.`cms_dictionary_runflat` ADD PRIMARY KEY (`id`);

ALTER TABLE `cms_dictionary_runflat` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;