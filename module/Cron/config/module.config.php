<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Cron\Controller\MainController' => 'Cron\Factory\MainControllerFactory'
        )
    ),
    'console'  => include 'console.config.php'
);