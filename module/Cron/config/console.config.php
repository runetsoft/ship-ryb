<?php
return [
    'router' => [
        'routes' => [
            'cron-module-start' => [
                'type' => 'Simple',
                'options' => [
                    'route' => 'cron module start',
                    'defaults' => [
                        'controller' => 'Cron\Controller\MainController',
                        'action' => 'index'
                    ],
                ],
            ],
        ],
    ],
];