<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 24.05.2017
 * Time: 19:11
 */

namespace Cron\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Utilities\DateFormat;
use Cron\Model\CronTable;

class Run
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Cron\Model\CronTable
     */
    protected $cronTable;

    /**
     * RunCron constructor.
     * @param $sm
     * @param CronTable $cronTable
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->cronTable = $this->sm->get('Cron\Model\CronTable');
    }

    /**
     * Получает сервис из талицы и запускает его.
     *
     * @param $id
     */
    public function run($id)
    {
        $cron = $this->cronTable->getList(array('id' => $id))->current();
        $cronService = $this->sm->get($cron->service);
        call_user_func(array($cronService, $cron->method));
        $this->cronTable->update(array('run_date' => (new \DateTime())->format(DateFormat::STANDARD)), array('id' => $id));
    }

}