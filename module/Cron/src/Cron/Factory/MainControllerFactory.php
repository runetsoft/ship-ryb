<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 14:08
 */

namespace Cron\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Cron\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator) {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController(
            $parentLocator,
            $parentLocator->get('Cron\Model\CronTable'),
            $parentLocator->get('Cron\Service\Run')
        );
    }
}