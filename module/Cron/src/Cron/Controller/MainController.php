<?php
namespace Cron\Controller;

use Cron\Model\Cron;
use Cron\Model\CronTable;
use Cron\Service\Run;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\JsonModel;
use Application\Utilities\DateFormat;

class MainController extends AbstractActionController
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Cron\Model\CronTable
     */
    protected $cronTable;

    /**
     * @var \Cron\Service\Run
     */
    protected $cronRun;

    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    /**
     * MainController constructor.
     * @param $sm
     * @param CronTable $cronTable
     * @param Run $run
     */
    public function __construct($sm, CronTable $cronTable, Run $run)
    {
        $this->sm = $sm;
        $this->cronTable = $cronTable;
        $this->cronRun = $run;
        ini_set("error_reporting", E_ERROR);
        $sm->get('Logger')->crit('error_reporting => ' . ini_get('error_reporting'));
    }

    /**
     * Главный контроллер крона.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function indexAction()
    {
        $cronIterator = $this->cronTable->getList();
        /**
         * @var $item Cron
         */
        foreach ($cronIterator as $item) {
            if ($item->start_date == null && $item->run_date == null) {
                // Крон не настроен.
                continue;
            }

            $curDate = new \DateTime();
            $startDate = new \DateTime($item->start_date);
            if ($startDate->getTimestamp() - $curDate->getTimestamp() > 0) {
                // Должно запустится в будущем.
                continue;
            }

            if (is_numeric($item->period)) {
                $runIntervalSec = $item->period;
            } else {
                $periodDate = new \DateTime($item->period);
                $runIntervalSec = $periodDate->getTimestamp() - $curDate->getTimestamp();
            }

            $runDate = new \DateTime($item->run_date);
            if ($curDate->getTimestamp() - $runDate->getTimestamp() > $runIntervalSec || !$item->run_date) {
                $this->cronRun->run($item->id);
            }
        }
    }
}
