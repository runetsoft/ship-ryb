<?php
namespace Cron\Model;

use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class CronTable работа с таблицами прайса.
 *
 * @package Cron\Model
 */
class CronTable
{
    protected $dbAdapter;
    protected $sm;
    protected $cronTableGateway;

    /**
     * CronTable constructor.
     * @param ServiceLocatorInterface $sm
     * @param \Zend\Db\TableGateway\TableGateway $cronTableGateway
     */
    public function __construct($sm, $cronTableGateway)
    {
        $this->sm = $sm;
        $this->cronTableGateway = $cronTableGateway;
    }

    /**
     * Возвращает лист с данными по фильтру.
     *
     * @param array $filter
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getList($filter = array())
    {
       return  $this->cronTableGateway->select($filter);
    }

    /**
     * Добавляет и обновляет данные в таблице.
     *
     * @param array $set данные
     * @param array $where условия
     * @return int
     */
    public function update($set = array(), $where = array())
    {
        return $this->cronTableGateway->update($set, $where);
    }

}