<?php
namespace Cron\Model;

use Application\Model\BaseModel;

/**
 * Class Cron Таблица cms_cron
 * @package Cron\Model
 */
class Cron extends BaseModel
{
    /**
     * @description "ID"
     */
    public $id;
    /**
     * @description "Сервис"
     */
    public $service;
    /**
     * @description "Выполняемый метод"
     */
    public $method;

    /**
     * @description "Дата первого запуска"
     */
    public $start_date;

    /**
     * @description "Переодичность выполнения"
     */
    public $period;

    /**
     * @description "Последний запуск"
     */
    public $run_date;

}