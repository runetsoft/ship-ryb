<?php
namespace Cron;

use Cron\Model\Cron;
use Cron\Model\CronTable;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/**
 * Class Module
 * Модуль для раздела прайсы.
 *
 * @package Cron
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Cron\Model\CronTable' => function ($sm) {
                    $cronTableGateway = $sm->get('CronTableGateway');
                    $table = new CronTable($sm, $cronTableGateway);
                    return $table;
                },
                'CronTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Cron());
                    return new TableGateway('cms_cron', $dbAdapter, $features, $resultSetPrototype);
                }
            )
        );
    }

    public function getConsoleUsage(AdapterInterface $console)
    {
        return [
            'cron module start' => 'Run cron processes.',
        ];
    }
}