<?php
namespace Import\Model;

use Zend\Db\RowGateway\RowGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form\Element\DateTime;
use Zend\Json\Json;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Text\Table\Row;


/**
 * Class SupplierPriceImportTable для работы с таблицами импорта.
 *
 * @package Import\Model
 */
class SupplierPriceImportFileTable
{
    /**
     * @var \Zend\Db\Adapter\AdapterInterface
     */
    protected $dbAdapter;
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;
    /**
     * @var TableGateway;
     */
    protected $supplierPriceImportTableGateway;
    /**
     * @var TableGateway
     */
    protected $supplierTableGateway;
    /**
     * @var string
     */
    protected $supplierTableName;
    /**
     * @var string
     */
    protected $supplierPriceImportTableName;

    /**
     * @var TableGateway;
     */
    protected $supplierPriceImportFileTableGateway;
    /**
     * @var string
     */
    protected $supplierPriceImportFileTableName;

    /**
     * @var TableGateway;
     */
    protected $importLogTableGateway;
    /**
     * @var string
     */
    protected $importLogTableName;

    /**
     * @var TableGateway;
     */
    protected $importFileStructureTableGateway;
    /**
     * @var string
     */
    protected $importFileStructureTableName;

    /**
     * GoodTable constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;

        $this->supplierTableGateway = $this->sm->get('SupplierTableGateway');
        $this->supplierTableName = $this->supplierTableGateway->getTable();

        $this->supplierPriceImportTableGateway = $this->sm->get('SupplierPriceImportTableGateway');
        $this->supplierPriceImportTableName = $this->supplierPriceImportTableGateway->getTable();

        $this->importFileStructureTableGateway = $this->sm->get('ImportFileStructureTableGateway');
        $this->importFileStructureTableName = $this->importFileStructureTableGateway->getTable();

        $this->supplierPriceImportFileTableGateway = $this->sm->get('SupplierPriceImportFileTableGateway');
        $this->supplierPriceImportFileTableName = $this->supplierPriceImportFileTableGateway->getTable();

        $this->importLogTableGateway = $this->sm->get('ImportLogTableGateway');
        $this->importLogTableName = $this->importLogTableGateway->getTable();


        $this->dbAdapter = $this->supplierPriceImportFileTableGateway->getAdapter();
    }

    /**
     * @param  $where
     * @param  $asArray
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAllInfo($where, $asArray = false)
    {
        $res = $this->supplierPriceImportFileTableGateway->select($where);
        if ($asArray) {
            return $res->toArray();
        } else {
            return $res;
        }
    }

    /**
     * @param $id
     * @return array|\ArrayObject|null
     */
    public function getOneRow($id)
    {
        return $this->getAllInfo(array('id' => $id))->current();
    }

    /**
     * Получить строку.
     *
     * @return array|object
     */
    public function getRow()
    {
        return $this->sm->get('SupplierPriceImportFileRow');
    }

    /**
     * Обновление данных импортированного файла по id.
     *
     * @param $data
     * @return bool
     */
    public function itemUpdate($data)
    {

        if ($data['id']) {
            $this->supplierPriceImportFileTableGateway->update($data, array('id' => $data['id']));
        } else {
            $this->supplierPriceImportFileTableGateway->insert($data);
        }
        return true;
    }

    /**
     * Получает информацию по импорту для поставщика.
     *
     * @param $priceImportFileId Ид записи в таблице импорта.
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getDataForImport($priceImportFileId)
    {
        $select = $this->supplierPriceImportFileTableGateway->getSql()->select()
            ->columns(
                array(
                    'id',
                    'supplier_price_import_id',
                    'encoding'
                ))
            ->join(
                $this->supplierPriceImportTableName,
                $this->supplierPriceImportTableName . '.id = ' .
                $this->supplierPriceImportFileTableName . '.supplier_price_import_id',
                array(
                    'import_type',
                    'import_info',
                    'supplier_id'
                ),
                Select::JOIN_LEFT
            )
            ->join(
                $this->importFileStructureTableName,
                $this->importFileStructureTableName . '.supplier_price_import_file_id = ' .
                $this->supplierPriceImportFileTableName . '.id',
                array(
                    'book_name',
                    'entity',
                    'fields_match',
                    'default_data',
                    'preparer_class',
                    'primary_field',
                    'start_row',
                    'no_headers',
                    'no_upload'
                ), Select::JOIN_LEFT
            )
            ->where(array($this->supplierPriceImportFileTableName . '.id = ?' => $priceImportFileId))
            ->where(array($this->importFileStructureTableName . '.no_upload <> 1'))
            ->where(array($this->supplierPriceImportFileTableName . '.no_upload <> 1'));

        $statement = $this->supplierPriceImportTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $config = array();
        foreach ($resultSet as $res) {
            $res['fields_match'] = ($res['fields_match'] == null) ? array() : (array)Json::decode($res['fields_match']);
            $res['default_data'] = ($res['default_data'] == null) ? array() : (array)Json::decode($res['default_data']);
            $config[$res['book_name']] = $res;
        }

        return $config;
    }

    /**
     * @param $supplierid
     *
     * @return \Iterator
     */
    public function getSupplierImportInfo($supplierid)
    {

        $res = $this->supplierPriceImportFileTableGateway->select(
            function ($select) use ($supplierid) {
                $select->join(
                    $this->supplierPriceImportTableName,
                    $this->supplierPriceImportTableName . '.id = ' .
                    $this->supplierPriceImportFileTableName . '.supplier_price_import_id',
                    array(),
                    Select::JOIN_LEFT
                )
                    ->where(array($this->supplierPriceImportTableName . '.supplier_id = ?' => $supplierid));
            }
        );

        return $res;
    }

    public function getAllImportSuppliers($where = null)
    {
        $iterator = $this->getAllImportSuppliersInfo($where);
        $suppliers = array();
        $curDate = null;
        foreach ($iterator as $item) {
            $suppliers[$item->supplier_id]['items'][$item->id] = $item->toArray();
            $suppliers[$item->supplier_id]['oldestInterval'] = $item->cron_periodicity;
            $suppliers[$item->supplier_id]['oldestDate'] = $item->date_import;
        }
        return $suppliers;
    }

    /**
     * Получает всех данные по импортируемым файлам.
     * @param array|null $where
     * @return mixed
     */
    public function getAllImportSuppliersInfo($where = null)
    {
        $supplierPriceImportTable = $this->supplierPriceImportFileTableGateway->select(
            function ($select) use ($where) {
                $select->columns(
                    array(
                        'filename_mask' => 'filename_mask',
                        // 'date_import_log' => new Expression('MAX(`cms_import_log`.`date_import`)'),
                        'id' => 'id'
                    )
                )
                    ->join(
                        $this->supplierPriceImportTableName,
                        $this->supplierPriceImportTableName . '.id = ' .
                        $this->supplierPriceImportFileTableName . '.supplier_price_import_id',
                        array(
                            'supplier_id',
                            'import_type',
                            'import_info',
                            'is_cron',
                            'cron_periodicity',
                            'date_import'
                        ),
                        Select::JOIN_LEFT
                    )
                    ->join(
                        $this->supplierTableName,
                        $this->supplierTableName . '.id = ' .
                        $this->supplierPriceImportTableName . '.supplier_id',
                        array(
                            'status'
                        ),
                        Select::JOIN_INNER
                    )
                    ->join(
                        array('cli' => new Expression('(SELECT * FROM cms_import_log cli ORDER BY cli.date_import DESC LIMIT 1)')),
                        'cli.supplier_price_import_file_id = ' .
                        $this->supplierPriceImportFileTableName . '.id',
                        array(
                            'date_import_log'=>'date_import',
                            'success',
                            'detail',
                            'warning',
                        ),
                        Select::JOIN_LEFT
                    );

                    if (isset($where['is_cron'])) {
                        $select->where(array($this->supplierPriceImportTableName . '.is_cron = ?' => $where['is_cron']));
                    }
                    if (isset($where['status'])) {
                        $select->where(array($this->supplierTableName . '.status = ?' => $where['status']));
                    }
                    if (isset($where['priceImportId'])) {
                        $select->where(array($this->supplierPriceImportTableName . '.id = ?' => $where['priceImportId']));
                    }
            }
        );
        return $supplierPriceImportTable;
    }

    /**
     * Удаление импорта из БД.
     *
     * @param array $filter
     */
    public function delete(array $filter)
    {
        $this->supplierPriceImportFileTableGateway->delete($filter);
    }

}