<?php
namespace Import\Model;

use Application\Model\BaseModel;

/**
 * Class SupplierPriceImport Способы импорта остатков cms_supplier_price_import.
 *
 * @package Import\Model
 */
class SupplierPriceImport extends BaseModel
{
    public $id;
    public $supplier_id;
    public $import_type;
    public $import_info;
    public $encoding;
    public $is_cron;
    public $cron_periodicity;
    public $date_import;
}