<?php
namespace Import\Model;

use Application\Model\BaseModel;

/**
 * Таблица настроек для импорта файла cms_import_file_structure.
 *
 * Class ImportFileStructure
 * @package Import\Model
 */
class ImportFileStructure extends BaseModel
{
    public $id;
    public $supplier_price_import_id;
    public $supplier_price_import_file_id;
    public $book_name;
    public $entity;
    public $fields_match;
    public $default_data;
    public $no_headers;
}