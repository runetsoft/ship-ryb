<?php
namespace Import\Model;

use Application\Model\BaseModel;

/**
 * Class SupplierPriceImportFile Способы импорта остатков cms_supplier_price_import_file.
 *
 * @package Import\Model
 */
class SupplierPriceImportFile extends BaseModel
{
    public $id;
    public $supplier_price_import_id;
    public $encoding;
    public $filename_mask;
    public $no_upload;
}