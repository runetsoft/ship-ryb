<?php
namespace Import\Model;

use Zend\Db\RowGateway\RowGateway;
use Zend\Json\Json;
use Zend\Text\Table\Row;

/**
 * Таблица настроек для импорта файла cms_import_file_structure.
 *
 * Class ImportFileStructure
 * @package Import\Model
 */
class ImportLogTable
{
    /**
     * @var \Zend\Db\Adapter\AdapterInterface
     */
    protected $dbAdapter;
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $importLogTableGateway;

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $importLogTableName;


    /**
     * ImportFileStructureTable constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->importLogTableGateway = $this->sm->get('ImportLogTableGateway');
        $this->importLogTableName = $this->importLogTableGateway->getTable();
    }

    /**
     * Добавить сообщение в лог.
     *
     * @param int $supplier_price_import_fie_id Ид импорта.
     * @param string $message Сообщение в лог.
     * @param bool $success Отметка что импорт был не успешный.
     * @param bool $warning Отметка об изменениях структуры файла.
     * @param bool $newLog Новый лог или обновление старого (если первая запись то добавленеи несмотря на флаг).
     *
     * @return  RowGateway
     */
    public function addMessageToLog($supplier_price_import_fie_id, $message, $success = true, $warning = false, $newLog =
false)
    {
        $results = $this->importLogTableGateway->select(function ($select) use ($supplier_price_import_fie_id) {
            $select
                ->where(array('supplier_price_import_file_id'=>$supplier_price_import_fie_id))
                ->order('id DESC')->limit(1);
        });
        $logRow = $results->current();

        if (!$logRow || $newLog === true) {
            $logRow = $this->sm->get('ImportLogRow');
            $logRow->supplier_price_import_file_id = $supplier_price_import_fie_id;
            $detail = array($message);
        } else {
            $detail = (array)Json::decode($logRow->detail) ;
            $detail[] = $message;
            $success  = $logRow->success ? $success : false;
            $warning  = !$logRow->warning ? $warning : true;
        }
        $logRow->detail = Json::encode($detail);
        $logRow->success = $success;
        $logRow->warning = $warning;
        $date = new \DateTime();
        $logRow->date_import = $date->format('Y-m-d H:i:s');
        $logRow->save();
        return $logRow;
    }
}