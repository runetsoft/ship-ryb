<?php
namespace Import\Model;

use Application\Utilities\DateFormat;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Json\Json;

/**
 * Class SupplierPriceImportTable для работы с таблицами импорта.
 *
 * @package Import\Model
 */
class SupplierPriceImportTable
{
    /**
     * @var \Zend\Db\Adapter\AdapterInterface
     */
    protected $dbAdapter;
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;
    /**
     * @var TableGateway;
     */
    protected $supplierPriceImportTableGateway;
    /**
     * @var string
     */
    protected $supplierPriceImportTableName;
    /**
     * @var TableGateway;
     */
    protected $importFileStructureTableGateway;
    /**
     * @var string
     */
    protected $importFileStructureTableName;

    /**
     * @var string
     */
    protected $importLogTableName;


    /**
     * GoodTable constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->supplierPriceImportTableGateway = $this->sm->get('SupplierPriceImportTableGateway');
        $this->supplierPriceImportTableName = $this->supplierPriceImportTableGateway->getTable();

        $this->importFileStructureTableGateway = $this->sm->get('ImportFileStructureTableGateway');
        $this->importFileStructureTableName = $this->importFileStructureTableGateway->getTable();
        $this->importLogTableName = $this->sm->get('ImportLogTableGateway')->getTable();

        $this->dbAdapter = $this->importFileStructureTableGateway->getAdapter();
    }

//    /**
//     * Получает информацию по импорту для поставщика.
//     *
//     * @param $priceImportId Ид записи в таблице импорта.
//     *
//     * @return null|\Zend\Db\ResultSet\ResultSetInterface
//     */
//    public function getAllInfo($priceImportId)
//    {
//
//        $select = $this->supplierPriceImportTableGateway->getSql()->select()
//            ->columns(
//                array(
//                    'id',
//                    'supplier_id',
//                    'import_type',
//                    'import_info'
//                ))
//            ->join(
//                $this->importFileStructureTableName,
//                $this->importFileStructureTableName . '.supplier_price_import_id = ' .
//                $this->supplierPriceImportTableName . '.id',
//                array(
//                    'book_name',
//                    'entity',
//                    'fields_match',
//                    'default_data',
//                    'preparer_class',
//                    'primary_field',
//                    'start_row',
//                    'no_headers'
//                ), Select::JOIN_LEFT
//            )
//            ->where(array($this->supplierPriceImportTableName . '.id = ?' => $priceImportId));
//
//        $statement = $this->supplierPriceImportTableGateway->getSql()->prepareStatementForSqlObject($select);
//        $resultSet = $statement->execute();
//        $config = array();
//        foreach ($resultSet as $res) {
//            $res['fields_match'] = (array) Json::decode($res['fields_match']);
//            $res['default_data'] = (array) Json::decode($res['default_data']);
//            $config[$res['book_name']] = $res;
//        }
//
//        return $config;
//    }

    /**
     * Получает информацию о способах импорта поставщика.
     *
     * @param array $filter Фильтр.
     * @return array|\ArrayObject|null
     */
    public function getSupplierImportInfo(array $filter)
    {
        $supplierPriceImportTable = $this->supplierPriceImportTableGateway->select(
            function ($select) use ($filter) {
                $select->join(
                    $this->importLogTableName,
                    $this->importLogTableName . '.supplier_price_import_id = cms_supplier_price_import.id',
                    array('date_import_log'=>'date_import', 'detail' => 'detail', 'warning' => 'warning', 'success' => 'success'),
                    Select::JOIN_LEFT
                )->where($this->prepareFilter($filter))->order('date_import_log DESC');
            }
        );

        return $supplierPriceImportTable;
    }

    /**
     * Получает  все импорты поставщика
     *
     * @param int $supplierId ID поставщика.
     * @return array|\ArrayObject|null
     */
    public function getSupplierImport($supplierId)
    {
        $supplierPriceImportTable = $this->supplierPriceImportTableGateway->select(
            function ($select) use ($supplierId) {
                $select->where(array('supplier_id = ?' => $supplierId));            }
        );

        return $supplierPriceImportTable;
    }

    /**
     * Получает всех поставщиков, у которых есть импорт.
     */
    public function getAllImportSuppliers()
    {
        $supplierPriceImportTable = $this->supplierPriceImportTableGateway->select(
            function ($select){
                $select->columns(
                    array(
                        'supplier_id' => new Expression('DISTINCT(supplier_id)'),
                        'id'=>'id'
                    )
                );
            }
        );
        return $supplierPriceImportTable;
    }

    /**
     * Обработка фильтра
     *
     * @param $arrayFilter
     *
     * @return array
     */
    private function prepareFilter($arrayFilter)
    {
        $newFilter = [];
        foreach ($arrayFilter as $key => $item) {
            $newFilter[$this->supplierPriceImportTableName . '.' . $key] = $item;
        }
        return $newFilter;
    }

    /**
     * Обновление данных импортированного файла по id.
     *
     * @param $data
     * @return bool
     */
    public function itemUpdate($data)
    {
        if ($data['id']) {
            $this->supplierPriceImportTableGateway->update($data, array('id' => $data['id']));
        } else {
            $this->supplierPriceImportTableGateway->insert($data);
        }
        return true;
    }

    /**
     * @param  $where
     * @param  $asArray
     *
     * @return \Zend\Db\Adapter\AdapterInterface
     */
    public function getAllInfo($where, $asArray = false)
    {
        $res =  $this->supplierPriceImportTableGateway->select($where);
        if ($asArray) {
            return $res->toArray();
        } else {
            return $res;
        }
    }

    /**
     * @return RowGateway
     */
    public function getOneRow($id)
    {
        return $this->getAllInfo(array('id'=>$id))->current();
    }


    /**
     * Сбросить дату импорта до 01.01.1970.
     *
     * @param $importId int импорта.
     */
    public function resetImportDate($importId)
    {
        $date = new \DateTime();
        $date->setDate(1970, 1, 1);
        $date->setTime(0, 0, 0);
        $this->itemUpdate(array('id' => $importId, 'date_import' => $date->format(DateFormat::STANDARD)));
    }

}