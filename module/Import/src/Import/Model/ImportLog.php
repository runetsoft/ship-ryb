<?php
namespace Import\Model;

use Application\Model\BaseModel;

/**
 * Таблица лога импорта cms_import_log.
 *
 * Class ImportLog
 * @package Import\Model
 */
class ImportLog extends BaseModel
{
    public $id;
    public $supplier_price_import_id;
    public $date_import;
    public $success;
    public $detail;
}