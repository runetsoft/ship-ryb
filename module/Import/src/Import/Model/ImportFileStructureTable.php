<?php
namespace Import\Model;

use Zend\Json\Json;
use Import\Service\ImportFileProcessor;
use Zend\Db\Sql\Select;
use Application\Utilities\DateFormat;

/**
 * Таблица настроек для импорта файла cms_import_file_structure.
 *
 * Class ImportFileStructure
 * @package Import\Model
 */
class ImportFileStructureTable
{
    /**
     * @var \Zend\Db\Adapter\AdapterInterface
     */
    protected $dbAdapter;
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var TableGateway;
     */
    protected $importFileStructureTableGateway;

    /**
     * @var \Import\Model\SupplierPriceImportTable
     */
    protected $supplierPriceImportTable;

    protected $supplierPriceImportTableGateway;
    /**
     * @var string
     */
    protected $supplierPriceImportTableName;

    protected $supplierPriceImportFileTableGateway;
    /**
     * @var string
     */
    protected $supplierPriceImportFileTableName;

    /**
     * @var \Import\Model\ImportLogTable;
     */
    protected $importLogTable;

    /**
     * @var string
     */
    protected $importFileStructureTableName;

    /**
     * Ридер файла.
     *
     * @var \Import\Service\ImportFileProcessor
     */
    protected $fileProcessor;

    /**
     * ImportFileStructureTable constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->importFileStructureTableGateway = $this->sm->get('ImportFileStructureTableGateway');
        $this->supplierPriceImportTableGateway = $this->sm->get('SupplierPriceImportTableGateway');
        $this->importLogTable = $this->sm->get('Import\Model\ImportLogTable');
        $this->importFileStructureTableName = $this->importFileStructureTableGateway->getTable();
        $this->dbAdapter = $this->importFileStructureTableGateway->getAdapter();
        $this->supplierPriceImportTable = $this->sm->get('Import\Model\SupplierPriceImportTable');
        $this->supplierPriceImportTableName = $this->supplierPriceImportTableGateway->getTable();

        $this->supplierPriceImportFileTableGateway = $this->sm->get('SupplierPriceImportFileTableGateway');
        $this->supplierPriceImportFileTableName = $this->supplierPriceImportFileTableGateway->getTable();
    }

    /**
     * Добавляем информацию о книгах для файла импорта.
     *
     * @param int $priceImportFileId ИД файла импорта
     * @param array $sheets Массив книг.
     * @return array
     */
    public function addWorksheets($priceImportFileId, array $sheets)
    {
        foreach ($sheets as $sheet) {
            $results = $this->importFileStructureTableGateway->select(array(
                'supplier_price_import_file_id' => $priceImportFileId,
                'book_name' => $sheet
            ));
            $fsRow = $results->current();
            if (!$fsRow) {
                $fsRow = $this->sm->get('ImportFileStructureRow');
                $fsRow->supplier_price_import_file_id = $priceImportFileId;
                $fsRow->book_name = $sheet;
                $fsRow->save();
                $this->importLogTable->addMessageToLog(
                    $priceImportFileId,
                    'Добавлена новая таблица ' . $sheet,
                    true,
                    true
                );
            }
        }
    }

    /**
     * Получает все сохраненные книги для файла импорта.
     *
     * @param int $priceImportId ИД файла импорта.
     * @return array
     */
    public function getWorksheets($priceImportId)
    {
        $results = $this->importFileStructureTableGateway->select(
            function ($select) use ($priceImportId) {
                $select->columns(array('id', 'book_name'))
                    ->where(array(
                        'supplier_price_import_id' => $priceImportId
                    ));
            }
        )->toArray();
        return array_column($results, 'book_name');
    }

    /**
     * Сохраняем заголовки в базе.
     *
     * @param $id
     * @param $headers
     */
    public function saveWorksheetHeaders($id, $headers, $headersRow = 0, $noHeaders = 0)
    {
        $importFileStructureTable = $this->importFileStructureTableGateway->select(array('id' => $id));
        $row = $importFileStructureTable->current();
        $newMatches = array();
        if (empty($row->fields_match)) {
            $row->fields_match = '[]';
        }
        $matches = (array)Json::decode($row->fields_match);
        foreach ($headers as $key=>$header) {
            $header = $noHeaders ? $header : $key;
            $newMatches[$header] = $matches[$header] ? $matches[$header] : null;
        }
        $row->fields_match = Json::encode($newMatches);
        $row->start_row = $headersRow;
        $row->no_headers = $noHeaders;
        $row->save();
    }

    /**
     * Сохраняем заголовки в базе.
     *
     * @param $id
     * @param $headers
     */
    public function saveNoUpload($id, $noUpload = 0)
    {
        $importFileStructureTable = $this->importFileStructureTableGateway->select(array('id' => $id));
        $row = $importFileStructureTable->current();
        $row->no_upload = $noUpload;
        $row->save();
    }

    /**
     * Получает информацию о структуре книги файла импорта поставщика.
     *
     * @param array $filter Фильтр.
     * @return array|\ArrayObject|null
     */
    public function getImportFileStructureInfo(array $filter)
    {
        $importFileStructureTable = $this->importFileStructureTableGateway->select($filter);
        return $importFileStructureTable->current();
    }

    /**
     * Получает информацию о структуре книги файла импорта поставщика по всем импортам.
     *
     * @param array $filter Фильтр.
     * @return array|\ArrayObject|null
     */
    public function getImportFileStructureAllInfo(array $filter)
    {
        $importFileStructureTable = $this->supplierPriceImportTableGateway->select($filter);
        return $importFileStructureTable->toArray();
    }

    /**
     * Обновляет структуру файла по ID
     *
     * @param $id int ID записи.
     * @param $fields string(json) дата.
     */
    public function updateImportFileStructure($id, $fields)
    {
        $ImportFileStructureTableGateway = $this->sm->get('ImportFileStructureTableGateway');
        /**
         * @var TableGateway $ImportFileStructureTableGateway
         */
        $ImportFileStructureTableGateway->update(
            array(
                'fields_match' => Json::encode($fields),
                'default_data' => Json::encode(array())
            ),
            array('id' => $id)
        );
    }

    /**
     * Добавляет информацию в базу данных о добавленном файле.
     *
     * @param $data
     */
    public function saveFileImportInfo($data)
    {
        $artistRow = false;
        if (!$this->supplierPriceImportTableGateway instanceof SupplierPriceImport && !empty($data['id'])) {
            $this->sm->features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id');
            $this->supplierPriceImportTableGateway = $this->sm->get('SupplierPriceImportTableGateway');
            $results = $this->supplierPriceImportTableGateway->select(array('id' => $data['id']));
            $artistRow = $results->current();
        }

        // Если описания к товару были пустые, то создаем пустой объект для сохранения (insert)
        if (!$artistRow) {
            $artistRow = $this->sm->get('SupplierPriceImportRow');
        }

        // Сохраняемые данные
        if (isset($data['supplierId'])){
            $artistRow->supplier_id = $data['supplierId'];
        }
        if (isset($data['importType'])) {
            $artistRow->import_type = $data['importType'];
        }
        if (isset($data['importInfo'])) {
            $artistRow->import_info = $data['importInfo'];
        }

        if (isset($data['isCron'])) {
            $artistRow->is_cron = $data['isCron'];
        }
        $artistRow->cron_periodicity = (empty($data['cronPeriodicity'])) ? '' : $data['cronPeriodicity'];
        $artistRow->date_import = (empty($data['date_import'])) ? (new \DateTime())->format(DateFormat::STANDARD) : $data['date_import'];

        $artistRow->save();
        return $artistRow->offsetGet('id');
    }

    public function getFileImportStructureByFileId($supplierPriceImportFileId)
    {
        $select = $this->importFileStructureTableGateway->getSql()->select()
        ;
    }

    /**
     * Информация о загруженным файлам по поставщику.
     *
     * @param $supplierPriceImportFileId
     * @return array
     */
    public function getFileImportStructureByFileIdInfo($supplierPriceImportFileId)
    {
        $select = $this->supplierPriceImportFileTableGateway->getSql()->select()
            ->columns(
                    array(
                        'id'
                    )
                )
            ->join(
                $this->supplierPriceImportTableName,
                $this->supplierPriceImportFileTableName . '.supplier_price_import_id = ' .
                $this->supplierPriceImportTableName . '.id',
                array(
                    'supplier_id',
                    'import_type',
                    'import_info'
                ),
                Select::JOIN_INNER
            )
            ->join(
                $this->importFileStructureTableName,
                $this->supplierPriceImportFileTableName . '.id = ' .
                $this->importFileStructureTableName . '.supplier_price_import_file_id',
                array(
                    'id',
                    'supplier_price_import_id',
                    'book_name',
                    'entity',
                    'fields_match',
                    'default_data',
                    'preparer_class',
                    'primary_field',
                    'start_row',
                    'no_headers',
                    'no_upload'
                ), Select::JOIN_LEFT
            )
            ->where(array($this->supplierPriceImportFileTableName . '.id = ?' => $supplierPriceImportFileId));

        $statement = $this->supplierPriceImportTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $config = array();
        foreach ($resultSet as $res) {
            $config[] = array(
                'worksheet' => $res['book_name'],
                'importFileStructureId' => $res['id'],
                'fieldsSaved' => !empty($res['fields_match']),
                'noHeaders' => !empty($res['no_headers']),
                'noUpload' => !empty($res['no_upload']),
                'headersRow' => $res['start_row']
            );
        }
        return $config;
    }

    /**
     * Информация о загруженным файлам по поставщику.
     *
     * @param $supplierPriceImportId
     * @return array
     */
    public function getFileImportStructureBySupplierImportIdInfo($supplierPriceImportId)
    {
        $select = $this->supplierPriceImportTableGateway->getSql()->select()
            ->columns(
                array(
                    'id',
                    'supplier_id',
                    'import_type',
                    'import_info'
                ))
            ->join(
                $this->importFileStructureTableName,
                $this->importFileStructureTableName . '.supplier_price_import_id = ' .
                $this->supplierPriceImportTableName . '.id',
                array(
                    'id',
                    'supplier_price_import_id',
                    'book_name',
                    'entity',
                    'fields_match',
                    'default_data',
                    'preparer_class',
                    'primary_field',
                    'start_row',
                    'no_headers'
                ), Select::JOIN_LEFT
            )
            ->where(array($this->supplierPriceImportTableName . '.id = ?' => $supplierPriceImportId));

        $statement = $this->supplierPriceImportTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $config = array();
        foreach ($resultSet as $res) {
            $config[] = array(
                'worksheet' => $res['book_name'],
                'importFileStructureId' => $res['id'],
                'fieldsSaved' => !empty($res['fields_match']),
                'noHeaders' => !empty($res['no_headers']),
                'headersRow' => $res['start_row']
            );
        }
        return $config;
    }

    /**
     * Обновляет структуру файла по id импорта.
     *
     * @param $supplierPriceImportFileId int id файла импорта.
     */
    public function removeImportFileStructure($supplierPriceImportFileId)
    {
        $this->importFileStructureTableGateway->delete(array('supplier_price_import_file_id' => $supplierPriceImportFileId));
    }

}