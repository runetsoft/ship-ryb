<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 11:52
 */

namespace Import\Factory;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Import\Service\RowPreparerInterface;
/**
 * Фабрика для классво вида {name}RowPreparer.
 * Class BaseRowPreparerFactory
 * @package Import\Factory
 */
class RowPreparerAbstractFactory implements AbstractFactoryInterface
{

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return preg_match('/RowPreparer$/', $requestedName);
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        if (!class_exists($requestedName)) {
            throw new \Exception('Класса ' + $requestedName + 'не существует!');
        }
        $service =  new $requestedName($serviceLocator);
        if (!$service instanceof RowPreparerInterface) {
            throw new \Exception('Класс ' + $requestedName + 'должен реализовывать RowPreparerInterface!');
        }
        return $service;
    }
}