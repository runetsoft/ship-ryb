<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 14:08
 */

namespace Import\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Import\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController($serviceLocator,
            $parentLocator->get('Import\Service\PriceParser'),
            $parentLocator->get('Import\Service\ImportService'),
            $parentLocator->get('Import\Model\SupplierPriceImportTable'),
            $parentLocator->get('Import\Model\ImportFileStructureTable'),
            $parentLocator->get('Import\Model\SupplierPriceImportFileTable')
        );
    }
}