<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 11:11
 */

namespace Import\Service;

use Application\Model\BaseModel;
use \Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use \Zend\Db\TableGateway\Feature\RowGatewayFeature;

/**
 * Базовый класс обработчик строк. Обрабатывает строки excel и сохраняет в базе.
 *
 * Class BaseRowPreparer
 * @package Import\Service
 */
class BaseRowPreparer implements RowPreparerInterface
{
    /**
     * Поле для сравнения поумолчанию.
     */
    const DEFAULT_PRIMARY = 'good';
    /**
     * Поле поставщик поумоляанию.
     */
    const DEFAULT_SUPPLIER_FIELD = 'provider_id';

    /**
     * Регулярка для обработки чисел с >
     */
    const NUM_MATCH = '/.*(?:>|более|больше)\s*([0-9\.,]+)\s*$/i';

    /**
     * Пустое поле.
     */
    const EMPTY_COLUMN = '_empty_';

    /**
     * @var ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var \Dictionary\Model\DictionaryTable
     */
    private $dictionaryService;

    /**
     * Имя таблицы для загрузки.
     *
     * @var BaseModel
     */
    protected $entityName;
    /**
     * ИД поставщика.
     *
     * @var int
     */
    protected $supplierId;

    /**
     * Класс таблицы для загрузки.
     *
     * @var BaseModel
     */
    protected $entity;

    /**
     * Обработчик файла импорта.
     *
     * @var ImportFileProcessor
     */
    protected $fileProcesor;

    /**
     * В какие поля таблицы сохранять колонки базы.
     * @var array
     */
    protected $fieldsMatch = array();

    /**
     * Поле по которому будет сравнение.
     *
     * @var string
     */
    protected $primaryField;

    /**
     * массив записанных элементов
     *
     * @var array
     */
    protected $curIds;

    /**
     * @var дата начала импорта
     */
    protected $startImportDate;
    
    /**
     * BaseRowPreparer constructor.
     * @param $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        // WordToNumberDictionary - модел справочника таблицы "cms_dictionary_word_to_number".
        $this->dictionaryService = $this->sm->get('WordToNumberDictionary');
        $this->curIds = [];
    }


    /**
     * Устанавливает конфигурацию.
     * @param array $importInfo
     * @param ImportFileProcessor $fileProcessor
     * @param $startImportDate дата начала импорта
     * @return mixed
     */
    public function configure(array $importInfo, $fileProcessor, $startImportDate)
    {
        $this->fileProcesor = $fileProcessor;
        $this->supplierId = $importInfo['supplier_id'];
        $this->setColumns($importInfo);
        $this->entityName = $importInfo['entity'] ? $importInfo['entity'] : 'Price';
        $this->setEntity();
        $this->primaryField = $importInfo['primary_field'] ? $importInfo['primary_field'] : self::DEFAULT_PRIMARY;
        $this->startImportDate = $startImportDate;
    }

    /**
     * Устанавливает конфигурацию колонок.
     *
     * @param array $importInfo массив с данными колонок
     * @throws \Exception
     */
    private function setColumns(array $importInfo)
    {
        $columnsInfo = $this->fileProcesor->getColumnsInfo($importInfo['book_name'], $importInfo['start_row'], $importInfo['no_headers']);
        $this->fieldsMatch = array();
        foreach ($importInfo['fields_match'] as $colName => $field) {
            if (self::EMPTY_COLUMN == $colName || empty($field)) {
                continue;
            }
            $key = (empty($columnsInfo[$colName])) ? $this->getFileColumnByMask($columnsInfo, "/$colName/") : $columnsInfo[$colName];
            if ($key == null) {
                throw new \Exception('Не найдено соответствие для колонки(маски) "' . $colName . '", надо заново Прочитать заголовки и Выгрузить данные');
            }
            $this->fieldsMatch[$key] = $field;
        }
    }

    /**
     * Функция возвращает букву колонки из файла, если есть совпадение в заголовках через регулярные выражения.
     *
     * @param $columnsInfo
     * @param $mask
     * @return null | string
     */
    private function getFileColumnByMask(array $columnsInfo, $mask)
    {
        foreach ($columnsInfo as $column => $fileColumn) {
            preg_match($mask, $column, $matches, PREG_OFFSET_CAPTURE);
            if (count($matches) > 0) {
                return $fileColumn;
            }
        }
        return null;
    }

    /**
     * Устанавливает объект БД в которую загружаются текущие данные.
     * @return mixed
     */
    private function setEntity()
    {
        $this->entity = $this->sm->get($this->entityName);
    }

    /**
     * Обработчик строки таблицы, на вход получает объект строки таблицы.
     *
     * @param \PHPExcel_Worksheet_Row $objRow
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function process(\PHPExcel_Worksheet_Row $objRow)
    {
        $arr = [];
        foreach ($objRow->getCellIterator() as $key => $objCell) {
            if ($this->fieldsMatch[$key]) {
                $arr[$this->fieldsMatch[$key]] = $this->calculateColumn($arr[$this->fieldsMatch[$key]], $objCell->getValue(), $this->fieldsMatch[$key]);
            }
        }
        $ids = $this->updateEntity($arr);
        $loadedGoods = @array_intersect($this->curIds, $ids);
        if ($loadedGoods) {
            foreach ($loadedGoods as $k => $v)
            {

                throw new \Exception('Товар уже загружался ('.implode(';',$arr).') id = ' . $v);
            }
        }
        $this->curIds = $ids;
    }

    /**
     * Возвращает массив буквенных обозначенийй только с теми колонками, которые будут обрабатываться.
     * Сделано для уменьшения потребления памяти.
     *
     * @return array Пример: array('A', 'C', 'E')
     */
    public function getColumnLetterKeys()
    {
        return array_keys($this->fieldsMatch);
    }

    /**
     * добавляет или обновляет записи в БД
     *
     * @param null $dbRow
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    private function updateData($dbRow = null, $data)
    {
        $date = new \DateTime();
        if (!$dbRow) {
            $dbRow              = $this->sm->get($this->entityName . 'Row');
            $dbRow->dt_price    = $date->format('Y-m-d H:i:s');
            $dbRow->provider_id = $this->supplierId;

            if (!$dbRow) {
                throw new \Exception('Сконфигурируйте сервис ' . $this->entityName . 'Row как объект RowGateway.');
            }
            $dbRow->dt_insert = $date->format('Y-m-d H:i:s');
        }
        else {
            if ($dbRow->price != $data['price']) {
                $dbRow->dt_price = $date->format('Y-m-d H:i:s');
            }
        }
        foreach ($data as $field => $value) {
                    $dbRow->$field = $value;
        }
        $dbRow->dt_update = $date->format('Y-m-d H:i:s');
        $dbRow->save();
        return $dbRow->id;
    }


    /**
     * обрабатывает строку из файла, сравнивая со значениями из базы
     *
     * @param $results
     * @param $data
     * @return array
     */
    private function strHandler($results = null, $data)
    {
        $unmatchedElements = array();
        $ids = [];
        $itNotEmpty = false;

        if ($results->current()) {
            while ($current = $results->current()) {

                $tmp_arr = $current->toArray();//массив текущих значений из бызы
                if (isset($data['zarticle']))
                {
                    if (isset($data['sarticle']))
                    {
                        switch ($data) {
                            case $data['zarticle'] == $tmp_arr['zarticle'] && $data['sarticle'] == $tmp_arr['sarticle']:
                                if (new \DateTime($tmp_arr['dt_update']) >= $this->startImportDate)
                                {
                                    $data['sklad'] += $tmp_arr['sklad'];
                                }
                                $itNotEmpty = false;
                                $ids[] = $this->updateData($current, $data);
                                break 2;
                            default:
                                $unmatchedElements = $current;
                                $itNotEmpty = true;
                                break;
                        }
                    }
                    else{
                        switch ($data) {
                            case $data['zarticle'] != $tmp_arr['zarticle'] && $tmp_arr['zarticle'] != null:
                                $unmatchedElements = null;
                                $itNotEmpty = true;
                                break;
                            case $data['zarticle'] != $tmp_arr['zarticle'] && $tmp_arr['zarticle'] == null:
                                $unmatchedElements = $current;
                                $itNotEmpty = true;
                                break;
                            case $data['zarticle'] == $tmp_arr['zarticle']:
                                if (new \DateTime($tmp_arr['dt_update']) >= $this->startImportDate)
                                {
                                    $data['sklad'] += $tmp_arr['sklad'];
                                }
                                $itNotEmpty = false;
                                $ids[] = $this->updateData($current, $data);
                                break 2;
                        }
                    }
                }
                else{
                    if (isset($data['sarticle']))
                    {
                        switch ($data) {
                            case $data['sarticle'] != $tmp_arr['sarticle'] && $tmp_arr['sarticle'] != null:
                                $unmatchedElements = null;
                                $itNotEmpty = true;
                                break;
                            case $data['sarticle'] != $tmp_arr['sarticle'] && $tmp_arr['sarticle'] == null:
                                $unmatchedElements = $current;
                                $itNotEmpty = true;
                                break;
                            case $data['sarticle'] == $tmp_arr['sarticle']:
                                if (new \DateTime($tmp_arr['dt_update']) >= $this->startImportDate)
                                {
                                    $data['sklad'] += $tmp_arr['sklad'];
                                }
                                $itNotEmpty = false;
                                $ids[] = $this->updateData($current, $data);
                                break 2;
                        }
                    }
                    else
                    {
                        if (new \DateTime($tmp_arr['dt_update']) >= $this->startImportDate)
                        {
                            $data['sklad'] += $tmp_arr['sklad'];
                        }
                        $ids[] = $this->updateData($current, $data);
                    }
                }
                $results->next();
            }
            if ($itNotEmpty == true)
            {
                $ids[] = $this->updateData($unmatchedElements, $data);
            }
        }
        else{
            $ids[] = $this->updateData(null, $data);
        }
        return $ids;
    }


    /**
     * проверка на добавление или изменение записи в таблице.
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    private function updateEntity(array $data)
    {

        $data['price'] = (int)$data['price'];
        $data[$this->primaryField] = trim($data[$this->primaryField]);
        if (!$this->entity instanceof TableGateway) {
            throw new \Exception('Не задан объект для загрузки!');
        }
        if (!$data[$this->primaryField]) {
            throw new \Exception('Уникальное имя не задано! ('.implode(';',$data).')');
        }

        $convertedString = str_replace('\\', '\\\\', $data[$this->primaryField]);
        $results = $this->entity
            ->select(
                function (Select $select) use($convertedString) {
                    $select->where->like($this->primaryField, $convertedString);
                    $select->where(array(self::DEFAULT_SUPPLIER_FIELD => $this->supplierId));
                });
        return $this->strHandler($results, $data);
    }

    /**
     * Обработка значений с сравнением.
     *
     * @param $value
     * @return mixed
     */
    private function prepareQuantity($value)
    {
        $params = array('filter' => array('%code' => $value));
        foreach ($this->dictionaryService->getList($params) as $item) {
            if (mb_strtolower($item['code'], 'UTF-8') == mb_strtolower($value, 'UTF-8')) {
                return $item['name'];
            }
        }
        preg_match_all(self::NUM_MATCH, $value, $matches, PREG_SET_ORDER, 0);
        list(, $match) = current($matches);
        return $match;
    }

    /**
     * Обработка значений Прайса.
     *
     * @param $value
     * @return mixed
     */
    private function preparePrice($value)
    {
        $value = str_replace(',', '.', $value);
        // Gets numbers and dots only.
        preg_match_all('/(\d+(\.\d+)*)/', $value, $matches);
        if (is_array($matches) && !empty($matches)) {
            return (int)implode("", current($matches));
        }
        return (int)$value;
    }

    /**
     * Рассчитывает колонки
     *
     * @param $field
     * @param $value
     * @param $key
     * @return int|string
     */
    private function calculateColumn($field, $value, $key)
    {
        switch ($key) {
            case 'good':
                $value = $value === null ? '' : $value;
                if (!$field)
                    return $value;
                return $field . ' ' . $value ;
            break;
            case 'sklad':
                $value = $value === null ? 0 : $value;
                if ($match = $this->prepareQuantity($value)){
                    $value = $match;
                }
                if (!$field)
                    return (int)$value;
                return (int)($field  + $value);
            break;
            case 'price':
            case 'rprice':
                return $this->preparePrice($value);
                break;
            default:
                return $value;
        }
    }
}