<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 24.05.2017
 * Time: 15:27
 */

namespace Import\Service;

/**
 * Interface FileUnpackerInterface]
 *
 * Интерфейс распаковки.
 *
 * @package Import\Service
 */
interface FileUnpackerInterface
{
    /**
     * Распаковывает файл.
     *
     * @param $fileSrc string
     *
     * @param $format string
     *
     * @param $target string
     *
     * @return mixed
     */
    function unpack($fileSrc, $format, $target);

}