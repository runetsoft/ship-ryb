<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 11:13
 */

namespace Import\Service;


interface RowPreparerInterface
{
    /**
     * Устанавливает конфигурацию необходимую для парсинга строки.
     * @param array $importInfo Массив информации о файле.
     * @param ImportFileProcessor $fileProcessor Процессор.
     * @param $startImportDate дата начала импорта
     * @return mixed
     */
    public function configure(array $importInfo, $fileProcessor, $startImportDate);

    /**
     * Обработчик строки таблицы, на вход получает объект строки таблицы.
     *
     * @param \PHPExcel_Worksheet_Row $worksheet
     * @return mixed
     */
    public function process(\PHPExcel_Worksheet_Row $worksheet);

    /**
     * Возвращает массив буквенных обозначенийй только с теми колонками, которые будут обрабатываться.
     * Сделано для уменьшения потребления памяти.
     *
     * @return array Пример array('A', 'C', 'E')
     */
    public function getColumnLetterKeys();
}