<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 24.05.2017
 * Time: 19:11
 */

namespace Import\Service;

use Supplier\Model\SupplierTable;
use Zend\ServiceManager\ServiceLocatorInterface;

class Cron
{

    /**
     * @var \Import\Model\SupplierPriceImportTable
     */
    protected $supplierPriceImportTable;

    /**
     * @var \Import\Model\supplierPriceImportFileTable
     */
    protected $supplierPriceImportFileTable;
    /**
     * @var SupplierTable
     */
    protected $supplierTable;

    /**
     * @var PriceParser
     */
    private $priceParser;

    /**
     * Cron constructor.
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->supplierPriceImportTable = $this->sm->get('Import\Model\SupplierPriceImportTable');
        $this->supplierPriceImportFileTable = $this->sm->get('Import\Model\SupplierPriceImportFileTable');
        $this->priceParser = $this->sm->get('Import\Service\PriceParser');
        $this->supplierTable = $this->sm->get('Supplier\Model\SupplierTable');
    }

    /**
     * Берет все импорты у которых установлен импорт по крону, и импортирует те которые нужно (по периодам).
     */
    public function run()
    {
        $suppliers = $this->supplierPriceImportFileTable->getAllImportSuppliers(array('is_cron' => 1, 'status' => 1));
        $curDate = new \DateTime();

        foreach ($suppliers as $key=>$supplier) {
            if ($supplier['oldestInterval']) {
                $supplierObj = $this->supplierTable->fetchOne($key);
                $start = new \DateTimeImmutable($supplier['oldestDate']);
                $datetime = $start->modify('+' . $supplier['oldestInterval']);
                if (!$supplierObj->run_import && $datetime->getTimestamp() < $curDate->getTimestamp()) {
                    $supplierObj->run_import = 1;
                    $this->sm->get('Logger')->crit('run_import => ' . $key);
                    $supplierObj->save();
                        $this->priceParser->importAllSupplierPrices($key);
                    $supplierObj->run_import = 0;
                    $this->sm->get('Logger')->crit('end_run_import => ' . $key);
                    $supplierObj->save();
                }
            }
        }
    }

    public function import()
    {
        $iterator = $this->supplierPriceImportTable->getSupplierImportInfo(array('is_cron' => 1));
        $curDate = new \DateTime();
        while ($current = $iterator->current()) {
            $runDate = new \DateTime($current->date_import);
            if ($curDate->getTimestamp() - $runDate->getTimestamp() > $current->cron_periodicity) {
                $this->priceParser->loadFileImport(array('id' => $current->id));
            }
            $iterator->next();
        }
    }

}