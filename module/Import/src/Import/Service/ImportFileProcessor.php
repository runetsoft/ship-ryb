<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 24.04.2017
 * Time: 14:30
 *
 */

namespace Import\Service;

use Import\Model\ImportFileStructureTable;
use \Import\Model\SupplierPriceImport;
use Application\Service\ChunkReadFilter;
use Zend\Db\RowGateway\RowGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Utilities\ArchiveFormat;

/**
 * Class ImportFileProcessor
 * @package Import\Service
 *
 * Класс для работы с  файлами импрорта (получение, сохранение, чтение служебных данных).
 */
class ImportFileProcessor
{
    /**
     * Нулевая строка.
     */
    const ZERO_ROW = 0;

    /**
     * Строка с заголовками.
     */
    const HEADERS_ROW = 1;

    private $baseDir = null;

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $sm;

    /**
     * Сервис менеджер.
     *
     * @var \Import\Model\SupplierPriceImport
     */
    private $supplierPriceImportFile;

    /**
     * Модель таблицы данных по книгам импорта.
     *
     * @var \Import\Model\ImportFileStructureTable
     */
    private $importFileStructureTable;

    /**
     * Сервис менеджер.
     *
     * @var Loader
     */
    private $fileLoader;

    /**
     * Имя файла.
     *
     * @var string
     */
    private $filename;

    /**
     * Ридер Excel.
     *
     * @var \PHPExcel_Reader_IReader
     */
    private $excelReader;

    /**
     * Кодировка файла.
     *
     * @var string
     */
    private $encoding;

    /**
     * Разделитель.
     *
     * @var string
     */
    private $delimiter = ';';

    /**
     * ImportFileProcessor constructor.
     * @param Loader $fileLoader объект таблицы данных файла импорта.
     * @param RowGateway $supplierPriceImportFile объект таблицы данных файла импорта.
     * @param ServiceLocatorInterface $sm .
     * @param ImportFileStructureTable $importFileStructureTable .
     * @param bool $loadFile Загружать ли файл, если не загружен.
     *
     * @throws \Exception
     */
    public function __construct(
        RowGateway $supplierPriceImportFile,
        ServiceLocatorInterface $sm,
        Loader $fileLoader,
        $importFileStructureTable = null
    )
    {
        if (!$supplierPriceImportFile instanceof RowGateway) {
            throw new \Exception('Некорректная запись таблицы импорта!');
        }
        $this->sm = $sm;
        $this->importFileStructureTable = $importFileStructureTable ? importFileStructureTable : $this->sm->get
        ('Import\Model\ImportFileStructureTable');
        $this->supplierPriceImportFile = $supplierPriceImportFile;
        $this->fileLoader = $fileLoader;
        $this->filename = $this->getImportFileName();
        $this->setReader();
    }

    public function getImportFileName()
    {
        return $this->fileLoader->getFileName($this->supplierPriceImportFile);
    }

    private function setReader()
    {
        $t = file_get_contents($this->filename, null, null, null, 8);
        $this->encoding = $this->supplierPriceImportFile->encoding ? $this->supplierPriceImportFile->encoding :
            mb_detect_encoding($t);
        $inputFileType = \PHPExcel_IOFactory::identify($this->filename);
        $this->excelReader = \PHPExcel_IOFactory::createReader($inputFileType);
    }

    /**
     * Удаляет импортированный файл вместе с директорией.
     * Удалить текущий фал импорта.
     */
    public function removeFile()
    {
        try {
            $dir = dirname($this->getImportFileName());
            unlink($this->getImportFileName());
            rmdir($dir);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Сохраняем данные о книгах файла.
     *
     */
    public function saveFileWorksheetsInfo()
    {
        $sheets = $this->getExcelWorksheets();
        $this->importFileStructureTable->addWorksheets($this->supplierPriceImportFile->id, $sheets);
    }

    /**
     * Получаем данные о книгах файла.
     *
     */
    public function getFileWorksheetsInfo()
    {
        return $this->importFileStructureTable->getWorksheets($this->supplierPriceImportFile->supplier_id);
    }

    /**
     * Получить текущий загрузчик.
     *
     * @return array|\Import\Service\FileLoaderInterface|object
     * @throws \Exception
     */
    public function getFileLoader()
    {
        if ($this->fileLoader instanceof Loader) {
            return $this->fileLoader;
        }

        switch ($this->supplierPriceImportFile->import_type) {
            case self::LOAD_TYPE_URL:
                $this->fileLoader = $this->sm->get('HttpLoader');
                break;
            case self::LOAD_TYPE_MESSAGE:
                $this->fileLoader = $this->sm->get('GoogleMailLoader');
                break;
            default:
                throw new \Exception('загрузчик для данного типа отсутствует');
        }

        return $this->fileLoader;
    }

    /**
     * Возвращает ридер excel.
     *
     * @return \PHPExcel_Reader_IReader
     */
    public function getExcelReader()
    {
        return $this->excelReader;
    }

    /**
     * Устанавливает параметры чтения (PHPExcel_Reader_IReader) для текщего файла.
     *
     * @param integer $startRow Начальная строка.
     * @param integer $chunk Количество считываемых строк.
     * @param array $columns Коллонки.
     * @param bool $readMode Только на чтение.
     *
     * @return boolean
     */
    public function setPhpExcelReader($startRow, $chunk, $columns = null, $readMode = true)
    {
        try {
            $chunkFilter = new ChunkReadFilter();
            $chunkFilter->setRows($startRow, $chunk, $columns);
            $this->excelReader->setReadFilter($chunkFilter);
            if ($this->excelReader instanceof \PHPExcel_Reader_CSV) {
                $this->excelReader->setDelimiter($this->delimiter);
                $this->excelReader->setEnclosure('');
                $this->excelReader->setInputEncoding($this->encoding);
            }
            if ($readMode) {
                $this->excelReader->setReadDataOnly(true);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Возвращает объект таблицы для текущего файла с заданным PHPExcel_Reader.
     *
     * @return bool|\PHPExcel
     */
    public function getPHPExcelObject()
    {
        try {
            return $this->excelReader->load($this->getImportFileName());
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Получает массив с именами книг текущего файла импорта.
     *
     * @return array
     */
    public function getExcelWorksheets()
    {
        $this->setPhpExcelReader(self::ZERO_ROW, self::ZERO_ROW);
        $objPHPExcel = $this->getPHPExcelObject();
        $list = array();
        foreach ($objPHPExcel->getSheetNames() as $worksheet) {
            $list[] = $worksheet;
        }
        unset($objPHPExcel);
        return $list;
    }

    /**
     * Возвращает шапку таблицы.
     *
     * @param string $sheetName Имя книги.
     * @param int $sheetRow Строка в которой находятся заголовки.
     *
     * @return array
     */
    public function getColumnsInfo($sheetName, $sheetRow = null, $no_headers = false)
    {
        $sheetRow = $sheetRow ? $sheetRow : self::HEADERS_ROW;
        $this->setPhpExcelReader($sheetRow, $sheetRow);
        $objPHPExcel = $this->getPHPExcelObject();
        $objPHPExcel->setActiveSheetIndexByName($sheetName);
        $sheet = $objPHPExcel->getActiveSheet();
        $iterator = $sheet->getColumnIterator();
        $list = array();
        foreach ($iterator as $key => $item) {
            $index = $item->getColumnIndex();
            $value = $no_headers ? $key : $sheet->getCell($index . $sheetRow)->getValue();
            $list[trim($value)] = $index;
        }

        unset($objPHPExcel);
        return $list;
    }

    /**
     * Добавить сообщение об ошибках в файле.
     *
     * @param $message
     */
    public function addMessageToLogByFile($message)
    {
        $log = $this->sm->get('Import\Model\ImportLogTable');
        $log->addMessageToLog(
            $this->supplierPriceImportFile->id,
            $message,
            false,
            false
        );
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        // TODO Delimiter hardcoded.
        if (in_array($delimiter, [',', ';'])) {
            $this->delimiter = $delimiter;
        }
    }

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }


}