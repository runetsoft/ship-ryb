<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 22:05
 */

namespace Import\Service;

use Import\Model\SupplierPriceImportFileTable;
use Zend\Db\RowGateway\RowGateway;

/**
 * Interface FileLoaderInterface
 *
 * Интерфейс загрузчиков.
 *
 * @package Import\Service
 */
interface FileLoaderInterface
{
    /**
     * Конфигурация сервиса.
     *
     * @param RowGateway $supplierPriceImport
     * @param SupplierPriceImportFileTable $fileTable
     *
     * @return mixed
     */
    public function configure(RowGateway $supplierPriceImport, SupplierPriceImportFileTable $fileTable);

    /**
     * Загружает все файлы.
     *
     * @return mixed
     */
    public function loadAllFiles();
}