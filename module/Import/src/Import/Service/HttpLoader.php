<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 22:06
 */

namespace Import\Service;

use Application\Utilities\ArchiveFormat;
use Import\Model\SupplierPriceImport;
use Import\Model\SupplierPriceImportFileTable;
use Zend\Db\RowGateway\RowGateway;


/**
 * Class HttpLoader
 * Загрузкчик файлов с http.
 *
 * @package Import\Service
 */
class HttpLoader extends Loader
{
    /**
     * @var SupplierPriceImport;
     */
    public $supplierPriceImport;

    /**
     * установка ftp соеденения и запись файла на сервер
     *
     * @param $connection
     * @return string
     * @throws \Exception
     */
    private function loadFromFtp($connection)
    {
        $fileSrc = $this->getTmpDir(). self::FILE_NAME;
        $matches = parse_url($connection);
        $conn_id = ftp_connect($matches['host']);
        $realPath = urldecode($matches['path']);

        $login_result = ftp_login($conn_id, $matches['user']?:"anonymous", $matches['pass']?:"");

        if (ftp_get($conn_id, $fileSrc, $realPath, FTP_BINARY)) {
            ftp_close($conn_id);
            return $fileSrc;

        } else {
            throw new \Exception('Не удалось произвести запись, попробуйте еще раз');
        }
    }

    /**
     * установка http соеденения и запись файла на сервер
     *
     * @param $connection
     * @return string
     * @throws \Exception
     */
    private function loadFromHttp($connection)
    {
        $urlHandle = @fopen($connection, "rb");
        if (!$urlHandle) {
            throw new \Exception('Неправильно задан путь файла (' . $this->supplierPriceImport->import_info . ')', '404');
        }
        $fileSrc = $this->getTmpDir(). self::FILE_NAME;
        $dirHandle = fopen($fileSrc, "wb");
        stream_copy_to_stream($urlHandle, $dirHandle);
        fclose($dirHandle);
        fclose($urlHandle);
        return $fileSrc;
    }

    /**
     * проверка протокола, типа загруженного файла и распаковка если это архив
     *
     * @return array|string
     * @throws \Exception
     */
    protected function load()
    {
        if (preg_match ('/(http.*)/' , $this->supplierPriceImport->import_info))
        {
            $fileSrc = $this->loadFromHttp($this->supplierPriceImport->import_info);
        }
        elseif(preg_match ('/(ftp.*)/' , $this->supplierPriceImport->import_info))
        {
            $fileSrc = $this->loadFromFtp($this->supplierPriceImport->import_info);
        }
        else
        {
            throw new \Exception('Некоректный протокол передачи данных используйте ftp или http соеденение');
        }

        $this->loadedFiles = array(self::FILE_NAME);
        $type = mime_content_type($fileSrc);
        // Если файл архив, то распаковываем его.
        $archiveFormat = new ArchiveFormat();
        $archives = $archiveFormat->getConstants();
        if (in_array($type, array_values($archives))) {
            $this->loadedFiles = $this->unpack($fileSrc, array_search($type, $archives));
        }
        return $this->loadedFiles;
    }

    /**
     * @return mixed
     */
    public function loadAllFiles()
    {
        return parent::loadAllFiles();
    }
}