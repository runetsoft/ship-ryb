<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 18.04.2017
 * Time: 12:44
 */

namespace Import\Service;

use Application\Service\ChunkReadFilter;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Класс обработчик прайса общий. Готовит прайс к обрабтке, получает его, создает нужные парсеры запускает процесс.
 *
 * Class BaseParser
 * @package Price\Service
 */
class BaseParser
{
    /**
     * Размер считываемого фрагмента файла.
     */
    const CHUNK_SIZE = 1000;
    /**
     *  Стортова строка для парсинга.
     */
    const START_ROW = 2;
    /**
     * Максимальное количество строк.
     */
    const MAX_ROW = 999999;
    /**
     * Базовый класс обработки строк.
     */
    const BASE_PREPARER_CLASS = 'BaseRowPreparer';

    /**
     * @var ServiceLocatorInterface
     */
    protected $sm;

    /**
     * Ридер файла.
     *
     * @var \Import\Service\ImportFileProcessor
     */
    protected $fileProcessor;

    /**
     * Ридер Excel.
     *
     * @var \PHPExcel_Reader_IReader
     */
    protected $excelReader;

    /**
     *
     *
     * @var RowPreparer
     */
    protected $rowPreparer;

    /**
     * @var array Вся информация об импортируемом файле.
     */
    protected $importInfo;


    /**
     * @var дата начала импорта
     */
    protected $startImportDate;

    /**
     * @var
     */
    protected $pricesImported = false;

    /**
     * Запускаем процесс парсинга файла.
     */
    public function parseImportFile()
    {
        $this->excelReader = $this->fileProcessor->getExcelReader();
        $logInfo = array();
        foreach ($this->importInfo as $bookInfo) {
            if (is_array($bookInfo['fields_match']) && count($bookInfo['fields_match']) > 0) {
                try {
                    $this->parseImportWorksheet($bookInfo);
                    $this->pricesImported = true;
                } catch (\Exception $e) {
                    $logInfo[] = $e->getMessage();
                }
            } else if (empty($bookInfo['no_headers']) && empty($bookInfo['fields_match']) && empty($bookInfo['start_row'])) {
                $logInfo[] = "Отсутствуют настройки для книги '" . $bookInfo['book_name'] . "'";
            }
        }
        if (!empty($logInfo)) {
            throw new \Exception(implode("<br>", $logInfo));
        }
    }

    /**
     * Парсит книгу;
     *
     * @param $bookInfo
     */
    public function parseImportWorksheet($bookInfo)
    {
        $preparerClass = ($bookInfo['preparer_class']) ? $bookInfo['preparer_class'] : self::BASE_PREPARER_CLASS;
        /**
         * @var RowPreparerInterface $preparer
         */
        $this->rowPreparer = $this->sm->get($preparerClass);
        $this->rowPreparer->configure($bookInfo, $this->fileProcessor, $this->startImportDate);
        $startRow = $bookInfo['start_row'] ? $bookInfo['start_row'] + 1 : self::START_ROW;
        while ($this->parseChunk($startRow, $bookInfo['book_name']) && $startRow <= self::MAX_ROW) {
            $startRow += self::CHUNK_SIZE;
        }
    }

    /**
     * Обрабатывает набор строк заданного размера начиная со стартовой.
     *
     * @param int $startRow стратовая строка
     * @param string $bookName имя книги
     *
     * @return bool
     */
    public function parseChunk($startRow, $bookName)
    {
        $this->fileProcessor->setPhpExcelReader($startRow, self::CHUNK_SIZE, $this->rowPreparer->getColumnLetterKeys());
        $this->excelReader = $this->fileProcessor->getExcelReader();
        $objPHPExcel = $this->excelReader->load($this->fileProcessor->getImportFileName());
        ini_set('precision', 14);
        $objPHPExcel->setActiveSheetIndexByName($bookName);
        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator($startRow);
        $counter = $startRow;

        foreach ($rowIterator as $objRow) {
            try {
                $this->rowPreparer->process($objRow);
            } catch (\Exception $e) {
                $this->sm->get('Logger')->crit('Exception from  parseChunk => ' . $e->getMessage());
                $this->fileProcessor->addMessageToLogByFile($e->getMessage());
            }
            $counter++;
        }
        unset($objPHPExcel);
        return $counter < self::CHUNK_SIZE + $startRow ? false : true;
    }
}