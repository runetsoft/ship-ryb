<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 24.05.2017
 * Time: 15:22
 */

namespace Import\Service;

use Application\Utilities\Encoding;
use Ddeboer\Imap\Exception\Exception;
use Dictionary\Service\EncodingDictionary;
use Import\Model\SupplierPriceImportFileTable;
use Zend\Db\RowGateway\RowGateway;
use Zend\Filter\Decompress;

/**
 * Class Loader
 *
 * @package Import\Service
 */
abstract class Loader implements FileLoaderInterface, FileUnpackerInterface
{

    /**
     * Директория для хранения файлов.
     */
    const BASE_DIR = 'data/cache/import/';

    /**
     * Директория для временных файлов.
     */
    const TMP_DIR = 'tmp/';

    /**
     * Шаблон пути до файла.
     * supplierId - id поставщика
     * source - откуда загружен
     */
    const FILE_DIR_TPL = '{importId}/{fileId}/';
    /**
     * Название файла.
     */
    const FILE_NAME = 'import';

    /**
     * Файлы, сохраненные ранее.
     *
     * @var array
     */
    protected $savedFiles;

    /**
     * Файлы, полученные текущим загрузчиком.
     *
     * @var array
     */
    protected $loadedFiles = array();

    /**
     * @var RowGateway
     */
    protected $supplierPriceImport;
    /**
     * @var SupplierPriceImportFileTable
     */
    protected $fileTable;

    public function configure(RowGateway $supplierPriceImport, SupplierPriceImportFileTable $fileTable)
    {
        $this->supplierPriceImport = $supplierPriceImport;
        $this->fileTable = $fileTable;
        $resultSet = $fileTable->getAllInfo(array(
            'supplier_price_import_id' => $supplierPriceImport->id
        ));
        $this->savedFiles = array();
        while ($resultSet->valid()) {
            $this->savedFiles[] = $resultSet->current();
            $resultSet->next();
        }
    }

    public function __construct()
    {
        $this->baseDir = dirname(__FILE__) . '/../../../../../' . self::BASE_DIR;
    }

    /**
     * Получает директорию для временных файлов импорта.
     *
     * @return string
     */
    protected function getTmpDir()
    {

        $path = $this->baseDir . self::TMP_DIR . $this->supplierPriceImport->id . '/';
        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }
        return $path;
    }

    /**
     * Получает имя для постоянного файла импорта.
     *
     * @param RowGateway $fileRow
     *
     * @throws \Exception
     *
     * @return string
     */
    public function getFileName(RowGateway $fileRow)
    {
        if (!$fileRow instanceof RowGateway) {
            throw  new \Exception('Некоректная строка');
        }
        $fileDirName = str_replace(
            array('{importId}', '{fileId}'),
            array(
                '{importId}' => $fileRow->supplier_price_import_id,
                '{fileId}' => $fileRow->id
            ),
            self::FILE_DIR_TPL
        );

        if (!file_exists($this->baseDir . $fileDirName)) {
            mkdir($this->baseDir . $fileDirName, 0775, true);
        }
        return $this->baseDir . $fileDirName . self::FILE_NAME;
    }


    /**
     * Копируем файл из временной директории в постоянную.
     *
     * @param $filename
     * @param $row
     * @return  mixed
     */
    private function copyFile($filename, $row)
    {
        $fileTmpSrc = $this->getTmpDir() . $filename;
        $fileCopySrc = $this->getFileName($row);
        $fileTmpSrcHandle = fopen($fileTmpSrc, "rb");
        $fileCopyHandle = fopen($fileCopySrc, "wb");
        stream_copy_to_stream($fileTmpSrcHandle, $fileCopyHandle);
        fclose($fileCopyHandle);
        fclose($fileTmpSrcHandle);
        unlink($fileTmpSrc);
        return $row;
    }

    /**
     * Получает объект файла если такого файла в базе еще не было, то создается новый.
     *
     * @param $filename
     * @return array|mixed|object
     */
    private function getFileRow($filename)
    {
        /*
         * Так как название файла задает поставщик, и непонятно в какой кодировке будет, делаем md5 в тех случах,
         * если не известная кодировка. Из админки нельзя менять маску, но будет работать правильно.
         */
        if (!mb_detect_encoding($filename)) {
            $filename = md5($filename);
            // TODO это временно, надо будет удалить.
            file_put_contents("tmp.txt", 'Неизвестная Кодировка! supplier_price_import_id: ' . current($this->savedFiles)->supplier_price_import_id . "\n", FILE_APPEND | LOCK_EX);
        }
        foreach ($this->savedFiles as $savedFile) {
            if (!empty($savedFile) && !empty(trim($savedFile->filename_mask))) {
                $re = '/' . $savedFile->filename_mask . '/iu';
                preg_match_all($re, $filename, $matches, PREG_SET_ORDER, 0);
                if (count($matches) > 0) {
                    return $savedFile;
                }
            }
        }
        $row = $this->fileTable->getRow();
        $row->filename_mask = addslashes($filename);
        $row->encoding = Encoding::UTF_8;
        $row->supplier_price_import_id = $this->supplierPriceImport->id;
        $row->save();
        return $row;
    }

    /**
     * Сохраняем инфо по всем загруженным файлам.
     *
     */
    protected function saveFilesInfo()
    {
        $newFiles = array();
        foreach ($this->loadedFiles as $loadedFile) {
            $newFiles[] = $this->copyFile($loadedFile, $this->getFileRow($loadedFile));
        }
        $this->loadedFiles = $newFiles;
    }

    /**
     * @param $fileSrc string
     *
     * @param $format string
     *
     * @param $target string
     *
     * @return string
     */
    public function unpack($fileSrc, $format, $target = '')
    {
        if (empty($target)) {
            $target = pathinfo($fileSrc, PATHINFO_DIRNAME);
        }
        $filter = new Decompress(array(
            'adapter' => $format,
            'options' => array(
                'target' => $target
            )
        ));
        $decompressed = $filter->filter($fileSrc);
        unlink($fileSrc);
        $files = [];
        foreach (array_diff(scandir($decompressed, 1), array('..', '.')) as $file) {
            $files[] = $file;
        }
        return $files;
    }

    /**
     * Загружает все файлы.
     *
     * @return mixed
     */
    public function loadAllFiles()
    {
        $this->load();
        $this->saveFilesInfo();
        return $this->loadedFiles;
    }

    public function remove($importId, $importFileId)
    {
        $filePath = self::BASE_DIR . "$importId/$importFileId/" . self::FILE_NAME;
        if (!file_exists($filePath)) {
            return false;
        }
        if (!unlink($filePath)) {
            throw new \Exception("Не удалос удлаить файл <br>Неправильный путь файла: '$filePath'");
        }
        rmdir(self::BASE_DIR . "$importId/$importFileId");
    }
}