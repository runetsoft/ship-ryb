<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 22:06
 */

namespace Import\Service;

use Application\Utilities\ArchiveFormat;
use Application\Utilities\DateFormat;
use Ddeboer\Imap\Exception\Exception;
use Ddeboer\Imap\Search\Date\After;
use Ddeboer\Imap\Search\Flag\Unseen;
use Import\Model\SupplierPriceImport;
use Zend\Db\RowGateway\RowGateway;
use Ddeboer\Imap\Server;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Search\Email\FromAddress;
use Ddeboer\Imap\MessageIterator;

/**
 * Class GoogleMailLoader
 * Загрузкчик файлов с http.
 *
 * @package Import\Service
 */
class GoogleMailLoader extends Loader
{
    /**
     * Почтовый сервер.
     */
    const IMAP_SERVER = 'imap.gmail.com';

    /**
     * Порт
     */
    const IMAP_PORT = '993';

    /**
     * SSL
     */
    const IMAP_SSL = '/imap/ssl/novalidate-cert';

    /**
     * Почтовый ящик откуда забирать письма.
     */
    const MAIL_ADDRESS = 'stock@ship-ship.ru';

    /**
     *  Пароль от почты.
     */
    const MAIL_PASSWORD = 'toil-align1';

    /**
     * С какого ящика почты берутся письма.
     */
    const MAILBOX = 'INBOX';

    /**
     * Код ошибки письмо не получено.
     */
    const ERROR_MAIL_NOT_RECEIVED_CODE = 2;

    /**
     * @var SupplierPriceImport;
     */
    public $supplierPriceImport;

    public function load()
    {
        $server = new Server(
            self::IMAP_SERVER,
            self::IMAP_PORT,
            self::IMAP_SSL
        );
        $connection = $server->authenticate(self::MAIL_ADDRESS, self::MAIL_PASSWORD);
        $mailbox = $connection->getMailbox(self::MAILBOX);
        $emails = explode(',', str_replace(' ', '', $this->supplierPriceImport->import_info));

        $emailReceived = false;
        foreach ($emails as $email) {
            if ($emailReceived) {
                break;
            }
            $search = new SearchExpression();

            $search->addCondition(new FromAddress($email));
            if ($this->supplierPriceImport->date_import) {
                $dateTime = new \DateTime($this->supplierPriceImport->date_import);
                $search->addCondition(new After($dateTime));
            }
            $messages = $mailbox->getMessages($search);

            $fileSrc = $this->getTmpDir() . self::FILE_NAME;

            $this->loadedFiles = array();
            while ($messages->valid()) {
                $emailReceived = true;
                // Загрузите только последнее письмо.
                if ($messages->key() < $messages->count() - 1) {
                    $messages->next();
                    continue;
                }
                $message = $messages->current();
                // Если это импорт по крону и нет новых писем.
                if ($this->supplierPriceImport->cron_periodicity && !empty($dateTime) && $dateTime > $message->getDate()) {
                    $messages->next();
                    $emailReceived = false;
                    continue;
                }

                $attachments = $message->getAttachments();
                foreach ($attachments as $attachment) {
                    if ($attachment->getType() == 'application') {
                        $fileSrc = $this->getTmpDir() . $attachment->getFilename();
                        $dirHandle = fopen($fileSrc, "wb");
                        fwrite($dirHandle, $attachment->getDecodedContent());
                        fclose($dirHandle);
                        $files = array($attachment->getFilename());
                        $type = mime_content_type($fileSrc);
                        // Если файл архив, то распаковываем его.
                        $archiveFormat = new ArchiveFormat();
                        $archives = $archiveFormat->getConstants();
                        if (in_array($type, array_values($archives))) {
                            $files = $this->unpack($fileSrc, array_search($type, $archives));
                        }
                        $this->loadedFiles = array_merge($this->loadedFiles, $files);
                    }
                }
                $messages->next();
            }
        }
        if (!$emailReceived && $this->supplierPriceImport->import_type == 2) {
            throw new Exception('Нет письма с этого ящика (' . $this->supplierPriceImport->import_info . ')', self::ERROR_MAIL_NOT_RECEIVED_CODE);
        }
        return $this->loadedFiles;
    }

    /**
     * @return mixed
     */
    public function loadAllFiles()
    {
        return parent::loadAllFiles();
    }
}