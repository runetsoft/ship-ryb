<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 18.04.2017
 * Time: 12:44
 */

namespace Import\Service;

use Application\Utilities\DateFormat;
use Ddeboer\Imap\Exception\Exception;
use Export\Utilities\ExportType;
use Zend\Db\RowGateway\RowGateway;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Класс обработчик прайса общий. Готовит прайс к обрабтке, получает его, создает нужные парсеры запускает процесс.
 *
 * Class PriceParser
 * @package Import\Service
 */
class PriceParser extends BaseParser
{

    /**
     * Код ошибки письмо не получено.
     */
    const ERROR_MAIL_NOT_RECEIVED_CODE = 2;

    /**
     * @var \Import\Model\SupplierPriceImportTable
     */
    protected $supplierPriceImportTable;

    /**
     * @var \Import\Model\SupplierPriceImportFileTable
     */
    protected $supplierPriceImportFileTable;

 /**
     * @var \Import\Model\ImportFileStructureTable
     */
    protected $importFileStructureTable;

    /**
     * @var RowGateway
     */
    protected $priceImport;
    /**
     * @var \Import\Model\ImportLogTable
     */
    protected $importLogTable;

    /**
     * @var Loader
     */
    protected $fileLoader;

    /**
     * @var
     */
    protected $supplierId;

    /**
     * PriceParser constructor.
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->supplierPriceImportTable = $this->sm->get('Import\Model\SupplierPriceImportTable');
        $this->supplierPriceImportFileTable = $this->sm->get('Import\Model\SupplierPriceImportFileTable');
        $this->importFileStructureTable = $this->sm->get('Import\Model\ImportFileStructureTable');
        $this->importLogTable = $this->sm->get('Import\Model\ImportLogTable');
    }

    /**
     * Импорт прайса из одного файла.
     *
     * @param RowGateway $supplierPriceImportFileRow
     *
     * @return bool
     */
    public function importPrice(RowGateway $supplierPriceImportFileRow)
    {
        set_time_limit(0);
        ini_set('mysql.connect_timeout','0');
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '0');
        ini_set('memory_limit', '-1');
        $this->importLogTable->addMessageToLog(
            $supplierPriceImportFileRow->id,
            'Старт импорта!',
            true,
            false,
            true
        );
        $this->startImportDate = new \DateTime();
        try {
            $supplierPriceImportRow = $this->supplierPriceImportTable->getOneRow($supplierPriceImportFileRow->supplier_price_import_id);
            $this->supplierId = $supplierPriceImportRow->supplier_id;
            $fileLoader = $this->getFileLoaderBySupplierPriceImportFile($supplierPriceImportFileRow);

            /**
             * @var ImportFileProcessor $fileProcessor
             */
            $this->fileProcessor = new ImportFileProcessor(
                $supplierPriceImportFileRow,
                $this->sm,
                $fileLoader
            );
            $this->fileProcessor->setDelimiter($supplierPriceImportFileRow->delimiter);
            $this->importInfo = $this->supplierPriceImportFileTable->getDataForImport($supplierPriceImportFileRow->id);
            if (empty($this->importInfo)) {
                throw new \Exception('В настройках файла включена галочка "не загружать".');
            }
            $this->parseImportFile();
            $priceImportupdate = $this->supplierPriceImportTable->getOneRow($this->priceImport->id);
            $priceImportupdate->date_import = $this->startImportDate->format(DateFormat::STANDARD);
            $priceImportupdate->save();
        } catch (\Exception $e) {
            $this->importLogTable->addMessageToLog(
                $supplierPriceImportFileRow->id,
                $e->getMessage(),
                false,
                false
            );
            $priceImportupdate = $this->supplierPriceImportTable->getOneRow($this->priceImport->id);
            $priceImportupdate->date_import = $this->startImportDate->format(DateFormat::STANDARD);
            $priceImportupdate->save();
        }
        return $this->pricesImported;
    }
    /**
     * Импорт всех прайсов поставщика.
     */
    public function importAllSupplierPrices($supplierId) {
        $this->supplierId = $supplierId;
        $supplierPriceImportRows = $this->supplierPriceImportTable->getSupplierImport($supplierId);
        if (!$supplierPriceImportRows) {
            throw new \Exception('Нет импортированных файлов!');
        }
        $curDate = new \DateTime();
        $this->pricesImported = false;
        foreach ($supplierPriceImportRows as $supplierPriceImportRow) {
            try {
                $this->sm->get('Logger')->crit('Load files from => (' .$supplierPriceImportRow->import_info. ')');
                $this->loadFilesImport(array('id' => $supplierPriceImportRow->id));
            } catch (\Exception $e) {
                if ($e->getCode() == self::ERROR_MAIL_NOT_RECEIVED_CODE) {
                    $this->importLogTable->addMessageToLog(
                        $supplierId,
                        $e->getMessage(),
                        false,
                        false,
                        true
                    );
                    $this->supplierPriceImportTable->itemUpdate(array(
                        'id' => $supplierPriceImportRow->id,
                        'date_import' => (new \DateTime())->format(DateFormat::STANDARD)
                    ));
                }
                echo $e->getMessage();
                $this->sm->get('Logger')->crit('Load Exception => (' .$e->getMessage(). ')');
            }
        }
        if ($this->pricesImported) {
            $this->clearImportData($curDate->format(DateFormat::STANDARD));
        }
    }
    /**
     * Очищает колонки записи
     *
     * @param srtring $fromDateTimeUpdate Время обновлени
     */
    public function clearImportData($fromDateTimeUpdate)
    {
        $priceTableGateway = $this->sm->get('Price');
        $priceTableGateway->update(array('sklad' => 0),
            array(
                'provider_id' => $this->supplierId,
                'dt_update < \'' . $fromDateTimeUpdate . '\''
            )
        );
    }

    /**
     * Загрузка файлов импорта поставщика.
     *
     * @param $priceImportId int id импорта
     */
    public function loadFilesImport($priceImportId)
    {
       $this->priceImport =  $this->supplierPriceImportTable->getSupplierImportInfo(array('id'=>$priceImportId))->current();
        $this->loadAllFiles();
    }

    /**
     * Получить загрузчик по файлу.
     *
     * @param $supplierPriceImportFileRow
     * @return array|FileLoaderInterface|object
     */
    public function getFileLoaderBySupplierPriceImportFile($supplierPriceImportFileRow)
    {
        $this->priceImport =  $this->supplierPriceImportTable->getSupplierImportInfo(
            array('id'=>$supplierPriceImportFileRow->supplier_price_import_id)
        )->current();
         return $this->getFileLoader();
    }

    /**
     * Получить текущий загрузчик.
     *
     * @return array|\Import\Service\FileLoaderInterface|object
     * @throws \Exception
     */
    public function getFileLoader()
    {
        if ($this->fileLoader instanceof Loader) {
            return $this->fileLoader;
        }

        switch ($this->priceImport->import_type) {
            case ExportType::LOAD_TYPE_URL:
                $this->fileLoader = $this->sm->get('HttpLoader');
                break;
            case ExportType::LOAD_TYPE_MESSAGE:
                $this->fileLoader = $this->sm->get('GoogleMailLoader');
                break;
            default:
                throw new \Exception('загрузчик для данного типа отсутствует');
        }

        return $this->fileLoader;
    }

    public function loadAllFiles()
    {
        if (!$this->priceImport) {
            throw new Exception('Не задан объект импорта!');
        }
        if ( !$this->getFileLoader() instanceof Loader){
            throw new \Exception('Ошибка инициации загрузчика!');
        };
        $this->fileLoader->configure($this->priceImport, $this->supplierPriceImportFileTable);
        $loadedFiles = $this->fileLoader->loadAllFiles();
        if (empty($loadedFiles)) {
            return;
        }
        $importFlag = false;
        foreach ($loadedFiles as $loadedFile) {
            $fileProcessor = new ImportFileProcessor(
                $loadedFile,
                $this->sm,
                $this->fileLoader
            );
            $fileProcessor->saveFileWorksheetsInfo();
            if ($this->importPrice($loadedFile)) {
                $importFlag = true;
            };
        }
        if (!$importFlag) {
            throw new \Exception('Ни один файл не был импортирован!');
        }
    }


    /**
     * Загрузить файл импорта в директорию.
     *
     * @throws \Exception
     */
    public function loadImportFile()
    {
        if (!$this->getFileLoader() instanceof Loader) {
            throw new \Exception('Неудалось получить загрузчик!');
        } else {
            $this->fileLoader->configure($this->supplierPriceImportFile);
            $fileSrc = $this->fileLoader->load($this->filename);
            $type = mime_content_type($fileSrc);
            // Если файл архив, то распаковываем его.
            $archiveFormat = new ArchiveFormat();
            $archives = $archiveFormat->getConstants();
            if (in_array($type, array_values($archives))) {
                $this->fileLoader->unpack($fileSrc, array_search($type, $archives));
            }
            $this->setReader();
            $this->saveFileWorksheetsInfo();
        }
    }

    /**
     * Удаляет импортированный файл вместе с директорией.
     *
     * @param $priceImportId int id импорта
     *
     */
    public function deleteAllFilesImport($priceImportId)
    {
        $this->priceImport =  $this->supplierPriceImportTable->getSupplierImportInfo(
            array('id'=>$priceImportId)
        )->current();
        $this->getFileLoader();
        $loadedFiles = $this->supplierPriceImportFileTable->getAllInfo(
            array(
                'supplier_price_import_id' => $this->priceImport->id
            )
        );
        foreach ($loadedFiles as $loadedFile) {
            $fileProcessor  =  new ImportFileProcessor(
                $loadedFile,
                $this->sm,
                $this->fileLoader
            );
           if ( $fileProcessor->removeFile() ) {
               $this->importFileStructureTable->removeImportFileStructure($loadedFile->id);
               $loadedFile->delete();
           }
        }
        $this->priceImport->delete();
    }

    /**
     * Удаление импорта.
     *
     * @param $supplierPriceImportFileId
     */
    public function removeFailedImport($supplierPriceImportFileId)
    {
        $supplierPriceImportFile = $this->supplierPriceImportFileTable->getOneRow($supplierPriceImportFileId);
        $supplierPriceImportId = $supplierPriceImportFile->supplier_price_import_id;
        $this->priceImport =  $this->supplierPriceImportTable->getSupplierImportInfo(array('id'=>$supplierPriceImportId))->current();
        $this->getFileLoader()->configure($this->priceImport, $this->supplierPriceImportFileTable);
        // Удаление записи импорт из таблицы.
        $this->supplierPriceImportFileTable->delete(array('id' => $supplierPriceImportFileId));
        // Удаление файла импорта.
        $this->fileLoader->remove($supplierPriceImportId, $supplierPriceImportFileId);
    }

}