<?php
namespace Import\Controller;

use Application\Utilities\DateFormat;
use Import\Model\ImportFileStructureTable;
use Import\Model\SupplierPriceImportFileTable;
use Import\Model\SupplierPriceImportTable;
use Import\Service\ImportFileProcessor;
use Import\Service\ImportService;
use Import\Service\PriceParser;
use Zend\Db\RowGateway\RowGateway;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\JsonModel;
use Import\Service\GoogleMailLoader;

class MainController extends AbstractActionController
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var PriceParser
     */
    private $priceParser;

    /**
     * @var ImportService
     */
    private $importService;

    /**
     * @var \Import\Model\SupplierPriceImportTable
     */
    protected $supplierPriceImportTable;

    /**
     * @var \Import\Model\SupplierPriceImportFileTable
     */
    protected $supplierPriceImportFileTable;

    /**
     * @var \Import\Model\ImportFileStructureTable
     */
    protected $importFileStructureTable;

    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    /**
     * MainController constructor.
     * @param ServiceLocatorInterface $sm
     * @param \Import\Service\PriceParser $priceParser
     * @param SupplierPriceImportTable $supplierPriceImportTable
     * @param ImportFileStructureTable $importFileStructureTable
     * @param SupplierPriceImportFileTable $supplierPriceImportFileTable
     */
    public function __construct(
        $sm,
        $priceParser,
        $importService,
        SupplierPriceImportTable $supplierPriceImportTable,
        ImportFileStructureTable $importFileStructureTable,
        SupplierPriceImportFileTable $supplierPriceImportFileTable
    )
    {
        $this->sm = $sm;
        $this->priceParser = $priceParser;
        $this->importService = $importService;
        $this->supplierPriceImportTable = $supplierPriceImportTable;
        $this->importFileStructureTable = $importFileStructureTable;
        $this->supplierPriceImportFileTable = $supplierPriceImportFileTable;
    }

    /**
     * Сохраняем и загружаем файл.
     */
    public function loadFilesImportAction()
    {
        $supplierPriceImportId = 0;
        try {
            $data = $this->request->getPost('data');
            $data = (array)Json::decode($data);
            $query = $this->request->getQuery()->toArray();
            $supplierPriceImportId = $query['supplierPriceImportId'];
            if (!$supplierPriceImportId) {
                $supplierPriceImportId = $this->importFileStructureTable->saveFileImportInfo($data);
            }
            $this->priceParser->loadFilesImport(array('id' => $supplierPriceImportId));
            $response = new JsonModel(array("success" => true));
            return $response;
        } catch (\Exception $e) {
            if ($e->getCode() == GoogleMailLoader::ERROR_MAIL_NOT_RECEIVED_CODE) {
                // @TODO В логах записать информацию о незагруженном файле.
            }
            $this->importFileStructureTable->removeImportFileStructure($supplierPriceImportId);
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Сохраняем и загружаем файл.
     */
    public function cronSaveAction()
    {
        $supplierPriceImportId = 0;
        try {
            $query = $this->request->getQuery()->toArray();
            $supplierPriceImportId = $query['supplierPriceImportId'];
            if (!$supplierPriceImportId) {
                throw new \Exception('Не передан id импорта');
            }
            $data = array(
                'id' => $supplierPriceImportId,
                'isCron' => $query['isCron'] == 'on' ? 1 : 0,
                'cronPeriodicity' => $query['cronPeriodicity'],
                'importInfo' => $query['importInfo']
            );
            $this->importFileStructureTable->saveFileImportInfo($data);
            $response = new JsonModel(array("success" => true));
            return $response;
        } catch (\Exception $e) {
            if ($e->getCode() == GoogleMailLoader::ERROR_MAIL_NOT_RECEIVED_CODE) {
                // @TODO В логах записать информацию о незагруженном файле.
            }
            $this->importFileStructureTable->removeImportFileStructure($supplierPriceImportId);
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Получаем информацию по файлу. Это информация по конкретной книге.
     */
    public function getFileImportStructureAction()
    {
        try {
            $query = $this->request->getQuery();
            $data = $query->toArray();
            $response = $this->importFileStructureTable->getImportFileStructureInfo(array(
                'id' =>
                    $data['importFileStructureId']
            ));
            if ($response instanceof RowGateway) {
                $response->toArray();
            }
            $fields = (array)Json::decode($response['fields_match']);
            $fields = $this->prepareFileImportStructureData($fields, true);
            return new JsonModel(array("success" => true, "data" => $fields));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }

    /**
     * Получаем информацию по файлу поставщика.
     */
    public function getSupplierPriceImportFilesAction()
    {
        try {
            $query = $this->request->getPost();
            $data = $query->toArray();
            $allInfo = $this->supplierPriceImportFileTable->getAllInfo(array(
                'supplier_price_import_id' => $data['supplierPriceImportId']
            ),
                true
            );
            return new JsonModel(array("success" => true, "data" => $allInfo));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }

    /**
     * Получаем информацию по файлу поставщика.
     */
    public function getFileImportStructureBySupplierImportIdAction()
    {
        try {
            $query = $this->request->getPost();
            $data = $query->toArray();
            $allInfo = $this->importFileStructureTable->getFileImportStructureByFileIdInfo($data['supplierPriceImportFileId']);
            return new JsonModel(array("success" => true, "data" => $allInfo));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }

    /**
     * Получаем информацию об импортированным файлам .
     */
    public function getSupplierImportsAction()
    {
        try {
            $query = $this->request->getPost();
            $supplierId = $query->get('supplierId');
            $allInfo = $this->importFileStructureTable->getImportFileStructureAllInfo(array('supplier_id' => $supplierId));
            return new JsonModel(array("success" => true, "data" => $this->getColumnValuesFromAllInfo($allInfo, 'id')));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }

    /**
     * Сохраняем признак выгрузки таблицы.
     */
    public function saveNoUploadAction(){
        try {
            $query = $this->request->getQuery();
            $noUpload = $query->get('noUpload') == 'on' ? true : false;
            $importFileStructureId = (int)$query->get('importFileStructureId');
            $this->importFileStructureTable->saveNoUpload($importFileStructureId, $noUpload);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Сохраняет заголовки таблицы.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function saveWorksheetHeadersAction()
    {
        try {
            $query = $this->request->getQuery();
            $importFileStructureId = (int)$query->get('importFileStructureId');
            $headersRow = $query->get('headersRow');
            $noHeaders = $query->get('noHeaders') == 'on' ? true : false;
            $importFileStructureRow = $this->importFileStructureTable->getImportFileStructureInfo(array(
                'id' => $importFileStructureId
            ));
            $supplierPriceImportFileRow = $this->supplierPriceImportFileTable->getAllInfo(array(
                'id' => $importFileStructureRow->supplier_price_import_file_id
            ))->current();

            $fileLoader = $this->priceParser->getFileLoaderBySupplierPriceImportFile($supplierPriceImportFileRow);

            /**
             * @var ImportFileProcessor $fileProcessor
             */
            $fileProcessor = new ImportFileProcessor(
                $supplierPriceImportFileRow,
                $this->sm,
                $fileLoader,
                $this->importFileStructureTable
            );
            $fileProcessor->setDelimiter($supplierPriceImportFileRow->delimiter);
            $headers = $fileProcessor->getColumnsInfo($importFileStructureRow->book_name, $headersRow, $noHeaders);
            $this->importFileStructureTable->saveWorksheetHeaders($importFileStructureId, $headers, $headersRow, $noHeaders);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Обновляет структуру файла.
     *
     * @return JsonModel
     */
    public function updateImportFileStructureAction()
    {
        try {
            $query = $this->request->getPost('data');
            $data = (array)Json::decode($query);
            $fields = $this->prepareFileImportStructureDataToSave($data['fields']);
            $this->importFileStructureTable->updateImportFileStructure($data['importFileStructureId'], $fields);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Сбросить дату импорта до 01.01.1970.
     *
     * @return JsonModel
     */
    public function resetImportDateAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportId = $query->get('supplierPriceImportId');
            $this->supplierPriceImportTable->resetImportDate($supplierPriceImportId);
            return new JsonModel(array("success" => true, 'message' => 'Дата импорта успешно сброшено.'));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Обновляет инпортацию об импорте файла поставщика.
     *
     * @return JsonModel
     */
    public function updateSupplierPriceImportAction()
    {
        try {
            $query = $this->request->getPost('data');
            $data = (array)Json::decode($query);
            $data['id'] = $data['supplierPriceImportId'];
            unset($data['supplierPriceImportId']);
            $data['is_cron'] = (!empty($data['is_cron']) && $data['is_cron'] == 'on') ? 1 : 0;
            $response = $this->supplierPriceImportTable->itemUpdate($data);
            return new JsonModel(array("success" => true, 'data' => $response));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Сохранить данные импортируемого файла.
     *
     * @return JsonModel
     */
    public function saveSupplierFileImportInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportFile = $query->get('supplierPriceImportFile');
            $noUpload = $query->get('noUploadFile') == 'on' ? true : false;
            $supplierPriceImportFileRow = $this->supplierPriceImportFileTable->itemUpdate(array(
                'id' => $supplierPriceImportFile,
                'encoding' => $query->get('encoding'),
                'no_upload' => $noUpload,
                'filename_mask' => $query->get('filenameMask'),
                'delimiter' => $query->get('delimiter')
            ));
            return new JsonModel(array("success" => true, "message" => "Информация успешно обновлена"));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function importOneFileAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportFile = $query->get('supplierPriceImportFile');
            $supplierPriceImportFileRow = $this->supplierPriceImportFileTable->getAllInfo(array(
                'id' => $supplierPriceImportFile
            ))->current();
            $curDate = new \DateTime();
            if ($this->priceParser->importPrice($supplierPriceImportFileRow)) {
                $this->priceParser->clearImportData($curDate->format(DateFormat::STANDARD));
            }
            return new JsonModel(array("success" => true));
        } catch (\Throwable $ex) {
            return new JsonModel(array("success" => false, 'message' => $ex->getMessage()));
        }
    }

    /**
     * Удаляет структуру файла из БД по id импорта.
     *
     * @return JsonModel
     */
    public function removeImportFileAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportId = $query->get('supplierPriceImportId');
            $this->priceParser->deleteAllFilesImport($supplierPriceImportId);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Записываем в БД информацию об удалении импорта.
     *
     * @return JsonModel
     */
    public function removeImportOneFileAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportFileId = (int)$query->get('supplierPriceImportFileId');
            $this->priceParser->removeFailedImport($supplierPriceImportFileId);
            return new JsonModel(array("success" => true, "message" => "Файл успешно удален"));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Получаем информацию об импортированном файле по supplierPriceImportId.
     *
     * @return JsonModel
     */
    public function supplierImportInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportId = (int)$query->get('supplierPriceImportId');
            $response = $this->supplierPriceImportTable->getSupplierImportInfo(array(
                'id' => $supplierPriceImportId
            ))->current();
            return new JsonModel(array("success" => true, 'data' => $response->toArray()));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Получаем информацию об импортированном файле по supplierPriceImportId.
     *
     * @return JsonModel
     */
    public function supplierImportAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierId = (int)$query->get('supplierId');
            // Сброс import date для этого поставщика.
            $supplierImports = $this->supplierPriceImportTable->getAllInfo(array('supplier_id' => $supplierId));
            foreach ($supplierImports as $import) {
                $this->supplierPriceImportTable->resetImportDate((int)$import->id);
            }
            $this->priceParser->importAllSupplierPrices($supplierId);
            return new JsonModel(array("success" => true, 'data' => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Импорт складских остатков из одного файла.
     */
    public function importPriceAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierId = (int)$query->get('supplierId');
            $this->priceParser->importAllSupplierPrices($supplierId);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Лог импорта.
     *
     * @return JsonModel
     */
    public function logImportAction() {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportId = (int)$query->get('supplierPriceImportId');
            $logsInfo = $this->supplierPriceImportFileTable->getAllImportSuppliersInfo(array('priceImportId' => $supplierPriceImportId));
            $data = array();
            foreach ($logsInfo as $item) {
                $arr = $item->toArray();
                $arr['detail'] = Json::decode($arr['detail']);
                $arr['success']  = $arr['success'] ? 'Без ошибок' : 'Есть ошибки';
                 $arr['warning']  = !$arr['warning'] ? 'Без изменений' : 'Есть изменения в структуре';
                $data[]  = $arr;
            }
            return new JsonModel(array("success" => true, 'data' => $data));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация о последнем импорта.
     */
    public function lastImportInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $supplierPriceImportId = (int)$query->get('supplierPriceImportId');
            $response = $this->supplierPriceImportTable->getSupplierImportInfo(array('id' => $supplierPriceImportId))->current();
            return new JsonModel(array("success" => true, 'data' => $response->toArray()));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Конвертирует данные для модели грида.
     *
     * @param $data
     * @param bool $isSaved
     * @return array
     */
    public function prepareFileImportStructureData(array $data, $isSaved = false)
    {
        $result = array();
        if (empty($data)) {
            return $result;
        }
        foreach ($data as $key => $value) {
            $result[] = array('headerFile' => $key, 'columnTable' => ($isSaved) ? $value : '');
        }
        return $result;
    }

    /**
     * Подготовка данных для записи в БД.
     *
     * @param $data
     * @return array
     */
    public function prepareFileImportStructureDataToSave($data)
    {
        $result = array();
        if (empty($data)) {
            return $result;
        }
        foreach ($data as $key => $item) {
            $result[$item->headerFile] = $item->columnTable;
        }
        return $result;
    }

    /**
     * @param array $allInfo
     * @param $column
     * @return array
     */
    private function getColumnValuesFromAllInfo($allInfo, $column)
    {
        $result = array();
        if (empty($allInfo)) {
            return $result;
        }
        foreach ($allInfo as $item) {
            $result[] = $item[$column];
        }
        return $result;
    }
}