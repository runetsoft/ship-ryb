<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Import\Controller\MainController' => 'Import\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'import' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/import[/:action].json',
                    'defaults' => array(
                        'controller' => 'Import\Controller\MainController'
                    ,
                    ),
                ),
            )
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);