<?php
namespace Import;

use Cron\Model\CronTable;
use Import\Model\ImportFileStructureTable;
use Import\Model\ImportLog;
use Import\Model\ImportLogTable;
use Import\Model\SupplierPriceImport;
use Import\Model\ImportFileStructure;
use Import\Model\SupplierPriceImportFile;
use Import\Model\SupplierPriceImportFileTable;
use Import\Model\SupplierPriceImportTable;
use Import\Service\Cron;
use Import\Service\GoogleMailLoader;
use Import\Service\HttpLoader;
use Cron\Service\Run;
use Import\Service\ImportService;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Import\Service\PriceParser;

/**
 * Class Module
 * Модуль для раздела прайсы.
 *
 * @package Price
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Import\Model\SupplierPriceImportTable' => function ($sm) {
                    $table = new SupplierPriceImportTable($sm);
                    return $table;
                },
                'SupplierPriceImportTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPriceImport());
                    return new TableGateway('cms_supplier_price_import', $dbAdapter, new RowGatewayFeature('id'),
                        $resultSetPrototype);
                },
                'Import\Model\SupplierPriceImportFileTable' => function ($sm) {
                    $table = new SupplierPriceImportFileTable($sm);
                    return $table;
                },
                'SupplierPriceImportFileTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPriceImportFile());
                    return new TableGateway('cms_supplier_price_import_file', $dbAdapter, new RowGatewayFeature('id'),
                        $resultSetPrototype);
                },
                'Import\Model\ImportFileStructureTable' => function ($sm) {
                    $table = new ImportFileStructureTable($sm);
                    return $table;
                },

                'ImportFileStructureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ImportFileStructure());
                    return new TableGateway('cms_import_file_structure', $dbAdapter, new RowGatewayFeature('id'),
                        $resultSetPrototype);
                },

                'Import\Model\ImportLogTable' => function ($sm) {
                    $table = new ImportLogTable($sm);
                    return $table;
                },
                'ImportLogTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ImportFileStructure());
                    return new TableGateway('cms_import_log', $dbAdapter, new RowGatewayFeature('id'),
                        $resultSetPrototype);
                },
                'Import\Service\PriceParser' => function ($sm) {
                    $parser = new PriceParser($sm);
                    return $parser;
                },
                'Import\Service\ImportService' => function ($sm) {
                    $parser = new ImportService($sm);
                    return $parser;
                },
                'Import\Service\Cron' => function ($sm) {
                    $cron = new Cron($sm);
                    return $cron;
                },
                'Cron\Service\Run' => function ($sm) {
                    $run = new Run($sm);
                    return $run;
                },
                'HttpLoader' => function ($sm) {
                    $loader = new HttpLoader();
                    return $loader;
                },
                'GoogleMailLoader' => function ($sm) {
                    $loader = new GoogleMailLoader();
                    return $loader;
                }
            ),
            'aliases' => array(
                'BaseRowPreparer' => 'Import\Service\BaseRowPreparer',
                'Price' => 'PriceTableGateway'
            ),
            'abstract_factories' => array(
                'Import\Factory\RowPreparerAbstractFactory',
            ),
            'shared' => array(
                'ImportFileStructureTableGateway' => false,
                'ImportFileStructureRow' => false,
                'SupplierPriceImportTableGateway' => false,
                'SupplierPriceImportFileTableGateway' => false,
                'SupplierPriceImportRow' => false,
                'SupplierPriceImportFileRow' => false,
                'ImportLogTableGateway' => false,
                'ImportLogRow' => false
            )
        );
    }
}