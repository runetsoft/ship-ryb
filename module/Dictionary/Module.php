<?php
namespace Dictionary;

use Dictionary\Model\Dictionary;
use Dictionary\Model\DictionaryBind;
use Dictionary\Model\DictionaryTable;
use Dictionary\Model\MultiselectDictionaryTable;
use Dictionary\Model\Tables;
use Dictionary\Service\ListDictionary;
use Dictionary\Service\DictionaryUtils;
use Zend\Db\Metadata\Metadata;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\RowGateway\RowGateway;
use Zend\Mvc\Application;

/**
 * Class Module
 * Модуль для справочников.
 *
 * @package Dictionary
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'DictionaryTable' => function ($sm) {
                    $table = new DictionaryTable($sm);
                    return $table;
                },
                'MultiselectDictionaryTable' => function ($sm) {
                    $table = new MultiselectDictionaryTable($sm);
                    return $table;
                },
                'ListDictionary' => function ($sm) {
                    $shipRybAdapter = $sm->get('ShipRybAdapter');
                    $dictionariesDictionaryTableGateway = $sm->get('ListDictionaryTableGateway');
                    $table = new ListDictionary($sm, $dictionariesDictionaryTableGateway, $shipRybAdapter);
                    return $table;
                },
                'ListDictionaryTableGateway' => function ($sm) {
                    $schemaAdapter = $sm->get('SchemaAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Tables());
                    return new TableGateway('TABLES', $schemaAdapter, null, $resultSetPrototype);
                },
                'DictionaryUtils' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $binderDictionaryTableGateway = $sm->get('BinderDictionaryTableGateway');
                    return new DictionaryUtils($sm, $dbAdapter, $binderDictionaryTableGateway);
                },
                'BinderDictionaryTableGateway' => function ($sm){
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('table_name');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DictionaryBind());
                    return new TableGateway('cms_binder_dictionary', $dbAdapter, $features, $resultSetPrototype);
                }
            ),
            'abstract_factories' => array(
                'Dictionary\Factory\DictionaryTableGatewayAbstractFactory',
                'Dictionary\Factory\DictionaryAbstractFactory',
            ),
            'shared' => array(
                'DiskModelDictionaryTableGateway' => false,
                'DiskModelDictionary' => false,
                'BaseModelDictionary' => false,
                'TyreModelDictionaryTableGateway' => false,
                'TyreModelDictionary' => false,
                'DictionaryTable' => false,
                'MultiselectDictionaryRow' => false,
                'ListDictionary' => false,
                'DictionaryUtils' => false,
                'BinderDictionaryRow' => false,
            )
        );
    }
}