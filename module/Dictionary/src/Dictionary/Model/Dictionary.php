<?php
namespace Dictionary\Model;

use Application\Model\BaseModel;

/**
 * Общий вид структуры таблиц справочников
 *
 * Class Dictionary
 * @package Dictionary\Model
 */
class Dictionary extends BaseModel
{
    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $code;

    /**
     * @var
     */
    public $name;
}