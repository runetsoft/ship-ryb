<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 26.09.2017
 * Time: 14:29
 */

namespace Dictionary\Model;

use Application\Model\BaseModel;

/**
 * Структура таблицы TABLES в information_schema
 *
 * Class Dictionary
 * @package Dictionary\Model
 */
class Tables extends BaseModel
{
    public $TABLE_CATALOG;
    public $TABLE_SCHEMA;
    public $TABLE_NAME;
    public $TABLE_TYPE;
    public $ENGINE;
    public $VERSION;
    public $ROW_FORMAT;
    public $TABLE_ROWS;
    public $AVG_ROW_LENGTH;
    public $DATA_LENGTH;
    public $MAX_DATA_LENGTH;
    public $INDEX_LENGTH;
    public $DATA_FREE;
    public $AUTO_INCREMENT;
    public $CREATE_TIME;
    public $UPDATE_TIME;
    public $CHECK_TIME;
    public $TABLE_COLLATION;
    public $CHECKSUM;
    public $CREATE_OPTIONS;
    public $TABLE_COMMENT;
}