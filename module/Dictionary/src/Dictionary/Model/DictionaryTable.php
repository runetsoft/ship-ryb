<?php

namespace Dictionary\Model;

use Application\Model\BaseModelTable;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Dictionary\Model\Dictionary;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;

/**
 * Таблица настроек для работы со словарями.
 *
 * Class DictionaryTable
 * @package Dictionary\Model
 */
class DictionaryTable extends BaseModelTable 
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var TableGateway;
     */
    protected $tableGateway;

    /**
     * Dictionary constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    /**
     * Устанавливает таблицу словаря.
     *
     * @param $tableGateway
     *
     * @return DictionaryTable
     */
    public function setTable($tableGateway)
    {
        $this->tableGateway = $tableGateway;
        return $this;
    }

    public function getColumns($prefix = '')
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $tableName = $this->tableGateway->getTable();
        $separator = ',';
        $sql = "SELECT GROUP_CONCAT(column_name SEPARATOR '$separator') FROM information_schema.columns WHERE table_name='" . $tableName . "'";
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        $current = $resultSet->current()->getIterator()->current();
        $result = explode($separator, $current);
        if (!empty($prefix)) {
            foreach ($result as $index => $column) {
                $result[$prefix . $column] = $column;
                unset($result[$index]);
            }
        }
        return $result;

    }

    /**
     * Выводим данные по справочнику из Базы Данных
     * @param array | null $array
     * $array = array(
        'limit' => 10,
     *  'filter' => array(),
     *  'order' => 'id ASC'
     * )
     * @return array
     * @throws \Exception
     *
     */
    public function getList($array = null)
    {
        $items = array();

        $result = $this->tableGateway->select(
            function (Select $select) use ($array) {
                if (is_array($array['filter']) && count($array['filter']) > 0) {
                    $this->setFilter($select, $array['filter'], $this->tableGateway->getTable());
                }
                $select->order('name ASC');
            }
        );

        foreach ($result as $item) {
            $items[] = $item->toArray();
        }
        return $items;
    }

    /**
     * Добавляем новый элемент справочника в БД
     *
     * @param $array
     * @return int last id
     * @throws \Exception
     *
     */
    public function add($array)
    {
        $this->tableGateway->insert($array);
        return $this->tableGateway->lastInsertValue;
    }

    /**
     * Получает данные для пагинатора
     *
     * @param array $filter
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllInfo(array $filter = array())
    {
        $select = $this->tableGateway->getSql()
            ->select();
        $this->setFilter($select, $filter, $this->tableGateway->getTable());
        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    /**
     * Пагинатор
     *
     * @param $page
     * @param $limit
     * @param array $filter
     * @return array
     */
    public function getAllInfoPaginator($page, $limit, array $filter = array())
    {
        $resultSet = $this->getAllInfo($filter);
        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);
        return array($paginator, $resultSet->count());
    }

    /**
     * Удаление значений справочника
     *
     * @param $id
     * @return bool
     */
    public function deleteItem($id){
        $this->tableGateway->delete(array("id" => $id));
        return true;
    }

    public function udateValue($data){
        $result = $this->tableGateway->select(array("id" => $data['id']));
        $dbRow = $result->current();
        if ($dbRow)
        {
            $dbRow->{$data['fieldName']} = $data['fieldValue'];
            $dbRow->save();
            return true;
        }
        else{
            return false;
        }
    }
}