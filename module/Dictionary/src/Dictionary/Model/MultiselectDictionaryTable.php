<?php

namespace Dictionary\Model;

use Application\Model\BaseModelTable;
use Application\Utilities\FieldType;
use Application\Utilities\Utilite;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\TableGateway\TableGateway;

/**
 * Class MultiselectDictionaryTable
 *
 * @package Dictionary\Model
 */
class MultiselectDictionaryTable extends BaseModelTable
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var TableGateway;
     */
    protected $tableGateway;

    /**
     * MultiselectDictionaryTable constructor.
     *
     * @param $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    public function setTableByDictionary($dictionaryName)
    {
        $tableName = Utilite::getTableFullName(FieldType::DICTIONARYMULTISELECT . '_' . $dictionaryName);
        $dbAdapter = $this->sm->get('ShipRybAdapter');
        $resultSetPrototype = new ResultSet();
        $arrObj = new \ArrayObject();
        $resultSetPrototype->setArrayObjectPrototype($arrObj);
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, new RowGatewayFeature('id'), $resultSetPrototype);
    }

    public function add($array)
    {
        $this->tableGateway->insert($array);
        return $this->tableGateway->lastInsertValue;
    }

    public function updateValuesByGood($goodId, $items)
    {
        $this->deleteValuesByGood($goodId);
        foreach ($items as $item) {
            $this->tableGateway->insert(
                array(
                    'good_id' => $goodId,
                    'dictionary_id' => $item
                ));
        }
    }

    public function deleteValuesByGood($goodId)
    {
        $this->tableGateway->delete(
            array(
                'good_id' => $goodId
            ));
    }

    public function fetch($where)
    {
        $resultSet = $this->tableGateway->select($where);
        return $resultSet;
    }
}