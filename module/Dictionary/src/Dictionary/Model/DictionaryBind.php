<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 09.10.2017
 * Time: 18:05
 */

namespace Dictionary\Model;

use Application\Model\BaseModel;

/**
 * Структура таблицы binder_dictionary в shipryb_db
 *
 * Class DictionaryBind
 * @package Dictionary\Model
 */
class DictionaryBind extends BaseModel
{
    public $table_name;
    public $parent_table_name;

}