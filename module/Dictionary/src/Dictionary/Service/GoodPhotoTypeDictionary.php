<?php

namespace Dictionary\Service;

use Application\Utilities\GoodPhotoType;
use Zend\Json\Json;

/**
 * Class GoodPhotoTypeDictionary
 * @package Dictionary\Service
 */
class GoodPhotoTypeDictionary extends BaseConstDictionary
{
    /**
     * GoodPhotoTypeDictionary constructor.
     */
    public function __construct()
    {
        parent::__construct(new GoodPhotoType());
    }

    public function convertQuery($query)
    {
        return (is_object($query) && !empty($query->filter)) ? (array)Json::decode($query->filter) : array();
    }


}