<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 26.05.2017
 * Time: 10:35
 */

namespace Dictionary\Service;

use Application\Utilities\CronPeriod;

/**
 * Class CronPeriodDictionary
 * @package Dictionary\Service
 */
class CronPeriodDictionary extends BaseConstDictionary
{
    /**
     * CronPeriodDictionary constructor.
     */
    public function __construct()
    {
        parent::__construct(new CronPeriod());
    }
}