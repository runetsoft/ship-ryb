<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 22.09.2017
 * Time: 18:18
 */
namespace Dictionary\Service;

use Application\Model\BaseModelTable;
use Dictionary\Model\Tables;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;

/**
 * Получает информацию из information_schema
 *
 * Class ListDictionary
 * @package Dictionary\Service
 */
class ListDictionary extends BaseModelTable{
    /**
     * @var $sm service locator
     */
    protected $sm;

    /**
     * @var $dictionaryTableGateway tables
     */
    protected $dictionaryTableGateway;

    /**
     * @var $shipRybAdapter ShipRybAdapter
     */
    protected $shipRybAdapter;

    public function __construct($sm, TableGateway $dictionaryTableGateway, $shipRybAdapter)
    {
        $this->sm = $sm;
        $this->dictionaryTableGateway = $dictionaryTableGateway;
        $this->shipRybAdapter = $shipRybAdapter;
    }

    private function getCurrentDb()
    {
        return $this->shipRybAdapter->getCurrentSchema();
    }

    /**
     * Получает данные для страницы
     *
     * @param array $filter
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllInfo(array $filter = array())
    {
        $currentDb = $this->getCurrentDb();
        $dictionariesDictionary = new Tables();
        $dictionariesDictionaryColumns = $dictionariesDictionary->getColumns();
        $select = $this->dictionaryTableGateway->getSql()
            ->select()
            ->columns($dictionariesDictionaryColumns)
            ->where(array("TABLE_SCHEMA" => $currentDb));
        $select->where($select->where->like('TABLE_NAME', '%cms_dictionary%'));

        $this->setFilter($select, $filter, $this->dictionaryTableGateway->getTable());
        $statement = $this->dictionaryTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    /**
     * Получает данные для страницы, и оптимизирует их согласно параметрам пагинатора
     *
     * @param $page
     * @param $limit
     * @param array $filter
     * @return array
     */
    public function getAllInfoPaginator($page, $limit, array $filter = array())
    {
        $resultSet = $this->getAllInfo($filter);
        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);
        return array($paginator, $resultSet->count());
    }
}