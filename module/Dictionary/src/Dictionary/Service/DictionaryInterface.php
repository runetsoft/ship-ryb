<?php
namespace Dictionary\Service;

interface DictionaryInterface
{
    /**
     * Возвращает данные из массива с учетом фильтрации
     * @param $array (фильтрация и ограничения по справочнику).
     * @return mixed
     */
    public function getList($array);

    /**
     * Отдает имя по параметру id или code. Возвращаетяс всегда одна запись.
     *
     * @param $query
     * @return mixed
     */
    public function getName($query);

    /**
     * Конвертирует объект в массив с учетом дополнительных параметров
     *
     * @param $query object
     * @return $array array
     */
    public function convertQuery($query);

    /**
     * Возвращает данные с учетом фильтрации
     *
     * @param $query array
     * @return mixed
     */
    public function getData($query);

}