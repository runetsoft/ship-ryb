<?php

namespace Dictionary\Service;

use Price\Model\Price;

/**
 * Справочник для таблицы Price
 *
 * Class PriceTableDictionary
 * @package Dictionary\Service
 */
class PriceTableDictionary extends BaseTableDictionary
{

    /**
     * PriceTableDictionary constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        parent::__construct($sm, new Price());
    }
}