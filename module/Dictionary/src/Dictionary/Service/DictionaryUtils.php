<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 04.10.2017
 * Time: 16:20
 */

namespace Dictionary\Service;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Ddl;
use Zend\Db\Sql\Ddl\Column;
use Zend\Db\Sql\Ddl\Constraint;


/**
 * Класс методов-утилит для работы со справочниками
 *
 * Class DictionaryUtils
 * @package Dictionary\Service
 */
class DictionaryUtils{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $dbAdapter;

    /**
     * @var
     */
    protected $binderDictionaryTableGateway;

    /**
     * DictionaryUtils constructor.
     * @param $sm
     * @param $dbAdapter
     * @param $binderDictionaryTableGateway\
     */
    public function __construct($sm, $dbAdapter, $binderDictionaryTableGateway)
    {
        $this->sm = $sm;
        $this->dbAdapter = $dbAdapter;
        $this->binderDictionaryTableGateway = $binderDictionaryTableGateway;
    }

    /**
     * Изменение комментариев таблицы
     *
     * @param $data
     * @return bool
     */
    public function changeTableComment($data)
    {
        $this->dbAdapter->query("ALTER TABLE `".$data['tableName']."` COMMENT = '".$data['tableComment']."'", Adapter::QUERY_MODE_EXECUTE);
        return true;
    }

    /**
     * Добавление таблиц
     *
     * @param $data
     * @return mixed
     */
    public function addTable($data)
    {
        // Create the $table
        $sql = new Sql($this->dbAdapter);

        $createTable = new Ddl\CreateTable($data['tableName']);
        $createTable->addColumn(new Column\Integer('id', false, null, array('autoincrement'=>true)));
        $createTable->addColumn(new Column\Varchar('code', 255, array('DEFAULT'=>null)));
        $createTable->addColumn(new Column\Varchar('name', 55, array('DEFAULT'=>null)));
        $createTable->addColumn(new Column\Integer('parent_id', 11));

        $createTable->addConstraint(new Constraint\PrimaryKey('id'));

        $this->dbAdapter->query(
            $sql->buildSqlString($createTable),
            Adapter::QUERY_MODE_EXECUTE
        );
        $this->changeTableComment($data);

        if (!empty($data['valuesTableName'])) {
            // Create the $table
            $sql = new Sql($this->dbAdapter);

            $createTable2 = new Ddl\CreateTable($data['valuesTableName']);
            $createTable2->addColumn(new Column\Integer('id', false, null, array('autoincrement' => true)));
            $createTable2->addColumn(new Column\Integer('good_id', 11));
            $createTable2->addColumn(new Column\Integer('dictionary_id', 11));

            $createTable2->addConstraint(new Constraint\PrimaryKey('id'));

            $this->dbAdapter->query(
                $sql->buildSqlString($createTable2),
                Adapter::QUERY_MODE_EXECUTE
            );
        }

        return $data;
    }

    /**
     * Добавление привязки справочника
     *
     * @param $data
     * @return bool
     */
    public function addBind($data)
    {
        $result = $this->binderDictionaryTableGateway->select(array("table_name" => $data['table_name']));
        $dbRow = $result->current();
        if (!$dbRow)
        {
            $row = $this->sm->get('BinderDictionaryRow');
            $row->table_name = $data['table_name'];
            $row->parent_table_name = $data['parent_table_name'];
            $row->save();
        }
        else{
            return false;
        }
    }

    /**
     * Получение привязки
     *
     * @param $tableName
     * @return bool
     */
    public function getBind($tableName)
    {
        $result = $this->binderDictionaryTableGateway->select(array("table_name" => $tableName));
        if ($result->current())
        {
            return $result->current()->toArray();
        }
        else{
            return false;
        }

    }
}