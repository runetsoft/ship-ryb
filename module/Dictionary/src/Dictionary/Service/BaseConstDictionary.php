<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 19.05.2017
 * Time: 16:07
 */

namespace Dictionary\Service;

use Application\Utilities\BaseConstant;



class BaseConstDictionary extends AbstractDictionary
{

    /**
     * @var BaseConstant
     */
    private $model;

    /**
     * PriceTableDictionary constructor.
     * @param BaseConstant $model
     */
    public function __construct(BaseConstant $model)
    {
        $this->model = $model;
    }

    public function getList($array = null)
    {
        $result = array();
        $constants = $this->model->getConstants(true);


        foreach ($constants as $key => $constant) {
            $constName = (!empty($constant['description'])) ? $constant['description'] : $constant['value'];

            if (isset($array) && $array != "") {

                if (isset($array['name']) && $array['name'] != ""){
                    $array['name'] = is_array($array['name']) ? $array['name'] : array($array['name']);

                    //TODO: Если этого не достаточно, можно сделать цикл и в нем проверять совпадения по preg_match_all

                    if (!in_array($constName, $array['name'])){
                        continue;
                    }
                }

                if (isset($array['code']) && $array['code'] != ""){
                    $array['code'] = is_array($array['code']) ? $array['code'] : array($array['code']);
                    if (!in_array($constant['value'], $array['code'])){
                        continue;
                    }
                }

                if (isset($array['id']) && $array['id'] != ""){
                    $filter['id'] = is_array($array['id']) ? $array['id'] : array($array['id']);
                    if (!in_array($key, $array['id'])){
                        continue;
                    }
                }
            }

            $result[] = array(
                'id' => $key,
                'code' => $constant['value'],
                'name' => $constName
            );
        }
        return $result;
    }
}