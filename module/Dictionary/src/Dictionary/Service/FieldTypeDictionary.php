<?php

namespace Dictionary\Service;

use Application\Utilities\FieldType;

/**
 * Class FieldTypeDictionary
 * @package Dictionary\Service
 */
class FieldTypeDictionary extends BaseConstDictionary
{
    /**
     * FieldTypeDictionary constructor.
     */
    public function __construct()
    {
        parent::__construct(new FieldType());
    }
}