<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 24.05.2017
 * Time: 16:22
 */

namespace Dictionary\Service;


class AbstractDictionary implements DictionaryInterface
{
    public function convertQuery($query)
    {
        return $query;
    }

    public function getList($array = null)
    {
        // TODO: Implement getList() method.
    }

    public function getData($query)
    {
        $array = $this->convertQuery($query);
        $filter = (!empty($f = $query->get('filter'))) ? (array)json_decode($f) : [];
        foreach ($filter as $column => $val) {
            if ($val) {
                $array['filter'][$column] = $val;
            }
        }
        return $this->getList($array);
    }

    /**
     * Получает справочник по имени.
     *
     * @param $query
     * @return mixed
     */
    public function getName($query)
    {
       $row = current($this->getData($query));
       return $row['name'];
    }

}