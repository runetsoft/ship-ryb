<?php

namespace Dictionary\Service;

use Application\Utilities\Delimiter;

/**
 * Class DelimiterDictionary
 * @package Dictionary\Service
 */
class DelimiterDictionary extends BaseConstDictionary
{
    /**
     * DelimiterDictionary constructor.
     */
    public function __construct()
    {
        parent::__construct(new Delimiter());
    }
}