<?php

namespace Dictionary\Service;

use Price\Model\Price;

/**
 * Class PriceTableExpandedDictionary
 * @package Dictionary\Service
 */
class PriceTableExpandedDictionary extends BaseTableDictionary
{
    private $additionalGoodFields = array(
        array(
            'column' => 'good_type',
            'description' => 'Тип товара (1-диск, 2-шина)'
        ),
        array(
            'column' => 'good_orig',
            'description' => 'Имя товара полностью'
        ),
        array(
            'column' => 'good_id',
            'description' => 'ИД товара'
        ),
        array(
            'column' => 'good_rprice',
            'description' => 'МРЦ товара'
        ),
        array(
            'column' => 'good_zarticle',
            'description' => 'Артикул'
        ),
        array(
            'column' => 'good_photo',
            'description' => 'Фотография'
        ),
        array(
            'column' => 'good_dt_update_photo',
            'description' => 'Дата обновления фото товара'
        ),
        array(
            'column' => 'good_dt_update',
            'description' => 'Дата обновления товара'
        )
    );
    private $additionalFields = array(
        array(
            'column' => 'id',
            'description' => 'ИД предложения'
        ),
        array(
            'column' => 'model',
            'description' => 'Модель'
        ),
        array(
            'column' => 'model_id',
            'description' => 'ID Модели'
        ),
        array(
            'column' => 'brand',
            'description' => 'Бренд'
        ),
        array(
            'column' => 'brand_id',
            'description' => 'ID Бренда'
        ),
        array(
            'column' => 'provider_id',
            'description' => 'ID Поставщика'
        ),
        array(
            'column' => 'provider_name',
            'description' => 'Поставщик'
        ),
        array(
            'column' => 'price',
            'description' => 'Цена'
        ),
        array(
            'column' => 'sklad',
            'description' => 'Остаток'
        ),
        array(
            'column' => 'dt_update',
            'description' => 'Дата обновления прайса'
        ),
    );
    private $additionalDiskFields = array(
        array(
            'column' => 'disk_width',
            'description' => 'Диск.Ширина'
        ),
        array(
            'column' => 'disk_diameter',
            'description' => 'Диск.Диаметр'
        ),
        array(
            'column' => 'disk_hole_num',
            'description' => 'Диск.Количество отверстий'
        ),
        array(
            'column' => 'disk_diameter_hole',
            'description' => 'Диск.Диаметр отверстий'
        ),
        array(
            'column' => 'disk_overhand',
            'description' => 'Диск.Вылет'
        ),
        array(
            'column' => 'disk_diameter_nave',
            'description' => 'Диск.Диаметр ступичного отверстия'
        ),
        array(
            'column' => 'disk_color',
            'description' => 'Диск.Цвет'
        ),
        array(
            'column' => 'dictionary_disk_type',
            'description' => 'Диск.Тип диска (1 -Штампованные 2 - Литые 3 - Кованые)'
        )
    );

    private $additionalTyreFields = array(
        array(
            'column' => 'tyre_width',
            'description' => 'Шина.Ширина'
        ),
        array(
            'column' => 'tyre_height',
            'description' => 'Шина.Высота'
        ),
        array(
            'column' => 'dictionary_tyre_construction',
            'description' => 'Шина.Коснструкция'
        ),
        array(
            'column' => 'tyre_diameter',
            'description' => 'Шина.Диаметр'
        ),
        array(
            'column' => 'tyre_index_load',
            'description' => 'Шина.Индекс нагрузки'
        ),
        array(
            'column' => 'tyre_index_speed',
            'description' => 'Шина.Индекс скорости'
        ),
        array(
            'column' => 'tyre_features',
            'description' => 'Шина.Характеризующие аббревиатуры'
        ),
        array(
            'column' => 'dictionary_tyre_runflat',
            'description' => 'Шина.Ранфлэт'
        ),
        array(
            'column' => 'tyre_cargo',
            'description' => 'Шина.Карго'
        ),
        array(
            'column' => 'dictionary_tyre_xl',
            'description' => 'Шина.XL'
        ),
        array(
            'column' => 'dictionary_tyre_camers',
            'description' => 'Шина.Камерность'
        ),
        array(
            'column' => 'dictionary_tyre_season',
            'description' => 'Шина.Сезон (1 - Зимние (шипованные), 2 - Зимние (нешипованные), 3 - Летние, 4 - Всесезонные, 5 - Внедорожные )'
        )
    );

    /**
     * PriceTableExpandedDictionary constructor.
     *
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        parent::__construct($sm, new Price());
    }

    /**
     * Выводим данные по справочнику из Описании класса модели и по дополнительным полям .
     *
     * @param $query
     * @return array
     */
    public function getList($query = array())
    {
        $list = array();
        $size = count($list);
        foreach (array_merge($this->additionalGoodFields, $this->additionalFields, $this->additionalDiskFields,
            $this->additionalTyreFields) as $key => $field) {
            $list[] = array(
                'id' => $key + $size,
                'code' => $field['column'],
                'name' => (!empty($field['description'])) ? $field['description'] : $field['column']
            );
        }
        return $list;
    }
}