<?php

namespace Dictionary\Service;

use Application\Model\BaseModel;

/**
 * Базовый класс, обработчик справочников моделей.
 *
 * Class BaseDictionary
 * @package Import\Service
 */
class BaseTableDictionary extends AbstractDictionary
{

    /**
     * @var ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var BaseModel
     */
    private $model;
    /**
     * PriceTableDictionary constructor.
     * @param ServiceLocatorInterface $sm
     * @param BaseModel $model
     */
    public function __construct($sm, BaseModel $model)
    {
        $this->sm = $sm;
        $this->model = $model;
    }

    /**
     * Выводим данные по справочнику из Описании класса модели.
     * @param $query
     * @return array
     * @throws \Exception
     *
     */
    public function getList($query)
    {
        $array = [];

        $modelColumns = $this->model->getColumns(true);

        foreach ($modelColumns as $key => $modelColumn) {
            $array[] = array(
                'id' => $key,
                'code' => $modelColumn['column'],
                'name' => (!empty($modelColumn['description'])) ? $modelColumn['description'] : $modelColumn['column']
            );
        }
        return $array;
    }
}