<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 23.05.2017
 * Time: 10:35
 */

namespace Dictionary\Service;

use Application\Utilities\Encoding;

/**
 * Class EncodingDictionary
 * @package Dictionary\Service
 */
class EncodingDictionary extends BaseConstDictionary
{
    /**
     * EncodingDictionary constructor.
     */
    public function __construct()
    {
        parent::__construct(new Encoding());
    }
}