<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 21.08.2017
 * Time: 16:35
 */

namespace Dictionary\Service;

class BaseModelDictionary extends BaseDictionary{

    /**
     * Переписываем метод convertQuery c добавлением новых параметров
     *
     * @param $query object
     * @return $array array
     */
    public function convertQuery($query)
    {
        $array = parent::convertQuery($query);
        if ($query->get('parent_id') != null) {
            $array['filter']['=parent_id'] = $query->get('parent_id');
        }
        return $array;
    }

}