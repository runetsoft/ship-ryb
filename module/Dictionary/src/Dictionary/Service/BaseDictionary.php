<?php

namespace Dictionary\Service;

use Dictionary\Model\DictionaryTable;

/**
 * Базовый класс обработчик справочников.
 *
 * Class BaseDictionary
 * @package Import\Service
 */
class BaseDictionary extends AbstractDictionary
{

    /**
     * @var ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var DictionaryTable
     */
    private $table;

    /**
     * Dictionary constructor.
     * @param  ServiceLocatorInterface $sm
     * @param $tableGateway table name
     */
    public function     __construct($sm, $tableGateway)
    {
        $this->sm = $sm;
        $this->table = $this->sm->get('DictionaryTable')->setTable($tableGateway);
    }

    /**
     * Пагинатор
     *
     * @param $page
     * @param $limit
     * @param $filter
     * @return array
     */
    public function getAllInfoPaginator($page, $limit, $filter)
    {
        return $this->table->getAllInfoPaginator($page, $limit, $filter);
    }

    /**
     * конвертируем массив в зависимости от наличия параметров
     *
     * @param object $query
     * @return array
     */
    public function convertQuery($query)
    {
        $array = array(
            'filter'=> array(
                '%name' => $query->get('query')
        ));

        return $array;
    }

    /**
     * Выводим данные по справочнику из Базы Данных
     * @param $array array
     * @return array
     * @throws \Exception
     *
     */
    public function getList($array = null)
    {
        return $this->table->getList($array);
    }

    /**
     * Добавляем строку в словарь.
     * 
     * @param array $params
     * @return mixed
     */
    public function add(array $params) {
        return $this->table->add($params);
    }

    /**
     *
     * Удаление значений
     * @param $id
     * @return bool
     */
    public function deleteItem($id)
    {
        return $this->table->deleteItem($id);
    }


    public function updateValue ($data)
    {
        return $this->table->udateValue($data);
    }

}