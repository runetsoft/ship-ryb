<?php

namespace Dictionary\Service;

use Application\Utilities\Utilite;

/**
 * Базовый класс, обработчик справочников моделей.
 *
 * Class BaseDictionary
 * @package Import\Service
 */
class DictionariesListDictionary extends AbstractDictionary
{

    /**
     * @var ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var ListDictionary
     */
    private $dictionariesListService;
    /**
     * PriceTableDictionary constructor.
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    /**
     * Возвращает все справочники как справочник.
     * @param $query
     * @return array
     * @throws \Exception
     *
     */
    public function getList($query)
    {
        $this->dictionariesListService = $this->sm->get('ListDictionary');
        $results = $this->dictionariesListService->getAllInfo();

        $list = [];
        $id = 0;
        foreach ($results as $row) {
            preg_match ('/(cms_dictionary_)(.+)/', $row['TABLE_NAME'], $matches);
            list(,,$row['TABLE_NAME']) = $matches;
            $list[] = [
                'id' => ++$id,
                'name' => $row['TABLE_COMMENT'],
                'code' => Utilite::underscoreToCamelCase($row['TABLE_NAME'], true)
            ];
        }
        return $list;
    }
}