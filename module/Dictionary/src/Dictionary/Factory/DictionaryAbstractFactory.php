<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 11:52
 */

namespace Dictionary\Factory;

use Dictionary\Service\BaseDictionary;
use Dictionary\Service\DictionaryInterface;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Фабрика для классво вида {name}Dictionary.
 * Class DictionaryAbstractFactory
 * @package Dictionary\Factory
 */
class DictionaryAbstractFactory implements AbstractFactoryInterface
{

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return preg_match('/Dictionary$/', $requestedName);
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $requestedName = '\\Dictionary\\Service\\' . $requestedName;

        if (!class_exists($requestedName)) {
            $service = new BaseDictionary($serviceLocator, $serviceLocator->get($requestedName .
                'TableGateway'));
        } else {
            $service = new $requestedName($serviceLocator, $serviceLocator->get($requestedName .
                'TableGateway'));
        }

        if (!$service instanceof DictionaryInterface) {
            throw new \Exception('Класс ' + $requestedName + 'должен реализовывать DictionaryInterface!');
        }
        return $service;
    }
}