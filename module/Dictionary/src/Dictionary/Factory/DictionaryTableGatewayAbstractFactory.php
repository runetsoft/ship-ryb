<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 01.05.2017
 * Time: 11:52
 */

namespace Dictionary\Factory;

use Dictionary\Model\Dictionary;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Фабрика для классво вида {name}DictionaryTableGateway.
 * Class DictionaryTableGatewayAbstractFactory
 * @package Dictionary\Factory
 */
class DictionaryTableGatewayAbstractFactory implements AbstractFactoryInterface
{

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return preg_match('/DictionaryTableGateway$/', $requestedName);
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        preg_match('/([A-Za-z]+)DictionaryTableGateway$/', $requestedName, $matches, PREG_OFFSET_CAPTURE, 0);
        list(, $serviceName) = $matches;

        $table = preg_replace('/[A-Z]/', '_$0', current($serviceName));
        $table = strtolower($table);

        $dbAdapter = $serviceLocator->get('ShipRybAdapter');
        $features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Dictionary());
        return new TableGateway('cms_dictionary' . $table, $dbAdapter, $features, $resultSetPrototype);

    }
}