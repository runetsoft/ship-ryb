<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 14:08
 */

namespace Dictionary\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Dictionary\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController($parentLocator);
    }
}