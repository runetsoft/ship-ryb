<?php

namespace Dictionary\Controller;

use Application\Utilities\FieldType;
use Application\Utilities\Utilite;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

class MainController extends AbstractActionController
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var \Dictionary\Model\DictionaryTable
     */
    private $dictionaryService;

    /**
     * MainController constructor.
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    /**
     * Получаем данные из справочника
     */
    public function getListAction()
    {
        try {
            $query = $this->request->getQuery();
            $dictionary = $query->get('dictionary');

            if ($dictionary) {
                $this->dictionaryService = $this->sm->get($dictionary . 'Dictionary');
                $data = $this->dictionaryService->getData($query);
            }
            else
            {
                $data = null;
            }

            return new JsonModel(array("success" => true, "data" => $data));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function getDictionaryListAction()
    {
        try {
            $query = $this->request->getQuery();
            $this->dictionaryService = $this->sm->get($query->get('dictionary') . 'Dictionary');

            $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit', 'sort')));
            unset($filter['_dc']);
            unset($filter['dictionary']);

            list($results, $total) = $this->dictionaryService->getAllInfoPaginator((int)$query->get('page'), (int)$query->get('limit'), $filter, $query->get('sort'));

            $list = array();
            foreach ($results as $row) {
                $list[] = $row;
            }

            return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *сохранение новых значений для текущего справочника
     */
    public function saveNewDictionaryValueAction()
    {
        try {
            $data = array_filter((array)Json::decode($this->request->getPost('data')));
            $this->dictionaryService = $this->sm->get($this->request->getPost('dictionary') . 'Dictionary');
            $res = $this->dictionaryService->add($data);
            return new JsonModel(array("success" => true, "request" => $res));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Получение таблиц справочников
     *
     * @return JsonModel
     */
    public function getDictionariesAction()
    {
        try {
            $query = $this->request->getQuery();
            $this->dictionaryService = $this->sm->get('ListDictionary');

            $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit', 'sort')));
            $filter['!TABLE_NAME'] = 'cms_dictionary_good_type';
            unset($filter['_dc']);

            list($results, $total) = $this->dictionaryService->getAllInfoPaginator((int)$query->get('page'), (int)$query->get('limit'), $filter, $query->get('sort'));

            $list = array();
            foreach ($results as $row) {
                preg_match ('/(cms_dictionary_)(.+)/', $row['TABLE_NAME'], $matches);
                list(,,$row['TABLE_NAME']) = $matches;
                $list[] = $row;
            }

            return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Обновление комментариев к таблице
     *
     * @return JsonModel
     */
    public function updateTableCommentAction()
    {
        try {
            $data = (array)Json::decode($this->request->getPost('data'));

            $data['tableName'] = "cms_dictionary_".$data['tableName'];

            $this->dictionaryService = $this->sm->get('DictionaryUtils');
            $result = $this->dictionaryService->changeTableComment($data);

            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }

    /**
     * Добавление новых таблиц
     *
     * @return JsonModel
     */
    public function addTableAction()
    {
        try {
            $data = (array)Json::decode($this->request->getPost('data'));

            $data['valuesTableName'] = Utilite::getTableFullName(FieldType::DICTIONARYMULTISELECT . "_" . trim($data['tableName']));
            $data['tableName'] =  Utilite::getTableFullName(FieldType::DICTIONARY . "_" . trim($data['tableName']));

            $this->dictionaryService = $this->sm->get('DictionaryUtils');
            $result = $this->dictionaryService->addTable($data);

            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Добавление привязки в словарях
     *
     * @return JsonModel
     */
    public function addDictionaryBindAction()
    {
        try {
            $data = (array)Json::decode($this->request->getPost('data'));

            $data['parent_table_name'] = "cms_dictionary_".$data['parent_table_name'];
            $data['table_name'] = "cms_dictionary_".$data['table_name'];

            $this->dictionaryService = $this->sm->get('DictionaryUtils');
            $result = $this->dictionaryService->addBind($data);

            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Получение привязки в словаре
     *
     * @return JsonModel
     */
    public function getDictionaryBindAction()
    {
        try {
            $table = Json::decode($this->request->getPost('table'));
            $table = "cms_dictionary_".$table;

            $this->dictionaryService = $this->sm->get('DictionaryUtils');
            $result = $this->dictionaryService->getBind($table);
            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Удаление значений словаря
     *
     * @return JsonModel
     */
    public function deleteItemAction()
    {
        try {
            $data = (array)Json::decode($this->request->getPost('data'));

            $this->dictionaryService = $this->sm->get($data['dictionary'] . 'Dictionary');
            $result = $this->dictionaryService->deleteItem($data['id']);
            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function updateValueAction()
    {
        try {
            $data = (array)Json::decode($this->request->getPost('data'));
            $dictionary = Json::decode($this->request->getPost('dictionary'));

            $this->dictionaryService = $this->sm->get($dictionary . 'Dictionary');
            $result = $this->dictionaryService->updateValue($data);
            return new JsonModel(array("success" => true, "data" => $result));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }
}