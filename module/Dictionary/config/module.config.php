<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Dictionary\Controller\MainController' => 'Dictionary\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'dictionary' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/dictionary[/:action].json',
                    'defaults' => array(
                        'controller' => 'Dictionary\Controller\MainController'
                    ,
                    ),
                ),
            )
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);