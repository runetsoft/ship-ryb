<?php
namespace Log;

use Cron\Service\Run;
use Log\Model\ImportLog;
use Log\Model\ImportLogTable;
use Log\Model\SupplierPriceLog;
use Log\Model\SupplierPriceLogTable;
use Log\Service\Cron;
use Log\Service\PriceLog;
use Zend\Db\RowGateway\RowGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/**
 * Class Module
 *
 * @package Log
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Log\Model\ImportLogTable' => function ($sm) {
                    $priceTableGateway = $sm->get('ImportLogTableGateway');
                    $table = new ImportLogTable($sm, $priceTableGateway);
                    return $table;
                },
                'ImportLogTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ImportLog());
                    return new TableGateway('cms_import_log', $dbAdapter, $features, $resultSetPrototype);
                },
                'ImportLogRow' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    return new RowGateway('id', 'cms_import_log', $dbAdapter);
                }
            ),
            'aliases' => array(),
            'abstract_factories' => array(),
            'shared' => array()
        );
    }
}