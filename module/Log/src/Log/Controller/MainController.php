<?php
namespace Log\Controller;

use Log\Model\ImportLogTable;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;


class MainController extends AbstractActionController
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $sm;
    /**
     * @var ImportLogTable
     */
    private $importLogTable;

    /**
     * MainController constructor.
     * @param $sm
     * @param $importLogTable
     */
    public function __construct(
        $sm,
        $importLogTable
    )
    {
        $this->sm = $sm;
        $this->importLogTable = $importLogTable;
    }

    /**
     * Список экспортов.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function listAction()
    {
        $query = $this->request->getQuery();

        $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit', '_dc', 'sort')));
        list($results, $total) = $this->importLogTable->getAllInfo(
            (int)$query->get('page'),
            (int)$query->get('limit'),
            $filter,
            $query->get('sort')
        );

        $list = array();
        foreach ($results as $row) {
            $list[] = $row;
        }

        return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
    }

    /**
     * Список экспортов.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function logImportAction()
    {
        $query = $this->request->getQuery();
        list($results, $total) = $this->importLogTable->getAllInfo(
            (int)$query->get('page'),
            (int)$query->get('limit'),
            array ('id' => (int)$query->get('importLogId')),
            $query->get('sort')
        );

        $list = array();
        foreach ($results as $row) {
            $row['detail'] = Json::decode( $row['detail']);
            $row['success']  = $row['success'] ? 'Без ошибок' : 'Есть ошибки';
            $row['warning']  = !$row['warning'] ? 'Без изменений' : 'Есть изменения в структуре';
            $list[] = $row;
        }

        return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
    }




}