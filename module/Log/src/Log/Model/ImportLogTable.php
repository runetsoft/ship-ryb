<?php

namespace Log\Model;

use Application\Model\BaseModelTable;
use Ddeboer\Imap\Exception\Exception;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;
use Zend\Session\Container;

/**
 * Class ImportLogTable работа с таблицами лога.
 *
 * @package Export\Model
 */
class ImportLogTable extends BaseModelTable
{
    protected $dbAdapter;
    protected $sm;
    protected $logTableGateway;

    /**
     * ImportLogTable constructor.
     * @param $sm
     * @param \Zend\Db\TableGateway\TableGateway $logTableGateway
     */
    public function __construct($sm, TableGateway $logTableGateway)
    {
        $this->dbAdapter = $logTableGateway->getAdapter();
        $this->sm = $sm;
        $this->logTableGateway = $logTableGateway;
    }

    /**
     * Получает список импортов
     *
     * @param $page
     * @param $limit
     * @param $filter
     * @param $sort
     * @return array
     */
    public function getAllInfo($page, $limit, $filter, $sort)
    {
        $log = new ImportLog();
        $logColumns = $log->getColumns();

        $select = $this->logTableGateway->getSql()->select()
            ->columns($logColumns)
            ->join(
                'cms_supplier_price_import_file',
                'cms_supplier_price_import_file.id = ' . $this->logTableGateway->getTable() . '.supplier_price_import_file_id',
                array('fileMask' => 'filename_mask'),
                Select::JOIN_INNER
            )
            ->join(
                'cms_supplier_price_import',
                'cms_supplier_price_import_file.supplier_price_import_id = cms_supplier_price_import.id',
                array('supplier_price_import_id' => 'id', 'importInfo' => 'import_info'),
                Select::JOIN_INNER
            )
            ->join(
                'cms_provider',
                'cms_supplier_price_import.supplier_id = cms_provider.id',
                array('supplierName' => 'provider'),
                Select::JOIN_INNER
            );

        if ($filter['supplierName']) {
            $select->where(array('cms_supplier_price_import.supplier_id =?' => $filter['supplierName']));
            unset($filter['supplierName']);
        }

        if ($filter['fileMask']) {
            $select->where(array('fileMask LIKE ?' => $filter['fileMask']));
            unset($filter['fileMask']);
        }

        $this->setFilter($select, $filter, $this->logTableGateway->getTable());

        if ($sort && $sort->property) {
            $select->order(array(
                    $sort->property . ' ' . $sort->direction
                )
            );
        } else {
            $select->order(array(
                    'date_import DESC'
                )
            );
        }
        $statement = $this->logTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);

        return array($paginator, $resultSet->count());
    }
}