<?php
namespace Log\Model;

use Application\Model\BaseModel;

/**
 * Class SupplierPriceExport Таблица cms_supplier_price_export
 * @package SupplierPriceExport\Model
 */
class ImportLog extends BaseModel
{
    /**
     * @description "ID"
     */
    public $id;
    /**
     * @description "ID файла импорта"
     */
    public $supplier_price_import_file_id;
    /**
     * @description "Дата импорта"
     */
    public $date_import;
    /**
     * @description "Успех"
     */
    public $success;
    /**
     * @description "Детально"
     */
    public $detail;
    /**
     * @description "Внимание!"
     */
    public $warning;

}