<?php

namespace Log\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Log\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController($serviceLocator->getServiceLocator(),
            $parentLocator->get('Log\Model\ImportLogTable')
        );
    }
}