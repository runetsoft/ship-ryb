<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Log\Controller\MainController' => 'Log\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'log' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/log[/:action].json',
                    'defaults' => array(
                        'controller' => 'Log\Controller\MainController'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        )
    )
);