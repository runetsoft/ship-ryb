<?php
namespace Good\Controller;

use Application\Utilities\BaseConstant;
use Application\Utilities\DateFormat;
use Application\Utilities\Utilite;
use Ddeboer\Imap\Exception\Exception;
use Good\Model\GoodAttributesTable;
use Good\Model\GoodTypeStructure;
use Good\Service\GoodType;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class MainController extends AbstractActionController
{
    const IMAGES_UPLOADS_FOLDER = '/files/downloads/';
    /**
     * @var \Good\Model\GoodTable
     */
    protected $goodTable;
    protected $goodDiskTable;
    protected $goodTypeStructureTable;
    protected $goodTyreTable;
    protected $goodTyreMotoTable;
    protected $attributesTable;
    /**
     * @var GoodType
     */
    protected $goodTypeService;
    protected $sm;

    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    public function __construct(
        $sm, $goodTable, $goodDiskTable, $goodTyreTable, $goodTyreMotoTable, $goodTypeStructureTable, $attributesTable,
        $goodTypeService
    )
    {
        $this->sm = $sm;
        $this->goodTable = $goodTable;
        $this->goodDiskTable = $goodDiskTable;
        $this->goodTyreTable = $goodTyreTable;
        $this->goodTyreMotoTable = $goodTyreMotoTable;
        $this->goodTypeStructureTable = $goodTypeStructureTable;
        $this->attributesTable = $attributesTable;
        $this->goodTypeService = $goodTypeService;
        $this->request = $this->getRequest();
    }

    /**
     * Список товаров.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function listAction()
    {
        try {
            $combo = false;
            $query = $this->request->getQuery();

            // @TODO Это можно улучшить.
            if (!empty($query->{'=type'}) && $query->{'=type'} == Utilite::GOOD_NO_TYPE_ID) {
                $query->set('=type', 0);
            } elseif (!empty($query->{'type'}) && $query->{'type'} == Utilite::GOOD_NO_TYPE_ID) {
                $query->set('type', 0);
            }

            $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit')));
            if (!empty($filter['combo'])) {
                $combo = true;
                unset($filter['combo']);
            }

            list($results, $total) = $this->goodTable->getGoodsList((int)$query->get('page'), (int)$query->get('start'),
                (int)$query->get('limit'), $filter, $query->get('sort'));

            $goodsList = iterator_to_array($results);
            $goodIdsList = array_column($goodsList, 'id');

            $priceCounts = array();
            if (!empty($goodIdsList)) {
                $priceCountsResultSet = $this->goodTable->getPriceCountsByGoodIds($goodIdsList);
                foreach (iterator_to_array($priceCountsResultSet) as $item) {
                    $priceCounts[$item['good_id']] = $item['price_cnt'];
                }
            }
            $list = array();
            foreach ($goodsList as $row) {
                $row['good_url'] = rawurlencode($row['good']);
                $row['price_cnt'] = $priceCounts[$row['id']];
                $list[] = $row;
            }
            // TODO Надо понять надо ли это здесь.
            if ($combo && empty($filter['query'])) {
                $list = array_merge(array(
                    array(
                        'id' => 'null',
                        'good' => 'без соответствия'
                    ),
                    array(
                        'id' => 'not_null',
                        'good' => 'с соответствием')
                ), $list);
            }
            return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));

        } catch (\Throwable $ex) {
            return new JsonModel(array("success" => false, "message" => $ex->getMessage()));
        }
    }

    /**
     * Информация о Диске.
     *
     * @return \Zend\View\Model\JsonModel
     */

    public function getWheelInfoAction()
    {
        $query = $this->request->getQuery();
        $goodId = $query->get('goodId');
        $array = $this->goodDiskTable->getWheelInfo($goodId);
        try {
            return new JsonModel(array("success" => true, "data" => $array));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация о Диске.
     *
     * @return \Zend\View\Model\JsonModel
     */

    public function getGoodInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $goodId = $query->get('goodId');
            $goodDataResult = $this->goodTable->fetch(array('id' => $goodId));
            $goodData = $goodDataResult->current()->toArray();
            $goodTypeId = (empty($goodData['type'])) ? $query->get('goodType') : $goodData['type'];
            if (empty($goodTypeId)) {
                throw new \Exception('Не задан ID типа товара!');
            }
            $this->attributesTable->setGoodType($goodTypeId);
            $attributesData = $this->attributesTable->getAllDataOneRow(array('good_id' => $goodId));
            return new JsonModel(array("success" => true, "data" => array_merge($goodData, $this->goodTypeService->prepareDataForLoad($attributesData))));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация о Диске.
     *
     * @return \Zend\View\Model\JsonModel
     */

    public function getGoodTypeFormDataAction()
    {
        try {
            $query = $this->request->getQuery();
            $goodTypeId = $query->get('goodTypeId');
            $goodId = $query->get('goodId');
            $goodData = $this->goodTable->fetch(array('id' => $goodId));
            $goodData = current($goodData->toArray());
            if (empty($goodTypeId)) {
                $goodTypeId = intval($goodData['type']);
            }
            $responseData = $this->goodTypeStructureTable->getInfoByTypeId($goodTypeId)->current();
            if (!empty($responseData)) {
                $responseData['brandClass'] = Utilite::underscoreToCamelCase($responseData['good_type_code'] . '_brand', true);
                $responseData['modelClass'] = Utilite::underscoreToCamelCase($responseData['good_type_code'] . '_model', true);
            }
            return new JsonModel(array("success" => true, "data" => $responseData));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация о Шине.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function getTireInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $goodId = $query->get('goodId');
            $array = $this->goodTyreTable->getTireInfo($goodId);
            return new JsonModel(array("success" => true, "data" => $array));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация о Шине мото.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function getTireMotoInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $goodId = $query->get('goodId');
            $array = $this->goodTyreMotoTable->getTireMotoInfo($goodId);
            return new JsonModel(array("success" => true, "data" => $array));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *  Удаление товара.
     *
     * @return JsonModel
     *
     */

    public function itemDeleteAction()
    {
        $query = $this->request->getQuery();
        $goodId = $query->get('goodId');
        try {
            $results = $this->goodTable->itemDelete($goodId);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Групповое удаление товаров.
     *
     * @return JsonModel
     *
     */
    public function deleteAction()
    {
        $query = $this->request->getQuery();
        $goodIds = $query->get('goodIds');
        $goodIds = empty($goodIds) ? [] : json_decode($goodIds);
        try {
            $results = $this->goodTable->delete($goodIds);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Информация по товару.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function goodInfoAction()
    {
        try {
            $query = $this->request->getQuery();

            $results = $this->goodTable->getGoodInfo((int)$query->get('goodId'));
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Очистить все МРЦ.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function clearMrcAction()
    {
        try {
            $results = $this->goodTable->clearMrc();
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Обновление МРЦ.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function updateMrcAction()
    {
        try {
            $file = $this->request->getFiles('file');
            $type = $this->request->getPost('type');
            $results = $this->goodTable->updateMrc($file, $type);
            return new JsonModel(array("success" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Разбирает все товары из базы на характеристики.
     *
     * @return JsonModel
     */
    public function explodeGoodsAction()
    {
        $results = $this->sm->get('ExplodeData')->process();

        return new JsonModel(array("success" => true, "data" => $results));
    }

    /**
     *
     * Разбирает конкретный товар и сохраняет его.
     *
     * @return JsonModel
     */
    public function explodeGoodAction()
    {
        try {
            $query = $this->request->getQuery();

            $good = $this->goodTable->getGoodInfoByFilter(array('=good' => $query->get('goodName')));
            if ($good->current()) {
                return new JsonModel(array("success" => true, "data" => array(
                    'id' => $good->current()['id'],
                    'goodType' => $good->current()['type'],
                    'isNotNew' => true
                )));
            }

            $id = $this->goodTable->updateInfo(
                array(
                    'good' => $query->get('goodName'),
                    'zarticle' => $query->get('zarticle'),
                    'rprice' => (empty($query->get('rprice'))) ? null : $query->get('rprice'),
                )
            );
            if (!$id) {
                throw new \Exception('Не удалось сохранить товар!');
            }
            $results = $this->sm->get('ExplodeData')->process($id);
            return new JsonModel(array("success" => true, "data" => array(
                'id' => $id,
                'goodType' => $results['type']
            )));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /*
     * Обновление данных продукта
     *
     * @return JsonModel
     */
    public function updateGoodAction()
    {
        try {
            $query = $this->request->getPost('data');
            $data = Json::decode($query);
            $file = $this->request->getFiles('photofilefield');
            $photo = $this->goodTable->uploadPhoto($file);
            $data->photo = $photo ? self::IMAGES_UPLOADS_FOLDER . $photo : $data->photo;
            $data->cargo = $data->cargo == 'on' ? 1 : 0;

            $data->rprice = (!empty($data->rprice) || $data->rprice === 0 || $data->rprice === '0') ? $data->rprice : null;
            $data->zarticle = (string)$data->zarticle;
            $data->dt_update = (new \DateTime())->format(DateFormat::STANDARD);
            $id = $this->goodTable->updateInfo((array)$data);
            unset($data->id);
            $data->good_id = $id;
            $this->saveGoodAttributes((array)$data);
            return new JsonModel(array("success" => true, "data" => $data));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    private function saveGoodAttributes(array $data)
    {
        if (!empty($data['type'])) {
            $this->attributesTable->setGoodType($data['type']);
            $this->attributesTable->updateItem($data);
        } else {
            throw new \Exception('Не выбрано тип товара!');
        }
    }

    public function updateGoodPhotoByPriceAction()
    {
        try {
            $priceId = $this->request->getPost('priceId');
            $this->goodTable->updateGoodPhotoByPrice($priceId);
            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function addNewGoodTypeAction()
    {
        try {
            $params = json_decode($this->request->getPost('data'));
            if (empty($params->good_type)) {
                throw new \Exception('Тип товара обязательно!');
            }
            if (empty($params->columns)) {
                throw new \Exception('Колонки таблицы отсутствуют!');
            }
            $this->goodTypeService->addNewGoodType($params);
            return new JsonModel(array("success" => true, "message" => "Новый тип товара добавлено успешно!"));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function getGoodTypeStructureAction()
    {
        try{
            $params = $this->request->getQuery();
            if (empty($params->good_type_id)) {
                throw new \Exception('Id типа товара обязательно!');
            }

            $result = $this->goodTypeService->getGoodTypeInfoLikeExtJs($params->good_type_id);

            return new JsonModel(array("success" => true, "data" => $result, "totalCount" => count($result)));

        } catch (\Throwable $ex) {
            return new JsonModel(array("success" => false, "message" => $ex->getMessage()));
        }

    }

    public function updateGoodTypeAction()
    {
        try {
            $params = json_decode($this->request->getPost('data'));
            if (empty($params->good_type)) {
                throw new \Exception('Tип товара обязательно!');
            }
            if (empty($params->good_type_id)) {
                throw new \Exception('Id типа товара обязательно!');
            }
            if (empty($params->columns)) {
                throw new \Exception('Колонки таблицы отсутствуют!');
            }
            $this->goodTypeService->updateGoodTypeTable($params);
            return new JsonModel(array("success" => true, "message" => "Новый тип товара добавлено успешно!"));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

}