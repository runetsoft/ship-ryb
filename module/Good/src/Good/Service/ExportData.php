<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 09.06.2017
 * Time: 15:54
 */

namespace Good\Service;


use Export\Service\ExportInterface;

class ExportData implements ExportInterface
{
    protected $sm;

    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    public function getExportDataIterator($filter = null)
    {
        /**
         * @var $priceTable \Price\Model\PriceTable;
         */
        $priceTable = $this->sm->get('Price\Model\PriceTable');
        return current($priceTable->getAllInfo($filter, null));
    }
}