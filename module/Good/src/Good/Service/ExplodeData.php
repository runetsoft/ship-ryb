<?php

namespace Good\Service;

use Application\Utilities\UtilitiesGoodType;
use Dictionary\Service\TyreAxisDictionary;
use Good\Model\GoodTyreMotoTable;
use Good\Model\GoodTyreTable;
use Zend\Db\Sql\Select;


/**
 * Created by PhpStorm.
 * User: yu.khristodorov
 * Date: 19.05.2017
 * Time: 17:28
 */
class ExplodeData
{
    const DICTIONARY_NAME_DISK_BRAND = 'DiskBrand';
    const DICTIONARY_NAME_DISK_MODEL = 'DiskModel';
    const DICTIONARY_NAME_DISK_TYPE = 'DiskType';
    const DICTIONARY_NAME_TYRE_BRAND = 'TyreBrand';
    const DICTIONARY_NAME_TYRE_MODEL = 'TyreModel';
    const DICTIONARY_NAME_TYRE_RUNFLAT = 'TyreRunflat';
    const DICTIONARY_NAME_TYRE_SEASON = 'TyreSeason';
    const DICTIONARY_NAME_TYREMOTO_BRAND = 'TyremotoBrand';
    const DICTIONARY_NAME_TYREMOTO_MODEL = 'TyremotoModel';
    const DICTIONARY_NAME_TYREMOTO_AXIS = 'TyremotoAxis';

    /**
     * Регулярка для парсинга автомобильных шин.
     */
    const TYRE_REG = '/^(%s){1}\s(.*?)\s([0-9,]{1,5})\/?([0-9,]{0,7})([RB]{1})([0-9C]{1,3})\s([0-9\/]{0,7})([A-Z]{0,1})(\sXL)?\s?(?=(?:(.*)(%s))?(.*)(\s[TLT\/]{0,5})\s(%s))/i';

    /**
     * Регулярка для парсинга мото шин.
     */
    const TYREMOTO_REG = '/^(%s){1}\s(.*?)\s([0-9]{1,3})\/([0-9]{1,3})([-A-Z]{1})([0-9]{1,2})\s([0-9]{1,3})([A-Z]{1})(.*?)([TLT\/]{1,5})\s(Rear|Front|F\/R)/i';

    /**
     * Регулярка для парсинга дисков.
     */
    const DISK_REG = '/^(%s){1}\s(.*?)\s([0-9,]{1,5})x([0-9,]{1,5})\s([0-9]{1,3})\/([0-9,]{1,6})\sET(-?[0-9,]{1,7})\sd([0-9,]{1,5})(.*?)(%s)/i';

    protected $sm;
    protected $goodTable;
    protected $goodDiskTable;
    /**
     * @var GoodTyreTable
     */
    protected $goodTyreTable;
    /**
     * @var GoodTyreMotoTable
     */
    protected $goodTyreMotoTable;
    protected $dictionaryService;
    protected $dictionaryModels;
    protected $dictionaries;

    protected $tyreReg;
    protected $tyremotoReg;
    protected $diskReg;

    protected $explodeGoodTypes = [
        'disk',
        'tyre',
        'tyremoto'
    ];
    protected $models = [];

    public function __construct($sm, $goodTable, $goodDiskTable, $goodTyreTable, $goodTyreMotoTable)
    {
        $this->sm = $sm;
        $this->goodTable = $goodTable;
        $this->goodDiskTable = $goodDiskTable;
        $this->goodTyreTable = $goodTyreTable;
        $this->goodTyreMotoTable = $goodTyreMotoTable;
        $this->tyreReg = $this->getTyreReg();
        $this->tyremotoReg = $this->getTyremotoReg();
        $this->diskReg = $this->getDiskReg();

       $this->setGoodModels();
    }

    protected function setGoodModels()
    {
        foreach ($this->explodeGoodTypes as $goodType) {
            $dictionaryService = $this->sm->get(ucfirst($goodType) . 'ModelDictionary');
            $dictionaryList = $dictionaryService->getList();
            foreach ($dictionaryList as $item) {
                $this->models[$goodType][$item['name']] = $item['id'];
            }
        }
    }

    /**
     * Форматирует строку в номер для вставки в decimal(5,2).
     *
     * @param $number
     * @return string
     */
    public function formatNumberForDecimal($number)
    {
        $number = str_replace(',', '.', $number);
        return number_format((float)$number, 2, '.', '');
    }

    /**
     * карго или нет.
     *
     * @param $number
     * @return string
     */
    public function getCargo($number)
    {
        return strpos($number, 'C') ? 1 : 0;
    }

    /**
     * Выводим массив любого справочника, где ключ это $param
     *
     * @param $dictionary
     * @param string $param
     * @return array
     */
    public function getDictionaryArray($dictionary, $param = 'id', $extra = null)
    {
        if (is_array($this->dictionaries[$dictionary])) return $this->dictionaries[$dictionary];
        $this->dictionaries[$dictionary] = array();

        $dictionaryService = $this->sm->get($dictionary . 'Dictionary');
        $array = $dictionaryService->getList();
        foreach ($array as $item) {
            if (!$extra) {
                $this->dictionaries[$dictionary][$item['name']] = $item[$param];
            } else {
                $this->dictionaries[$dictionary][$item['name']][$item[$extra]] = $item[$param];
            }
        }
        return $this->dictionaries[$dictionary];
    }

    /**Создания нового элемента справочника в БД
     *
     * @param $dictionary
     * @param $name
     * @param $brandId
     * @return mixed
     */
    public function getModelByName($dictionary, $name, $brandId)
    {
        if (!is_array($this->dictionaries[$dictionary]))
            $this->dictionaries[$dictionary] = $this->getDictionaryArray($dictionary, 'id', 'parent_id');

        if ($this->dictionaries[$dictionary][$name][$brandId]) return $this->dictionaries[$dictionary][$name][$brandId];

        if (!$this->dictionaryModels[$dictionary . 'Dictionary'])
            $this->dictionaryModels[$dictionary . 'Dictionary'] = $this->sm->get($dictionary . 'Dictionary');

        $dictionaryModel = $this->dictionaryModels[$dictionary . 'Dictionary'];
        $dictionaryModel->add(
            array(
                'name' => $name,
                'parent_id' => $brandId
            )
        );
        $model = $dictionaryModel->getList(array(
            'filter' => array('name' => $name, 'parent_id' => $brandId)
        ));
        $model = current($model);
        $this->dictionaries[$dictionary][$name][$model['parent_id']] = $model['id'];
        return $this->dictionaries[$dictionary][$name][$model['parent_id']];
    }

    /**
     * Обработка товаров. Парсит названия и заполняет характеристиками.
     *
     */

    function process($id = null)
    {
        set_time_limit(0);
        $type = UtilitiesGoodType::NO_TYPE;

        $select = $this->sm->get('goodTableGateway')->getSql()->select()
            ->columns(array('id', 'good', 'type'))
            ->join('cms_good_tyre', 'cms_good_tyre.good_id = ' . $this->sm->get('goodTableGateway')->getTable() . '.id',
                array('good_tyre_id' => 'good_id'),
                Select::JOIN_LEFT)
            ->join('cms_good_tyremoto', 'cms_good_tyremoto.good_id = ' . $this->sm->get('goodTableGateway')->getTable() . '.id',
                array('good_tyre_id' => 'good_id'),
                Select::JOIN_LEFT)
            ->join('cms_good_disk', 'cms_good_disk.good_id = ' . $this->sm->get('goodTableGateway')->getTable() . '.id',
                array('good_disk_id' => 'good_id'),
                Select::JOIN_LEFT);
        if ($id) {
            $select->where(array('id = ?' => $id));
        }

        //@todo Надо бы переделать на получение связанного объекта.
        $statement = $this->sm->get('goodTableGateway')->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $i = 0;
        $tyre = 0;
        $wheels = 0;
        foreach ($resultSet as $res) {
            preg_match_all($this->diskReg, $res['good'], $diskMatches, PREG_SET_ORDER, 0);
            // Если это диск добавляем его свойства.
            if ($diskMatches) {
                $match = current($diskMatches);
                $brand = $this->getDictionaryIdByValue(self::DICTIONARY_NAME_DISK_BRAND, trim($match['1']));
                $this->goodDiskTable->updateInfo(
                    array(
                        'id' => $res['id'],
                        'brand' => $brand,
                        'model' => $this->setModelValueIfNotExist('disk', trim($match['2']), $brand),
                        'width' => $this->formatNumberForDecimal($match['3']),
                        'diameter' => trim($match['4']),
                        'hole_num' => trim($match['5']),
                        'diameter_hole' => $this->formatNumberForDecimal(trim($match['6'])),
                        'overhand' => $this->formatNumberForDecimal(trim($match['7'])),
                        'diameter_nave' => $this->formatNumberForDecimal(trim($match['8'])),
                        'color' => trim($match['9']),
                        'dictionary_disk_type' => $this->getDictionaryIdByValue('DiskType', trim($match['10'])),
                    )
                );
                $this->sm->get('goodTableGateway')->update(array('type' => UtilitiesGoodType::DISK), array('id' => $res['id']));
                $type = UtilitiesGoodType::DISK;
                $wheels++;
            } else {
                preg_match_all($this->tyremotoReg, $res['good'], $tyresMotoMatches, PREG_SET_ORDER, 0);
                // Если это Мотошина добавляем его свойства.
                if ($tyresMotoMatches) {
                    $match = current($tyresMotoMatches);
                    $brand = $this->getDictionaryIdByValue(self::DICTIONARY_NAME_TYREMOTO_BRAND, trim($match['1']));
                    $this->goodTyreMotoTable->updateInfo(
                        array(
                            'id' => $res['id'],
                            'brand' => $brand,
                            'model' => $this->setModelValueIfNotExist('tyremoto', trim($match['2']), $brand),
                            'width' => $this->formatNumberForDecimal($match['3']),
                            'height' => $this->formatNumberForDecimal($match['4']),
                            'dictionary_tyremoto_construction' => $this->getDictionaryIdByValue('TyremotoConstruction', trim($match['5'])),
                            'diameter' => $this->formatNumberForDecimal($match['6']),
                            'index_load' => $match['7'],
                            'index_speed' => $match['8'],
                            'features' => $match['9'],
                            'dictionary_tyremoto_camers' => $this->getDictionaryIdByValue('TyremotoCamers', trim($match['10'])),
                            'dictionary_tyremoto_axis' => $this->getDictionaryIdByValue('TyremotoAxis', trim($match['11'])),
                        )
                    );
                    $this->sm->get('goodTableGateway')->update(array('type' => UtilitiesGoodType::TYRE_MOTO), array('id' => $res['id']));
                    $type = UtilitiesGoodType::TYRE_MOTO;
                } else {
                    preg_match_all($this->tyreReg, $res['good'], $tyresMatches, PREG_SET_ORDER, 0);
                    // Если это Автошина добавляем его свойства.
                    if ($tyresMatches) {
                        $match = current($tyresMatches);
                        $brand = $this->getDictionaryIdByValue(self::DICTIONARY_NAME_TYRE_BRAND, trim($match['1']));
                        $this->goodTyreTable->updateInfo(
                            array(
                                'id' => $res['id'],
                                'brand' => $brand,
                                'model' => $this->setModelValueIfNotExist('tyre', trim($match['2']), $brand),
                                'width' => $this->formatNumberForDecimal($match['3']),
                                'height' => $this->formatNumberForDecimal($match['4']),
                                'dictionary_tyre_construction' => $this->getDictionaryIdByValue('TyreConstruction', trim($match['5'])),
                                'diameter' => $this->formatNumberForDecimal($match['6']),
                                'cargo' => $this->getCargo($match['6']),
                                'index_load' => $match['7'],
                                'index_speed' => $match['8'],
                                'dictionary_tyre_xl' => $this->getDictionaryIdByValue('TyreXl', trim($match['9'])),
                                'features' => trim(trim($match['10']) . ' ' . trim($match['12'])),
                                'dictionary_tyre_runflat' => $this->getDictionaryIdByValue('TyreRunflat', trim($match['11'])),
                                'dictionary_tyre_camers' => $this->getDictionaryIdByValue('TyreCamers', trim($match['13'])),
                                'dictionary_tyre_season' => $this->getDictionaryIdByValue('TyreSeason', trim($match['14'])),
                            )
                        );
                        $this->sm->get('goodTableGateway')->update(array('type' => UtilitiesGoodType::TYRE), array('id' => $res['id']));
                        $type = UtilitiesGoodType::TYRE;
                    } else {
                        // Тут можно добавить еще регулярку другого товара и добавить его, а пока ошибка о том, что товар не распарсен.
                        $data['bad'][] = $res['good'];
                        $i++;
                    }
                }
                $tyre++;
            }
        }

        $data['tyre'] = $tyre;
        $data['wheels'] = $wheels;
        $data['type'] = $type;
        return $data;
    }

    public function getTyreReg()
    {
        return sprintf(
            self::TYRE_REG,
            $this->getDictionaryString(self::DICTIONARY_NAME_TYRE_BRAND),
            $this->getDictionaryString(self::DICTIONARY_NAME_TYRE_RUNFLAT),
            $this->getDictionaryString(self::DICTIONARY_NAME_TYRE_SEASON)
        );
    }

    public function getTyremotoReg()
    {
        return sprintf(
            self::TYREMOTO_REG,
            $this->getDictionaryString(self::DICTIONARY_NAME_TYREMOTO_BRAND)
        );
    }

    public function getDiskReg()
    {
        return sprintf(
            self::DISK_REG,
            $this->getDictionaryString(self::DICTIONARY_NAME_DISK_BRAND),
            $this->getDictionaryString(self::DICTIONARY_NAME_DISK_TYPE)
        );
    }

    private function getDictionaryString($dictionaryName, $field = 'name', $delimiter = "|")
    {
        $this->dictionaryService = $this->sm->get($dictionaryName . 'Dictionary');
        $data = $this->dictionaryService->getList();
        $result = [];
        foreach ($data as $item) {
            $result[] = $item[$field];
        }
        rsort($result);
        return quotemeta(implode($delimiter, $result));
    }

    protected function getDictionaryIdByValue($dictionaryName, $value)
    {
        if (empty($value)) {
            return null;
        }
        $this->dictionaryService = $this->sm->get($dictionaryName . 'Dictionary');
        $dictionary = $this->dictionaryService->getList(['filter' => ['name' => $value]]);
        return (!empty($dictionary)) ? current($dictionary)['id'] : null;
    }

    protected function setModelValueIfNotExist($goodType, $modelValue, $parentId = null)
    {
        $modelValue = trim($modelValue);
        if (in_array($modelValue, array_keys($this->models[$goodType]))) {
            return $this->models[$goodType][$modelValue];
        } else {
            $dictionaryService = $this->sm->get(ucfirst($goodType) . 'ModelDictionary');
            $this->models[$goodType][$modelValue] = $dictionaryService->add(['name' => $modelValue, 'parent_id' => $parentId]);
            return $this->models[$goodType][$modelValue];
        }
    }
}