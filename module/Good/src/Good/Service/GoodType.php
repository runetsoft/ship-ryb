<?php

namespace Good\Service;

use Application\Utilities\FieldType;
use Application\Utilities\Utilite;
use Dictionary\Model\DictionaryTable;
use Dictionary\Service\DictionaryInterface;
use Dictionary\Service\DictionaryUtils;
use Good\Model\GoodAttributesTable;
use Good\Model\GoodTypeStructure;
use Good\Model\GoodTypeStructureTable;
use Good\Model\NullableBoolean;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Ddl;
use Zend\Db\Sql\Ddl\Column;

class GoodType
{
    protected $sm;
    protected $dbAdapter;

    /**
     * @var GoodTypeStructureTable
     */
    protected $goodTypeStructureTable;
    /**
     * @var DictionaryTable
     */
    protected $dictionaryGoodType;
    /**
     * @var DictionaryUtils
     */
    protected $dictionaryUtils;
    /**
     * @var GoodAttributesTable
     */
    protected $goodAttributesTable;
    /**
     * @var \stdClass
     */
    private $config;
    public $mainIntegerColumns;

    public function __construct($sm, $dbAdapter, $goodTypeStructureTable)
    {
        $this->sm = $sm;
        $this->dbAdapter = $dbAdapter;
        $this->goodTypeStructureTable = $goodTypeStructureTable;
        $this->dictionaryUtils = $this->sm->get('DictionaryUtils');
        $this->goodAttributesTable = new GoodAttributesTable($sm);
        $this->mainIntegerColumns = ['good_id' => 'Id товара', 'brand' => 'Бренд', 'model' => 'Модель'];
    }

    private function getGoodTypeTableName()
    {
        return implode('_', array(Utilite::DB_PREFIX, Utilite::GOOD, trim($this->config->good_type)));
    }

    public function addNewGoodType(\stdClass $config)
    {
        $this->config = $config;
        $this->createGoodTypeTable();
    }

    private function createGoodTypeTable()
    {
        // Create the $table
        $sql = new Sql($this->dbAdapter);
        $createTable = $this->setColumns();
        $this->dbAdapter->query(
            $sql->buildSqlString($createTable),
            Adapter::QUERY_MODE_EXECUTE
        );

        $this->dictionaryGoodType = $this->sm->get('GoodTypeDictionary');

        $goodTypeId = $this->dictionaryGoodType->add(
            array(
                'code' => $this->config->good_type,
                'name' => $this->config->good_type_title)
        );

        $goodTypeStructure = new GoodTypeStructure();
        $goodTypeStructure->good_type_id = $goodTypeId;
        $goodTypeStructure->generateStructureFromColumns((object)$this->config->columns);

        $this->goodTypeStructureTable->updateInfo((array)$goodTypeStructure);

        $this->dictionaryUtils->addTable(
            array(
                'tableName' => Utilite::getDictionaryTableName(implode('_', array(trim($this->config->good_type), Utilite::MODEL))),
                'valuesTableName' => Utilite::getTableFullName(implode('_', array(FieldType::DICTIONARYMULTISELECT, trim($this->config->good_type), Utilite::MODEL))),
                'tableComment' => $this->config->good_type_title . ' - модел'
            )
        );
        $this->dictionaryUtils->addTable(
            array(
                'tableName' => Utilite::getDictionaryTableName(implode('_', array(trim($this->config->good_type), Utilite::BRAND))),
                'valuesTableName' => Utilite::getTableFullName(implode('_', array(FieldType::DICTIONARYMULTISELECT, trim($this->config->good_type), Utilite::BRAND))),
                'tableComment' => $this->config->good_type_title . ' - бренд'
            )
        );

        return true;
    }

    public function updateGoodTypeTable(\stdClass $config)
    {
        $this->config = $config;

        // Create the $table
        $sql = new Sql($this->dbAdapter);
        $alterTable = $this->updateColumns();
        $this->dbAdapter->query(
            $sql->buildSqlString($alterTable),
            Adapter::QUERY_MODE_EXECUTE
        );

        $goodTypeStructure = new GoodTypeStructure();
        $goodTypeStructure->good_type_id = $this->config->good_type_id;
        $goodTypeStructure->generateStructureFromColumns((object)$this->config->columns);

        $this->goodTypeStructureTable->updateInfo((array)$goodTypeStructure);

        return true;
    }

    private function setColumns()
    {
        $createTable = new Ddl\CreateTable($this->getGoodTypeTableName());


        foreach ($this->mainIntegerColumns as $key => $comment) {
            $col = new Column\Integer($key, 11);
            $col->setOptions(array('COMMENT' => $comment));
            $createTable->addColumn($col);
        }

        foreach ($this->config->columns as $columnData) {
            switch ($columnData->field_type) {
                // TODO will be implementing
                case FieldType::NUMBERFIELD:
                case FieldType::DICTIONARY:
                    $col = new Column\Integer($columnData->column, 11);
                    break;
                case FieldType::CHECKBOX:
                    $col = new NullableBoolean($columnData->column);
                    break;
                case FieldType::TEXTFIELDNUMBER:
                    $col = new Column\Decimal($columnData->column, 8);
                    $col->setDecimal(2);
                    break;
                default:
                    $col = new Column\Varchar($columnData->column, 255);
                    break;
            }
            $col->setNullable(true);
            $col->setOptions(array('COMMENT' => $columnData->title));
            $createTable->addColumn($col);
        }
        return $createTable;
    }

    private function updateColumns()
    {
        $alterTable = new Ddl\AlterTable($this->getGoodTypeTableName());
        $this->goodAttributesTable->setGoodType($this->config->good_type_id);
        $oldColumns = $this->goodAttributesTable->getColumns();
        $changedColumns = [];
        foreach ($this->config->columns as $columnData) {
            switch ($columnData->field_type) {
                // TODO will be implementing
                case FieldType::NUMBERFIELD:
                case FieldType::DICTIONARY:
                    $col = new Column\Integer($columnData->column, 11);
                    break;
                case FieldType::DICTIONARYMULTISELECT:
                    $col = new Column\Varchar($columnData->column, 5000);
                    $col->setDefault('[]');
                    break;
                case FieldType::CHECKBOX:
                    $col = new NullableBoolean($columnData->column);
                    break;
                case FieldType::TEXTFIELDNUMBER:
                    $col = new Column\Decimal($columnData->column, 8);
                    $col->setDecimal(2);
                    break;
                default:
                    $col = new Column\Varchar($columnData->column, 255);
                    break;
            }
            $col->setNullable(true);
            $col->setOptions(array('COMMENT' => $columnData->title));
            if (in_array($columnData->column, $oldColumns)) {
                $alterTable->changeColumn($columnData->column, $col);
            } else {
                $alterTable->addColumn($col);
            }
            $changedColumns[] = $columnData->column;
        }
        $allColumns = array_merge($changedColumns, array_keys($this->mainIntegerColumns));
        foreach ($oldColumns as $item) {
            if (!in_array($item, $allColumns)) {
                $alterTable->dropColumn($item);
            }
        }
        return $alterTable;
    }

    public function getGoodTypeInfoLikeExtJs($goodTypeId)
    {
        $this->goodAttributesTable->setGoodType($goodTypeId);
        $columns = $this->goodAttributesTable->getColumnsAllInfo();
        return $this->prepareColumnDataLikeExtJs($columns);
    }

    protected function prepareColumnDataLikeExtJs($columnData)
    {
        $result = [];
        $mainColumns = array_keys($this->mainIntegerColumns);
        foreach ($columnData as $item) {
            if (in_array($item['Field'], $mainColumns)) {
                continue;
            }
            $row = [
                'title' => $item['Comment'],
                'column' => $item['Field']
            ];
            switch ($item['Type']) {
                case (preg_match('/^tinyint.*/', $item['Type']) ? true : false) :
                    $row['field_type'] = FieldType::CHECKBOX;
                    break;
                case (preg_match('/^decimal.*/', $item['Type']) ? true : false) :
                    $row['field_type'] = FieldType::TEXTFIELDNUMBER;
                    break;
                case (preg_match('/^int.*/', $item['Type']) ? true : false) :
                    $row['field_type'] = FieldType::NUMBERFIELD;
                    break;
                default :
                    $row['field_type'] = FieldType::TEXTFIELD;
            }
            preg_match('/^(' . implode('|', [FieldType::DICTIONARY, FieldType::DICTIONARYMULTISELECT]) . ')_(.*)/',
                $item['Field'], $matches);
            if (!empty($matches)) {
                list(, $fieldType, $name) = $matches;
                $row['dictionary_name'] = Utilite::underscoreToCamelCase($name, true);
                $row['field_type'] = $fieldType;
            }
            $result[] = $row;
        }
        return $result;
    }

    public function prepareDataForLoad($data)
    {
        foreach ($data as $key => $dictionaryData) {
            preg_match('/^' . FieldType::DICTIONARYMULTISELECT . '_(.*)/', $key, $matches);
            if (count($matches) > 0 && !empty($dictionaryData)) {
                $data[$key] = [];
                foreach ($dictionaryData as $value) {
                    $data[$key][] = $value['dictionary_id'];
                }
            }
        }
        return $data;
    }
}