<?php
namespace Good\Model;

use Application\Model\BaseModel;

/**
 * Class GoodDisk cms_good_disk
 *
 * @package Good\Model
 */
class GoodDisk extends BaseModel
{
    /**
     * @description "ID товара"
     */
    public $good_id;

    /**
     * @description "Бренд"
     */
    public $brand;

    /**
     * @description "Модель"
     */
    public $model;

    /**
     * @description "Ширина"
     */
    public $width;

    /**
     * @description "Диаметр"
     */
    public $diameter;

    /**
     * @description "Количество отверстий"
     */
    public $hole_num;

    /**
     * @description "Диаметр отверстий"
     */
    public $diameter_hole;

    /**
     * @description "Вылет"
     */
    public $overhand;

    /**
     * @description "Диаметр ступичного отверстия"
     */
    public $diameter_nave;

    /**
     * @description "Цвет"
     */
    public $color;

    /**
     * @description "Тип диска (1 -Штампованные 2 - Литыеб 3 - Кованые)"
     */
    public $dictionary_disk_type;
}