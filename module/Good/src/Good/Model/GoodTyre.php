<?php
namespace Good\Model;

use Application\Model\BaseModel;

/**
 * Class GoodTyre таблица cms_good_tyre
 *
 * @package Good\Model
 */
class GoodTyre extends BaseModel
{
    /**
     * @description "ID товара"
     */
    public $good_id;

    /**
     * @description "Бренд"
     */
    public $brand;

    /**
     * @description "Модель"
     */
    public $model;

    /**
     * @description "Ширина"
     */
    public $width;

    /**
     * @description "Высота"
     */
    public $height;

    /**
     * @description "Конструкция"
     */
    public $dictionary_tyre_construction;

    /**
     * @description "Диаметр"
     */
    public $diameter;

    /**
     * @description "Карго"
     */
    public $cargo;

    /**
     * @description "XL"
     */
    public $dictionary_tyre_xl;

    /**
     * @description "Индекс нагрузки"
     */
    public $index_load;

    /**
     * @description "Индекс скорости"
     */
    public $index_speed;

    /**
     * @description "Характеризующие аббревиатуры"
     */
    public $features;

    /**
     * @description "Ранфлэт"
     */
    public $dictionary_tyre_runflat;

    /**
     * @description "Камерность"
     */
    public $dictionary_tyre_camers;

    /**
     * @description "Сезон"
     */
    public $dictionary_tyre_season;
}