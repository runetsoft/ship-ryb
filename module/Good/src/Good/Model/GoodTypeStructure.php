<?php
namespace Good\Model;

use Application\Model\BaseModel;
use Application\Utilities\FieldType;

/**
 * Class GoodTypeStructure cms_good_type_structure
 *
 * @package Good\Model
 */
class GoodTypeStructure extends BaseModel
{
    const ITEMS_IN_ROW = 2;
    public $id;
    public $good_type_id;
    public $structure;

    public function generateStructureFromColumns(\stdClass $config)
    {
        $rowTemplate = '{ "xtype":"container", "layout":"hbox", "margin":"0 0 5 0", "items": [<%items%>]}';
        $items = [];
        $row = [];
        foreach ($config as $index => $item) {
            $items[] = $this->getExtJsObjByType($item);
            if ($index > 0 && $index % self::ITEMS_IN_ROW == 0) {
                $row[] = str_replace(
                    '<%items%>', implode(',',
                    array_slice($items, $index - self::ITEMS_IN_ROW, self::ITEMS_IN_ROW)),
                    $rowTemplate
                );
            }
        }
        if (count($row) < round(count($items) / self::ITEMS_IN_ROW)) {
            $row[] = str_replace(
                '<%items%>', implode(',',
                array_slice($items, count($row) * self::ITEMS_IN_ROW, self::ITEMS_IN_ROW)),
                $rowTemplate
            );
        }
        $this->structure = '[' . implode(',', $row) . ']';
    }

    public function __toArray()
    {
        return array(
            'good_type_id' => $this->good_type_id,
            'structure' => $this->structure
        );
    }

    private function getExtJsObjByType(\stdClass $configs)
    {

        switch ($configs->field_type) {
            case FieldType::DICTIONARY:
                $template = '{ "xtype":"dictionary", "fieldLabel":"<%field_label%>", "name":"<%name%>", "dictionaryName":"<%dictionary_name%>", "valueField":"id" }';
                break;
            case FieldType::DICTIONARYMULTISELECT:
                $template = '{ "xtype":"dictionarymultiselect", "fieldLabel":"<%field_label%>", "name":"<%name%>", "width": "275px", "dictionaryName":"<%dictionary_name%>", "valueField":"id" }';
                break;
            case FieldType::NUMBERFIELD:
                $template = '{ "xtype":"textfield", "fieldLabel":"<%field_label%>", "name":"<%name%>" }';
                break;
            default:
                $template = '{"xtype":"<%field_type%>",  "fieldLabel":"<%field_label%>", "name":"<%name%>" }';
                break;
        }
        $data = array(
            '<%field_type%>' => $configs->field_type,
            '<%name%>' => $configs->column,
            '<%field_label%>' => $configs->title,
            "<%dictionary_name%>" => $configs->dictionary_name
        );
        return str_replace(array_keys($data), array_values($data), $template);
    }
}