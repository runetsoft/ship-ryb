<?php
namespace Good\Model;

use Application\Model\BaseModel;

/**
 * Class GoodPrice таблица cms_good_price.
 *
 * @package Good\Model
 */
class GoodPrice extends BaseModel
{
    public $id;
    public $dt_insert;
    public $adm_id;
    public $good_id;
    public $price_id;
}