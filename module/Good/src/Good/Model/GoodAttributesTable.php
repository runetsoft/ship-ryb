<?php
namespace Good\Model;

use Application\Model\BaseModel;
use Application\Model\BaseModelTable;
use Application\Utilities\DateFormat;
use Application\Utilities\FieldType;
use Application\Utilities\Utilite;
use Dictionary\Model\DictionaryTable;
use Dictionary\Model\MultiselectDictionaryTable;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\RowGateway\RowGateway;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class GoodAttributesTable
 * @package Good\Model
 */
class GoodAttributesTable extends BaseModelTable
{
    protected $sm;
    protected $dictionaryTable;
    /**
     * @var MultiselectDictionaryTable
     */
    protected $multiselectdictionaryTable;
    protected $tableClass;
    protected $tableName;
    protected $tableGateway;
    protected $columns;


    /**
     * GoodAttributesTable constructor.
     * @param  ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->dictionaryTable = $this->sm->get('GoodTypeDictionary');
        $this->multiselectdictionaryTable = $this->sm->get('MultiselectDictionaryTable');
    }

    public function setGoodType($goodTypeId)
    {
        $typeData = current($this->dictionaryTable->getList(array('filter' => array('=id' => $goodTypeId))));
        $tableName = implode("_", array(Utilite::GOOD, $typeData['code']));
        $this->tableClass = Utilite::underscoreToCamelCase($tableName, true);
        $this->tableName = Utilite::getTableFullName($tableName);
        $this->setTable();
    }

    public function setTable()
    {
        if (!empty($this->tableClass)) {
            $dbAdapter = $this->sm->get('ShipRybAdapter');
            $resultSetPrototype = new ResultSet();
            $arrObj = new \ArrayObject();
            $resultSetPrototype->setArrayObjectPrototype($arrObj);
            $this->tableGateway = new TableGateway($this->tableName, $dbAdapter, new RowGatewayFeature('good_id'), $resultSetPrototype);
        }
    }

    public function getColumns($prefix = '', $types = false)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $tableName = $this->tableGateway->getTable();
        $dataType = ($types) ? ",DATA_TYPE" : "";
        $sql = "SELECT COLUMN_NAME " . $dataType . " FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $tableName . "'";
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        $result = array();
        foreach ($resultSet as $item) {
            $itemData = $item->getArrayCopy();
            if ($types) {
                $result[] = $itemData;
            } else {
                if (!empty($prefix)) {
                    $result[$prefix . $itemData['COLUMN_NAME']] = $itemData['COLUMN_NAME'];
                } else {
                    $result[] = $itemData['COLUMN_NAME'];
                }
            }
        }
        return $result;
    }

    public function getColumnsAllInfo()
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $tableName = $this->tableGateway->getTable();
        $sql = "SHOW FULL COLUMNS FROM " . $tableName;
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        $result = array();
        foreach ($resultSet as $item) {
            $result[] = $item->getArrayCopy();
        }
        return $result;
    }

    /**
     * Удаление значений справочника
     *
     * @param $good_id
     * @return bool
     */
    public function deleteItem($good_id)
    {
        $this->tableGateway->delete(array("good_id" => $good_id));
        return true;
    }

    public function updateItem($data)
    {
        if (empty($data['good_id'])) {
            throw new \Exception('Не задан good_id товара!');
        }
        $results = $this->tableGateway->select(array('good_id' => $data['good_id']));
        $artistRow = $results->current();

        if (!$artistRow) {
            $artistRow = new RowGateway('good_id', $this->tableName, $this->sm->get('ShipRybAdapter'));
        }

        $data['dt_update'] = (new \DateTime())->format(DateFormat::STANDARD);

        foreach ($this->getColumns('', true) as $item) {
            if (!empty($data[$item['COLUMN_NAME']]) || ($data[$item['COLUMN_NAME']] !== 0 && $data[$item['COLUMN_NAME']] !== "")) {
                preg_match('/^' . FieldType::DICTIONARYMULTISELECT . '_(.*)/', $item['COLUMN_NAME'], $matches);
                if (count($matches) > 0) {
                    list(, $dictionaryName) = $matches;
                    $this->multiselectdictionaryTable->setTableByDictionary($dictionaryName);
                    $artistRow->{$item['COLUMN_NAME']} = $this->multiselectdictionaryTable->updateValuesByGood($data['good_id'], $data[$item['COLUMN_NAME']]);
                } else if ($data[$item['COLUMN_NAME']] == 'on') {
                    $artistRow->{$item['COLUMN_NAME']} = 1;
                } else {
                    $artistRow->{$item['COLUMN_NAME']} = $data[$item['COLUMN_NAME']];
                }
            } else {
                $artistRow->{$item['COLUMN_NAME']} = null;
            }
        }
        $artistRow->save();
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->tableGateway->select($where);
        return $resultSet;
    }

    /**
     * @param $where
     * @return array
     */
    public function getAllDataOneRow($where)
    {
        $goodAttributes = $this->fetch($where);
        $data = (empty($current = $goodAttributes->current())) ? array() : $current->toArray();
        $columns = $this->getColumns();
        foreach ($columns as $column) {
            preg_match('/^' . FieldType::DICTIONARYMULTISELECT . '_(.*)/', $column, $matches);
            if (count($matches) > 0) {
                list(, $dictionaryName) = $matches;
                $this->multiselectdictionaryTable->setTableByDictionary($dictionaryName);
                $multiselectdictionaryData = $this->multiselectdictionaryTable->fetch(['good_id' => $data['good_id']]);
                $data[$column] = $multiselectdictionaryData->toArray();
            }
        }
        return $data;
    }

    /**
     * Возвращает TableGateway
     *
     * @return TableGateway|null mixed
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }


    public function getColumnsCommentsInfo(TableGateway $tableGateway, $prefix = '', array $ignoredFields = [])
    {
        $result = array();
        $colsInfo = $this->getColumnsFullInfo($tableGateway);
        if (!empty($colsInfo)) {
            foreach ($colsInfo as $col) {
                if (in_array($prefix . $col->Field, $ignoredFields)) {
                    continue;
                }
                $result[$prefix . $col->Field] = $col->Comment;
            }
        }
        return $result;
    }

}