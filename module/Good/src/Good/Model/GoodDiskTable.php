<?php
namespace Good\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

/**
 * Class GoodTable класс для работы с таблицами раздела товары.
 *
 * @package Good\Model
 */
class GoodDiskTable
{
    protected $dbAdapter;
    protected $sm;
    protected $goodTableGateway;
    protected $goodDiskTableGateway;

    /**
     * Тип товара Диск.
     */
    const GOOD_TYPE_DISK = 1;

    /**
     * GoodTable constructor.
     * @param \Zend\Db\TableGateway\TableGateway $goodTableGateway
     */
    public function __construct($sm, TableGateway $goodTableGateway, TableGateway $goodDiskTableGateway)
    {
        $this->dbAdapter = $goodTableGateway->getAdapter();
        $this->sm = $sm;
        $this->goodTableGateway = $goodTableGateway;
        $this->goodDiskTableGateway = $goodDiskTableGateway;
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->goodTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->goodTableGateway->select($where);
        return $resultSet;
    }


    /**
     * Информация о Диске по его ИД.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getWheelInfo($goodId)
    {
        if ($goodId == '0'){
            return true;
        }else {
            $artistRow = array();
            $goodDisk = new GoodDisk();
            $goodDiskColumns = $goodDisk->getColumns();
            $select = $this->goodTableGateway->getSql()->select()
                ->columns(array('id','good', 'zarticle', 'rprice', 'type', 'photo', 'dt_update_photo'))
                ->join($this->goodDiskTableGateway->getTable(),
                 $this->goodDiskTableGateway->getTable() .'.good_id = ' . $this->goodTableGateway->getTable() . '.id' ,
                    $goodDiskColumns,
                    Select::JOIN_LEFT)
                ->where(array($this->goodTableGateway->getTable() . '.id' => $goodId))
                ->order(array(
                    'id ASC'
                ));
            $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            foreach ($resultSet as $res) {
                $res['rprice'] = $res['rprice'] == 0 ? '' : $res['rprice'];
                $artistRow = $res;
            }
            return $artistRow;
        }
    }

    /**
     * Обновить информацию.
     *
     * @param array $data
     */
    public function updateInfo(array $data)
    {
        $artistRow = false;

        $results = $this->goodDiskTableGateway->select(array('good_id' => $data['id']));
        $artistRow = $results->current();

        // Если описания к товару были пустые, то создаем пустой объект для сохранения (insert)
        if (!$artistRow) {
            $artistRow = $this->sm->get('GoodDiskRow');
        }

        // Сохраняемые данные
        $artistRow->good_id = $data['id'];
        $artistRow->brand = $data['brand'];
        $artistRow->model = $data['model'];
        $artistRow->width = $data['width'];
        $artistRow->diameter = $data['diameter'];
        $artistRow->hole_num = $data['hole_num'];
        $artistRow->diameter_hole = $data['diameter_hole'];
        $artistRow->overhand = $data['overhand'];
        $artistRow->diameter_nave = $data['diameter_nave'];
        $artistRow->color = $data['color'];
        $artistRow->dictionary_disk_type = $data['dictionary_disk_type'];
        $artistRow->save();
    }
}