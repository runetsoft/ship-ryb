<?php
namespace Good\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

/**
 * Class GoodTyreMotoTable класс для работы с таблицами Товара типа Шины для мото.
 *
 * @package Good\Model
 */
class GoodTyreMotoTable
{
    protected $dbAdapter;
    protected $sm;
    protected $goodTableGateway;
    protected $goodTyreMotoTableGateway;

    public function __construct($sm, TableGateway $goodTableGateway, TableGateway $goodTyreMotoTableGateway)
    {
        $this->dbAdapter = $goodTableGateway->getAdapter();
        $this->sm = $sm;
        $this->goodTableGateway = $goodTableGateway;
        $this->goodTyreMotoTableGateway = $goodTyreMotoTableGateway;
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->goodTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->goodTableGateway->select($where);
        return $resultSet;
    }

    /**
     * Информация о Шине мото по ее ИД.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getTireMotoInfo($goodId)
    {
        if ($goodId == '0'){
            return true;
        }else {
            $artistRow = array();
            $goodTyreMoto = new GoodTyreMoto();
            $goodTyreMotoColumns = $goodTyreMoto->getColumns();
            $select = $this->goodTableGateway->getSql()->select()
                ->columns(array('id','good', 'zarticle', 'rprice', 'type', 'photo', 'dt_update_photo'))
                ->join($this->goodTyreMotoTableGateway->getTable(),
                    $this->goodTyreMotoTableGateway->getTable() . '.good_id = ' . $this->goodTableGateway->getTable() . '.id',
                    $goodTyreMotoColumns,
                    Select::JOIN_LEFT)
                ->where(array($this->goodTableGateway->getTable() . '.id' => $goodId))
                ->order(array(
                    'id ASC'
                ));

            $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            foreach ($resultSet as $res) {
                $artistRow = $res;
            }
            return $artistRow;
        }
    }


    /**
     * Обновить информацию.
     *
     * @param array $data
     */

    public function updateInfo(array $data)
    {
        $artistRow = false;

        $results = $this->goodTyreMotoTableGateway->select(array('good_id' => $data['id']));
        $artistRow = $results->current();

        // Если описания к товару были пустые, то создаем пустой объект для сохранения
        if (!$artistRow) {
            $artistRow = $this->sm->get('GoodTyreMotoRow');
        }

        // Сохраняемые данные
        $artistRow->good_id = $data['id'];
        $artistRow->brand = $data['brand'];
        $artistRow->model = $data['model'];
        $artistRow->width = $data['width'];
        $artistRow->height = $data['height'];
        $artistRow->dictionary_tyremoto_construction = $data['dictionary_tyremoto_construction'];
        $artistRow->diameter = $data['diameter'];
        $artistRow->index_load = $data['index_load'];
        $artistRow->index_speed = $data['index_speed'];
        $artistRow->features = $data['features'];
        $artistRow->dictionary_tyremoto_camers = $data['dictionary_tyremoto_camers'];
        $artistRow->dictionary_tyremoto_axis = $data['dictionary_tyremoto_axis'];
        $artistRow->save();
    }

}