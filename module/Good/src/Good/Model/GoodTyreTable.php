<?php
namespace Good\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

/**
 * Class GoodTyreTable класс для работы с таблицами Товара типа Шины.
 *
 * @package Good\Model
 */
class GoodTyreTable
{
    protected $dbAdapter;
    protected $sm;
    protected $goodTableGateway;
    protected $goodTyreTableGateway;

    public function __construct($sm, TableGateway $goodTableGateway, TableGateway $goodTyreTableGateway)
    {
        $this->dbAdapter = $goodTableGateway->getAdapter();
        $this->sm = $sm;
        $this->goodTableGateway = $goodTableGateway;
        $this->goodTyreTableGateway = $goodTyreTableGateway;
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->goodTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->goodTableGateway->select($where);
        return $resultSet;
    }

    /**
     * Информация о Шине по ее ИД.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getTireInfo($goodId)
    {
        if ($goodId == '0'){
            return true;
        }else {
            $artistRow = array();
            $goodTyre = new GoodTyre();
            $goodTyreColumns = $goodTyre->getColumns();
            $select = $this->goodTableGateway->getSql()->select()
                ->columns(array('id','good', 'zarticle', 'rprice', 'type', 'photo', 'dt_update_photo'))
                ->join($this->goodTyreTableGateway->getTable(),
                    $this->goodTyreTableGateway->getTable() . '.good_id = ' . $this->goodTableGateway->getTable() . '.id',
                    $goodTyreColumns,
                    Select::JOIN_LEFT)
                ->where(array($this->goodTableGateway->getTable() . '.id' => $goodId))
                ->order(array(
                    'id ASC'
                ));

            $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            foreach ($resultSet as $res) {
                $res['rprice'] = $res['rprice'] == 0 ? '' : $res['rprice'];
                $artistRow = $res;
            }
            return $artistRow;
        }
    }


    /**
     * Обновить информацию.
     *
     * @param array $data
     */

    public function updateInfo(array $data)
    {
        $artistRow = false;

        $results = $this->goodTyreTableGateway->select(array('good_id' => $data['id']));
        $artistRow = $results->current();

        // Если описания к товару были пустые, то создаем пустой объект для сохранения
        if (!$artistRow) {
            $artistRow = $this->sm->get('GoodTyreRow');
        }

        // Сохраняемые данные
        $artistRow->good_id = $data['id'];
        $artistRow->brand = $data['brand'];
        $artistRow->model = $data['model'];
        $artistRow->width = $data['width'];
        $artistRow->height = $data['height'];
        $artistRow->dictionary_tyre_construction = $data['dictionary_tyre_construction'];
        $artistRow->diameter = $data['diameter'];
        $artistRow->cargo = $data['cargo'];
        $artistRow->dictionary_tyre_xl = $data['dictionary_tyre_xl'];
        $artistRow->index_load = $data['index_load'];
        $artistRow->index_speed = $data['index_speed'];
        $artistRow->features = $data['features'];
        $artistRow->dictionary_tyre_runflat = $data['dictionary_tyre_runflat'];
        $artistRow->dictionary_tyre_camers = $data['dictionary_tyre_camers'];
        $artistRow->dictionary_tyre_season = $data['dictionary_tyre_season'];
        $artistRow->save();
    }

}