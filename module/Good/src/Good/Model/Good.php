<?php
namespace Good\Model;

use Application\Model\BaseModel;

/**
 * Class Good таблица cms_good
 *
 * @package Good\Model
 */
class Good extends BaseModel
{
    public $id;
    public $dt_insert;
    public $dt_update;
    public $dt_update_photo;
    public $adm_id;
    public $good;
    public $rprice;
    public $zarticle;
    public $not_load;
    public $type;
    public $photo;
}