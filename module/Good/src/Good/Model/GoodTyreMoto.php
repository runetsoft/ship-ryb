<?php
namespace Good\Model;

use Application\Model\BaseModel;

/**
 * Class GoodTyreMoto таблица cms_good_tyre_moto
 *
 * @package Good\Model
 */
class GoodTyreMoto extends BaseModel
{
    /**
     * @description "ID товара"
     */
    public $good_id;

    /**
     * @description "Бренд"
     */
    public $brand;

    /**
     * @description "Модель"
     */
    public $model;

    /**
     * @description "Ширина"
     */
    public $width;

    /**
     * @description "Высота"
     */
    public $height;

    /**
     * @description "Конструкция"
     */
    public $dictionary_tyre_construction;

    /**
     * @description "Диаметр"
     */
    public $diameter;

    /**
     * @description "Индекс нагрузки"
     */
    public $index_load;

    /**
     * @description "Индекс скорости"
     */
    public $index_speed;

    /**
     * @description "Характеризующие аббревиатуры"
     */
    public $features;

    /**
     * @description "Камерность"
     */
    public $dictionary_tyre_camers;

    /**
     * @description "Ось (1 - перед, 2 - зад)"
     */
    public $dictionary_tyremoto_axis;
}
