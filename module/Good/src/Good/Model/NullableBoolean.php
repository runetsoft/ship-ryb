<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 29.06.2018
 * Time: 11:01
 */

namespace Good\Model;


use Zend\Db\Sql\Ddl\Column\Boolean;

class NullableBoolean extends Boolean
{

    /**
     * @param  bool $nullable
     * @return self
     */
    public function setNullable($nullable)
    {
        $this->isNullable = (bool)$nullable;
        return $this;
    }

}