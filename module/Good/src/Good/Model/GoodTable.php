<?php
namespace Good\Model;

use Application\Model\BaseModelTable;
use Application\Utilities\DateFormat;
use Application\Utilities\GoodPhotoType;
use Application\Utilities\UtilitiesGoodType;
use Application\Utilities\Utilite;
use Price\Model\Price;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;

/**
 * Class GoodTable класс для работы с таблицами раздела товары.
 *
 * @package Good\Model
 */
class GoodTable extends BaseModelTable
{
    protected $dbAdapter;
    protected $sm;
    protected $goodTableGateway;
    protected $priceTableGateway;
    protected $goodPriceTableGateway;
    protected $goodTyreTableGateway;
    protected $goodDiskTableGateway;
    protected $dictionaryService;
    protected $goodTypeStructureTableGateway;

    /**
     * Регулярка для парсинга автомобильных шин.
     */

    /**
     * Путь для загрузки файлов
     */
    const UPLOAD_PATH = 'public/files/downloads/';
    /**
     * Путь для загрузки файлов
     */
    const GOOD_PHOTO_UPLOAD_PATH = 'public/files/downloads/goods/';
    /**
     * Путь для загрузки файлов
     */
    const GOOD_PHOTO_URL = '/files/downloads/goods/';

    /**
     * GoodTable constructor.
     * @param $sm
     * @param TableGateway $goodTableGateway
     * @param TableGateway $goodPriceTableGateway
     * @param TableGateway $goodTypeStructureTableGateway
     * @param \Zend\Db\TableGateway\TableGateway $priceTableGateway
     */
    public function __construct($sm, TableGateway $goodTableGateway, TableGateway $goodPriceTableGateway, TableGateway $priceTableGateway, TableGateway $goodTypeStructureTableGateway)
    {
        $this->dbAdapter = $goodTableGateway->getAdapter();
        $this->sm = $sm;
        $this->dictionaryService = $this->sm->get(Utilite::getDictionaryModelFromType(Utilite::GOOD_TYPE));
        $this->goodTableGateway = $goodTableGateway;
        $this->priceTableGateway = $priceTableGateway;
        $this->goodPriceTableGateway = $goodPriceTableGateway;
        $this->goodTypeStructureTableGateway = $goodTypeStructureTableGateway;
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->goodTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->goodTableGateway->select($where);
        return $resultSet;
    }

    /**
     * Получает полную информацию по товарам.
     *
     * @param $page
     * @param $start
     * @param $limit
     * @param array $filter Фильтр. Если добавлете параметры до добавьте сюда!
     * array(
            notProcessed => true // Необработанные товары.
     * )
     * @param mixed $sort Сортировка.
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getGoodsList($page, $start, $limit, array $filter, $sort)
    {
        $good = new Good();
        $goodColumns = $good->getColumns();
        $priceTableName = $this->priceTableGateway->getTable();
        $goodTableName = $this->goodTableGateway->getTable();

        $select = $this->goodTableGateway->getSql()->select()
            ->columns($goodColumns)
            ->join($priceTableName, $priceTableName . '.good_id = ' . $goodTableName . '.id',
                array(),
                Select::JOIN_LEFT);

        // Set filters.
        if (!empty($filter['photo_type'])) {
            $oWhere = new Where();
            $oWhere = $oWhere->nest();
            $oWhere->isNull($goodTableName . '.photo');
            $oWhere->or->equalTo($goodTableName . '.photo', '');
            $select->where($oWhere);
            switch ($filter['photo_type']) {
                case GoodPhotoType::WITHOUT_PHOTO:
                    break;
                case GoodPhotoType::WITH_PRICE_PHOTO:
                    $select->where($priceTableName . '.photo IS NOT NULL');
                    break;
            }
            unset($filter['photo_type']);
        }

        foreach ($filter as $key => $value) {
            preg_match_all(self::FILTER, $key, $matches, PREG_SET_ORDER, 0);
            list(, $condition, $column) = current($matches);
            if (in_array($column, $goodColumns)) {
                $this->setFilter($select, array($key => $value), $goodTableName);
            }
        }

        if ($filter['query']) {
            $select->where($goodTableName . '.good like \'%' . $filter['query'] . '%\'');
        }

        // Sorting and group.
        if ($sort && $sort->property) {
            $select->order(array(
                    $sort->property . ' ' . $sort->direction
                )
            );
        } else {
            $select->order(array(
                    'good ASC'
                )
            );
        }

        $select->group(array(
            'id'
        ));

        $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);

        return array($paginator, $resultSet->count());
    }

    /**
     * Функция возвращает количество предложений(прайсов) по товару.
     *
     * @param array $goodsIdsList
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getPriceCountsByGoodIds(array $goodsIdsList)
    {
        $priceTableName = $this->priceTableGateway->getTable();
        $select = $this->priceTableGateway->getSql()->select()
            ->columns(array(
                'good_id',
                'price_cnt' => new Expression('COUNT(*)')
            ));
        $select->where($priceTableName . '.good_id' . ' IN (' . implode(',', $goodsIdsList) . ')');
        $select->group(array(
            'good_id'
        ));
        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @param array $filter
     * @param $sort
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getGoodInfoByFilter(array $filter, $sort = null)
    {
        $good = new Good();
        $goodColumns = $good->getColumns();

        $select = $this->goodTableGateway->getSql()->select()
            ->columns($goodColumns);

        if ($sort && $sort->property) {
            $select->order(array(
                    $sort->property.' '. $sort->direction
                )
            );
        } else {
            $select->order(array(
                    'good ASC'
                )
            );
        }

        $goodTableName = $this->goodTableGateway->getTable();

        foreach ($filter as $item => $value) {
            $this->setFilter($select, array($item => $value), $goodTableName);
        }

        $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    /**
     * Обработка фильтра по типам.
     *
     * @param $select
     * @param $goodTableName
     * @param $type
     */
    public function prepareTypes($select, $goodTableName, $type)
    {
        $type = !is_array($type) ? array($type) : $type;
        if (in_array(UtilitiesGoodType::NO_TYPE, $type)) {
            $select->where($goodTableName . '.type IS NULL');
        } else if (!in_array(UtilitiesGoodType::ALL, $type)) {
            $this->setFilter($select, array('type' => $type), $goodTableName);
        }
    }

    /**
     * Информация о Диске по его ИД.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getWheelInfo($goodId)
    {
        if ($goodId == '0'){
            return true;
        }else {

            if (!$this->goodDiskTableGateway instanceof GoodTyre) {
                $this->goodDiskTableGateway = $this->sm->get('GoodDiskTableGateway');
            }
            $artistRow = array();
            $goodDisk = new GoodDisk();
            $goodDiskColumns = $goodDisk->getColumns();
            $select = $this->goodDiskTableGateway->getSql()->select()
                ->columns($goodDiskColumns)
                ->join($this->goodTableGateway->getTable(),
                    $this->goodTableGateway->getTable() . '.id = ' . $this->goodDiskTableGateway->getTable() . '.good_id',
                    array('good', 'zarticle', 'rprice'),
                    Select::JOIN_LEFT)
                ->where(array($this->goodDiskTableGateway->getTable() . '.good_id' => $goodId))
                ->order(array(
                    'good_id ASC'
                ));
            $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            foreach ($resultSet as $res) {
                $artistRow = $res;
            }
            return $artistRow;
        }
    }

    /**
     * Информация о Шине по ее ИД.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getTireInfo($goodId)
    {
        if ($goodId == '0'){
            return true;
        }else {
            if (!$this->goodTyreTableGateway instanceof GoodTyre) {
                $this->goodTyreTableGateway = $this->sm->get('GoodTyreTableGateway');
            }
            $artistRow = array();
            $goodDisk = new GoodTyre();
            $goodDiskColumns = $goodDisk->getColumns();
            $select = $this->goodTyreTableGateway->getSql()->select()
                ->columns($goodDiskColumns)
                ->join($this->goodTableGateway->getTable(),
                    $this->goodTableGateway->getTable() . '.id = ' . $this->goodTyreTableGateway->getTable() . '.good_id',
                    array('good', 'zarticle', 'rprice'),
                    Select::JOIN_LEFT)
                ->where(array($this->goodTyreTableGateway->getTable() . '.good_id' => $goodId))
                ->order(array(
                    'good_id ASC'
                ));

            $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            foreach ($resultSet as $res) {
                $artistRow = $res;
            }
            return $artistRow;
        }
    }

    /**
     * Информация от товаре.
     *
     * @param $goodId
     * @return null|string
     *
     */
    public function getGoodInfo($goodId)
    {
        $info = array();
        $good = new Price();
        $goodColumns = $good->getColumns();

        $priceTableName = $this->priceTableGateway->getTable();
        $supplierTableName = $this->sm->get('SupplierTableGateway')->getTable();

        $select = $this->sm->get('PriceTableGateway')->getSql()->select()
            ->columns($goodColumns)
            ->join($supplierTableName,
                $supplierTableName . '.id = ' . $priceTableName . '.provider_id',
                array('provider'),
                Select::JOIN_LEFT)
            ->where(array($priceTableName . '.good_id' => $goodId))
            ->order(array(
                'good_id ASC'
            ));
        $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        foreach ($resultSet as $res) {
            $info[] = $res;
        }
        return $info;
    }

    /**
     * Удалить товар по его ИД.
     */
    public function itemDelete($goodId)
    {
        $good = (empty($res = $this->fetch(array('id' => $goodId)))) ? null : $res->current();
        if (!empty($good)) {
            $goodData = $good->toArray();
            if (!empty($goodData['type'])) {
                $tableAttributesGateway = new GoodAttributesTable($this->sm);
                $tableAttributesGateway->setGoodType($goodData['type']);
                $tableAttributesGateway->deleteItem($goodData['id']);
            }
            $good->delete();
        }
        return true;
    }

    /**
     * Групповое удаление товаров.
     *
     * @param $goodIds
     * @return bool
     */
    public function delete($goodIds)
    {
        foreach ($goodIds as $goodId) {
            $this->itemDelete($goodId);
        }
        return true;
    }

    /**
     * Очиститть МРЦ
     */
    public function clearMrc()
    {
        return $this->goodTableGateway->update(array('rprice' => 0));
    }

    /**
     * Обновление МРЦ.
     *
     * @param $fileTmp
     * @param $goodsType
     * @return bool
     */
    public function updateMrc($fileTmp, $goodsType)
    {
        if (($handle = fopen($fileTmp['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                list($article, $mrc) = $data;
                if (!$this->goodTableGateway instanceof Good) {
                    $results = $this->goodTableGateway->select(array('zarticle' => $article, 'type' => $goodsType));
                    $goodRow = $results->current();
                    if (!empty($goodRow)) {
                        $goodRow->rprice = $mrc;
                        $goodRow->dt_update = (new \DateTime())->format(DateFormat::STANDARD);
                        $goodRow->save();
                    }
                }
            }
            fclose($handle);
        }
        return true;
    }

    /**
     * Обновить информацию.
     *
     * @param array $data
     */
    public function updateInfo(array $data)
    {
        $goodRow = false;
        $now = (new \DateTime())->format(DateFormat::STANDARD);

        if (!$this->goodTableGateway instanceof Good) {
            $results = $this->goodTableGateway->select(array('id' => $data['id']));
            $goodRow = $results->current();
        }

        if (!$goodRow) {
            $goodRow = $this->getGoodRowDefault();
        }

        foreach ($goodRow->toArray() as $key => $item) {
            if (!empty($data[$key]) && $data[$key] != $item) {
                if ($key == "dt_update_photo" || $key == "photo") {
                    continue;
                }
                $goodRow->dt_update = $now;
                break;
            }
        }
        if (!empty($data["photo"]) && $data["photo"] != $goodRow->photo) {
            $goodRow->dt_update_photo = $now;
        }
        $goodRow->dt_insert = (empty($goodRow->dt_insert)) ? $goodRow->dt_update : $goodRow->dt_insert;
        $goodRow->dt_update_photo = (!empty($data['dt_update_photo'])) ? $data['dt_update_photo'] : $goodRow->dt_update_photo;
        $goodRow->type = (!empty($data['type'])) ? (int)$data['type'] : (int)$goodRow->type;
        $goodRow->not_load = (int)$data['not_load'];
        $goodRow->good = (!empty($data['good'])) ? $data['good'] : $goodRow->good;
        $goodRow->zarticle = $data['zarticle'];
        $goodRow->rprice = $data['rprice'];
        $goodRow->photo = (!empty($data['photo'])) ? $data['photo'] : $goodRow->photo;
        $goodRow->save();
        return $goodRow->id;
    }

    /**
     * Возвращает новый GoodRow объект с значениями по умолчанию.
     *
     * @return mixed
     */
    private function getGoodRowDefault()
    {
        $now = (new \DateTime())->format(DateFormat::STANDARD);
        $goodRow = $this->sm->get('GoodRow');
        $goodRow->dt_update_photo = $now;
        $goodRow->dt_update = $now;
        $goodRow->type = 0;
        $goodRow->not_load = 0;
        $goodRow->good = '';
        $goodRow->zarticle = '';
        $goodRow->rprice = 0;
        $goodRow->photo = '';
        return $goodRow;
    }

    /**
     * Загружает фотографию продукта, возвращает путь к загруженному файлу
     *
     * @return string
     */
    public function uploadPhoto($file)
    {
        if (!is_dir(self::UPLOAD_PATH)) {
            try {
                mkdir(self::UPLOAD_PATH, 0777);
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
            }
        }

        $imageSelected = strpos($file['type'], 'image') !== false;

        if ($imageSelected) {
            $fileTmpName = basename($file['tmp_name']);
            $uploadFile = self::UPLOAD_PATH . $fileTmpName;
            $uploadSuccessful = move_uploaded_file($file['tmp_name'], $uploadFile);
            if ($uploadSuccessful) {
                $extension = strstr($file['name'], '.');
                $info = pathinfo($uploadFile);
                $newName = $info['filename'] . $extension;
                rename($uploadFile, self::UPLOAD_PATH . $newName);

                return $newName;
            }
        }
    }

    public function updateGoodPhotoByPrice($priceId)
    {
        $priceTableName = $this->priceTableGateway->getTable();
        $select = $this->priceTableGateway->getSql()->select()
            ->columns(array('good_id', 'photo'));
        $this->setFilter($select, array('=id' => $priceId), $priceTableName);
        $statement = $this->goodTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        if (!empty($result = (object)$resultSet->current())) {
            if (empty($result->photo)) {
                throw new \Exception('У прайса не загружен файл. ID Прайса: ' . $priceId);
            }
            $goodId = $result->good_id;
            $photoUrl = str_replace(' ', '%20',$result->photo);
            $info = getimagesize($photoUrl);
            $extension = image_type_to_extension($info[2]);
            $photoPath = self::GOOD_PHOTO_UPLOAD_PATH . $goodId . $extension;
            $result = file_put_contents($photoPath, file_get_contents($photoUrl));
            if (empty($result)) {
                throw new \Exception('Не удалось скопировать файл: ' . $photoUrl);
            }
            $this->updateInfo(
                array(
                    'id' => $goodId,
                    'dt_update_photo' => (new \DateTime())->format(DateFormat::STANDARD),
                    'photo' => self::GOOD_PHOTO_URL . $goodId . $extension
                )
            );
            return $result;
        }
        return false;
    }

}