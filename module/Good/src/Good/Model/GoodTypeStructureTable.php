<?php
namespace Good\Model;

use Application\Model\BaseModel;
use Application\Utilities\Utilite;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

/**
 * Class GoodTypeStructureTable
 *
 * @package Good\Model
 */
class GoodTypeStructureTable extends BaseModel
{
    protected $sm;
    protected $goodTypeStructureTableGateway;
    protected $tableName;

    /**
     * GoodTypeStructureTable constructor.
     * @param $sm
     * @param TableGateway $goodTypeStructureTableGateway
     */
    public function __construct($sm, TableGateway $goodTypeStructureTableGateway)
    {
        $this->sm = $sm;
        $this->goodTypeStructureTableGateway = $goodTypeStructureTableGateway;
        $this->tableName = $this->goodTypeStructureTableGateway->getTable();
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->goodTypeStructureTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->goodTypeStructureTableGateway->select($where);
        return $resultSet;
    }

    /**
     * Обновить информацию.
     *
     * @param array $data
     */
    public function updateInfo(array $data)
    {
        $filter = (!empty($data['id'])) ? ['id' => $data['id']] : ['good_type_id' => $data['good_type_id']];
        $results = $this->goodTypeStructureTableGateway->select($filter);
        $artistRow = $results->current();

        if (!$artistRow) {
            $artistRow = $this->sm->get('GoodTypeStructureRow');
        }

        $artistRow->id = ($data['id']) ? $data['id'] : (!empty($artistRow->id)) ? $artistRow->id : null;
        $artistRow->good_type_id = $data['good_type_id'];
        $artistRow->structure = $data['structure'];
        $artistRow->save();
    }

    /**
     * @param $goodTypeId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getInfoByTypeId($goodTypeId)
    {
        $typesTableName = Utilite::getDictionaryTableName(Utilite::GOOD_TYPE);
        $select = $this->goodTypeStructureTableGateway->getSql()->select()
            ->join($typesTableName,
                $typesTableName . '.id = ' . $this->tableName . '.good_type_id',
                array('good_type_code' => 'code', 'good_type_name' => 'name'),
                Select::JOIN_INNER)
            ->where(array($this->tableName . '.good_type_id' => $goodTypeId));
        $statement = $this->goodTypeStructureTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }
}