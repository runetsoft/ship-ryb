<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 14:08
 */

namespace Good\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Good\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator) {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController(
            $serviceLocator->getServiceLocator(),
            $parentLocator->get('Good\Model\GoodTable'),
            $parentLocator->get('Good\Model\GoodDiskTable'),
            $parentLocator->get('Good\Model\GoodTyreTable'),
            $parentLocator->get('Good\Model\GoodTyreMotoTable'),
            $parentLocator->get('Good\Model\GoodTypeStructureTable'),
            $parentLocator->get('Good\Model\GoodAttributesTable'),
            $parentLocator->get('Good\Service\GoodType')
        );
    }
}