<?php
namespace Good;

use Dictionary\Model\DictionaryTable;
use Good\Model\GoodAttributesTable;
use Good\Model\GoodTypeStructure;
use Good\Model\GoodTypeStructureTable;
use Good\Service\ExportData;
use Good\Service\GoodType;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\RowGateway\RowGateway;
use Good\Model\Good;
use Good\Model\GoodPrice;
use Good\Model\GoodTyre;
use Good\Model\GoodTyreMoto;
use Good\Model\GoodDisk;
use Good\Model\GoodTable;
use Good\Model\GoodDiskTable;
use Good\Model\GoodTyreTable;
use Good\Model\GoodTyreMotoTable;
use Good\Service\ExplodeData;


class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Good\Model\GoodTable' => function ($sm) {
                    $tableGateway = $sm->get('GoodTableGateway');
                    $goodPriceTableGateway = $sm->get('GoodPriceTableGateway');
                    $priceTableGateway = $sm->get('PriceTableGateway');
                    $goodTypeStructureTableGateway = $sm->get('GoodTypeStructureTableGateway');
					$table = new GoodTable($sm, $tableGateway, $goodPriceTableGateway, $priceTableGateway, $goodTypeStructureTableGateway);
                    return $table;
                },
                'Good\Model\GoodAttributesTable' => function ($sm) {
                    $table = new GoodAttributesTable($sm);
                    return $table;
                },
                'Good\Model\GoodDiskTable' => function ($sm) {
                    $tableGateway = $sm->get('GoodTableGateway');
                    $goodDiskTableGateway = $sm->get('GoodDiskTableGateway');
                    $table = new GoodDiskTable($sm, $tableGateway, $goodDiskTableGateway);
                    return $table;
                },
                'Good\Model\GoodTypeStructureTable' => function ($sm) {
                    $goodTypeStructureTableGateway = $sm->get('GoodTypeStructureTableGateway');
                    $table = new GoodTypeStructureTable($sm, $goodTypeStructureTableGateway);
                    return $table;
                },
                'Good\Model\GoodTyreTable' => function ($sm) {
                    $tableGateway = $sm->get('GoodTableGateway');
                    $goodTyreTableGateway = $sm->get('GoodTyreTableGateway');
                    $table = new GoodTyreTable($sm, $tableGateway, $goodTyreTableGateway);
                    return $table;
                },
                'Good\Model\GoodTyreMotoTable' => function ($sm) {
                    $tableGateway = $sm->get('GoodTableGateway');
                    $goodTyreMotoTableGateway = $sm->get('GoodTyreMotoTableGateway');
                    $table = new GoodTyreMotoTable($sm, $tableGateway, $goodTyreMotoTableGateway);
                    return $table;
                },

                'GoodTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Good());
                    return new TableGateway('cms_good', $dbAdapter, new RowGatewayFeature('id'), $resultSetPrototype);
                },
                'GoodPriceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodPrice());
                    return new TableGateway('cms_good_price', $dbAdapter, new RowGatewayFeature('id'), $resultSetPrototype);
                },
                'GoodTyreTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodTyre());
                    return new TableGateway('cms_good_tyre', $dbAdapter, new RowGatewayFeature('good_id'), $resultSetPrototype);
                },
                'GoodTyreMotoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodTyreMoto());
                    return new TableGateway('cms_good_tyremoto', $dbAdapter, new RowGatewayFeature('good_id'), $resultSetPrototype);
                },
                'GoodDiskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodDisk());
                    return new TableGateway('cms_good_disk', $dbAdapter, new RowGatewayFeature('good_id'), $resultSetPrototype);
                },
                'GoodTypeStructureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodTypeStructure());
                    return new TableGateway('cms_good_type_structure', $dbAdapter, new RowGatewayFeature('id'), $resultSetPrototype);
                },
                'Good\Service\ExplodeData' => function ($sm) {
                    $goodTable = $sm->get('Good\Model\GoodTable');
                    $goodTyreTable = $sm->get('Good\Model\GoodTyreTable');
                    $goodTyreMotoTable = $sm->get('Good\Model\GoodTyreMotoTable');
                    $goodDiskTable = $sm->get('Good\Model\GoodDiskTable');
                    $table = new ExplodeData($sm, $goodTable, $goodDiskTable, $goodTyreTable, $goodTyreMotoTable);
                    return $table;
                },
                'Good\Service\ExportData' => function ($sm) {
                    $service = new ExportData($sm);
                    return $service;
                },
                'Good\Service\GoodType' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $goodTypeStructureTable = $sm->get('Good\Model\GoodTypeStructureTable');
                    return new GoodType($sm, $dbAdapter, $goodTypeStructureTable);
                }
            ),
            'aliases' => array(
                'ExplodeData' => 'Good\Service\ExplodeData',
                'GoodPriceExport' => 'Good\Service\ExportData'
            ),
            'shared' => array(
                'GoodTableGateway' => false,
                'GoodTyreTableGateway' => false,
                'GoodTyreMotoTableGateway' => false,
                'GoodDiskTableGateway' => false,
                'GoodPriceTableGateway' => false,
                'GoodTyreRow' => false,
                'GoodTyreMotoRow' => false,
                'GoodDiskRow' => false,
                'GoodPriceRow' => false,
                'GoodRow' => false
            )
        );
    }
}