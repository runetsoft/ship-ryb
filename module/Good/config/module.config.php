<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Good\Controller\MainController' => 'Good\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'good_list' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/good[/:action].json',
                    'defaults' => array(
                        'controller' => 'Good\Controller\MainController'
                      ,
                    ),
                ),
            )
        ),
     ),    
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);