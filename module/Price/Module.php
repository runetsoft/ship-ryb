<?php
namespace Price;

use Application\Utilities\Utilite;
use Dictionary\Model\DictionaryTable;
use Price\Model\Price;
use Price\Model\PriceTable;
use Price\Service\CsvParser;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\RowGateway\RowGateway;
use Zend\Mvc\Application;

/**
 * Class Module
 * Модуль для раздела прайсы.
 *
 * @package Price
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Price\Model\PriceTable' => function ($sm) {
                    $priceTableGateway = $sm->get('PriceTableGateway');
                    $table = new PriceTable($sm, $priceTableGateway);
                    return $table;
                },
                'PriceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Price());
                    return new TableGateway('cms_price', $dbAdapter, $features, $resultSetPrototype);
                },
                'PriceRow' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    return new RowGateway('id', 'cms_price', $dbAdapter);
                },
                'CsvParser' => function($sm) {
                    $priceTable = $sm->get('Price\Model\PriceTable');
                    $goodTable = $sm->get('Good\Model\GoodTable');
                    $csvParser = new CsvParser($sm, $goodTable, $priceTable);
                    return $csvParser;
                }
            ),
            'shared' => array(
                'PriceRow' => false,
                'CsvParser' => false
            )
        );
    }
}