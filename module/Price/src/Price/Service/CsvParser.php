<?php

namespace Price\Service;

use Application\Utilities\UtilitiesGoodType;

class CsvParser
{
    /**
     * разделитель csv
     */
    const DELIMITER = ";";

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Price\Model\PriceTable
     */
    protected $priceTable;

    /**
     * @var \Good\Model\GoodTable
     */
    protected $goodTable;

    /**
     * CsvParser constructor.
     * @param $sm
     * @param $priceTable
     * @param $goodTable
     */
    public function __construct($sm, $goodTable, $priceTable)
    {
        $this->sm = $sm;
        $this->priceTable = $priceTable;
        $this->goodTable = $goodTable;
    }

    /**
     * разбирает записанный в базу товар на св-ва
     *
     * @param $id
     * @param $propertiesOfCurrentPosition
     * @return array
     */
    private function parseGood($id, $propertiesOfCurrentPosition){
        $explodeResult = $this->sm->get('ExplodeData')->process($id);
        return array("id" => $id, "good" => $propertiesOfCurrentPosition['good_in_csv'], "state" => $explodeResult['type'] == UtilitiesGoodType::NO_TYPE ? "Не разобранные товары" : "Успешно разобранные товары");
    }

    /**
     * обновление товара в базе, если id не передан, будет записан новый товар
     *
     * @param $propertiesOfCurrentPosition
     * @return mixed
     */
    private function updateGood($propertiesOfCurrentPosition){
        $id = $this->goodTable->updateInfo(
            array(
                'id' => $propertiesOfCurrentPosition['good_id'],
                'good' => $propertiesOfCurrentPosition['good_in_csv'],
                'zarticle' => $propertiesOfCurrentPosition['price_zarticle'],
                'rprice' => NULL
            )
        );
        if ($id) {
            $this->priceTable->saveConformInfo(array("good_id" => $id, "price_id" => $propertiesOfCurrentPosition['price_id']));
            $result = $this->parseGood($id, $propertiesOfCurrentPosition);
            return $result;

        } else {
            return array("id" => "", "good" => $propertiesOfCurrentPosition['good_in_csv'], "state" => "Не сохранненые товары");
        }
    }

    /**
     * Устанавливает необходимые для работы parseCsv параметры
     *
     * @param $item
     * @return array
     */
    private function getProperties($item){
        list($priceId, $priceName, $goodName) = $item;
        $priceName = iconv('cp1251', 'utf-8', $priceName);
        $currentGoodProperties = null;
        $goodName = iconv('cp1251', 'utf-8', $goodName);
        $currentGood = $this->goodTable->getGoodInfoByFilter(array("=good" => $goodName), null)->current();
        $currentPrice = current($this->priceTable->getPriceInfo(array("id" => $priceId), null))->current();
        switch ($currentGood['type']) {
            case UtilitiesGoodType::DISK:
                $currentGoodProperties = $this->goodTable->getWheelInfo($currentGood['id']);
                break;
            case UtilitiesGoodType::TYRE:
                $currentGoodProperties = $this->goodTable->getTireInfo($currentGood['id']);
                break;
            case UtilitiesGoodType::TYRE_MOTO:
                $currentGoodProperties = $this->goodTable->getGoodInfo($currentGood['id']);
                break;
        }
        $result = array(
            "price_id" => $priceId,
            "good_id" => $currentGood['id'],
            "good_properties" => $currentGoodProperties,
            "good_in_csv" => $goodName,
            "price_zarticle" => $currentPrice['zarticle']?:"");
        return $result;
    }

    /**
     * разбивает csv файл построчно и добавляет значения в базу, если такого товара нет, то записывает новый
     *
     * @param $file resource путь к временному файлу
     * @throws
     *
     * @return mixed
     */
    public function parseCsv ($file)
    {

        $openedFile = fopen($file['document']['tmp_name'], "rb");
        try {
            while (($item = fgetcsv($openedFile, 0, self::DELIMITER)) != false) {
                $propertiesOfCurrentPosition = $this->getProperties($item);
                try {
                    if ($propertiesOfCurrentPosition['good_id']) {
                        if ($propertiesOfCurrentPosition['good_properties'] != null && $propertiesOfCurrentPosition['good_properties'] != ""){
                            $this->priceTable->saveConformInfo(array("good_id" => $propertiesOfCurrentPosition['good_id'], "price_id" => $propertiesOfCurrentPosition['price_id']));
                            $result[] = array("id" => $propertiesOfCurrentPosition['good_id'], "good" => $propertiesOfCurrentPosition['good_in_csv'], "state" => "Добавленные товары");
                        }
                        else{
                            $result[] = $this->updateGood($propertiesOfCurrentPosition);
                        }
                    }
                    elseif(!$propertiesOfCurrentPosition['good_id'] && trim($propertiesOfCurrentPosition['good_in_csv']) != "") {
                        $result[] = $this->updateGood($propertiesOfCurrentPosition);
                    }
                } catch (\Exception $e) {
                    $result[] = array("id" => "", "good" => $propertiesOfCurrentPosition['good_in_csv'], "state" => $e->getMessage());
                }
            }
            return $result;
        }
        catch (\Exception $e) {
            return  $e->getMessage();
        }
    }

    /**
     * формирует строку с учетом передаваемого фильтра
     *
     * @param $filter
     * @return string
     */
    public function getCsv($filter)
    {
        $results = $this->priceTable->getPriceInfoForDownload($filter);
        $str = '';
        foreach(current($results) as $res) {
            $str.=  "\"".current($results)->current()['id']."\";\"".iconv('utf-8','cp1251//TRANSLIT',current($results)->current()['good'])."\";\"".iconv('utf-8','cp1251//TRANSLIT',current($results)->current()['good_orig'])."\"\r\n";
        }
        return $str;
    }
}