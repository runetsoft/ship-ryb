<?php
namespace Price\Model;

use Application\Model\BaseModelTable;
use Application\Utilities\FieldType;
use Application\Utilities\UtilitiesGoodType;
use Application\Utilities\DateFormat;
use Application\Utilities\Utilite;
use Dictionary\Model\DictionaryTable;
use Good\Model\GoodAttributesTable;
use Good\Model\GoodTyre;
use Price\Service\PriceParser;
use Zend\Code\Scanner\Util;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;

use Zend\Session\Container;
use Price\Service\ImportFileReader;

/**
 * Class PriceTable работа с таблицами прайса.
 *
 * @package Price\Model
 */
class PriceTable extends BaseModelTable
{
    protected $dbAdapter;
    protected $sm;
    protected $priceTableGateway;
    protected $goodPriceTableGateway;
    protected $goodTyreTableGateway;
    protected $goodDiskTableGateway;
    /**
     * @var \Price\Model\SupplierPriceImportTable;
     */
    protected $supplierPriceImportTable;

    /**
     * @var GoodAttributesTable
     */
    protected $goodAttributesTable;

    /**
     * @var DictionaryTable
     */
    protected $goodTypesDictionaryTable;
    protected $session;

    /**
     * GoodTable constructor.
     * @param \Zend\Db\TableGateway\TableGateway $priceTableGateway
     */
    public function __construct($sm, TableGateway $priceTableGateway)
    {
        $this->session = new Container('price');
        $this->dbAdapter = $priceTableGateway->getAdapter();
        $this->sm = $sm;
        $this->priceTableGateway = $priceTableGateway;
        $this->goodAttributesTable = $this->sm->get('Good\Model\GoodAttributesTable');
        $this->goodTypesDictionaryTable = $this->sm->get('GoodTypeDictionary');
    }

    public function getAllInfoPaginator($page, $limit, array $filter, $sort)
    {
        $iteratorAdapter = $this->getPriceInfo($filter, $sort);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);
        return array($paginator, $iteratorAdapter->count());
    }

    /**

     * @param $where
     *
     * @return mixed
     */
    public function fetch($where)
    {
        $resultSet = $this->priceTableGateway->select($where);
        return $resultSet;
    }

    /**
     * @param array $filter
     * @param $sort
     * @return Iterator
     */
    public function getPriceInfo(array $filter = array(), $sort)
    {
        $price = new Price();
        $priceColumns = $price->getColumns();
        $goodTableName = $this->sm->get('GoodTableGateway')->getTable();
        $priceTableName = $this->priceTableGateway->getTable();
        $supplierTableName = $this->sm->get('SupplierTableGateway')->getTable();

        $select = $this->priceTableGateway->getSql()->select()
            ->columns($priceColumns)
            ->join($supplierTableName, $supplierTableName . '.id = ' . $priceTableName . '.provider_id',
                array(
                    'provider_name' => 'provider',
                    'provider_id' => 'id'
                ),
                Select::JOIN_LEFT)
            ->join($goodTableName, $goodTableName . '.id = ' . $priceTableName . '.good_id',
                array(
                    'good_orig' => 'good',
                    'good_type' => 'type',
                    'good_rprice' => 'rprice',
                    'good_zarticle' => 'zarticle',
                    'good_photo' => 'photo',
                    'good_dt_update' => 'dt_update'
                ),
                Select::JOIN_LEFT);

        if (!empty($filter['good_type'])) {
            if (in_array(UtilitiesGoodType::NO_TYPE, $filter['good_type'])) {
                $select->where($goodTableName . '.type IS NULL');
            } else if (!in_array(UtilitiesGoodType::ALL, $filter['good_type'])) {
                $this->setFilter($select, array('type' => $filter['good_type']), $goodTableName);
            }
            unset($filter['good_type']);
        }

        if ($filter['good_orig']) {
            switch ($filter['good_orig']) {
                case 'null' :
                    $select->where($goodTableName . '.id IS NULL');
                    unset($filter['good_orig']);
                    break;
                case 'not_null' :
                    $select->where($goodTableName . '.id IS NOT NULL');
                    unset($filter['good_orig']);
                    break;
                default :
                    $select->where(array($goodTableName . '.id =?' => $filter['good_orig']));
                    unset($filter['good_orig']);
            }
        }

        $this->setFilter($select, $filter, $this->priceTableGateway->getTable());

        if ($sort && $sort->property) {
            $select->order(array(
                    $sort->property . ' ' . $sort->direction
                )
            );
        } else {
            $select->order(array(
                    'id ASC'
                )
            );
        }

        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return new Iterator($resultSet);
    }

    /**
     * Получает полную информацию по товарам.
     *
     * @param filter array
     *
     * @param sort
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getAllInfo(array $filter = array(), $sort)
    {
        $price = new Price();
        $priceColumns = $price->getColumns();

        $goodTableName = $this->sm->get('GoodTableGateway')->getTable();
        $priceTableName = $this->priceTableGateway->getTable();
        $supplierTable = $this->sm->get('SupplierTableGateway')->getTable();
        // TODO id = 4, NO Type.
        $goodTypesFilter = (!empty($filter['good_type']) && is_array($filter['good_type'])
            && empty(current($filter['good_type']))) ? array('!id' => UtilitiesGoodType::ALL) : array('id' => $filter['good_type']);
        $goodTypesFilter['!code'] = Utilite::GOOD;
        $goodTypes = $this->goodTypesDictionaryTable->getList(array('filter' => $goodTypesFilter));

        $brandAndModelExpression = $this->generateBrandAndModelExpressions($goodTypes);
        $priceColumns = array_merge($priceColumns, $brandAndModelExpression);

        $select = $this->priceTableGateway->getSql()->select()
            ->columns($priceColumns)
            ->join($supplierTable, $this->sm->get('SupplierTableGateway')->getTable() . '.id = ' . $priceTableName . '.provider_id',
                array('provider_name' => 'provider', 'provider_id' => 'id'),
                Select::JOIN_LEFT)
            ->join($goodTableName, $goodTableName . '.id = ' . $priceTableName . '.good_id',
                array(
                    'good_orig' => 'good',
                    'good_type' => 'type',
                    'good_rprice' => 'rprice',
                    'good_zarticle' => 'zarticle',
                    'good_photo' => 'photo',
                    'good_dt_update' => 'dt_update',
                    'good_dt_update_photo' => 'dt_update_photo'
                ),
                Select::JOIN_LEFT)
            ->join($typeTableName = Utilite::getTableFullName('dictionary_good_type'), $goodTableName . '.type = ' . $typeTableName . '.id',
                array(
                    'good_type_name' => 'name'
                ),
                Select::JOIN_LEFT);
        foreach ($goodTypes as $goodType) {
            $this->goodAttributesTable->setGoodType($goodType['id']);
            $typeTableName = Utilite::getTableFullName(Utilite::GOOD . '_' . $goodType['code']);
            $brandTableName = Utilite::getDictionaryTableName($goodType['code']) . '_brand';
            $modelTableName = Utilite::getDictionaryTableName($goodType['code']) . '_model';
            $goodColumns = $this->goodAttributesTable->getColumns($goodType['code'] . '_');
            $select->join($typeTableName, $typeTableName . '.good_id = ' . $priceTableName . '.good_id',
                $this->goodAttributesTable->getColumns($goodType['code'] . '_'),
                Select::JOIN_LEFT)
                ->join($brandTableName, $brandTableName . '.id = ' . $typeTableName . '.brand',
                    array(),
                    Select::JOIN_LEFT)
                ->join($modelTableName, $modelTableName . '.id = ' . $typeTableName . '.model',
                    array(),
                    Select::JOIN_LEFT);
            foreach ($goodColumns as $colFullName => $colName) {
                preg_match('/^(' . implode('|', [FieldType::DICTIONARY, FieldType::DICTIONARYMULTISELECT]) . ')_(.*)/',
                    $colName, $matches);
                if (!empty($matches)) {
                    list(, , $dictionaryName) = $matches;
                    $dictionaryTableName = Utilite::getTableFullName(FieldType::DICTIONARY . '_' . $dictionaryName);
                    if (substr($colName, 0, strlen(FieldType::DICTIONARYMULTISELECT)) === FieldType::DICTIONARYMULTISELECT) {
                        $dictionaryMultiselectTableName = Utilite::getTableFullName(FieldType::DICTIONARYMULTISELECT . '_' . $dictionaryName);
                        $select->join($dictionaryMultiselectTableName, $dictionaryMultiselectTableName . '.good_id = ' . $typeTableName . '.good_id',
                            array(
                                $colFullName => new Expression('GROUP_CONCAT(`'. $dictionaryTableName . '`.`id`, \'\')')
                            ),
                            Select::JOIN_LEFT);
                        $select->join(
                            $dictionaryTableName, $dictionaryTableName . '.id = ' . $dictionaryMultiselectTableName . '.dictionary_id',
                            array(
                                $colFullName . '_name' => new Expression('GROUP_CONCAT(`'. $dictionaryTableName . '`.`name`, \'\')')
                            ),
                            Select::JOIN_LEFT
                        );
                    } else {
                        $select->join($dictionaryTableName, $dictionaryTableName . '.id = ' . $typeTableName . '.' . $colName,
                            array($colFullName . '_name' => 'name'),
                            Select::JOIN_LEFT);
                    }
                }
            }
        }
        if (!empty($filter['good_type'])) {
            if (in_array(UtilitiesGoodType::NO_TYPE, $filter['good_type'])) {
                $select->where($goodTableName . '.type IS NULL');
            } else if (!in_array(UtilitiesGoodType::ALL, $filter['good_type'])) {
                $this->setFilter($select, array('type' => $filter['good_type']), $goodTableName);
            }
            unset($filter['good_type']);
        }
        if (!empty($filter['good_orig_only'])) {
            $select->where($goodTableName . '.good IS NOT NULL');
            unset($filter['good_orig_only']);
        }
        if (!empty($filter['provider_status'])) {
            $select->where(array($supplierTable . '.status = ?' => $filter['provider_status']));
            unset($filter['provider_status']);
        }
        if (!empty($filter['provider_name'])) {
            $select->where(array($supplierTable . '.id = ?' => $filter['provider_name']));
            unset($filter['provider_name']);
        }

        if ($filter['good_orig']) {
            switch ($filter['good_orig']) {
                case 'null' :
                    $select->where($goodTableName . '.id IS NULL');
                    unset($filter['good_orig']);
                    break;
                case 'not_null' :
                    $select->where($goodTableName . '.id IS NOT NULL');
                    unset($filter['good_orig']);
                    break;
                default :
                    $select->where(array($goodTableName . '.id =?' => $filter['good_orig']));
                    unset($filter['good_orig']);
            }
        }

        $this->setFilter($select, $filter, $priceTableName);

        $select->group(array(
            'id'
        ));

        if ($sort && $sort->property) {
            $select->order(array(
                    $sort->property . ' ' . $sort->direction
                )
            );
        } else {
            $select->order(array(
                    'id ASC'
                )
            );
        }
        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return new Iterator($resultSet);
    }

    public function getPriceInfoForDownload(array $filter = array())
    {
        $price = new Price();
        $priceColumns = $price->getColumns();

        $goodTableName = $this->sm->get('GoodTableGateway')->getTable();
        $priceTableName = $this->priceTableGateway->getTable();

        $select = $this->priceTableGateway->getSql()->select()
            ->columns($priceColumns)
            ->join($goodTableName, $goodTableName . '.id = ' . $priceTableName . '.good_id',
                array(
                    'good_orig' => 'good',
                    'good_type' => 'type',
                    'good_rprice' => 'rprice',
                    'good_zarticle' => 'zarticle',
                    'good_photo' => 'photo',
                    'good_dt_update' => 'dt_update',
                    'good_dt_update_photo' => 'dt_update_photo'
                ),
                Select::JOIN_LEFT);

        if ($filter['good_orig']) {
            switch ($filter['good_orig']) {
                case 'null' :
                    $select->where($goodTableName . '.id IS NULL');
                    unset($filter['good_orig']);
                    break;
                case 'not_null' :
                    $select->where($goodTableName . '.id IS NOT NULL');
                    unset($filter['good_orig']);
                    break;
                default :
                    $select->where(array($goodTableName . '.id =?' => $filter['good_orig']));
                    unset($filter['good_orig']);
            }
        }

        $this->setFilter($select, $filter, $priceTableName);

        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return new Iterator($resultSet);
    }

    private function generateBrandAndModelExpressions(array $goodTypes)
    {
        $expressions = array();

        $brandExpressionStr = 'CASE ';
        $modelExpressionStr = 'CASE ';
        $brandIdExpressionStr = 'CASE ';
        $modelIdExpressionStr = 'CASE ';

        foreach ($goodTypes as $goodType) {
            $brandTableName = Utilite::getDictionaryTableName($goodType['code']) . '_brand';
            $modelTableName = Utilite::getDictionaryTableName($goodType['code']) . '_model';

            $brandExpressionStr .= ' 
                WHEN `' . $brandTableName . '`.`name` IS NOT NULL
                THEN `' . $brandTableName . '`.`name`';
            $modelExpressionStr .= ' 
                WHEN `' . $modelTableName . '`.`name` IS NOT NULL
                THEN `' . $modelTableName . '`.`name`';
            $brandIdExpressionStr .= ' 
                WHEN `' . $brandTableName . '`.`id` IS NOT NULL
                THEN `' . $brandTableName . '`.`id`';
            $modelIdExpressionStr .= ' 
                WHEN `' . $modelTableName . '`.`id` IS NOT NULL
                THEN `' . $modelTableName . '`.`id`';
        }

        $brandExpressionStr .= ' ELSE NULL END';
        $modelExpressionStr .= ' ELSE NULL END';
        $brandIdExpressionStr .= ' ELSE NULL END';
        $modelIdExpressionStr .= ' ELSE NULL END';

        $expressions['brand'] = new Expression($brandExpressionStr);
        $expressions['model'] = new Expression($modelExpressionStr);
        $expressions['brand_id'] = new Expression($brandIdExpressionStr);
        $expressions['model_id'] = new Expression($modelIdExpressionStr);

        return $expressions;
    }

    /**
     * Получает объект таблицы SupplierPriceImportTable
     *
     * @return \Price\Model\SupplierPriceImportTable
     */
    public function getSupplierPriceImportTable()
    {
        if (!$this->supplierPriceImportTable instanceof SupplierPriceImportTable) {
            $this->supplierPriceImportTable = $this->sm->get('Import\Model\SupplierPriceImportTable');
        }
        return $this->supplierPriceImportTable;
    }

    /**
     * Удалить товар по его ИД.
     *
     * @param $itemId
     *
     */
    public function itemDelete($itemId)
    {
        $this->priceTableGateway->delete(array('id' => $itemId));
        return true;
    }

    /**
     * Удаление предложений без соответствия.
     *
     * @param $itemId
     *
     */
    public function deleteAllItemsWithoutConform()
    {
        $price = new Price();
        $priceColumns = $price->getColumns();
        $select = $this->priceTableGateway->getSql()->select()
            ->columns(array('id'))
            ->join($this->sm->get('GoodPriceTableGateway')->getTable(), $this->sm->get('GoodPriceTableGateway')->getTable() . '.price_id = ' . $this->priceTableGateway->getTable() . '.id',
                array(),
                Select::JOIN_LEFT)
            ->join($this->sm->get('GoodTableGateway')->getTable(), $this->sm->get('GoodTableGateway')->getTable() . '.id = ' . $this->sm->get('GoodPriceTableGateway')->getTable() . '.good_id',
                array(),
                Select::JOIN_LEFT)
            ->where($this->sm->get('GoodTableGateway')->getTable() . '.good IS NULL');


        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        foreach ($resultSet as $res) {
            $this->priceTableGateway->delete(array('id' => $res['id']));
        }
    }

    /**
     * поиск соответсвий для товаров выведенных на страницу
     * @param $priceIds array
     *
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */

    public function getConform($priceIds)
    {
        $select = $this->priceTableGateway->getSql()->select()
            ->columns(array('price_id' => 'id'))
            ->join(
                $this->sm->get('GoodTableGateway')->getTable(),
                $this->sm->get('GoodTableGateway')->getTable() . '.zarticle = ' . $this->sm->get('PriceTableGateway')->getTable() . '.zarticle',
                array(
                    'good',
                    'good_id' => 'id'
                ),
                Select::JOIN_LEFT
            )
            ->where(
                array($this->priceTableGateway->getTable() . '.id' => $priceIds)
            )
            ->where(
                array('length(' . $this->sm->get('PriceTableGateway')->getTable() . '.zarticle ) > 3')
            )
            ->where(
                array($this->sm->get('GoodTableGateway')->getTable() . '.zarticle != \'\'')
            );

        if (!empty($priceIds)) {
            $select->where(
                array($this->priceTableGateway->getTable() . '.id' => $priceIds)
            );
        }

        $statement = $this->priceTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();


        return $resultSet;
    }

    /**
     * сохранение массива соответствий и удаление тех которые не имеют соответствий
     *
     * @param $priceIds array
     * @return bool
     * @throws \Exception
     */

    public function saveConformAndDelete($priceIds)
    {
        if (!$priceIds || !count($priceIds)) {
            throw new \Exception('Не переданы ID!');
        }
        foreach ($priceIds as $priceId) {
            if ($priceId->good_id) {
                $this->saveConformInfo($priceId);
            } else {
                $results = $this->sm->get('PriceTableGateway')->select(
                    array('id' => $priceId->price_id)
                );
                $goodRow = $results->current();
                if ($goodRow) {
                    $goodRow->delete();
                }
            }
        }
        return true;
    }

    /**
     * сохранение соответсвий по одному
     *
     * @param $priceId array or object
     * @return bool
     * @throws \Exception
     */

    public function saveConformInfo($priceId)
    {
        if (!$priceId) {
            throw new \Exception('Не переданы ID!');
        }
        if (is_array($priceId)) $priceId = (object)$priceId;

        $date = new \DateTime();

        $this->updateInfo(array('id' => $priceId->price_id, 'good_id' => $priceId->good_id));

        // TODO для старого портала.
        $results = $this->sm->get('GoodPriceTableGateway')->select(
            array('price_id' => $priceId->price_id)
        );
        $goodRow = $results->current();

        if (!$goodRow) {
            $goodRow = $this->sm->get('GoodPriceRow');
            $goodRow->price_id = $priceId->price_id;
        }

        $goodRow->good_id = $priceId->good_id;
        $goodRow->adm_id = 1;
        $goodRow->dt_insert = $date->format('Y-m-d H:i:s');
        $goodRow->save();
        return true;

    }

    /**
     * Обновить информацию.
     *
     * @param array $data
     */
    public function updateInfo(array $data)
    {
        $priceRow = false;
        $now = (new \DateTime())->format(DateFormat::STANDARD);

        if (!$this->priceTableGateway instanceof Price) {
            $results = $this->priceTableGateway->select(array('id' => $data['id']));
            $priceRow = $results->current();
        }

        if (!$priceRow) {
            $priceRow = $this->getPriceRowDefault();
        }

        $data['dt_update'] = $now;
        foreach ($priceRow->toArray() as $key => $item) {
            $priceRow->{$key} = (empty($data[$key])) ? $item : $data[$key];
        }
        $priceRow->save();
        return $priceRow->id;
    }

    /**
     * Возвращает новый PriceRow объект с значениями по умолчанию.
     *
     * @return mixed
     */
    private function getPriceRowDefault()
    {
        $now = (new \DateTime())->format(DateFormat::STANDARD);
        $priceRow = $this->sm->get('PriceRow');
        $priceRow->good_id = 0;
        $priceRow->dt_update = $now;
        $priceRow->dt_insert = $now;
        $priceRow->dt_price = $now;
        $priceRow->provider_id = 0;
        $priceRow->good = '';
        $priceRow->photo = '';
        $priceRow->price = 0;
        $priceRow->rprice = 0;
        $priceRow->sarticle = null;
        $priceRow->zarticle = null;
        $priceRow->sklad = 0;
        $priceRow->pass = 1;
        $priceRow->status = 1;

        return $priceRow;
    }
}