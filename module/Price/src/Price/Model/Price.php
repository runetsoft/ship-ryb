<?php
namespace Price\Model;

use Application\Model\BaseModel;

/**
 * Class Price Таблица cms_price
 * @package Price\Model
 */
class Price extends BaseModel
{
    /**
     * @description "ID"
     */
    public $id;

    /**
     * @description "ID товара"
     */
    public $good_id;

    /**
     * @description "Дата добавленния"
     */
    public $dt_insert;

    /**
     * @description "Дата обновления"
     */
    public $dt_update;

    /**
     * @description "Дата обновления цены"
     */
    public $dt_price;

    /**
     * @description "ID поставщика"
     */
    public $provider_id;

    /**
     * @description "Название товара"
     */
    public $good;

    /**
     * @description "Фото"
     */
    public $photo;

    /**
     * @description "Цена"
     */
    public $price;

    /**
     * @description "RЦена"
     */
    public $rprice;

    /**
     * @description "ЗА"
     */
    public $zarticle;

    /**
     * @description "Артикул поставщика"
     */
    public $sarticle;


    /**
     * @description "Склад"
     */
    public $sklad;

    /**
     * @description "Пропуск"
     */
    public $pass;

    /**
     * @description "Статус"
     */
    public $status;
}