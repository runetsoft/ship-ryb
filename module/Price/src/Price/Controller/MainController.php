<?php
namespace Price\Controller;

use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class MainController extends AbstractActionController
{

    /**
     * @var \Price\Service\CsvParser
     */
    protected $csvParser;

    /**
     * @var \Price\Model\PriceTable
     */
    protected $priceTable;

    /**
     * @var \Import\Service\PriceParser
     */
    protected $priceParser;
    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;
    /**
     * MainController constructor.
     * @param \Price\Model\PriceTable $priceTable
     * @param \Import\Service\PriceParser $priceParser
     * @param \Price\Service\CsvParser
     */
    public function __construct(\Price\Model\PriceTable $priceTable, \Import\Service\PriceParser $priceParser, \Price\Service\CsvParser $csvParser)
    {
        $this->priceTable = $priceTable;
        $this->priceParser = $priceParser;
        $this->csvParser = $csvParser;
        $this->request = $this->getRequest();
    }

    /**
     * Список товаров поставщиков.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function listAction()
    {
        try {
            $query = $this->request->getQuery();
            $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit', 'sort')));
            unset($filter['_dc']);
            list($results, $total) = $this->priceTable->getAllInfoPaginator((int)$query->get('page'), (int)$query->get('limit'), $filter, $query->get('sort'));

            $list = array();
            foreach ($results as $row) {
                $row['good_url'] = rawurlencode($row['good']);
                $list[] = $row;
            }

            return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *  Удаление товара.
     *
     * @return JsonModel
     *
     */

    public function itemDeleteAction()
    {
        $query = $this->request->getQuery();
        $itemId = $query->get('itemId');
        try {
            $results = $this->priceTable->itemDelete($itemId);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *  Удаление предложений без соответствия.
     *
     * @return JsonModel
     *
     */

    public function deleteAllItemsWithoutConformAction()
    {
        try {
            $results = $this->priceTable->deleteAllItemsWithoutConform();
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *  поиск соответствий для предложений
     *
     * @return JsonModel
     *
     */

    public function getConformAction()
    {
        $priceIds = (array)Json::decode($this->request->getPost('priceIds'));
        $conform = null;
        $data = array();
        $results = $this->priceTable->getConform(array_keys($priceIds));
        foreach ($results as $result) {
            $data[$result['price_id']][] = $result;
        }
        foreach ($priceIds as $key => &$priceId) {
            $priceId = $data[$key] ? $data[$key] : array(
                array('price_id' => $key,
                    'good' => null,
                    'good_id' => null
                )
            );
        }
        return new JsonModel(array("success" => true, "data" => array_values($priceIds)));
    }

    /**
     *  сохранение соответствий для предложенией
     *
     * @return JsonModel
     *
     */
    public function saveConformAction()
    {
        $priceIds = (array)Json::decode($this->request->getPost('data'));
        try {
            $res = $this->priceTable->saveConformAndDelete($priceIds);
            return new JsonModel(array("success" => true, "data" => $res));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false));
        }

    }

    /**
     *  сохранение соответствия для конкретного предложения
     *
     * @return JsonModel
     *
     */

    public function saveConformInfoAction()
    {
        $priceId = (array)Json::decode($this->request->getPost('data'));
        try {
            $res = $this->priceTable->saveConformInfo($priceId);
            return new JsonModel(array("success" => true, "data" => $res));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false));
        }
    }

    /**
     *  получение данных для окна редактирования соответствия
     *
     * @return JsonModel
     *
     */

    public function getConformInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $filter = $query->toArray();
            unset($filter['_dc']);
            $data = $this->priceTable->getPriceInfo($filter, null);
            return new JsonModel(array("success" => true, "data" => current($data)->current()));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }


    /**
     * выгрузка csv файла со столбцами id, название и соответствие
     *
     * @return \Zend\Stdlib\ResponseInterface|JsonModel
     */

    public function downloadAction() {
        $filter = (array)Json::decode($this->request->getQuery('data'));
        try {
            $str = $this->csvParser->getCsv($filter);
            $date = new \DateTime();
            $response = $this->getResponse();
            $response->setContent($str);
            $headers = new \Zend\Http\Headers();
            $headers->addHeaders(array(
                'Content-Disposition' => 'attachment; filename="'.$date->format('Y-m-d H:i:s').'.csv"',
                'Content-Type' => 'application/octet-stream',
                'Content-Length' => strlen($str)
            ));
            $response->setHeaders($headers);
            return $response;

        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     *  загрузка и обработка csv файла
     */

    public function uploadAction() {
        try {
            $queryFile = $this->request->getFiles();

            $file = $queryFile->toArray();

            $res = $this->csvParser->parseCsv($file);

            return new JsonModel(array("success" => true, "data" => $res));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }
}
