<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Price\Controller\MainController' => 'Price\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'price_list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/price[/:action].json',
                    'defaults' => array(
                        'controller' => 'Price\Controller\MainController'
                    ,
                    ),
                ),
            )
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);