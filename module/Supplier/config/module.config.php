<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Supplier\Controller\MainController' => 'Supplier\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'supplier_list' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/supplier[/:action].json',
                    'defaults' => array(
                        'controller' => 'Supplier\Controller\MainController'
                      ,
                    ),
                ),
            )
        ),
     ),    
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        )
    )
);