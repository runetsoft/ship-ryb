<?php
namespace Supplier\Controller;

use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class MainController extends AbstractActionController
{
    /**
     * @var \Supplier\Model\SupplierTable
     */
    protected $supplierTable;
    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    public function __construct($supplierTable)
    {
        $this->supplierTable = $supplierTable;
        $this->request = $this->getRequest();
    }

    /**
     * Список поставщиков.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function listAction()
    {
        $query = $this->request->getQuery();
        $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit')));
        unset($filter['_dc']);
        list($results, $total) = $this->supplierTable->getAllInfo(
            (int)$query->get('page'),
            (int)$query->get('start'),
            (int)$query->get('limit'),
            $filter, $query->get('sort'));
        $list = array();
        foreach ($results as $row) {
            $list[] = $row;
        }

        return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
    }

    /**
     * Информация по Поставщику.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function supplierInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $results = $this->supplierTable->getSupplierInfo((int)$query->get('supplierId'));
            return new JsonModel(array("success" => true, "data" => $results?$results->toArray():""));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }


    /**
     * Удаление по поставщика по его ИД.
     *
     * @return \Zend\View\Model\JsonModel
     */

    public function supplierDeleteAction()
    {
        $query = $this->request->getQuery();
        $supplierId = $query->get('supplierId');
        try {
            $results = $this->supplierTable->itemDelete($supplierId);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Обвновление данных поставщика
     *
     * @return \Zend\View\Model\JsonModel
     */


    public function supplierUpdateAction()
    {
        $query = $this->request->getPost('data');
        $data = Json::decode($query);
        try {
            $results =  $this->supplierTable->itemUpdate((array)$data);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Список городов
     *
     * @return \Zend\View\Model\JsonModel
     */


    public function citiesListAction()
    {
        $query = $this->request->getQuery();
        $where = $query->get('query');
        try {
            $results =  $this->supplierTable->getCities($where);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Список Групп Поставщиков
     *
     * @return \Zend\View\Model\JsonModel
     */

    public function groupsListAction()
    {
        $query = $this->request->getQuery();
        $where = $query->get('query');
        try {
            $results =  $this->supplierTable->getSupplierGroups($where);
            return new JsonModel(array("success" => true, "data" => $results));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }
}