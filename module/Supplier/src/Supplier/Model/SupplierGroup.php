<?php
namespace Supplier\Model;

use Application\Model\BaseModel;

class SupplierGroup extends BaseModel
{
    public $id;
    public $provider_group;
}