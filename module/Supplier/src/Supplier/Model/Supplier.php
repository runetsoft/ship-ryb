<?php
namespace Supplier\Model;

use Application\Model\BaseModel;

class Supplier extends BaseModel
{
    public $id;
    public $dt_insert;
    public $dt_update;
    public $dt_price_load;
    public $city_id;
    public $provider_group_id;
    public $provider;
    public $org_name;
    public $status;
    public $uuid;
    public $phone;
    public $messenger;
    public $email_order;
    public $email_balance;
    public $payment_method;
    public $comments;
    public $run_import;
}