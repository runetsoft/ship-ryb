<?php
namespace Supplier\Model;

use Application\Model\BaseModel;

class SupplierCity extends BaseModel
{
    public $id;
    public $city;
}