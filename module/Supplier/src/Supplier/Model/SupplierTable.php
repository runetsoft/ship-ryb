<?php
namespace Supplier\Model;

use Import\Model\SupplierPriceImport;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Like;
use Zend\Form\Element\DateTime;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;
use Zend\Json\Json;
use Application\Utilities\DateFormat;
use Application\Model\BaseModelTable;

class SupplierTable extends BaseModelTable
{
    protected $dbAdapter;
    protected $sm;
    protected $supplierTableGateway;
    protected $supplierGroupTableGateway;
    protected $supplierPriceImportGateway;


    /**
     * SupplierTable constructor.
     * @param \Zend\Db\TableGateway\TableGateway $supplierTableGateway
     * @param \Zend\Db\TableGateway\TableGateway $supplierGroupTableGateway
     * @param \Zend\Db\TableGateway\TableGateway $supplierPriceImportGateway
     */
    public function __construct($sm, TableGateway $supplierTableGateway, TableGateway $supplierGroupTableGateway, TableGateway $supplierPriceImportGateway)
    {
        $this->dbAdapter = $supplierTableGateway->getAdapter();
        $this->sm = $sm;
        $this->supplierTableGateway = $supplierTableGateway;
        $this->supplierGroupTableGateway = $supplierGroupTableGateway;
        $this->supplierPriceImportGateway = $supplierPriceImportGateway;
    }

    /**
     * Получить все записи.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->supplierTableGateway->select();
        return $resultSet;
    }

    /**
     * Запрос на выборку.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetch($where)
    {
        $resultSet = $this->supplierTableGateway->select($where);
        return $resultSet;
    }

    /**
     * GПолучить поставщика по id
     *
     * @param $id
     * @return array|\ArrayObject|null
     */
    public function fetchOne($id)
    {
        $suppObj = $this->fetch(array('id'=>$id));
        if (!$suppObj)
            return null;
        return $suppObj->current();
    }

    /**
     * Получает полную информацию по товарам.
     *
     * @param $page
     * @param $start
     * @param $limit
     * @param $filter - поиск поставщика
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getAllInfo($page, $start, $limit, $filter, $sort = false)
    {
        $supplier = new Supplier();
        $supplierTableName = $this->supplierTableGateway->getTable();
        $supplierColumns = $supplier->getColumns();

        $supplierColumns['price_cnt'] = new Expression('(SELECT count(*) FROM cms_price WHERE provider_id=' . $supplierTableName . '.id' . ')');
        $supplierColumns['price_full_cnt'] = new Expression('(SELECT count(*) FROM cms_price WHERE provider_id=' . $supplierTableName . '.id' . ' AND sklad!=0 AND price!=0)');

        $select = $this->supplierTableGateway->getSql()->select()
            ->columns($supplierColumns)
            ->join(
                $this->sm->get('SupplierGroupTableGateway')->getTable(),
                $this->sm->get('SupplierGroupTableGateway')->getTable() . '.id = ' . $supplierTableName . '.provider_group_id',
                array('provider_group'
                ),
                Select::JOIN_LEFT)
            ->join(
                'cms_city',
                'cms_city.id = ' . $supplierTableName . '.city_id',
                array('city'),
                Select::JOIN_LEFT)
            ->join(
                $this->supplierPriceImportGateway->getTable(),
                $this->supplierPriceImportGateway->getTable() . '.supplier_id = ' . $supplierTableName . '.id',
                array(
                    'imports' => new Expression('GROUP_CONCAT(`cms_supplier_price_import`.`id`)'),
                    'imports_name' => new Expression('GROUP_CONCAT(`cms_supplier_price_import`.`import_info`)'),
                    'date_import' =>  new Expression('MAX(`cms_supplier_price_import`.`date_import`)'),
                    ),
                Select::JOIN_LEFT)
            ->group($supplierTableName . '.id');

            if ($sort && $sort->property) {
                $select->order(array(
                        $sort->property.' '. $sort->direction
                    )
                );
            } else {
                $select->order(array(
                        'provider ASC',
                        'dt_price_load DESC'
                    )
                );
            }

        foreach ($filter as $key => $value) {
            switch ($key) {
                case '>=date_import':
                case '<date_import':
                    $this->setFilter($select, array($key => $value), $this->supplierPriceImportGateway->getTable());
                    unset($filter[$key]);
                    break;
            }
        }

        $this->setFilter($select, $filter, $supplierTableName);

        $statement = $this->supplierTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);

        return array($paginator, $resultSet->count());
    }

    /**
     * Получаем информацию поставщика по его ИД.
     *
     * @param $supplierId
     * @return bool
     */

    public function getSupplierInfo($supplierId)
    {
        $results = $this->supplierTableGateway->select(array('id' => $supplierId));
        $artistRow = $results->current();
        return $artistRow;
    }

    /**
     * Удаляем поставщика по его ИД.
     *
     * @param $supplierId
     * @return bool
     */
    public function itemDelete($supplierId)
    {
        $this->supplierTableGateway->delete(array('id' => $supplierId));
        return true;
    }

    /**
     * Обновление данных поставщика.
     */

    public function itemUpdate($data)
    {

        $data['status'] = ($data['status'] == 'on') ? 1 : 0;
        $data['run_import'] = ($data['run_import'] == 'on') ? 1 : 0;
        $data['dt_insert'] = (empty($data['dt_insert'])) ? (new \DateTime())->format(DateFormat::STANDARD) : $data['dt_insert'];
        $data['dt_update'] = (empty($data['dt_update'])) ? (new \DateTime())->format(DateFormat::STANDARD) : $data['dt_update'];
        $data['dt_price_load'] = (empty($data['dt_price_load'])) ? (new \DateTime())->format(DateFormat::STANDARD) : $data['dt_price_load'];
        $data['org_name'] = (empty($data['org_name'])) ? '' : $data['org_name'];
        $data['uuid'] = (empty($data['uuid'])) ? '' : $data['uuid'];

        if (empty( $data['city_id'])){
            unset($data['city_id']);
        }

        if (!empty($data['id'])) {
            $this->supplierTableGateway->update($data, array('id' => $data['id']));
        } else {
            if (isset($data['id'])) {
                unset($data['id']);
            }
            $this->supplierTableGateway->insert($data);
        }
        return true;
    }

    /**
     * Получение списка городов
     * @param $query - запрос поиска по названию города
     * @return array
     */
    public function getCities($query)
    {
        $cities = array();
        $result = $this->sm->get('CitiesTableGateway')->select(
            function (Select $select) use ($query) {
                $select->where->like('city', '%' . $query . '%');
                $select->order('id ASC');
            }
        );

        foreach ($result as $city) {
            $cities[] = $city;
        }

        return $cities;
    }

    /**
     * Получение списка групп поставщиков
     * @param $query - запрос поиска по названию группы поставщика
     * @return array
     */
    public function getSupplierGroups($query)
    {
        $groups = array();
        $result = $this->sm->get('SupplierGroupTableGateway')->select(
            function (Select $select) use ($query) {
                $select->where->like('provider_group', '%' . $query . '%');
                $select->order('id ASC');
            }
        );

        foreach ($result as $group) {
            $groups[] = $group;
        }

        return $groups;
    }

}