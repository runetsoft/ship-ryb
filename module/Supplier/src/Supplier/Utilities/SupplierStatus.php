<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 17.06.2017
 * Time: 17:24
 */

namespace Supplier\Utilities;

use Application\Utilities\BaseConstant;

class SupplierStatus extends BaseConstant
{
    /**
     * Тип загрузки url.
     */
    const ACTIVE = 1;

    /**
     * Тип загрузки письмо.
     */
    const NON_ACTIVE = 0;

    /**
     * Описание констант.
     *
     * @var array
     */
    public $descriptions = array(
        'ACTIVE' => array(
            'description' => 'Активный',
            'type' => Type::INT
        ),
        'NON_ACTIVE' => array(
            'description' => 'Неактивный',
            'type' => Type::INT
        )
    );
}