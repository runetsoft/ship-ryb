<?php
namespace Supplier;

use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Supplier\Model\Supplier;
use Supplier\Model\SupplierGroup;
use Supplier\Model\SupplierCity;
use Supplier\Model\SupplierTable;
use Import\Model\SupplierPriceImport;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Supplier\Model\SupplierTable' => function ($sm) {
                    $tableGateway = $sm->get('SupplierTableGateway');
                    $supplierGroupTableGateway = $sm->get('SupplierGroupTableGateway');
                    $supplierPriceImportGateway = $sm->get('SupplierPriceImportGateway');
                    $table = new SupplierTable($sm, $tableGateway, $supplierGroupTableGateway, $supplierPriceImportGateway);
                    return $table;
                },
                'SupplierTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $features = new RowGatewayFeature('id');
                    $resultSetPrototype->setArrayObjectPrototype(new Supplier());
                    return new TableGateway('cms_provider', $dbAdapter, $features, $resultSetPrototype);
                },
                'SupplierGroupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierGroup());
                    return new TableGateway('cms_provider_group', $dbAdapter, null, $resultSetPrototype);
                },
                'SupplierPriceImportGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPriceImport());
                    return new TableGateway('cms_supplier_price_import', $dbAdapter, null, $resultSetPrototype);
                },
                'CitiesTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierCity());
                    return new TableGateway('cms_city', $dbAdapter, null, $resultSetPrototype);
                }
            )
        );
    }
}