<?php
namespace Export;

use Cron\Service\Run;
use Export\Model\SupplierPriceExport;
use Export\Model\SupplierPriceExportTable;
use Export\Service\Cron;
use Export\Service\PriceExport;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/**
 * Class Module
 *
 * @package Export
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Export\Model\SupplierPriceExportTable' => function ($sm) {
                    $supplierPriceExportTableGateway = $sm->get('SupplierPriceExportTableGateway');
                    $table = new SupplierPriceExportTable($sm, $supplierPriceExportTableGateway);
                    return $table;
                },
                'SupplierPriceExportTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = new RowGatewayFeature('id');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPriceExport());
                    return new TableGateway('cms_supplier_price_export', $dbAdapter, $features, $resultSetPrototype);
                },
                'Export\Model\SupplierPriceExport' => function () {
                    $table = new SupplierPriceExport();
                    return $table;
                },
                'Export\Service\Cron' => function ($sm) {
                    $cron = new Cron($sm);
                    return $cron;
                },
                'Export\Service\PriceExport' => function ($sm) {
                    $cron = new PriceExport($sm);
                    return $cron;
                },
                'Cron\Service\Run' => function ($sm) {
                    $run = new Run($sm);
                    return $run;
                }
            ),
            'aliases' => array(),
            'abstract_factories' => array(),
            'shared' => array()
        );
    }
}