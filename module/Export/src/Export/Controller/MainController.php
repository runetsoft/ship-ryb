<?php
namespace Export\Controller;

use Ddeboer\Imap\Exception\Exception;
use Export\Service\Cron;
use Export\Service\FileSaverConfig;
use Export\Service\FileSaverCsv;
use Zend\Db\Sql\Join;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Json\Json;

class MainController extends AbstractActionController
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $sm;

    /**
     * @var \Price\Model\PriceTable
     */
    protected $priceTable;

    /**
     * @var \Export\Model\SupplierPriceExportTable
     */
    protected $supplierPriceExportTable;

    /**
     * MainController constructor.
     * @param $sm
     * @param $priceTable
     * @param $supplierPriceExportTable
     */
    public function __construct(
        $sm,
        $priceTable,
        $supplierPriceExportTable
    )
    {
        $this->sm = $sm;
        $this->priceTable = $priceTable;
        $this->supplierPriceExportTable = $supplierPriceExportTable;
    }

    /**
     * Список экспортов.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function listAction()
    {
        $query = $this->request->getQuery();

        $filter = array_diff_key($query->toArray(), array_flip(array('page', 'start', 'limit')));
        list($results, $total) = $this->supplierPriceExportTable->getAllInfoPaginator(
            (int)$query->get('page'),
            (int)$query->get('start'),
            (int)$query->get('limit'),
            $filter,
            array('id', 'name', 'url')
        );

        $list = array();
        foreach ($results as $row) {
            $row['url'] = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $row['url'];
            $list[] = $row;
        }

        return new JsonModel(array("success" => true, "data" => $list, "totalCount" => $total));
    }

    /**
     * Информация о экспорте.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function exportInfoAction()
    {
        try {
            $query = $this->request->getQuery();
            $results = $this->supplierPriceExportTable->getSupplierExportInfo(array('id' => (int)$query->get('exportId')));
            return new JsonModel(array("success" => true, "data" => $results->current()));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    public function uploadPriceAction()
    {
        $exportId = 0;
        $is_new_export = true;
        try {
            $query = $this->request->getPost('data');
            $data = Json::decode($query);

            $columns = (array)$data->data->columns;
            $filter = (array)$data->filter;

            $exportInfo = (array)$data->data;
            $exportInfo['columns'] = Json::encode((array)$exportInfo['columns']);
            $exportInfo['provider_id'] = empty($filter['provider_id']) ? '[]' : Json::encode($filter['provider_id']);
            $exportInfo['good_type'] = empty($filter['good_type']) ? '' : Json::encode((array)$filter['good_type']);
            $exportInfo['all_good'] = $data->data->all_good == 'on' ? 1 : 0;
            $exportInfo['min_size'] = (int)$exportInfo['min_size'];
            $exportInfo['cron_periodicity'] = (string)$exportInfo['cron_periodicity'];
            if ($data->data->id) {
                $exportInfo['id'] = (int)$data->data->id;
                $is_new_export = false;
            } else {
                $exportInfo['url'] = FileSaverConfig::EXPORT_URL_TEMPLATE;
            }

            $exportObj = $this->supplierPriceExportTable->itemUpdate($exportInfo, true);
            /**
             * @var $priceExport \Export\Service\PriceExport;
             */
            $priceExport = $this->sm->get('Export\Service\PriceExport');
            $priceExport->setSupplierPriceExportTable($exportObj);
            $priceExport->export();

            return new JsonModel(array("success" => true, "data" => array()));
        } catch (Exception $e) {
            $this->deleteExport($exportId);
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }


    public function exportDeleteAction()
    {
        $query = $this->request->getQuery();
        $exportId = $query->get('exportId');
        try {
            $this->deleteExport($exportId);

            return new JsonModel(array("success" => true));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }
    }

    /**
     * Удалить файл экспорта.
     *
     * @param $exportId
     */
    private function deleteExport($exportId)
    {
        $results = $this->supplierPriceExportTable->getSupplierExportInfo(array('id' => $exportId));
        $fileExport = $results->current();

        $rootDir = dirname(__FILE__) . '/../../../../../' . FileSaverConfig::BASE_ROOT . '/';
        $baseDir = $rootDir . dirname($fileExport['url']) . '/';

        unlink($rootDir . $fileExport['url']);
        rmdir($baseDir);

        $this->supplierPriceExportTable->itemDelete(array('id' => $exportId));

    }
}