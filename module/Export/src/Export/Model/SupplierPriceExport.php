<?php
namespace Export\Model;

use Application\Model\BaseModel;

/**
 * Class SupplierPriceExport Таблица cms_supplier_price_export
 * @package SupplierPriceExport\Model
 */
class SupplierPriceExport extends BaseModel
{
    /**
     * @description "ID"
     */
    public $id;

    /**
     * @description "Имя"
     */
    public $name;

    /**
     * @description "Поставщики"
     */
    public $provider_id;

    /**
     * @description "Тип товара"
     */
    public $good_type;

    /**
     * @description "Экспорт по крону"
     */
    public $is_cron;

    /**
     * @description "Периодичность экспортa по крону"
     */
    public $cron_periodicity;

    /**
     * @description "Колонки"
     */
    public $columns;

    /**
     * @description "Путь файла"
     */
    public $url;

    /**
     * @description "От какого количества импортировать"
     */
    public $min_size;

    /**
     * @description "Выгружать все товары даже без соответствий"
     */
    public $all_good;
}