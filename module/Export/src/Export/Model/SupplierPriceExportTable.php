<?php

namespace Export\Model;

use Application\Model\BaseModelTable;
use Ddeboer\Imap\Exception\Exception;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator;
use Zend\Session\Container;

/**
 * Class SupplierPriceExportTable работа с таблицами экспорта прайсов.
 *
 * @package Export\Model
 */
class SupplierPriceExportTable extends BaseModelTable
{
    protected $dbAdapter;
    protected $sm;
    protected $supplierPriceExportTableGateway;

    protected $supplierPriceExportTableName;

    /**
     * SupplierPriceExportTable constructor.
     * @param $sm
     * @param \Zend\Db\TableGateway\TableGateway $supplierPriceExportTableGateway
     */
    public function __construct($sm, TableGateway $supplierPriceExportTableGateway)
    {
        $this->dbAdapter = $supplierPriceExportTableGateway->getAdapter();
        $this->sm = $sm;
        $this->supplierPriceExportTableGateway = $supplierPriceExportTableGateway;
        $this->supplierPriceExport = $this->sm->get('Export\Model\SupplierPriceExport');
        $this->supplierPriceExportTableName = $this->supplierPriceExportTableGateway->getTable();
    }

    /**
     *
     *
     * @param array $filter
     * @param array $columns
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getAllInfo(array $filter = array(), array $columns = array())
    {
        $columns = (empty($columns)) ? $this->supplierPriceExport->getColumns() : $columns;
        $select = $this->supplierPriceExportTableGateway->getSql()->select()
            ->columns($columns);

        $statement = $this->supplierPriceExportTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    /**
     * @param $page
     * @param $start
     * @param $limit
     * @param array $filter
     * @param array $columns
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllInfoPaginator($page, $start, $limit, array $filter = array(), array $columns = array())
    {
        $resultSet = $this->getAllInfo($filter, $columns);
        $iteratorAdapter = new Iterator($resultSet);
        $paginator = new Paginator($iteratorAdapter);
        $paginator->setItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);
        return array($paginator, $resultSet->count());
    }

    /**
     * Добавляет новый или обновляет уже существующий экспорт.
     *
     * @param $data
     * @param $getObject
     *
     * @return int
     */
    public function itemUpdate($data, $getObject = false)
    {
        $data['is_cron'] = ($data['is_cron'] == 'on') ? 1 : 0;
        if ($data['id']) {
            $this->supplierPriceExportTableGateway->update($data, array('id' => $data['id']));
            $id = $data['id'];
        } else {
            $this->supplierPriceExportTableGateway->insert($data);
            $id = $this->supplierPriceExportTableGateway->getLastInsertValue();
            $this->supplierPriceExportTableGateway->update(array('url' => str_replace(array('{id}','{exportName}' ),
                array ($id, 'price'), $data['url'])), array('id' => $id));
        }
        if (!$getObject)
            return $id;
        $current = $this->supplierPriceExportTableGateway->select(array('id'=>$id));
        return $current->current();
    }

    /**
     * Удаление импорта.
     *
     * @param $filter
     */
    public function itemDelete($filter)
    {
        $this->supplierPriceExportTableGateway->delete($filter);
    }

    /**
     * @param array $filter
     * @param array $columns
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getSupplierExportInfo(array $filter, array $columns = array())
    {
        $columns = (empty($columns)) ? $this->supplierPriceExport->getColumns() : $columns;
        $select = $this->supplierPriceExportTableGateway->getSql()->select()
            ->columns($columns);

        $this->setFilter($select, $filter, $this->supplierPriceExportTableGateway->getTable());

        $statement = $this->supplierPriceExportTableGateway->getSql()->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getSupplierExportInfoAllObjects()
    {
        $iterator = $this->supplierPriceExportTableGateway->select();
        return $iterator;
    }

    /**
     * Обработка фильтра
     *
     * @param $arrayFilter
     *
     * @return array
     */
    private function prepareFilter($arrayFilter)
    {
        $newFilter = [];
        foreach ($arrayFilter as $key => $item) {
            $newFilter[$this->supplierPriceExportTableName . '.' . $key] = $item;
        }
        return $newFilter;
    }
}