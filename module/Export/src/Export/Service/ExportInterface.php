<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 11.06.2017
 * Time: 10:29
 */

namespace Export\Service;


interface ExportInterface
{
    /**
     * Получить итератор данных экспорта.
     *
     * @param array $filter фильр
     * @return mixed
     */
    public function getExportDataIterator($filter = null);
}