<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 30.05.2017
 * Time: 18:54
 */

namespace Export\Service;

/**
 * Class FileSaverCsv
 * @package Export\Service
 */
class FileSaverCsv implements FileSaverInterface
{
    /**
     * Число байт для буферизации. Если аргумент buffer равен 0, то операции записи не буферизуются. Это гарантирует,
     * что все операции записи с использованием функции fwrite() будут завершены перед тем, как другим процессам будет
     * позволено записывать в поток вывода.
     */
    const BUFFER = 5000;

    /**
     * Получает итератор данных, путь файла и настройки колонок, данные записывает в файл.
     *
     * @param \Zend\Db\Adapter\Driver\Pdo\Result $iterator Итератор данных.
     * @param array $config Конфигурация о заголовках и очередности.
     * @param string $file Путь файла
     *
     * @return string Путь файла
     */
    public function save(\Zend\Db\Adapter\Driver\Pdo\Result $iterator, array $config, $file)
    {
        $rowSaver = new FileSaverRow($iterator, $config, ';', '"');

        $rootDir = dirname(__FILE__) . '/../../../../../';
        $baseDir = $rootDir . dirname($file) . '/';

        if (!file_exists($baseDir)) {
            mkdir($baseDir, 0775, true);
        }

        $fp = fopen($rootDir . $file, "w");
        if ($fp) {
            if ($rowSaver->getHeaders()) {
                fwrite($fp, $rowSaver->getHeaders() . "\n");
            }
            if (stream_set_write_buffer($fp, self::BUFFER) !== 0) {
                // @TODO  Можно прописать информацию в логах
            }
            while ($row = $rowSaver->nextRow()) {
                fwrite($fp, $row . "\n");
            }
            fclose($fp);
        }
        return $file;
    }
}