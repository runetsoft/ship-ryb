<?php

namespace Export\Service;

use Zend\ServiceManager\ServiceLocatorInterface;

class Cron
{

    /**
     * @var \Export\Model\SupplierPriceExportTable
     */
    protected $supplierPriceExportTable;

    /**
     * @var \Price\Model\PriceTable
     */
    protected $priceTable;

    /**
     * @var FileSaverCsv
     */
    private $fileSaver;

    /**
     * Cron constructor.
     * @param ServiceLocatorInterface $sm
     */
    public function __construct($sm)
    {
        $this->sm = $sm;
        $this->supplierPriceExportTable = $this->sm->get('Export\Model\SupplierPriceExportTable');
        $this->priceTable = $this->sm->get('Price\Model\PriceTable');
        $this->fileSaver = new FileSaverCsv();
    }

    /**
     * Берет все экспорты из таблицы, и экспортирует заново (по периодам).
     */
    public function run()
    {
        $exportInfoIterator = $this->supplierPriceExportTable->getSupplierExportInfoAllObjects();
        foreach ($exportInfoIterator as $current) {
            try {
                /**
                 * @var $priceExport \Export\Service\PriceExport;
                 */
                $priceExport = $this->sm->get('Export\Service\PriceExport');
                $priceExport->setSupplierPriceExportTable($current);
                $priceExport->export();
            } catch (\Exception $e) {
                $this->sm->get('Logger')->crit('Ошибка формирования экспортного файла! ' . $e->getMessage());
            }

        }
    }

}