<?php

namespace Export\Service;

/**
 * Interface FileSaverRowInterface
 *
 * @package Export\Service
 */
interface FileSaverRowInterface
{
    /**
     * Должен возвращать текущую строку для записи в файл.
     *
     * @return mixed
     */
    public function nextRow();

    /**
     * Должен возвращать заголовки колонок для первой строки в файле.
     *
     * @return mixed
     */
    public function getHeaders();
}