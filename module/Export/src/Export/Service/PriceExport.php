<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 11.06.2017
 * Time: 9:48
 */

namespace Export\Service;

use Application\Model\BaseModelTable;
use Application\Utilities\FieldType;
use Application\Utilities\GoodType;
use Application\Utilities\Utilite;
use Export\Model\SupplierPriceExport;
use Good\Model\GoodAttributesTable;
use Good\Service\ExportData;
use Supplier\Utilities\SupplierStatus;
use Zend\Json\Json;
use Zend\Paginator\Paginator;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class PriceExport
 * Класс реализующий экспорт прайса.
 * @package Export\Service
 */
class PriceExport
{

    const EXPORT_SERVICE = 'GoodPriceExport';

    /**
     * @var ServiceLocatorInterface
     */
    protected $sm;

    /**
     * @var \Export\Model\SupplierPriceExport
     */
    protected $supplierPriceExport;

    /**
     * PriceExport constructor.
     * @param ServiceLocatorInterface $serviceManager
     */
    public function __construct(ServiceLocatorInterface $serviceManager)
    {
        $this->sm = $serviceManager;
    }

    /**
     * Устанавливает текущий объект с информацией об импорте.
     *
     * @param SupplierPriceExport $supplierPriceExport
     */
    public function setSupplierPriceExportTable($supplierPriceExport)
    {
        $this->supplierPriceExport = $supplierPriceExport;
    }

    public function export()
    {
        $saver = new FileSaverCsv();
        $goodTypesDictionaryTable = $this->sm->get('GoodTypeDictionary');
        /**
         * @var $exportService ExportData
         */
        $exportService = $this->sm->get(self::EXPORT_SERVICE);
        $filter = array(
            '>=sklad' => (int)$this->supplierPriceExport->min_size,
            'provider_status' => SupplierStatus::ACTIVE,
            'good_orig_only' => !$this->supplierPriceExport->all_good
        );
        if (!empty($this->supplierPriceExport->good_type)) {
            $filter['good_type'] = Json::decode($this->supplierPriceExport->good_type);
        }
        if (!empty($this->supplierPriceExport->provider_id)) {
            $filter['provider_id'] = Json::decode($this->supplierPriceExport->provider_id);
        }
        $typesFilter = (!empty($filter['good_type'])) ? array('=id' => array_values($filter['good_type'])) : array('!id' => Utilite::GOOD_NO_TYPE_ID);
        $types = $goodTypesDictionaryTable->getList(array('filter' => $typesFilter));
        $columns = array(
            'brand' => 'Товар Бренд name',
            'model' => 'Товар Модель name',
            'good_type_name' => 'Товар Тип name',
            'provider_name' => 'Предложение Поставщик name'
        );
        if (!empty($types)) {
            $table = new GoodAttributesTable($this->sm);
            foreach ($types as $type) {
                $table->setGoodType($type['id']);
                $goodAttributesColumns = $table->getColumnsCommentsInfo($table->getTableGateway(), $type['code'] . '_', array($type['code'] . '_good_id'));
                foreach ($goodAttributesColumns as $key => $item) {
                    if ($key == $type['code'] . '_brand') {
                        $columns[$key] = 'Товар Бренд';
                    } else if ($key == $type['code'] . '_model') {
                        $columns[$key] = 'Товар Модель';
                    } else {
                        $columns[$key] = implode(' ', [$type['name'], $item]);
                    }
                }
            }
        }
        // Add dictionary names.
        $dictionaryNames = array();
        foreach ($columns as $column => $description) {
            preg_match('/^(.*)_(' . implode('|', [FieldType::DICTIONARY, FieldType::DICTIONARYMULTISELECT]) . ')_(.*)/',
                $column, $matches);
            if (!empty($matches)) {
                $dictionaryNames[$column . '_name'] = $description . ' name';
            }
        }
        $columns = array_merge($columns, $dictionaryNames);
        $table = new BaseModelTable();
        $columns = array_merge($columns, $table->getColumnsCommentsInfo($this->sm->get('PriceTableGateway'), '', self::ignoreFields()));
        $columns = array_merge($columns, $table->getColumnsCommentsInfo($this->sm->get('GoodTableGateway'), 'good_', self::ignoreFields()));
//        TODO good_orig В многих местах используется.
        $columns['good_orig'] = $columns['good_good'];
        unset($columns['good_good']);

        $columns = $this->prepareColumnsForExport($columns);

        $iterator = $exportService->getExportDataIterator($filter);
        if ($iterator->count()) {
            $saver->save($iterator, $columns, FileSaverConfig::BASE_ROOT . '/' . $this->supplierPriceExport->url);
        }
    }

    static function ignoreFields()
    {
        return [
            'good_adm_id',
            'good_not_load',
            'good_dt_insert',
            'pass',
            'dt_price',
            'dt_insert',
            'rprice',
            'good',
            'sarticle',
            'zarticle',
            'photo',
            'status',
        ];
    }

    private function prepareColumnsForExport(array $columns)
    {
        $columns = array_flip($columns);
        ksort($columns);
        return array_flip($columns);
    }
}