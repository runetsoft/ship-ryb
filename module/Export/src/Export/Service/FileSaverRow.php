<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 30.05.2017
 * Time: 18:23
 */

namespace Export\Service;

/**
 * Class FileSaverRow
 * @package Export\Service
 */
class FileSaverRow implements FileSaverRowInterface
{
    /**
     * Конфигурация колонок, где ключи это ключи данных а значения заголовки колонок.
     *
     * @var array
     */
    private $config = array();

    /**
     * Заголовки.
     *
     * @var array
     */
    private $headers = array();

    /**
     * Разделитель колонок.
     *
     * @var string
     */
    private $columnDelimiter;

    /**
     * Обертка колонок.
     *
     * @var string
     */
    private $columnWrapper;

    /**
     * Итератор данных.
     *
     * @var \Zend\Db\Adapter\Driver\Pdo\Result
     */
    private $iterator;

    /**
     * FileSaverRow constructor.
     * @param \Zend\Db\Adapter\Driver\Pdo\Result $iterator
     * @param array $config
     * @param string $columnDelimiter
     * @param string $columnWrapper
     */
    public function __construct( $iterator, array $config = array(), $columnDelimiter = ';', $columnWrapper = '')
    {
        $this->iterator = $iterator;
        $this->columnDelimiter = $columnDelimiter;
        $this->columnWrapper = $columnWrapper;

        if (!empty($config)) {
            $this->config = array_keys($config);
            $this->headers = array_values($config);
        }
    }

    /**
     * Возвращает текущую строку и итератор двигает дальше, в конце возвращает false.
     *
     * @return bool|string
     */
    public function nextRow()
    {
        $current = $this->iterator->next();
        if (!$current) {
            return false;
        }
        $row = array();
        $re = '/(\.00)$/';
        if (empty($this->config)) {
            $row = $this->columnWrapper . implode($this->columnWrapper . $this->columnDelimiter . $this->columnWrapper,
                    $current) . $this->columnWrapper;
        } else {
            foreach ($this->config as $item) {
                if (isset($current[$item])) {
                    $row[] = (preg_match($re, $current[$item])) ? preg_replace($re, '', $current[$item]) : $current[$item];
                } else {
                    $row[] = '';
                }
            }
            $row = implode($this->columnDelimiter, $row);
        }

        return $row;
    }

    /**
     * Возвращает заголовки.
     *
     * @return string
     */
    public function getHeaders()
    {
        return (empty($this->headers)) ? false : implode($this->columnDelimiter, $this->headers);
    }
}