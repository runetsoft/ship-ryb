<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 11.06.2017
 * Time: 9:27
 */

namespace Export\Service;

/**
 * Class FileSaverConfig
 * Конфигурация сохраняемого файла эжкспорта.
 * @package Export\Service
 */
class FileSaverConfig
{
    const BASE_ROOT = 'public';
    const EXPORT_URL_TEMPLATE = 'files/saver/{exportName}/{id}/export.csv';
    const FILE_CSV = 'export.csv';
}