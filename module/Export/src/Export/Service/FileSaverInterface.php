<?php

namespace Export\Service;

/**
 * Interface FileSaverInterface
 * @package Export\Service
 */
interface FileSaverInterface
{
    /**
     * Получает итератор данных, путь файла и настройки колонок, данные записывает в файл.
     *
     * @param \Zend\Db\Adapter\Driver\Pdo\Result $iterator Итератор данных.
     * @param array $config Конфигурация о заголовках и очередности.
     * @param string $file Путь файла
     * @return mixed
     */
    public function save(\Zend\Db\Adapter\Driver\Pdo\Result $iterator, array $config, $file);

}