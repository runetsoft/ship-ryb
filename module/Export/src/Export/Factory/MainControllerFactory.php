<?php

namespace Export\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Export\Controller\MainController;

class MainControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentLocator = $serviceLocator->getServiceLocator();
        return new MainController($serviceLocator->getServiceLocator(),
            $parentLocator->get('Price\Model\PriceTable'),
            $parentLocator->get('Export\Model\SupplierPriceExportTable')
        );
    }
}