<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 17.06.2017
 * Time: 17:24
 */

namespace Export\Utilities;

use Application\Utilities\BaseConstant;

class ExportType extends BaseConstant
{
    /**
     * Тип загрузки url.
     */
    const LOAD_TYPE_URL = 1;

    /**
     * Тип загрузки письмо.
     */
    const LOAD_TYPE_MESSAGE = 2;

    /**
     * Описание констант.
     *
     * @var array
     */
    public $descriptions = array(
        'LOAD_TYPE_URL' => array(
            'description' => 'Url',
            'type' => Type::INT
        ),
        'LOAD_TYPE_MESSAGE' => array(
            'description' => 'Письмо',
            'type' => Type::INT
        )
    );
}