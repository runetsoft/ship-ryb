<?php
return array(
    'controllers' => array(
        "factories" => array(
            'Export\Controller\MainController' => 'Export\Factory\MainControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'export' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/export[/:action].json',
                    'defaults' => array(
                        'controller' => 'Export\Controller\MainController'
                    ,
                    ),
                ),
            )
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);