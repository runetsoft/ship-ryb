<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 15.12.2017
 * Time: 17:13
 */

namespace Application\Utilities;

/**
 * Class Utilite
 * @package Application\Utilities
 */
class Utilite
{

    const DB_PREFIX = 'cms';
    const GOOD = 'good';
    const MODEL = 'model';
    const BRAND = 'brand';
    const GOOD_TYPE = 'good_type';
    const GOOD_NO_TYPE_ID = 4;
    const GOOD_TYPE_CATEGORY = 'good_type_category';
    const DICTIONARY = 'dictionary';

    static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }
        return $str;
    }

    static function camelCaseToUnderscore($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    static function getTableFullName($tableName)
    {
        if (strpos($tableName, self::DB_PREFIX)) {
            return $tableName;
        }
        return implode('_', array(self::DB_PREFIX, $tableName));
    }

    static function getDictionaryModelFromType($tableName)
    {
        $tableName = preg_replace('/^(' . self::DB_PREFIX . ')_/', '', $tableName);
        $fullTableName = implode('_', array($tableName, self::DICTIONARY));
        return self::underscoreToCamelCase($fullTableName, true);
    }

    static function getDictionaryTableName($name)
    {
        return self::getTableFullName(implode('_', array(self::DICTIONARY, $name)));
    }

    static function getDictionaryTableNameFromModel($model)
    {
        // TODO
    }
}