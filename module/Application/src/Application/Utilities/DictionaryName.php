<?php
/**
 * Created by PhpStorm.
 * User: a.terekhov
 * Date: 22.09.2017
 * Time: 18:35
 */

namespace Application\Utilities;

/**
 *Константа названия таблиц со словарями
 *
 * @package Application\Utilities
 */
class DictionaryName extends BaseConstant
{
    const DICTIONARY = 'dictionary';

    public $descriptions = array(
        'DICTIONARY' => array(
            'description' => 'Префикс таблиц справочников',
            'type' => 'string'
        )
    );

}