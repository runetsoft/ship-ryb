<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 24.05.2017
 * Time: 17:38
 */

namespace Application\Utilities;

/**
 * Константы типов архива.
 * Название константов в формате который поддерживает Zend\Filter, а значения в формате типа файла.
 *
 * Class ArchiveFormat
 *
 * @package Application\Utilities
 */
class ArchiveFormat extends BaseConstant
{
    const Zip = 'application/zip';

    public $descriptions = array(
        'Zip' => array(
            'description' => 'Zip архив',
            'type' => 'string'
        )
    );

}