<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 26.05.2017
 * Time: 10:30
 */

namespace Application\Utilities;

/**
 * Class CronPeriod
 * @package Application\Utilities
 */
class CronPeriod extends BaseConstant
{

    const ONE_HOUR = '1 hour';
    const ONE_THREE_HOUR = '3 hour';
    const ONE_SIX_HOUR = '6 hour';
    const ONE_TWENTY_HOUR = '12 hour';
    const ONE_DAY = '1 day';

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'ONE_HOUR' => array(
            'description' => 'Раз в час',
            'type' => Type::STRING
        ),
        'ONE_THREE_HOUR' => array(
            'description' => 'Раз в 3 часа',
            'type' => Type::STRING
        ),
        'ONE_SIX_HOUR' => array(
            'description' => 'Раз в 6 часов',
            'type' => Type::STRING
        ),
        'ONE_TWENTY_HOUR' => array(
            'description' => 'Раз в 12 часов',
            'type' => Type::STRING
        ),
        'ONE_DAY' => array(
            'description' => 'Раз в сутки',
            'type' => Type::STRING
        )
    );
}