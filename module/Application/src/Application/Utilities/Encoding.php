<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 23.05.2017
 * Time: 10:30
 */

namespace Application\Utilities;

/**
 * Class Encoding
 * @package Application\Utilities
 */
class Encoding extends BaseConstant
{

    const UTF_8 = 'UTF-8';
    const WINDOWS_1251 = 'WINDOWS-1251';

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'UTF_8' => array(
            'description' => 'Юникод (UTF-8)',
            'type' => Type::STRING
        ),
        'WINDOWS_1251' => array(
            'description' => 'Кириллица (WINDOWS-1251)',
            'type' => Type::STRING
        )
    );
}