<?php

namespace Application\Utilities;

/**
 * Class Delimiter
 * @package Application\Utilities
 */
class Delimiter extends BaseConstant
{
    const SEMICOLON = ';';
    const COMMA = ',';

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'SEMICOLON' => array(
            'description' => 'Точка с запятая',
            'type' => Type::STRING
        ),
        'COMMA' => array(
            'description' => 'Запятая',
            'type' => Type::STRING
        )
    );
}