<?php

/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 19.05.2017
 * Time: 16:23
 */

namespace Application\Utilities;

class BaseConstant
{

    /**
     * Здесь задается информация о константах (описание и тип), форнат ниже.
     *
     * array(
     *    'CONST_NAME' => array(
     *        'description' => 'Описание константа ',
     *        'type' => 'Тип константа'
     *    )
     * ...
     * )
     *
     * @var array
     */
    public $descriptions = array();

    /**
     * Возвращает информацию о константах.
     *
     * @param bool $descriptions описание
     * @param bool $type тип
     * @return array
     */
    public function getConstants($descriptions = false, $type = false)
    {
        $constants = array();
        $reflection = new \ReflectionClass(get_class($this));

        if ($descriptions || $type) {
            foreach ($reflection->getConstants() as $key => $value) {
                $constant = array('name' => $key);
                if ($descriptions) {
                    $constant['value'] = $value;
                    $constant['description'] = $this->descriptions[$key]['description'];
                }
                if ($type) {
                    $constant['type'] = $this->descriptions[$key]['type'];
                }
                $constants[] = $constant;
            }
        } else {
            return $reflection->getConstants();
        }
        return $constants;
    }
}