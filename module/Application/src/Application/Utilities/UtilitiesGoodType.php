<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 23.05.2017
 * Time: 10:55
 */

namespace Application\Utilities;

/**
 * Class GoodType
 * @package Application\Utilities
 */
class UtilitiesGoodType extends BaseConstant
{
    const DISK = 1;
    const TYRE = 2;
    const TYRE_MOTO = 3;
    const ALL = 4;
    const NO_TYPE = 5;

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'DISK' => array(
            'description' => 'Диск',
            'type' => Type::INT
        ),
        'TYRE' => array(
            'description' => 'Шины для авто',
            'type' => Type::INT
        ),
        'TYRE_MOTO' => array(
            'description' => 'Шины для мото',
            'type' => Type::INT
        ),
        'NO_TYPE' => array(
            'description' => 'Без типа',
            'type' => Type::INT
        ),
        'ALL' => array(
            'description' => 'Все',
            'type' => Type::INT
        )
    );
}