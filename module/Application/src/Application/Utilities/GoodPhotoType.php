<?php

namespace Application\Utilities;

/**
 * Class GoodPhotoType
 * @package Application\Utilities
 */
class GoodPhotoType extends BaseConstant
{
    const WITHOUT_PHOTO = 1;
    const WITH_PRICE_PHOTO = 2;
    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'WITHOUT_PHOTO' => array(
            'description' => 'Товары без фото',
            'type' => 1
        ),
        'WITH_PRICE_PHOTO' => array(
            'description' => 'Товары без фото, но с фото в прайсе',
            'type' => 2
        )
    );
}