<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 25.05.2017
 * Time: 13:12
 */

namespace Application\Utilities;


class DateFormat extends BaseConstant
{

    const STANDARD = 'Y-m-d H:i:s';

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'DB_GLOBAL' => array(
            'description' => 'Стандартный формат даты',
            'type' => Type::STRING
        )
    );
}