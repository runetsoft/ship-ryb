<?php

namespace Application\Utilities;

/**
 * Class FieldType
 * @package Application\Utilities
 */
class FieldType extends BaseConstant
{
    const TEXTFIELD = 'textfield';
    const NUMBERFIELD = 'numberfield';
    const TEXTFIELDNUMBER = 'textfieldnumber';
    const DICTIONARY = 'dictionary';
    const DICTIONARYMULTISELECT = 'dictionarymultiselect';
    const CHECKBOX = 'checkbox';

    /**
     * Описание константов.
     *
     * @var array
     */
    public $descriptions = array(
        'TEXTFIELD' => array(
            'description' => 'Текстовая',
            'type' => self::TEXTFIELD
        ),
        'NUMBERFIELD' => array(
            'description' => 'Цифра',
            'type' => self::NUMBERFIELD
        ),
        'TEXTFIELDNUMBER' => array(
            'description' => 'Дробная цифра',
            'type' => self::TEXTFIELDNUMBER
        ),
        'DICTIONARY' => array(
            'description' => 'Справочник',
            'type' => self::DICTIONARY
        ),
        'DICTIONARYMULTISELECT' => array(
            'description' => 'Справочник с множественным выбором',
            'type' => self::DICTIONARYMULTISELECT
        ),
        'CHECKBOX' => array(
            'description' => 'Чекбокс',
            'type' => self::CHECKBOX
        )
    );
}