<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 23.05.2017
 * Time: 10:55
 */

namespace Application\Utilities;

/**
 * Class Type
 * @package Application\Utilities
 */
class Type
{
    const INT = 'int';
    const STRING = 'string';
}