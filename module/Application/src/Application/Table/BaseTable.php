<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 11:52
 */

namespace Application\Table;

use Zend\Db\TableGateway\TableGateway;

class BaseTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}