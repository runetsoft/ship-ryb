<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Model\UserModel;


class IndexController extends AbstractActionController
{
    /**
     * @var \Good\Model\UserModel
     */
    protected $userModel;
    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    public function __construct($userModel)
    {
        $this->userModel = $userModel;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Проверка данных формы авторизации
     * @return JsonModel
     */
    public function loginAction(){
        $query = $this->request->getQuery();
        $login = $query->get('login');
        $password = $query->get('password');
        try {
            $data = $this->userModel->checkLogin($login, $password);
            return new JsonModel(array("success" => true, "data" => $data));
        } catch (\Exception $e) {
            return new JsonModel(array("success" => false, "message" => $e->getMessage()));
        }

    }
}
