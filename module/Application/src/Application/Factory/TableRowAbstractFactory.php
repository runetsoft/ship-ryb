<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 08.05.2017
 * Time: 10:21
 */

namespace Application\Factory;

use Zend\Db\RowGateway\RowGateway;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Import\Service\RowPreparerInterface;

/**
 * Фабрика для классво вида {tableClassName}Row.
 * Class TableRowAbstractFactory
 * @package Application\Factory
 */
class TableRowAbstractFactory implements AbstractFactoryInterface
{

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return preg_match('/Row$/', $requestedName);
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        preg_match('/([A-Za-z]+)Row$/', $requestedName, $matches, PREG_OFFSET_CAPTURE, 0);
        list(, $serviceName) = $matches;
        $serviceName = current($serviceName) . 'TableGateway';
        $tableGateway = $serviceLocator->get($serviceName);
        if (!$tableGateway) {
            throw new \Exception('Сервиса ' . $tableGateway . 'не существует');
        }

        if (!$tableGateway instanceof \Zend\Db\TableGateway\TableGateway) {
            throw new \Exception('Класс ' . $tableGateway . 'не наследует TableGateway' );
        }

        $resSet = $tableGateway->getResultSetPrototype()->getArrayObjectPrototype();

        if (!$resSet instanceof RowGateway) {
            throw new \Exception(
                'Для ' . $tableGateway . ' не задан resultSet типа RowGateway'
            );
        }
        return $resSet;
    }
}