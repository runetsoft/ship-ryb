<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 18.04.2017
 * Time: 12:38
 */

namespace Application\Service;


class ChunkReadFilter implements \PHPExcel_Reader_IReadFilter
{
    /**
     * @var int
     */
    private $startRow;
    /**
     * @var int
     */
    private $endRow;

    private $columns;

    /**  Set the list of rows that we want to read  */
    public function setRows($startRow, $chunkSize, $columns = null) {
        $this->startRow    = $startRow;
        $this->endRow      = $startRow + $chunkSize;
        $this->columns = $columns;
    }
    public function readCell($column, $row, $worksheetName = '') {
        $checkCols = is_array($this->columns) ? in_array($column, $this->columns) : true;
        if ($row >= $this->startRow && $row < $this->endRow && $checkCols) {
            return true;
        }
        return false;
    }
}