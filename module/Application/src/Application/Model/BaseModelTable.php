<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 02.06.2017
 * Time: 13:56
 */

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class BaseModelTable
{
    const FILTER = '/^([%<>!=]+)*(.+?)$/';

    public function setFilter(Select $select, array $filter, $table)
    {
        foreach ($filter as $column => $value) {
            if (!isset($value)) {
                continue;
            }
            preg_match_all(self::FILTER, $column, $matches, PREG_SET_ORDER, 0);
            list(, $condition, $column) = current($matches);
            if (is_array($value)) {
                if (!empty($value)) {
                    $select->where($table . '.' . $column . ' IN (' . implode(',', $value) . ')');
                } else {
                    continue;
                }
            } else {
                $value = addslashes($value);
                switch ($condition) {
                    case '':
//                        $select->where($table . '.' . $column . '=\'' . $value . '\'');
//                        TODO пока так для филтра гридов
                        $select->where($table . '.' . $column . ' LIKE \'%' . $value . '%\'');
                        break;
                    case '!':
                        $select->where($table . '.' . $column . ' <>\'' . $value . '\'');
                        break;
                    case '%':
                        $select->where($table . '.' . $column . ' LIKE \'%' . $value . '%\'');
                        break;
                    case '=':
                        $select->where($table . '.' . $column . ' = \''. $value . '\'' );
                        break;
                    default:
                        $select->where($table . '.' . $column . ' ' . $condition . ' \'' . $value . '\'');
                        break;
                }
            }
        }

    }

    public function getColumnsFullInfo(TableGateway $tableGateway)
    {
        $dbAdapter = $tableGateway->getAdapter();
        $tableName = $tableGateway->getTable();
        $sql = "SHOW FULL COLUMNS FROM `" . $tableName . "`";
        return $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
    }

    public function getColumnsCommentsInfo(TableGateway $tableGateway, $prefix = '', array $ignoredFields = [])
    {
        $result = array();
        $colsInfo = $this->getColumnsFullInfo($tableGateway);
        if (!empty($colsInfo)) {
            foreach ($colsInfo as $col) {
                if (in_array($prefix . $col->Field, $ignoredFields)) {
                    continue;
                }
                $result[$prefix . $col->Field] = $col->Comment;
            }
        }
        return $result;
    }
}