<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 05.04.2017
 * Time: 11:58
 */

namespace Application\Model;

class BaseModel
{
    public function exchangeArray($data)
    {
        $vars = get_object_vars($this);
        foreach ($vars as $key => $var) {
            $this->$key = (!empty($data[$key])) ? $data[$key] : null;
        }
    }

    /**
     * Возвращает колонки. Если будем вызывать в дочерних классах, будет возвращать колонки дочерних классов.
     *
     * @param bool $descriptions Если задан true, то возвращает еще описания колонок.
     * @return array
     */
    public function getColumns($descriptions = false)
    {
        $columns = array();
        foreach ($this as $key => $value) {
            if ($descriptions) {
                $descriptionsInfo = $this->getDescriptionsInfo($key);
                $columns[] = array(
                    'column' => $key,
                    'description' => $descriptionsInfo['description']
                );
            } else {
                $columns[] = $key;
            }
        }
        return $columns;
    }

    /**
     * Возвращает информацию из комментариев колонки.
     *
     * @param $name $commentsKeys ключи  из описания полей.
     * @return array
     */
    public function getDescriptionsInfo($name)
    {
        $prop = new \ReflectionProperty($this, $name);
        $descriptions = ($prop->getDocComment() ? $prop->getDocComment() : '');
        preg_match_all('/.@description +\"(.*)\"/', $descriptions, $matches, PREG_SET_ORDER, 0);
        list(, $description) = current($matches);
        return array(
            'description' => $description
        );
    }

    public function toArray()
    {
        $data = [];
        foreach ($this as $key => $value) {
            $data [$key] = $value;
        }
        return $data;
    }
}