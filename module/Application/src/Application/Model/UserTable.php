<?php
namespace Application\Model;

use Application\Model\BaseModel;

/**
 * Class UserTable
 * @package Application\Model
 *
 * Объект таблицы пользователей
 */
class UserTable extends BaseModel
{
    public $id;
    public $dt_insert;
    public $dt_update;
    public $dt_login;
    public $cnt_login;
    public $fio;
    public $login;
    public $password;
    public $email;
    public $status;
    public $access_id;
}