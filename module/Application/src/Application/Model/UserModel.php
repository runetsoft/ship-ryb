<?php
namespace Application\Model;

use Zend\Db\Sql\Expression;

/**
 * Created by PhpStorm.
 * User: yu.khristodorov
 * Date: 03.05.2017
 * Time: 13:36
 */


/**
 * Class UserModel
 * @package Application\Model
 *
 * Класс для работы с пользователями и их данными.
 */

class UserModel {

    protected $sm;
    public function __construct($sm)
    {
        $this->sm = $sm;
    }
    /**
     * Проверка данных формы авторизации
     * @param $login
     * @param $password
     * @return mixed
     *
     */
    public function checkLogin($login, $password)
    {
        $results = $this->sm->get('UsersModelGateway')->select(array('login' => $login,
            'password' => new Expression('encrypt(concat(\'adm\',' . $password . '), password)')));
        $data = $results->current();

        return $data;
    }
}