<?php
/**
 * Created by PhpStorm.
 * User: babkin
 * Date: 03.04.2017
 * Time: 11:13
 */

namespace Application\Entity;


class BaseEntity
{
    /**
     * @var array Тут храняться возможные магические методы
     */
    protected $_magicMethods = array('set', 'get');

    /**
     * Проверяет доступен ли магический метод для вызова.
     * @param $fname
     * @return bool
     */
    protected function __checkMethodCall($fname)
    {
        if (preg_match('@^(' . implode('|', $this->_magicMethods) . ')(.+)$@i', $fname, $matches)) {
            if (in_array(strtolower($matches[1]), $this->_magicMethods)) {
                return $matches;
            }
        }
        return false;
    }

    /**
     * @param $fname
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($fname, $arguments)
    {
        $matches = $this->__checkMethodCall($fname);
        if (!$matches) {
            throw new \Exception('Invalid method call: ' . $fname);
        }
        $method = '_' . $matches[1];
        if (method_exists($this, $method)) {
            $name = strtolower($matches[2]);
            return $this->$method($name, $arguments);
        }
        throw new \Exception('Invalid method call: ' . $fname);
    }

    /**
     * Получить значение поля.
     *
     * @param string $name Имя поля.
     *
     * @return mixed|null
     */
    protected function _get($name)
    {
        return $this->$name;
    }

    /**
     * Добавить значение поля.
     *
     * @param string $name Имя поля.
     * @param array $arguments Значения.
     *
     * @return \Core_Mapper
     *
     * @throws \Exception
     */
    protected function _set($name, array $arguments)
    {
        $this->$name = $arguments;
        return true;
    }
}