<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Model\ShemaModel;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\UserTable;
use Application\Model\UserModel;
use Zend\Http\PhpEnvironment\Request as PhpEnvironment_Request;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($event) {
            $controller = $event->getTarget();
            $request = $controller->getRequest();
            if ($request instanceof  PhpEnvironment_Request) {
                $query = $request->getQuery();
                if ($query->get('sort')) {
                    list($sort) = json_decode($query->get('sort'));
                    $query->set('sort',$sort);
                }
            }

        }, 100);

        /**
         * Log any Uncaught Errors
         */
        $sharedManager = $e->getApplication()->getEventManager()->getSharedManager();
        $sm = $e->getApplication()->getServiceManager();
        $sharedManager->attach('Zend\Mvc\Application', 'dispatch.error',
            function($e) use ($sm) {
                if ($e->getParam('exception')){
                    $sm->get('Logger')->crit($e->getParam('exception'));
                }
            }
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\UserModel' => function ($sm) {
                    $tableGateway = $sm->get('UsersModelGateway');
                    $table = new UserModel($sm, $tableGateway);
                    return $table;
                },
                'UsersModelGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ShipRybAdapter');
                    $features = $sm->features ? $sm->features : null;
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserTable());
                    return new TableGateway('cms_adm', $dbAdapter, $features, $resultSetPrototype);
                },
                'Logger' => function($sm){
                    $logger = new Logger;
                    $writer = new Stream(dirname(__FILE__).'/../../data/log/'.date('Y-m-d').'-error.log',null, null, 0665);
                    $logger->addWriter($writer);
                    return $logger;
                },
            ),
            'abstract_factories' => array(
                'Application\Factory\TableRowAbstractFactory',
            )
        );
    }


}
