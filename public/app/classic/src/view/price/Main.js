/**
 * Created by babkin on 30.03.2017.
 */
Ext.define('Pricer.view.price.Main', {
    extend: 'Ext.Panel',
    xtype: 'price',

    viewModel: 'price.main',
    requires: [
        'Pricer.view.price.MainController',
        'Pricer.view.Price.grid.PriceGrid'
    ],

    controller: 'price.main',
    items: [
        {
            xtype: 'priceList',
            reference: 'priceList'
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'right',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'button',
                    reference: 'onSaveConform',
                    text: "Сохранить соответствия",
                    iconCls: 'x-fa fa-check-circle-o',
                    style: {
                        marginBottom: '10px',
                        marginLeft: '10px',
                        background:'#1bc98e'
                    },
                    handler: 'onSaveConform',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: "Удалить все позиции без соответствия",
                    iconCls: 'x-fa fa-trash',
                    style: {
                        marginBottom: '10px',
                        marginLeft: '10px'
                    },
                    handler: 'onDeleteAllItemsWithoutConform'
                }
            ]
        }
    ]

});
