Ext.define('Pricer.view.price.form.PriceAfterFormLoad', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Pricer.view.price.form.PriceFormLoadcsvController'
    ],
    xtype: 'priceafterloadcsv',
    controller: 'price.form.loadcsv',
    reference: 'priceafterloadcsv',
    viewConfig: {
        trackOver: true,
        stripeRows: true,
        enableTextSelection: true,
        getRowClass: function () {
            return this.enableTextSelection ? 'x-selectable' : '';
        }
    },
    autoHeight: true,
    height: 550,
    columns: [
        {
            text: '#',
            dataIndex: 'id'
        },
        {
            text: 'Название',
            dataIndex: 'good',
            flex: 2
        }
    ],
    features: [{
        ftype: 'grouping',
        startCollapsed: true,
        groupHeaderTpl: '{name} :{rows.length}'
    }],
    fbar : [
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],

    initComponent: function () {
        var store = this.store;
        Ext.apply(this, {
            store: store
        });
        this.callParent();
    }
});