Ext.define('Pricer.view.price.form.PriceFormLoadCsv', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.price.form.PriceFormLoadcsvController'
    ],
    xtype: 'priceloadcsv',
    controller: 'price.form.loadcsv',
    width: 500,
    reference: 'priceloadcsv',
    bodyPadding: 10,
    timeout : 3600,
    defaults: {
        style: {
            margin: 10
        }
    },
    items: [
        {
            allowBlank: false,
            xtype: 'filefield',
            name: 'document',
            width: '100%',
            fieldLabel: 'Выберите файл'
        }
    ],
    buttons: [
        {
            text: 'Оправить CSV',
            handler: 'sendCsv'
        }
    ]
});