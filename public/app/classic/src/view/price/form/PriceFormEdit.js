Ext.define('Pricer.view.price.form.PriceFormEdit', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.price.form.PriceFormEditController',
        'Pricer.view.goods.forms.GoodsCombo'
    ],
    xtype: 'priceform',
    controller: 'price.form.edit',
    width: '100%',
    height: '375px',
    reference: 'priceform',
    bodyPadding: 10,
    autoScroll: true,
    url: '/price/getConformInfo.json',

    defaults: {
        style: {
            margin: 10
        }
    },
    fieldDefaults: {
        labelAlign: 'right',
        labelWidth: 100,
        msgTarget: 'side'
    },

    items: [
        {
            xtype: 'fieldset',
            title: 'Редактирование записи',
            defaultType: 'textfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'ЗА',
                    name: 'zarticle',
                    submitValue: true,
                    style: 'text-decoration:underline; text-decoration-style:dashed;',
                    listeners: {
                        afterrender: 'onFindConformByArticle'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Наименование поставщика',
                    name: 'provider_name'
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Наименование товара в прайсе',
                    name: 'good',
                    style: 'text-decoration:underline; text-decoration-style:dashed;',
                    listeners: {
                        afterrender: 'onSetTextInCombo'
                    }
                },
                {
                    fieldLabel: 'Соответствие',
                    xtype: 'goodsCombo',
                    reference: 'goodsCombo',
                    name: 'good_id'
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'МРЦ',
                    name: 'rprice',
                    submitValue: true
                }

            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            handler: 'beforeSaveData'
        },
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],
    listeners: {
        beforerender: 'onFormRender'
    }
});