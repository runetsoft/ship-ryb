/**
 * Created by babkin on 31.03.2017.
 */
Ext.define('Pricer.view.Price.grid.PriceGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'priceList',
    module: 'price',
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.Price.PriceStore',
        'Pricer.view.price.grid.PriceGridController',
        'Pricer.view.price.grid.PriceGridModel',
        'Ext.selection.CellModel',
        'Pricer.view.goods.windows.GoodsWindowsEdit'
    ],

    controller: 'price-grid-pricegrid',
    viewModel: {
        type: 'price-grid-pricegrid'
    },

    initComponent: function () {
        // create the Data Store
        var store = Ext.create('Pricer.store.Price.PriceStore', {remoteSort: true});
        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1,
            listeners: {
                beforeedit: 'beforeEdit',
                afteredit: 'afterEdit'
            }
        });

        this.cellEditing.on('edit', this.onEditComplete, this);
        Ext.apply(this, {
            store: store,
            minHeight: 600,
            viewConfig: {
                trackOver: true,
                stripeRows: true,
                enableTextSelection: true,
                listeners: {
                    cellclick: 'onCellClick'
                }
            },
            plugins: [
                this.cellEditing
            ],
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true
            },
            // grid columns
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width: 100,
                    filterField: 'textfield',
                    renderer: function (value, metaData) {
                        metaData.tdStyle = 'text-decoration:underline; text-decoration-style:dashed; cursor: pointer;';
                        return value;
                    }
                },
                {
                    text: 'Поставщик',
                    dataIndex: 'provider_id',
                    condition: '=provider_id',
                    align: 'left',
                    width: 250,
                    filterField: {
                        xtype: 'combobox',
                        minChars: 3,
                        store: Ext.create('Pricer.store.suppliers.SuppliersStore',
                            {
                                pageSize: 150
                            }),
                        valueField: 'id',
                        displayField: 'provider'
                    },
                    renderer: function (value, metaData, record) {
                        return record.data.provider_name;
                    }
                },
                {
                    text: 'Название',
                    dataIndex: 'good',
                    align: 'left',
                    width: 600,
                    filterField: 'textfield'
                },
                {
                    text: 'ID соответствия',
                    dataIndex: 'good_orig_id',
                    hidden: true

                },
                {
                    text: 'Много ли соответствий',
                    dataIndex: 'good_orig_many',
                    hidden: true
                },
                {
                    text: 'Соответствие',
                    dataIndex: 'good_orig',
                    align: 'left',
                    width: 600,
                    filterField: {
                        xtype: 'combobox',
                        minChars: 3,
                        store: Ext.create('Pricer.store.goods.GoodsStore',
                            {
                                pageSize: 150,
                                proxy: {
                                    type: 'ajax',
                                    extraParams: {
                                        combo: true
                                    }
                                },
                                autoLoad: true
                            }),
                        valueField: 'id',
                        displayField: 'good'
                    },
                    editor: {
                        xtype: 'combobox',
                        minChars: 3,
                        store: Ext.create('Pricer.store.goods.GoodsStore',
                            {
                                pageSize: 150,
                                proxy: {
                                    type: 'ajax'
                                }
                            }
                        ),
                        valueField: 'good',
                        displayField: 'good'
                    },
                    renderer: function (value, metaData, record) {
                        var good_orig_many = record.get('good_orig_many');
                        if (good_orig_many != undefined && good_orig_many) {
                            metaData.style = "color:red";
                        }
                        return value;
                    }

                },

                {
                    text: 'ЗА',
                    dataIndex: 'zarticle',
                    align: 'center',
                    width: 100
                },
                {
                    text: 'МРЦ',
                    dataIndex: 'rprice',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    align: 'center',
                    width: 100
                },
                {
                    text: 'Цена',
                    dataIndex: 'price',
                    align: 'left',
                    width: 100
                },
                {
                    text: 'Дата добавления',
                    dataIndex: 'dt_insert',
                    align: 'center',
                    width: 150,
                    period: true,
                    filterField: {
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }
                },
                {
                    text: 'Дата обновления',
                    dataIndex: 'dt_update',
                    period: true,
                    align: 'center',
                    width: 150,
                    filterField: {
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }
                },
                {
                    text: 'Склад',
                    dataIndex: 'sklad',
                    period: true,
                    align: 'center',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    width: 150,
                    filterField: 'textfield'
                },
                {
                    text: 'Дeйствия',
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    width: 150,
                    items: [{
                        iconCls: 'x-fa fa-edit',
                        tooltip: 'Редактировать',
                        handler: 'onItemEdit'
                    }, {
                        iconCls: 'x-fa fa-trash',
                        tooltip: 'Удалить',
                        handler: 'onItemDelete'
                    }]
                }

            ],
            dockedItems: [{
                xtype: 'panel',
                dock: 'top',
                cls: 'grid-tool-bar',
                items: [
                    {
                        xtype: 'gridfilter',
                        grid: this
                    },
                    {
                        xtype: 'button',
                        reference: 'onAddConform',
                        text: "Проставить соответсвия",
                        iconCls: 'x-fa fa-check-circle-o',
                        style: {
                            marginBottom: '10px',
                            height: '40px',
                            background: '#9f86ff'
                        },
                        handler: 'onAddConform'
                    },
                    {
                        xtype: 'button',
                        reference: 'unloadInCsv',
                        text: "Выгрузить в csv",
                        handler: 'unloadInCsv',
                        margin: '0 10px',
                        height: '40px'
                    },
                    {
                        xtype: 'button',
                        reference: 'downloadFromCsv',
                        text: "Загрузить из csv",
                        handler: 'downloadFromCsv',
                        margin: '0 10px',
                        height: '40px'
                    }
                ]
            }],
            // paging bar on the bottom
            bbar: [{
                xtype: 'checkbox',
                grid: this,
                reference: 'checkboxSelectAll',
                listeners: {
                    change: function () {
                        if (this.value == true) {
                            this.grid.getSelectionModel().selectAll();
                        } else {
                            if (this.grid.getSelectionModel().selected.length == this.grid.getStore().data.length) {
                                this.grid.getSelectionModel().deselectAll();
                            }
                        }
                    }
                }
            },
                {
                    xtype: 'pricertoolbar',
                    store: store,
                    autoScroll: true,
                    items: [{
                        xtype: 'prepage',
                        grid: this
                    }]
                }]
        });
        this.callParent();
        this.getSelectionModel().on('selectionchange', this.checkboxSelectAll, this);
    },

    /**
     * Выставлять нижнюю галочку "выбрать все" в зависисомти от верхней
     *
     */
    checkboxSelectAll: function (selected, eOpts) {
        var checkboxSelectAll = this.lookupReference('checkboxSelectAll');
        if (selected.selected.length == this.getStore().data.length) {
            checkboxSelectAll.setValue(true);
        }
        else {
            checkboxSelectAll.setValue(false);
        }
    },

    onEditComplete: function (editor, context) {
        this.getView().focusRow(context.record);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});