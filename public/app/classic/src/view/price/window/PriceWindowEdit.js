/**
 * Created by me.
 */

Ext.define('Pricer.view.price.window.PriceWindowEdit', {
    extend: 'Ext.window.Window',
    title: 'Редактирование прайса',
    controller: 'price.window.edit',
    referenceHolder: true,
    width:900,
    modal: true,
    items:[]
});