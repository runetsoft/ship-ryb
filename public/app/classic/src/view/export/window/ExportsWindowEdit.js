Ext.define('Pricer.view.export.window.ExportsWindowEdit', {
    extend: 'Ext.window.Window',
    title: 'Редактирование экспорта',
    controller: 'export.window.edit',
    referenceHolder: true,
    width: 800,
    maxHeight: 800,
    autoScroll: true,
    modal: true,
    items: [],
    listeners: {
        resize: function (win, width, height, oldWidth, oldHeight, eOpts) {
            var xPos = win.getPosition()[0],
                v,
                yPos = ((v = (Ext.getBody().getViewSize().height - height) / 2) > 0) ? v : 0;
            win.setPosition(xPos, yPos, true);
        }
    }
});