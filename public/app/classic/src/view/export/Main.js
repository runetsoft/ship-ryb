Ext.define('Pricer.view.export.Main', {
    extend: 'Ext.Panel',
    xtype: 'export',
    viewModel: 'export.main',
    requires: [
        'Pricer.view.export.grid.ExportsGrid'
    ],

    controller: 'export.main',

    items: [
        {
            xtype: 'exportList',
            reference: 'exportList'
        },
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'right',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Добавить экспорт',
                    iconCls: 'x-fa fa-file-excel-o',
                    style: {
                        marginBottom: '10px'
                    },
                    handler: 'addExport',
                    listeners: {
                        'onFileAdded': {
                            fn: function () {
                               this.up('export').getReferences().exportList.getStore().load();
                            }
                        }
                    }
                }
            ]
        }
    ]
});
