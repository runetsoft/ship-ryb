Ext.define('Pricer.view.export.grid.ExportsGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'exportList',
    module: 'export',
    controller: 'export.grid.exportgrid',
    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Pricer.store.export.ExportsStore',
        'Pricer.view.export.grid.ExportsGridController'
    ],
    exportId: null,
    initComponent: function () {
        // create the Data Store
        var store = Ext.create('Pricer.store.export.ExportsStore');
        Ext.apply(this, {
            store: store,
            minHeight: 600,
            plugins: [{
                ptype: 'cellediting',
                clicksToEdit: 1
            }],
            viewConfig: {
                enableTextSelection: true
            },
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center'
                },
                {
                    text: 'Наименование',
                    dataIndex: 'name',
                    align: 'left',
                    width: '40%'
                },
                {
                    text: 'Ссылка ',
                    dataIndex: 'url',
                    width: '40%',
                    align: 'left',
                    editor: {
                        completeOnEnter: false,
                        field: {
                            xtype: 'textfield',
                            allowBlank: false
                        }
                    }
                },
                {
                    text: 'Действия ',
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    width: '10%',
                    editor: false,
                    items: [
                        {
                            iconCls: 'x-fa fa-edit',
                            tooltip: 'Редактировать',
                            handler: 'onItemEdit'
                        },
                        {
                            iconCls: 'x-fa fa-trash',
                            tooltip: 'Удалить',
                            handler: 'onItemDelete'
                        }
                    ]
                }
            ],
            // paging bar on the bottom
            bbar: [
                {
                    xtype: 'pricertoolbar',
                    store: store,
                    autoScroll: true,
                    items: [
                        {
                            xtype: 'prepage',
                            grid: this
                        }
                    ]
                }
            ]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    }
});