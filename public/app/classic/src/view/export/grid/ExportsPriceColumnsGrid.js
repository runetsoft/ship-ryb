Ext.define('Pricer.view.export.grid.ExportsPriceColumnsGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'exportPriceColumnsList',
    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Pricer.view.export.grid.ExportsPriceColumnsGridController'
    ],
    items: [],
    controller: 'export.grid.priceColumnsGrid',
    listeners: {
        edit: 'onGridEdit',
        scope: 'controller'
    },
    initComponent: function () {
        var store = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'columnKey', type: 'string'},
                {name: 'columnName', type: 'string'}
            ],
            data: []
        });
        Ext.apply(this, {
            store: store,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ],
            columns: {
                items: [
                    {
                        text: 'Названия колонки',
                        dataIndex: 'columnName'
                    },
                    {
                        text: 'Поле таблицы',
                        dataIndex: 'columnKey',
                        editor: {
                            dictionaryName: 'PriceTableExpanded',
                            valueField: 'code',
                            xtype: 'dictionary',
                            listeners: {
                                select: 'onColumnKeySelect'
                            }
                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        dataIndex: 'actions',
                        width: 25,
                        editor: false,
                        items: [
                            {
                                iconCls: 'x-fa fa-trash',
                                tooltip: 'Удалить',
                                handler: 'onItemDelete'
                            }
                        ]
                    }
                ],
                defaults: {
                    width: 350,
                    textAlign: 'left',
                    editor: {
                        xtype: 'textfield',
                        allowBlank: false
                    }
                }
            },
            bbar: [{
                xtype: 'button',
                text: 'Добавить колонку',
                handler: function () {
                    store.add({columnKey: '', columnName: ''});
                }
            }]
        });

        this.callParent();
    }
});