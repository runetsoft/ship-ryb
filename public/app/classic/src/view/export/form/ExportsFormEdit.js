Ext.define('Pricer.view.export.form.ExportsFormEdit', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.export.grid.ExportsPriceColumnsGrid',
        'Pricer.view.export.form.ExportsFormEditController',
        'Pricer.view.suppliers.forms.SuppliersCombo'
    ],
    xtype: 'exportform',
    controller: 'export.form.edit',
    width: '100%',
    reference: 'exportform',
    bodyPadding: 10,
    autoScroll: true,
    url: '/export/exportInfo.json',

    defaults: {
        style: {
            margin: 10
        }
    },
    fieldDefaults: {
        labelAlign: 'right',
        labelWidth: 100,
        msgTarget: 'side'
    },

    items: [
        {
            xtype: 'fieldset',
            title: 'Фильтры для экспорта',
            defaultType: 'textfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Наименование',
                    name: 'name'
                },
                {
                    fieldLabel: 'Тип',
                    xtype: 'dictionarymultiselect',
                    reference: 'goodType',
                    name: 'good_type',
                    dictionaryName: 'GoodType',
                    valueField: 'id'
                },
                {
                    fieldLabel: 'Поставщики',
                    xtype: 'suppliersCombo',
                    reference: 'suppliers',
                    name: 'suppliers',
                    valueField: 'id',
                    displayField: 'provider'
                },
                {
                    fieldLabel: 'Количество',
                    xtype: 'numberfield',
                    reference: 'minSize',
                    name: 'min_size'
                },

                {
                    boxLabel: 'Выгружать все товары из прайса ( в том числе без соответствий )',
                    labelAlign: 'left',
                    hideLabel: true,
                    xtype: 'checkbox',
                    reference: 'all_good',
                    name: 'all_good'
                }

            ]
        },
        {
            xtype: 'fieldset',
            title: 'Колонки (поумолчанию выгружаются все)',
            defaultType: 'textfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    reference: 'exportPriceColumnsList',
                    xtype: 'exportPriceColumnsList',
                    width:750

                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            handler: 'onExportData'
        },
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],
    listeners: {
        beforerender: 'onFormRender'
    },

    setFormValues: function (data) {
        var reference = this.getReferences(),
            exportPriceColumnsList = reference.exportPriceColumnsList,
            suppliersCombo = reference.suppliers,
            store = exportPriceColumnsList.getStore(),
            columnsObj = Ext.decode(data.columns),
            goodTypeCombo = reference.goodType;

        this.getForm().setValues(data);
        goodTypeCombo.select(Ext.decode(data.good_type));
        suppliersCombo.select(Ext.decode(data.provider_id));

        Object.keys(columnsObj).map(function (objectKey, index) {
            store.add({columnKey: objectKey, columnName: columnsObj[objectKey]});
        });
    }
});
