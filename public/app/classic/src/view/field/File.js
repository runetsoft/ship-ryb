Ext.define('Ext.field.MyCustomElement', {
    extend: 'Ext.container.Container',
    mixins: [
        'Ext.form.field.Field'
    ],
    xtype: 'fileuploadwithpreview',
    defaultListenerScope: true,
    referenceHolder: true,
    viewModel: {
        data : {
            fieldValue: ""
        },
        formulas: {
            marginBind: function (get) {
                var param = get('fieldValue');
                if (param) {
                    return '0 -45 0 0';
                } else {
                    return '0 -45 0 0';
                }
            },
            widthBind: function (get) {
                var param = get('fieldValue');
                if (param) {
                    return 380;
                } else {
                    return 455;
                }
            }
        }
    },

    width: 600,
    layout: {
        type: 'vbox',
        margin: '0 0 0 0'
    },

    initComponent: function () {

        var myItems = [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults: {

                },
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: this.label,
                        reference: 'copyableInput',
                        name: this.name + 'textfield',
                        publishes: 'value',
                        readOnly: true,
                        submitValue: false,
                        bind: {
                            margin: '{marginBind}',
                            width: '{widthBind}',
                            value: '{fieldValue}'
                        }

                    },
                    {
                        xtype: 'filefield',
                        buttonText: this.buttonText,
                        buttonOnly: true,
                        reference: 'input',
                        publishes: 'value',
                        name: this.name + 'filefield',
                        bind: {
                            value: '{fieldValue}'
                        },
                        margin: '0 -60 0 0'
                    },
                    {
                        xtype: 'button',
                        text: 'Удалить',
                        listeners: {
                            click: 'onDelete'
                        },
                        bind: {
                            hidden: '{!fieldValue}'
                        },
                        margin: '0 0 0 17'
                    },
                    {
                        xtype: 'imagecomponent',
                        name: this.name + 'imagecomponent',
                        bind: {
                            src: '{fieldValue}'
                        },
                        style: 'position: absolute !important; width: 150px;',
                        margin: '0 0 0 25'
                    }

                ]
            }
        ];

        Ext.apply(this, {
            items: myItems
        });

        this.callParent();
    },

    setValue: function (value) {
        this.getViewModel().setData({fieldValue:value});
    },

    getValue: function () {
        return this.lookupReference('input').getValue();
    },

    onDelete: function () {
        this.getViewModel().setData({fieldValue:null});
    }

});