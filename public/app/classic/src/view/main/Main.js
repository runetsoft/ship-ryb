/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 */
Ext.define('Pricer.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    requires: [
        'Pricer.view.extention.QueryDispatcher',
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Pricer.view.main.MainController',
        'Pricer.view.main.MainModel',
        'Pricer.view.main.List',
        'Pricer.view.price.Main',
        'Pricer.view.goods.Main'
    ],
    cls:'main-panel',
    controller: 'main',
    viewModel: 'main',
    ui: 'navigation',
    id: 'main-tabs',
    activeModule: null,
    urlData: null,

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [
        {
            title: 'Прайсы',
            id: 'price',
            iconCls: 'fa-table',
            overflowY: 'auto',
            items: [{
                xtype: 'price'
            }]
        },
        {
            title: 'Товары',
            id: 'goods',
            iconCls: 'fa-truck',
            overflowY: 'auto',
            items: [{
                xtype: 'goods'
            }]
        },
        {
            title: 'Поставщики',
            iconCls: 'fa-industry',
            id: 'suppliers',
            overflowY: 'auto',
            items: [{
                xtype: 'suppliers'
            }]
        },
        {
            title: 'Экспорты',
            iconCls: 'fa-file-excel-o',
            id: 'export',
            overflowY: 'auto',
            items: [{
                xtype: 'export'
            }]
        },
        {
            title: 'Лог импорта',
            iconCls: 'fa-list',
            overflowY: 'auto',
            id: 'log',
            items: [{
                xtype: 'log'
            }]
        },
        {
            title: 'Справочники',
            iconCls: 'fa-book',
            overflowY: 'auto',
            id: 'dictionary',
            items: [{
                xtype: 'dictionaryGrid'
            }]
        },
        {
            title: 'Типы товаров',
            iconCls: 'fa-list-alt',
            overflowY: 'auto',
            id: 'good_types',
            items: [
                {
                    xtype: 'dictionaryValueList',
                    reference: 'dictionaryValueList',
                    dictionary: 'GoodType',
                    dictionaryBind: 'GoodTypeCategory',
                    showAddNewBtn: false
                }, {
                    xtype: 'button',
                    text: "Добавить новый тип товара",
                    iconCls: 'x-fa fa-plus',
                    style: {
                        marginBottom: '10px',
                        background: '#1bc98e'
                    },
                    handler: 'onAddGoodType'
                }
            ]
        },
        {
            title: 'Выход',
            iconCls: 'fa-sign-out',
            id: 'logout'
        }
    ],

    listeners: {
        tabchange: function (tabPanel, tab) {
            Pricer.view.extention.QueryDispatcher.setModule(tab.id);
            if(tab.id == 'logout'){
                localStorage.setItem("LoggedIn", '');
                window.location.href = '/app/';
            }
        },
        afterrender: function () {
            Pricer.view.extention.QueryDispatcher.getUrlData()
                .then(
                    function (result) {
                        this.setActiveItem(result.module);
                    }.bind(this)
                );
        }
}
});
