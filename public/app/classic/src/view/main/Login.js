/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 */
Ext.define('Pricer.view.main.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'app-login',
    requires: [
        'Pricer.view.main.LoginController'
    ],
    controller: 'login',

    layout: 'center',
    bodyStyle: 'padding:20px 0; background-color:#ececec',
    items: [{
            title: 'Авторизация',
            width: 350,
            height: 240,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Логин/E-mail:',
                name: 'login',
                style: 'text-align: right; margin-left: 26px; margin-top:14px;'
                },
                {

                    xtype: 'textfield',
                    fieldLabel: 'Пароль:',
                    inputType: 'password',
                    name: 'password',
                    style: 'text-align: right; margin-left: 26px; margin-top:14px;'
                },
                {
                    xtype:'checkbox',
                    hidden: true,
                    fieldLabel: 'На сутки:',
                    name: 'time24',
                    value: 1,
                    style: 'text-align: right; margin-left: 26px; margin-top:14px;'
                }
            ],
            buttons: [{
                    text: 'Вход',
                    handler: 'onLoginClick'
                }]
        }]

});
