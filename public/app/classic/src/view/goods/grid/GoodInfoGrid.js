/**
 * Created by babkin on 31.03.2017.
 */
Ext.define('Pricer.view.goods.grid.GoodInfoGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'goodInfoList',
    goodId: null,
    photo: '',
    maxHeight: 500,
    autoScroll: true,
    layout: 'fit',
    defaultPhoto: '/files/sass/missing.png',
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.goods.GoodInfoStore',
        'Pricer.view.goods.grid.GoodsGridController'
    ],
    controller: 'goods-grid-goodsgrid',
    listeners: {
        cellclick: 'onInfoGridCellClick'
    },


    initComponent: function () {

        // create the Data Store
        var store = Ext.create('Pricer.store.goods.GoodInfoStore');
        store.load({params:{'goodId': this.goodId}});

        Ext.apply(this, {
            store: store,
            viewConfig: {
                trackOver: false,
                stripeRows: false,
                enableTextSelection: true,
                getRowClass: function () {
                    return this.enableTextSelection ? 'x-selectable' : '';
                }
            },
            autoHeight: true,
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width: 80,
                    filterField: 'textfield',
                    renderer: function (value, metaData) {
                        metaData.tdStyle = 'text-decoration:underline; text-decoration-style:dashed; cursor: pointer;';
                        return value;
                    }
                },
                {
                    text: 'Фото',
                    dataIndex: 'photo',
                    align: 'center',
                    width: 150,
                    renderer: function (value) {
                        var photo = (value) ? value : this.defaultPhoto;
                        return "<div style=\"background-image: url('" + photo + "')\" class=\"price-grid-photo\"></div>";
                    },
                    filterField: 'textfield'
                },
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    width: 50,
                    items: [
                        {
                            iconCls: 'x-fa fa-image',
                            tooltip: 'Использовать как картинку товара',
                            handler: 'updateGoodImageByPrice'
                        }
                    ]
                },
                {
                    text: 'Поставщик',
                    dataIndex: 'provider',
                    align: 'left',
                    width: 100,
                    filterField: 'textfield'
                },
                {
                    text: 'Название',
                    dataIndex: 'good',
                    align: 'left',
                    width: 450,
                    filterField: 'textfield'
                },
                {
                    text: 'Склад',
                    dataIndex: 'sklad',
                    align: 'left',
                    width: 80,
                    filterField: 'textfield'
                },
                {
                    text: 'Цена',
                    dataIndex: 'price',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    align: 'center',
                    width: 80,
                    filterField: 'textfield'
                },
                {
                    text: 'ЗА',
                    dataIndex: 'zarticle',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    align: 'center',
                    width: 80,
                    filterField: 'textfield'
                },
                {
                    text: 'МРЦ',
                    dataIndex: 'rprice',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    align: 'center',
                    width: 80,
                    filterField: 'textfield'
                },
                {
                    text: 'Дата обновления',
                    dataIndex: 'dt_update',
                    width: 145,
                    align: 'left',
                    filterField: 'textfield'
                }
            ]
        });
        this.callParent();
    }
});