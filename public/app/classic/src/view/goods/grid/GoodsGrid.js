/**
 * Created by babkin on 31.03.2017.
 */
Ext.define('Pricer.view.goods.grid.GoodsGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'goodsList',
    module: 'goods',
    width: '99%',
    layout: 'fit',
    forceFit: true,
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.goods.GoodsStore',
        'Pricer.view.goods.grid.GoodsGridController',
        'Pricer.view.goods.grid.GoodsGridModel'
    ],
    controller: 'goods-grid-goodsgrid',
    viewModel: {
        type: 'goods-grid-goodsgrid'
    },

    initComponent: function () {

        // create the Data Store
        var store = Ext.create('Pricer.store.goods.GoodsStore', {remoteSort: true});
        Ext.apply(this, {
            store: store,
            minHeight: 600,
            viewConfig: {
                enableTextSelection: true,
                listeners: {
                    cellclick: 'onCellClick'
                }
            },
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true
            },
            // grid columns
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width: 100,
                    filterField: 'textfield',
                    renderer: function (value, metaData) {
                        metaData.tdStyle = 'text-decoration:underline; text-decoration-style:dashed; cursor: pointer;';
                        return value;
                    }
                },
                {
                    text: 'Название',
                    dataIndex: 'good',
                    align: 'left',
                    width: 600,
                    filterField: 'textfield'
                },
                {
                    text: 'ЗА',
                    dataIndex: 'zarticle',
                    align: 'center',
                    width: 100,
                    filterField: 'textfield'
                },
                {
                    text: 'МРЦ',
                    dataIndex: 'rprice',
                    renderer: Pricer.view.extention.Render.renderString,
                    align: 'center',
                    width: 100
                },
                {
                    text: 'Использовано',
                    dataIndex: 'price_cnt',
                    renderer: Pricer.view.extention.Render.renderNumber,
                    align: 'center',
                    width: 100
                },
                {
                    text: 'Маркет',
                    dataIndex: 'good_url',
                    align: 'center',
                    renderer: this.renderMarket,
                    width: 150
                },
                {
                    text: 'Тип товара',
                    dataIndex: 'type',
                    condition: '=type',
                    align: 'center',
                    width: 150,
                    filterField: {xtype: 'dictionary', dictionaryName: 'GoodType', valueField: 'id'}
                },
                {
                    text: 'Товары с фото',
                    hidden: true,
                    dataIndex: 'photo_type',
                    align: 'center',
                    width: 150,
                    filterField: {xtype: 'dictionary', dictionaryName: 'GoodPhotoType', valueField: 'code'}
                },
                {
                    text: 'Дeйствия',
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    width: 150,
                    items: [{
                        iconCls: 'x-fa fa-edit',
                        tooltip: 'Редактировать',
                        handler: 'onItemEdit'
                    }, {
                        iconCls: 'x-fa fa-trash',
                        tooltip: 'Удалить',
                        handler: 'onItemDelete'
                    }]
                }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                cls: 'grid-tool-bar',
                items: [
                    {
                        xtype: 'gridfilter',
                        grid: this
                    }
                ]
            }],
            listeners: {
                rowdblclick: 'onItemSelected'
            },
            // paging bar on the bottom
            bbar: [
                {
                    xtype: 'checkbox',
                    grid: this,
                    reference: 'checkboxSelectAll',
                    listeners: {
                        change: function () {
                            if (this.value == true) {
                                this.grid.getSelectionModel().selectAll();
                            } else {
                                if (this.grid.getSelectionModel().selected.length == this.grid.getStore().data.length) {
                                    this.grid.getSelectionModel().deselectAll();
                                }
                            }
                        }
                    }
                },
                {
                    xtype: 'pricertoolbar',
                    store: store,
                    autoScroll: true,
                    items: [{
                        xtype: 'prepage',
                        grid: this
                    }]
                },
                {
                    xtype: 'button',
                    text: 'Удалить выделенные',
                    iconCls: 'x-fa fa-times',
                    cls: 'btn-remove',
                    handler: function (btn) {
                        var grid = btn.up('gridpanel'),
                            s = grid.getSelectionModel().getSelection(),
                            selected = [];
                        Ext.each(s, function (item) {
                            selected.push(item.data.id);
                        });
                        if (selected.length == 0) {
                            Ext.MessageBox.alert(
                                'Ошибка',
                                'Не выбраны товары',
                                this
                            );
                            return;
                        } else {
                            Ext.MessageBox.show({
                                title: 'Внимание ',
                                msg: 'Вы точно хотите удалить товары!!!',
                                buttons: Ext.MessageBox.OK,
                                fn: function (confirmBtn) {
                                    if (confirmBtn == 'ok') {
                                        Pricer.view.extention.Ajax.request({
                                            targetMask: grid,
                                            url: '/good/delete.json',
                                            method: 'GET',
                                            params: {
                                                goodIds: JSON.stringify(selected)
                                            },
                                            success: function (response, opts) {
                                                Ext.Msg.alert('Успешно', 'Все товары успешно удалены');
                                                grid.getStore().loadPage(1);
                                            },
                                            failure: function (response, opts) {
                                                var responseText = Ext.decode(response.responseText);
                                                Ext.Msg.alert('Ошибка', responseText);
                                            }
                                        });
                                    }
                                }
                            }, this);
                        }
                    },
                    style: {
                        margin: '5px',
                        background: 'red',
                        color: 'white !importent'
                    },
                    scope: this
                }
            ]
        });
        this.callParent();
    },
    renderNumber: function (value, p, model) {
        return value || 0;
    },
    renderMarket: function (value, p, model) {
        return Ext.String.format(
            '<a target="_blank" href="http://market.yandex.ru/search.xml?text={0}&cvredirect=2">link</a>',
            value
        );
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});