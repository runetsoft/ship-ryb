Ext.define('Pricer.view.export.grid.AddGoodTypeGrid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Pricer.view.goods.forms.GoodTypeController'
    ],
    xtype: 'addgoodtypegrid',
    controller: 'goods.form.goodtype',
    reference: 'addgoodtypegrid',
    items: [],
    margin: '10',
    layout: 'fit',
    initComponent: function () {
        var store = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'title', type: 'string'},
                {name: 'column', type: 'string'},
                {name: 'field_type', type: 'string'},
                {name: 'dictionary_name', type: 'string'}
            ],
            data: []
        });
        Ext.apply(this, {
            store: store,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ],
            columns: {
                items: [
                    {
                        text: 'Загаловок *',
                        dataIndex: 'title',
                        editor:{
                            allowBlank: false
                        }
                    },
                    {
                        text: 'Имя колонки *',
                        dataIndex: 'column',
                        readOnly: true,
                        editor:{
                            maskRe: /[a-z0-9_]/,
                            regex: /^[a-z0-9_]+$/,
                            allowBlank: false
                        }
                    },
                    {
                        text: 'Тип поле *',
                        dataIndex: 'field_type',
                        editor: {
                            dictionaryName: 'FieldType',
                            valueField: 'code',
                            xtype: 'dictionary',
                            listeners: {
                                select: 'onGoodTypeChanged'
                            }
                        }
                    },
                    {
                        text: 'Справочники',
                        dataIndex: 'dictionary_name',
                        editor: {
                            dictionaryName: 'DictionariesList',
                            valueField: 'code',
                            xtype: 'dictionary',
                            listeners: {
                                select: 'onFieldTypeSelect'
                            }
                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        dataIndex: 'actions',
                        width: 60,
                        editor: false,
                        items: [
                            {
                                iconCls: 'x-fa fa-trash',
                                tooltip: 'Удалить',
                                handler: 'onItemDelete'
                            }
                        ]
                    }
                ],
                defaults: {
                    width: 165,
                    textAlign: 'left',
                    editor: {
                        xtype: 'textfield',
                        allowBlank: false
                    }
                }
            },
            bbar: [{
                xtype: 'button',
                text: 'Добавить строку',
                handler: function () {
                    store.add(
                        {
                            title: '',
                            column: '',
                            field_type: 'textfield'
                        }
                    );
                }
            }]
        });

        this.callParent();
    }
});