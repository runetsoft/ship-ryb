/**
 * Created by swds.dll on 11.04.2017.
 */

Ext.define('Pricer.view.goods.windows.GoodsWindowsEdit', {
    extend: 'Ext.window.Window',
    title: 'Редактирование товара',
    controller: 'goods.window.edit',
    parentGrid: null,
    referenceHolder: true,
    width: 800,
    maxHeight: 800,
    autoScroll:true,
    modal: true,
    items: [],
    listeners: {
        formShow: 'onShowForm',
        resize: function (win, width, height, oldWidth, oldHeight, eOpts) {
            var xPos = win.getPosition()[0],
                v,
                yPos = ((v = (Ext.getBody().getViewSize().height - height) / 2) > 0) ? v : 0;
            win.setPosition(xPos, yPos, true);
        }
    }
});