Ext.define('Pricer.view.export.window.EditGoodTypeWindow', {
    extend: 'Ext.window.Window',
    title: 'Редактировать новый тип товара',
    controller: 'goods.form.goodtype',
    referenceHolder: true,
    width: 1000,
    maxHeight: 800,
    autoScroll: true,
    modal: true,
    goodType: null,
    goodTypeId: null,
    items: [],
    initComponent: function () {
        var cmp = this;
        Ext.apply(this, {
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Колонки таблицы типа товара',
                    margin: '10',
                    defaults: {
                        anchor: '100%'
                    },
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'editgoodtypegrid',
                            goodTypeId: cmp.goodTypeId
                        }
                    ]
                }
            ]
        });
        this.callParent();
    },
    listeners: {
        resize: function (win, width, height, oldWidth, oldHeight, eOpts) {
            var xPos = win.getPosition()[0],
                v,
                yPos = ((v = (Ext.getBody().getViewSize().height - height) / 2) > 0) ? v : 0;
            win.setPosition(xPos, yPos, true);
        }
    },
    buttons: [
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        },
        {
            text: 'Сохранить',
            handler: 'onEditGoodType'
        }
    ]
});