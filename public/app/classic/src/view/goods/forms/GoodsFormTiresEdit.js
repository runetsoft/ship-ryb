/**
 * Created by swds.dll on 14.04.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsFormTiresEdit', {
    extend: 'Ext.form.Panel',
    xtype: 'goodsformtire',
    reference: 'tiresform',
    controller: 'goods.form.edit',
    url: '/good/getTireInfo.json',
    height: 400,
    width: 750,
    bodyPadding: 10,
    goodId: false,
    autoScroll: true,
    defaultType: 'textfield',
    fieldDefaults: {
        labelWidth: 100,
        labelAlign: 'right'
    },
    layout: {
        type: 'vbox'
    },
    items: [

    {
            xtype: 'hiddenfield',
            name: 'id'
        },
        {
            xtype: 'hiddenfield',
            name: 'type',
            value: Pricer.view.extention.Goods.GOODS_TIRES_TYPE
        },
        {
            xtype: 'fileuploadwithpreview',
            name: 'photo',
            label: 'Выбрать фото',
            buttonText: 'Выбрать',
            width: '100%',
            layout: 'border',
            style: 'background-color:rgba(0, 0, 0, 0);',
            margin: '0 0 -125 0',
            height: 180
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Название',
            name: 'good',
            reference: 'good',
            width: '75%'
        },
        {
            xtype: 'fieldcontainer',
            combineErrors: false,
            reference: 'modelBrand',
            width: '75%',
            items: [
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Бренд',
                    reference: 'brand',
                    name: 'brand',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'TyreBrand',
                    valueField: 'id',
                    savedText: 'новый бренд',
                    editorMode: true,
                    typeAhead: false,
                    forceSelection: false,
                    listeners: {
                        change: function () {
                            var fieldcontainer = this.up('fieldcontainer');
                            fieldcontainer.fireEvent('brandChanched');
                        },
                        addValueToDictionary: function (component, savingValue) {
                            component.saveData = {};
                            component.saveData.name = savingValue;
                        }
                    }
                },
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Модель',
                    reference: 'model',
                    name: 'model',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'TyreModel',
                    valueField: 'id',
                    editorMode: true,
                    savedText: 'новую модель',
                    listeners: {
                        addValueToDictionary: function (component, savingValue) {
                            var winObj = component.up('window');
                            var formObj = winObj.lookupReference('tiresform');
                            var brandObj = formObj.lookupReference('brand');
                            component.saveData = {};
                            component.saveData.parent_id = brandObj.value;
                            component.saveData.name = savingValue;
                        }
                    }
                }
            ],
            listeners: {
                'brandChanched': function () {
                    var winObj = this.up('window');
                    var formObj = winObj.lookupReference('tiresform');
                    var brandObj = formObj.lookupReference('brand');
                    var modelObj = formObj.lookupReference('model');
                    modelObj.store.getProxy().extraParams['parent_id'] = brandObj.value;
                    modelObj.store.reload();
                }
            }
        },
        {
            xtype: 'fieldset',
            title: 'Параметры',
            defaultType: 'textfield',
            layout: 'anchor',
            reference: 'characteristics',
            margin: '0 0 5 0',
            width: 700,
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Ширина',
                            name: 'width',
                            width: '30%'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Высота',
                            name: 'height',
                            width: '30%'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Диаметр',
                            name: 'diameter',
                            width: '30%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Construction',
                            fieldLabel: 'Конструкция',
                            valueField: 'code',
                            name: 'construction',
                            width: '30%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Нагрузка',
                            name: 'index_load',
                            width: '30%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Скорость',
                            name: 'index_speed',
                            width: '30%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Хар. Аббр.',
                            name: 'features',
                            width: '30%'
                        },
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Runflat',
                            fieldLabel: 'Ранфлэт',
                            valueField: 'code',
                            displayField: 'name',
                            name: 'runflat',
                            width: '30%'
                        },
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Camers',
                            fieldLabel: 'Камерность',
                            valueField: 'code',
                            name: 'camers',
                            width: '30%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'dictionary',
                            fieldLabel: 'Сезон',
                            name: 'season',
                            dictionaryName: 'TyreSeason',
                            valueField: 'code',
                            width: '30%'
                        },
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Xl',
                            fieldLabel: 'XL',
                            valueField: 'code',
                            name: 'xl',
                            width: '30%'
                        },
                        {
                            xtype: 'checkbox',
                            fieldLabel: 'Карго',
                            name: 'cargo',
                            width: '30%'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            defaultType: 'textfield',
            reference: 'zaMrc',
            width: 745,
            margin: '0 0 5 0',
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'МРЦ',
                    name: 'rprice',
                    width: '45%'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'ЗА',
                    name: 'zarticle',
                    width: '45%'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            handler: 'onSaveData'
        },
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],
    listeners: {
        beforerender: 'onFormRender'

    }
});
