
Ext.define('Pricer.view.goods.forms.GoodsCombo', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'Ext.form.field.ComboBox'
    ],
    xtype: 'goodsCombo',
    displayField: 'good',
    store: null,
    typeAhead: true,
    selectOnFocus: true,
    disabled: false,
    triggerAction: 'all',
    valueField: 'id',


    initComponent: function () {
            this.store = Ext.create('Pricer.store.goods.GoodsStore', {});
            this.callParent();
    },
    listeners: {
        dirtychange: function(){
            var comboC = this;
            var value = this.value;
            var store = this.store;
            var params = store.getProxy().getExtraParams();
            if (value) {
                if (Ext.Object.isEmpty(params)) {
                    store.getProxy().extraParams.id = value;
                    store.reload({
                        callback: function () {
                            comboC.setValue(value);
                            store.getProxy().extraParams = [];
                        }
                    });
                }
            }
        },
        change: function () {
            var params = this.store.getProxy().getExtraParams();
            if (!Ext.Object.isEmpty(params) && !Ext.isDefined(this.displayTplData)) {
                this.store.getProxy().extraParams = [];
                this.store.reload({});
            }
        }
    }
});
