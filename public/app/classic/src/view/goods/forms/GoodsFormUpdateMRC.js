/**
 *
 */
Ext.define('Pricer.view.goods.forms.GoodsFormUpdateMRC', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.goods.forms.GoodsFormUpdateMRCController'
    ],
    xtype: 'goodsformupdatemrc',
    reference: 'goodsformupdatemrc',
    controller: 'goods.form.update.mrc',
    bodyPadding: 10,
    items: [
        {
            xtype: 'container',
            html: '<span>Файл должен быть в формате csv с двумя столбцами без заголовков, первый столбец артикул, второй МРЦ</span>',
            style: {
                padding:'5px'
            }
        },
        {
            xtype: 'dictionary',
            text: 'Тип товара',
            dataIndex: 'type',
            align: 'center',
            width: 445,
            dictionaryName: 'GoodType',
            valueField: 'id',
            name: 'type',
            allowBlank: false
        },
        {
            xtype: 'filefield',
            name: 'file',
            anchor: '100%',
            allowBlank: false,
            buttonText: 'Выберите файл...'
        },
        {
            xtype: 'button',
            text: 'Обновить',
            handler: 'onUpdateMRC'
        }
    ]
});