/**
 * Created by swds.dll on 14.04.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsFormWheelsEdit', {
    extend: 'Ext.form.Panel',
    xtype: 'goodsformwheel',
    reference: 'wheelsform',
    controller: 'goods.form.edit',
    url: '/good/getWheelInfo.json',
    height: 400,
    width: 750,
    bodyPadding: 10,
    goodId: false,
    autoScroll: true,
    defaultType: 'textfield',
    fieldDefaults: {
        labelWidth: 100,
        labelAlign: 'right'
    },
    layout: {
        type: 'vbox'
    },
    items:[
        {
            xtype: 'hiddenfield',
            name: 'id'
        },
        {
            xtype: 'hiddenfield',
            name: 'type',
            value: Pricer.view.extention.Goods.GOODS_DISK_TYPE
        },
        {
            xtype: 'fileuploadwithpreview',
            name: 'photo',
            label: 'Выбрать фото',
            buttonText: 'Выбрать',
            width: '100%',
            layout: 'border',
            style: 'background-color:rgba(0, 0, 0, 0);',
            margin: '0 0 -125 0',
            height: 180
        },
        {
            xtype:'textfield',
            fieldLabel: 'Название',
            name: 'good',
            width: '75%'
        },
        {
            xtype: 'fieldcontainer',
            combineErrors: false,
            width: '75%',
            items: [
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Бренд',
                    reference: 'brand',
                    name: 'brand',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'DiskBrand',
                    valueField: 'id',
                    savedText: 'новый бренд',
                    editorMode: true,
                    typeAhead: false,
                    forceSelection: false,
                    listeners: {
                        change: function () {
                            var fieldcontainer = this.up('fieldcontainer');
                            fieldcontainer.fireEvent('brandChanched');
                        },
                        addValueToDictionary: function (component, savingValue) {
                            component.saveData = {};
                            component.saveData.name = savingValue;
                        }
                    }
                },
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Модель',
                    reference: 'model',
                    name: 'model',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'DiskModel',
                    savedText: 'новую модель',
                    editorMode: true,
                    valueField: 'id',
                    listeners: {
                        addValueToDictionary: function (component, savingValue) {
                            var winObj = component.up('window');
                            var formObj = winObj.lookupReference('wheelsform');
                            var brandObj = formObj.lookupReference('brand');
                            component.saveData = {};
                            component.saveData.parent_id = brandObj.value;
                            component.saveData.name = savingValue;
                        }
                    }
                }
            ],
            listeners: {
                'brandChanched': function () {
                    var winObj = this.up('window');
                    var formObj = winObj.lookupReference('wheelsform');
                    var brandObj = formObj.lookupReference('brand');
                    var modelObj = formObj.lookupReference('model');
                    modelObj.store.getProxy().extraParams['parent_id'] = brandObj.value;
                    modelObj.store.reload();
                }
            }
        },
        {
            xtype: 'fieldset',
            title: 'Параметры',
            defaultType: 'textfield',
            layout: 'anchor',
            margin: '0 0 5 0',
            width: 700,
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    items: [
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Ширина',
                            name: 'width',
                            width: '27%',
                            margin: '5 0 0 0',
                            labelWidth: 100
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'количество отверстий',
                            name: 'hole_num',
                            width: '24%',
                            labelWidth: 100
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Диаметр',
                            name: 'diameter',
                            width: '23%',
                            labelWidth: 70,
                            margin: '5 0 0 0'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'диаметр отверстий',
                            name: 'diameter_hole',
                            width: '25%',
                            labelWidth: 80
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'вылет',
                            name: 'overhand',
                            width: '26%'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Ø ступичного отверстия',
                            name: 'diameter_nave',
                            width: '40%',
                            labelWidth: 170
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'цвет',
                            name: 'color',
                            width: '33%',
                            labelWidth: 40
                        }
                    ]
                },
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Тип диска',
                    name: 'disk_type',
                    dictionaryName: 'DiskType',
                    valueField: 'code'
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            defaultType: 'textfield',
            margin: '0 0 5 0',
            width: 745,
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'МРЦ',
                    name: 'rprice',
                    width: '45%'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'ЗА',
                    name: 'zarticle',
                    width: '45%'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            handler: 'onSaveData'
        },
        {
            text: 'Отмена',
            handler: function() {
               this.up('window').destroy();
            }
        }
    ],
    listeners:{
        beforerender: 'onFormRender'
    }
});
