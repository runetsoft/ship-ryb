/**
 *
 */
Ext.define('Pricer.view.goods.forms.GoodsFormTiresMotoEdit', {
    extend: 'Ext.form.Panel',
    xtype: 'goodsformtiremoto',
    reference: 'tiresmotoform',
    controller: 'goods.form.edit',
    url: '/good/getTireMotoInfo.json',
    height: 400,
    width: 750,
    bodyPadding: 10,
    goodId: false,
    autoScroll: true,
    defaultType: 'textfield',
    fieldDefaults: {
        labelWidth: 100,
        labelAlign: 'right'
    },
    layout: {
        type: 'vbox'
    },
    items:[
        {
            xtype: 'hiddenfield',
            name: 'id'
        },
        {
            xtype: 'hiddenfield',
            name: 'type',
            value: Pricer.view.extention.Goods.GOODS_TIRES_MOTO_TYPE
        },
        {
            xtype: 'fileuploadwithpreview',
            name: 'photo',
            label: 'Выбрать фото',
            buttonText: 'Выбрать',
            width: '100%',
            layout: 'border',
            style: 'background-color:rgba(0, 0, 0, 0);',
            margin: '0 0 -125 0',
            height: 180
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Название',
            name: 'good',
            width: '75%'
        },
        {
            xtype: 'fieldcontainer',
            combineErrors: false,
            width: '75%',
            items: [
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Бренд',
                    reference: 'brand',
                    name: 'brand',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'TyreBrand',
                    savedText: 'новый бренд',
                    valueField: 'id',
                    editorMode: true,
                    typeAhead: false,
                    forceSelection: false,
                    listeners: {
                        change: function () {
                            var fieldcontainer = this.up('fieldcontainer');
                            fieldcontainer.fireEvent('brandChanched');
                        },
                        addValueToDictionary: function (component, savingValue) {
                            component.saveData = {};
                            component.saveData.name = savingValue;
                        }
                    }
                },
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Модель',
                    reference: 'model',
                    name: 'model',
                    width: '100%',
                    align: 'right',
                    dictionaryName: 'TyreModel',
                    savedText: 'новую модель',
                    editorMode: true,
                    valueField: 'id',
                    listeners: {
                        addValueToDictionary: function (component, savingValue) {
                            var winObj = component.up('window');
                            var formObj = winObj.lookupReference('tiresmotoform');
                            var brandObj = formObj.lookupReference('brand');
                            component.saveData = {};
                            component.saveData.parent_id = brandObj.value;
                            component.saveData.name = savingValue;
                        }
                    }
                }
            ],
            listeners: {
                'brandChanched': function () {
                    var formObj = this.up('form');
                    var brandObj = formObj.lookupReference('brand');
                    var modelObj = formObj.lookupReference('model');
                    modelObj.store.getProxy().extraParams['parent_id'] = brandObj.value;
                    modelObj.store.reload();
                }
            }
        },
        {
            xtype: 'fieldset',
            title: 'Параметры',
            defaultType: 'textfield',
            layout: 'anchor',
            margin: '0 0 5 0',
            width: 700,
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Ширина',
                            name: 'width',
                            width: '30%'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Высота',
                            name: 'height',
                            width: '30%'
                        },
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'Диаметр',
                            name: 'diameter',
                            width: '30%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Construction',
                            fieldLabel: 'Конструкция',
                            valueField: 'code',
                            name: 'construction',
                            width: '30%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Нагрузка',
                            name: 'index_load',
                            width: '30%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Скорость',
                            name: 'index_speed',
                            width: '30%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Хар. Аббр.',
                            name: 'features',
                            width: '30%'
                        },
                        {
                            xtype: 'dictionary',
                            dictionaryName: 'Camers',
                            fieldLabel: 'Камерность',
                            valueField: 'code',
                            name: 'camers',
                            width: '26%'
                        },
                        {
                            xtype: 'dictionary',
                            fieldLabel: 'Ось',
                            name: 'axis',
                            dictionaryName : 'TyreAxisMoto',
                            reference: 'axis',
                            valueField: 'code',
                            width: '34%'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            defaultType: 'textfield',
            width: 745,
            margin: '0 0 5 0',
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'МРЦ',
                    name: 'rprice',
                    width: '45%'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'ЗА',
                    name: 'zarticle',
                    width: '45%'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            handler: 'onSaveData'
        },
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],
    listeners: {
        beforerender: 'onFormRender'

    }
});
