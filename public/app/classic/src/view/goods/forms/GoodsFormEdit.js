/**
 * Created by swds.dll on 14.04.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsFormEdit', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.goods.forms.GoodsFormEditController'
    ],
    xtype: 'goodform',
    reference: 'goodform',
    controller: 'goods.form.edit',
    height: 400,
    width: '100%',
    bodyPadding: 10,
    goodId: false,
    autoScroll: true,
    defaultType: 'textfield',
    goodTypeId: 4,
    attributes: [],
    brandClass: '',
    modelClass: '',
    fieldDefaults: {
        labelWidth: 100,
        labelAlign: 'right'
    },
    layout: {
        type: 'vbox'
    },
    initComponent: function () {
        var attributes = this.attributes,
            goodType = this.goodType,
            brandClass = this.brandClass,
            modelClass = this.modelClass;

        Ext.apply(this, {
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'type',
                    value: goodType
                },
                {
                    xtype: 'fileuploadwithpreview',
                    name: 'photo',
                    label: 'Выбрать фото',
                    buttonText: 'Выбрать',
                    width: '100%',
                    layout: 'border',
                    style: 'background-color:rgba(0, 0, 0, 0);',
                    margin: '0 0 -125 0',
                    height: 180
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Название',
                    name: 'good',
                    reference: 'good',
                    width: '70%'
                },
                {
                    xtype: 'fieldcontainer',
                    combineErrors: false,
                    reference: 'modelBrand',
                    width: '75%',
                    items: [
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            defaultType: 'textfield',
                            reference: 'zaMrc',
                            width: '100%',
                            margin: '0 0 5 0',
                            items: [
                                {
                                    xtype: 'dictionary',
                                    fieldLabel: 'Бренд',
                                    reference: 'brand',
                                    name: 'brand',
                                    width: '45%',
                                    align: 'right',
                                    dictionaryName: brandClass,
                                    valueField: 'id',
                                    savedText: 'новый бренд',
                                    editorMode: true,
                                    typeAhead: false,
                                    forceSelection: false,
                                    listeners: {
                                        change: {
                                            fn: function () {
                                                var modelBrand = this.lookupReference('modelBrand');
                                                modelBrand.fireEvent('brandChanged');
                                            },
                                            scope: this
                                        },
                                        addValueToDictionary: {
                                            fn: function (component, savingValue) {
                                                component.saveData = {};
                                                component.saveData.name = savingValue;
                                            },
                                            scope: this
                                        }
                                    }
                                },
                                {
                                    xtype: 'dictionary',
                                    fieldLabel: 'Модель',
                                    reference: 'model',
                                    name: 'model',
                                    width: '45%',
                                    align: 'right',
                                    dictionaryName: modelClass,
                                    valueField: 'id',
                                    editorMode: true,
                                    savedText: 'новую модель',
                                    listeners: {
                                        addValueToDictionary: function (component, savingValue) {
                                            var winObj = component.up('window');
                                            var formObj = winObj.lookupReference('goodform');
                                            var brandObj = formObj.lookupReference('brand');
                                            component.saveData = {};
                                            component.saveData.parent_id = brandObj.value;
                                            component.saveData.name = savingValue;
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    listeners: {
                        'brandChanged': {
                            fn: function () {
                                var brandObj = this.getReferences().brand,
                                    modelObj = this.getReferences().model,
                                    modleFilter = JSON.parse(modelObj.store.getProxy().extraParams.filter);
                                modleFilter['=parent_id'] = brandObj.value;
                                modelObj.store.getProxy().extraParams.filter = JSON.stringify(modleFilter);
                                modelObj.store.reload();
                            },
                            scope: this.getController()
                        }
                    }
                },
                {
                    xtype: 'fieldset',
                    title: 'Параметры',
                    defaultType: 'textfield',
                    layout: 'anchor',
                    reference: 'attributes',
                    margin: '0 0 5 0',
                    width: '100%',
                    defaults: {
                        anchor: '100%'
                    },
                    items: attributes
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    reference: 'zaMrc',
                    width: '100%',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'textfieldnumber',
                            fieldLabel: 'МРЦ',
                            name: 'rprice',
                            width: '45%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'ЗА',
                            name: 'zarticle',
                            width: '45%'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Отмена',
                    handler: function () {
                        this.up('window').destroy();
                    }
                },
                {
                    text: 'Сохранить',
                    handler: 'onSaveData'
                }
            ],
            listeners: {
                beforerender: {
                    fn: 'onFormRender',
                    scope: this.getController()
                }
            }
        });
        this.callParent();
    }
});
