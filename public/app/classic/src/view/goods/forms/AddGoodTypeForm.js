Ext.define('Pricer.view.goods.forms.addgoodtypeform', {
    extend: 'Ext.form.Panel',
    xtype: 'addgoodtypeform',
    controller: 'goods.form.goodtype',
    reference: 'addgoodtypeform',
    autoHeight: true,
    bodyPadding: 0,
    autoScroll: true,
    defaultType: 'textfield',
    fieldDefaults: {
        labelWidth: 180,
        labelAlign: 'right'
    },
    layout: 'hbox',
    items: [
        {
            xtype: 'fieldset',
            title: 'Параметры типа *',
            layout: 'hbox',
            margin: '10',
            defaults: {
                xtype: 'textfield',
                anchor: '50%'
            },
            items: [
                {
                    fieldLabel: 'Загаловок',
                    name: 'good_type_title',
                    allowBlank: false
                },
                {
                    fieldLabel: 'Тип товара (уникальная) *',
                    name: 'good_type',
                    maskRe: /[a-z]/,
                    regex: /^[a-z]+$/,
                    allowBlank: false
                }
            ]
        }
    ]
});
