/**
 * Created by swds.dll on 18.04.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsFormTypesEdit', {
    extend: 'Ext.form.Panel',
    xtype: 'goodsformtype',
    reference: 'typeform',
    controller: 'goods.type.edit',
    requires: [
        'Pricer.view.goods.form.GoodsTypeEditModel'
    ],
    viewModel: {
        type: 'goods.type.edit'
    },
    bodyPadding: 10,
    items: [
        {
            fieldLabel: 'Тип',
            xtype: 'dictionary',
            reference: 'goodType',
            name: 'good_type',
            dictionaryName: 'GoodType',
            valueField: 'id',
            listeners: {
                change: 'onChange',
                beforerender: 'onBeforeRenderer'
            }
        }
    ]
});