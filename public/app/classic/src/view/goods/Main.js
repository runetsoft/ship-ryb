/**
 * Created by babkin on 30.03.2017.
 */
Ext.define('Pricer.view.goods.Main', {
    extend: 'Ext.Panel',
    xtype: 'goods',
    reference: 'goods',
    viewModel: 'goods.main',
    requires: [
        'Pricer.view.goods.MainController',
        'Pricer.view.goods.grid.GoodsGrid',
        'Pricer.view.goods.windows.GoodsWindowsEdit',
        'Pricer.view.goods.window.GoodsWindowEditController',
        'Pricer.view.goods.forms.GoodsTypeEditController',
        'Pricer.view.goods.forms.GoodsFormEditController',
        'Pricer.view.main.forms.FormTextfieldNumber'
    ],

    controller: 'goods.main',
    items:[
        {
            xtype:'goodsList',
            reference:'goodsList'

        },
        {
            xtype:'panel',
            layout: {
                type: 'vbox',
                align : 'left',
                pack  : 'start'
            },
            defaults: {
                listeners: {
                    'onMRCUpdated': {
                        fn: function () {
                            this.up('goods').getReferences().goodsList.getStore().load();
                        }
                    }
                }
            },
            items:[
                {
                    xtype: 'button',
                    text: "Очистить все МРЦ",
                    style: {
                        marginBottom: '10px'
                    },
                    handler: 'clearMRC'
                },
                {
                    xtype: 'button',
                    text: "Обновить МРЦ из файла",
                    style: {
                        marginBottom: '10px'
                    },
                    handler: 'updateMRC'
                },
                {
                    xtype: 'button',
                    text: "Добавить товар",
                    iconCls: 'x-fa fa-plus',
                    style: {
                        marginBottom: '10px'
                    },
                    handler: 'onAddGood'
                }
            ]
        }
    ]

});
