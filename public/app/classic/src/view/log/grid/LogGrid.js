Ext.define('Pricer.view.log.grid.LogGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'logList',
    module: 'log',
    controller: 'log-grid-loggrid',
    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Pricer.store.log.LogStore',
        'Pricer.view.log.grid.LogGridController'
    ],
    logId: null,
    initComponent: function () {
        // create the Data Store
        var store = Ext.create('Pricer.store.log.LogStore', {remoteSort: true});
        Ext.apply(this, {
            store: store,
            minHeight: 600,
            plugins: [{
                ptype: 'cellediting',
                clicksToEdit: 1
            }],
            viewConfig: {
                enableTextSelection: true
            },
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width:'5%',
                },
                {
                    text: 'Поставщик',
                    dataIndex: 'supplierName',
                    align: 'left',
                    width:'30%',
                    filterField: {
                        xtype: 'combobox',
                        minChars: 3,
                        store: Ext.create('Pricer.store.suppliers.SuppliersStore',
                            {
                                pageSize: 150
                            }),
                        valueField: 'id',
                        displayField: 'provider'
                    }
                },
                {
                    text: 'Дата',
                    dataIndex: 'date_import',
                    align: 'center',
                    width:'10%',
                    period: true,
                    filterField: {
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }
                },
                {
                    text: 'Файл',
                    dataIndex: 'fileMask',
                    align: 'center',
                    width:'25%'
                },
                {
                    text: 'Импорт инфо',
                    dataIndex: 'importInfo',
                    align: 'center',
                    width:'25%'
                },
                {
                    text: 'Изменения в структуре',
                    dataIndex: 'warning',
                    align: 'center',
                    width:'10%',
                    renderer: function (data) {
                        return data == 1 ? '<span style="color:red">С изменениями</span>' :
                            '<span style="color:green">Без изменений</span>'
                    }
                },
                {
                    text: 'Успешно?',
                    dataIndex: 'success',
                    width:'10%',
                    align: 'center',
                    renderer: function (data) {
                        return data == 1 ? '<span style="color:green">Успешно</span>' :
                            '<b style="color:red">С ошибками</b>'
                    }
                },
                {
                    text: 'Действия ',
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    align: 'center',
                    width: '5%',
                    editor: false,
                    items: [
                        {
                            iconCls: 'x-fa fa-eye',
                            tooltip: 'Подробно',
                            handler: 'onDetail'
                        }
                    ]
                }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                cls: 'grid-tool-bar',
                items: [
                    {
                        xtype: 'gridfilter',
                        grid: this
                    }
                ]
            }],
            // paging bar on the bottom
            bbar: [
                {
                    xtype: 'pricertoolbar',
                    store: store,
                    autoScroll: true,
                    items: [
                        {
                            xtype: 'prepage',
                            grid: this
                        }
                    ]
                }
            ]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});