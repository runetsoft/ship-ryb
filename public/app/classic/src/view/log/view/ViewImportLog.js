
Ext.define('Pricer.view.suppliers.view.ViewImportLog',{
    extend: 'Ext.view.View',
    xtype: 'viewimportlog',
    initComponent: function () {
        var store = Ext.create('Ext.data.Store', {
            pageSize: 50,
            proxy: {
                type: 'ajax',
                url: '/log/logImport.json',
                extraParams:{
                    importLogId: this.importLogId
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    totalProperty: 'totalCount'
                }
            },
            autoLoad: true
        });

        var template = new Ext.XTemplate([
            '<tpl for=".">',
            '<div style="margin-bottom: 10px; border:1px solid #6F6F6F; padding: 10px" class="thumb-wrap">',
            '<h3>{filename_mask}</h3>',
            '<strong>Дата импорта</strong>: {date_import} <br/> ',
            '<strong>Состояние импорта</strong>: {success} <br/> ',
            '<strong>Изменения в файле</strong>: {warning} <br/> ',
            '<strong>Детали</strong>: <br /> ',
            '<tpl for="detail">{.}<br/></tpl>',
            '</div>',
            '</tpl>'
        ]);

        Ext.apply(this, {
            store: store,
            tpl: template,
            itemSelector: 'div.thumb-wrap',
            emptyText: 'Нет данных'
        });
        this.callParent();
    }
});
