Ext.define('Pricer.view.log.Main', {
    extend: 'Ext.Panel',
    xtype: 'log',
    viewModel: 'log.main',
    requires: [
        'Pricer.view.log.grid.LogGrid'
    ],

    controller: 'log.main',

    items: [
        {
            xtype: 'logList',
            reference: 'logList'
        }
    ]
});
