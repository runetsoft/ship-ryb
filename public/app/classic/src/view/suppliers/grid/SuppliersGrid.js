/**
 * Created by swds.dll on 24.04.2017.
 */
Ext.define('Pricer.view.suppliers.grid.SuppliersGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'suppliersList',
    module: 'suppliers',
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.suppliers.SuppliersStore',
        'Pricer.view.suppliers.view.SuppliersImportLogButtons',
        'Pricer.view.suppliers.grid.SuppliersGridController'
    ],
    controller: 'suppliers-grid-suppliersgrid',

    initComponent: function () {

        // create the Data Store
        var store = Ext.create('Pricer.store.suppliers.SuppliersStore',{autoload:false, remoteSort: true});

        Ext.apply(this, {
            store: store,
            minHeight: 600,
            viewConfig: {
                enableTextSelection: true,
                listeners: {
                    cellclick: 'onCellClick'
                }
            },
            defaults: {
                sortable: true
            },
            // grid columns
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width: 50,
                    filterField: 'textfield',
                    renderer: function (value, metaData) {
                        metaData.tdStyle = 'text-decoration:underline; text-decoration-style:dashed; cursor: pointer;';
                        return value;
                    }
                },
                {
                    text: 'Наименование',
                    dataIndex: 'provider',
                    align: 'left',
                    width: 550,
                    filterField: 'textfield'
                },
                {
                    text: 'Номенклатура',
                    tooltip: 'Номенклатура',
                    dataIndex: 'price_cnt',
                    align: 'center',
                    width: 80
                },
                {
                    text: 'Номенклатура с ценой и складом',
                    tooltip: 'Номенклатура с ценой и складом',
                    dataIndex: 'price_full_cnt',
                    align: 'center',
                    width: 80
                },
                {
                    text: 'Дата последней загрузки',
                    tooltip: 'Дата последней загрузки',
                    period: true,
                    dataIndex: 'date_import',
                    align: 'center',
                    width: 100,
                    filterField: {
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }
                },
                {
                    text: 'Статус',
                    dataIndex: 'status',
                    align: 'center',
                    renderer: 'renderStatus',
                    width: 100
                },
                {
                    text: 'Дeйствия',
                    xtype: 'actioncolumn',
                    dataIndex: 'actions',
                    width: 100,
                    items: [
                        {
                            iconCls: 'x-fa fa-edit',
                            tooltip: 'Редактировать',
                            handler: 'onItemEdit'
                        }, {
                            iconCls: 'x-fa fa-trash',
                            tooltip: 'Удалить',
                            handler: 'onItemDelete'
                        }, {
                            iconCls: 'x-fa fa-eye',
                            hidden: true,
                            tooltip: 'Информация об импорте ',
                            handler: 'onLastImportInfoById'
                        }
                    ]
                }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                cls: 'grid-tool-bar',
                items: [
                    {
                        xtype: 'gridfilter',
                        grid: this
                    }
                ]
            }],
            // paging bar on the bottom
            bbar: [{
                xtype: 'pricertoolbar',
                store: store,
                autoScroll: true,
                items: [{
                    xtype: 'prepage',
                    grid: this
                }]
            }]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});