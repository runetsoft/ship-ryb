Ext.define('Pricer.view.suppliers.grid.SupplierImportFileStructureGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'supplierimportfilestructureList',
    reference: 'supplierimportfilestructureList',
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.suppliers.SupplierImportFileStructureStore',
        'Pricer.view.dictionary.forms.dictionaryCombo'
    ],
    forceFit: true,
    importFileStructureId: null,

    supplierId: null,
    worksheets: null,
    headersRow: null,

    initComponent: function () {

        // create the Data Store
        var store = Ext.create('Pricer.store.suppliers.SupplierImportFileStructureStore');

        Ext.apply(this, {
            store: store,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit : 1
                })
            ],
            height: 200,
            columns: {
                items: [
                    {
                        text: 'Заголовок файла',
                        dataIndex: 'headerFile'
                    },
                    {
                        text: 'Поле таблицы',
                        dataIndex: 'columnTable',
                        editor: {
                            dictionaryName: 'PriceTable',
                            valueField: 'code',
                            xtype: 'dictionary'
                        }
                    }
                ],
                defaults: {
                    width: '50%',
                    textAlign: 'left',
                    editor: {
                        xtype: 'textfield',
                        allowBlank: false
                    }
                }
            }
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().load({
            params: {
                importFileStructureId: this.importFileStructureId,

                supplierId: this.supplierId,
                worksheets: this.worksheets,
                headersRow: this.headersRow
            }
        });
    }
});