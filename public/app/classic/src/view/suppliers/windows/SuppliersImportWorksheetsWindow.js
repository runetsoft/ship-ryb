Ext.define('Pricer.view.suppliers.window.SuppliersImportWorksheetsWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Pricer.view.suppliers.window.SuppliersImportWorksheetsWindowController',
        'Pricer.view.suppliers.window.SuppliersImportWorksheetsWindowModel'
    ],

    controller: 'suppliers-window-suppliersimportworksheetswindow',
    viewModel: {
        type: 'suppliers-window-suppliersimportworksheetswindow'
    },
    layout: 'center',
    listeners: {
        // resize: function (win, width, height, oldWidth, oldHeight, eOpts) {
        //     var xPos = win.getPosition()[0],
        //         v,
        //         yPos = ((v = (Ext.getBody().getViewSize().height - height) / 2) > 0) ? v : 0;
        //     win.setPosition(xPos, yPos, true);
        // }
    },
    // afterRender: function () {
    //     this.callParent(arguments);
    //     this.toggleMaximize();
    // },
    items: []
});
