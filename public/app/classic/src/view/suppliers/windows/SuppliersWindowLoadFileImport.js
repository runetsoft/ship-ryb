
/**
 * Created by swds.dll on 24.04.2017.
 */

Ext.define('Pricer.view.suppliers.windows.SuppliersWindowsLoadFileImport', {
    extend: 'Ext.window.Window',
    title: 'Добавление файла импорта',
    controller: 'suppliers.window.edit',
    referenceHolder: true,
    width: 355,
    modal: true,
    items:[]
});