
/**
 * Created by swds.dll on 24.04.2017.
 */

Ext.define('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
    extend: 'Ext.window.Window',
    title: 'Редактирование поставщика',
    controller: 'suppliers.window.edit',
    referenceHolder: true,
    width: 800,
    maxHeight: 800,
    autoScroll: true,
    modal: true,
    items:[],
    listeners: {
        resize: function (win, width, height, oldWidth, oldHeight, eOpts) {
            var xPos = win.getPosition()[0],
                v,
                yPos = ((v = (Ext.getBody().getViewSize().height - height) / 2) > 0) ? v : 0;
            win.setPosition(xPos, yPos, true);
        }
    }
});