/**
 * Created by swds.dll on 24.04.2017.
 */
Ext.define('Pricer.view.suppliers.forms.SuppliersFormEdit', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.store.groups.GroupsStore',
        'Pricer.store.cities.CitiesStore',
        'Pricer.view.suppliers.grid.SuppliersGrid',
        'Pricer.view.suppliers.forms.SuppliersFormEditController',
        'Pricer.view.suppliers.forms.SuppliersImportFileWorksheetsItem',
        'Pricer.view.extention.Ajax'
    ],
    xtype: 'suppliersform',
    controller: 'suppliers.form.edit',
    width: '100%',
    reference: 'suppliersform',
    bodyPadding: 10,
    autoScroll: true,
    url: '/supplier/supplierInfo.json',
    supplierId: null,

    defaults: {
        width: '100%'
    },
    fieldDefaults: {
        labelAlign: 'right',
        labelWidth: 100,
        msgTarget: 'side'
    },
    items: [
        {
            xtype: 'fieldset',
            title: 'Данные поставщика',
            defaultType: 'textfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'city_id',
                    fieldLabel: 'Город',
                    store: {
                        type: 'citiesStore'
                    },
                    allowBlank: false,
                    triggerAction: 'all',
                    editable: true,
                    mode: 'local',
                    valueField: 'id',
                    displayField: 'city',
                    forceSelection: true,
                    lazyRender: true
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Группа',
                    name: 'provider_group_id',
                    store: {
                        type: 'groupsStore'
                    },
                    allowBlank: false,
                    triggerAction: 'all',
                    editable: true,
                    mode: 'local',
                    valueField: 'id',
                    displayField: 'provider_group',
                    forceSelection: true,
                    lazyRender: true
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Наименование',
                    name: 'provider'
                },
                {
                    xtype: 'hiddenfield',
                    fieldLabel: 'Организация',
                    name: 'org_name'
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    margin: '0 0 5 0',
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Телефон',
                        name: 'phone',
                        flex: 1
                    },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Мессенджер',
                            name: 'messenger'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Электронная почта для заказов',
                    name: 'email_order',
                    labelWidth: 210,
                    labelAlign: 'left'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Способ оплаты',
                    name: 'payment_method'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Комментарий',
                    name: 'comments'
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Активный',
                    name: 'status'
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Идет импорт!',
                    name: 'run_import'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Импортировать все',
            iconCls: 'x-fa fa-bolt',
            handler: 'onSupplierImport',
            style: {
                background:'#9f86ff'
            }
        },
        '->',
        {
            text: 'Сохранить',
                handler: 'onSaveData'
        },
        {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }
    ],

    afterRender: function () {
        this.callParent();
        var supplierId = this.supplierId;
        var winCmp = this.up('window');
        Pricer.view.extention.Ajax.request({
            targetMask: winCmp,
            url: '/import/getSupplierImports.json',
            params: {
                supplierId: supplierId
            },
            success: function (response) {
                var data = Ext.decode(response.responseText).data;
                if (data.length > 0) {
                    Ext.Array.forEach(data, function (item) {
                        winCmp.add({
                            supplierPriceImportId: item,
                            xtype: 'suppliersimportfileworksheets'
                        });
                    });
                }
                winCmp.add({
                    xtype: 'button',
                    text: 'Добавить импорт',
                    iconCls: 'x-fa fa-plus',
                    width: 200,
                    style: {
                        margin: '10px'
                    },
                    handler: 'createLoadFileImportWindow'
                });
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    listeners: {
        beforerender: 'onFormRender'
    }
});
