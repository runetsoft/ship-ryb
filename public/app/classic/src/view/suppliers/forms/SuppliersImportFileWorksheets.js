/**
 * Created by k.kochinyan on 05.05.2017.
 */

Ext.define('Pricer.view.suppliers.forms.SuppliersImportFileWorksheets', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.view.suppliers.forms.SuppliersImportFileWorksheetsItem'
    ],
    xtype: 'suppliersimportfileworksheets',
    url: '/import/supplierImportInfo.json',
    reference: 'suppliersimportfileworksheets',
    width: '100%',
    supplierPriceImportId: null,
    items: [
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            combineErrors: true,
            defaults: {
                labelAlign: 'top',
                readOnly: false,
                style: {
                    margin: '5px'
                }
            },
            items: [
                {
                    xtype: 'combo',
                    name: 'import_type',
                    fieldLabel: 'Тип импорта.',
                    readOnly:true,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['id', 'name'],
                        data: [
                            {id: 1, name: 'url'},
                            {id: 2, name: 'Почта'}
                        ]
                    }),
                    allowBlank: false,
                    triggerAction: 'all',
                    editable: true,
                    mode: 'local',
                    valueField: 'id',
                    displayField: 'name',
                    forceSelection: true,
                    lazyRender: true

                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'url или email',
                    name: 'import_info'
                },
                {
                    name: 'is_cron',
                    boxLabel: 'По расписанию',
                    xtype: 'checkbox',
                    labelAlign: 'right',
                    style: {
                        margin: '34px 5px 5px 5px'
                    }
                },
                {
                    xtype: 'dictionary',
                    fieldLabel: 'Периодичность импорта',
                    name: 'cron_periodicity',
                    dictionaryName: 'CronPeriod',
                    valueField: 'code'
                },
                {
                    xtype: 'button',
                    text: 'Сохранить',
                    handler: 'onCronSave',
                    style: {
                        margin: '35px 0 0 0 '
                    }
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            combineErrors: true,
            defaults: {
                hideLabel: 'true'
            },
            items: [{
                xtype: 'button',
                scale: 'small',
                iconCls: 'x-fa fa-times',
                text: 'Удалить импорт',
                handler: 'onRemoveImportFile',
                style: {
                    margin: '5px',
                    background: 'red'
                }
            },
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-arrow-circle-o-down',
                    text: 'Загрузить файлы',
                    handler: 'onUploadImportFiles',
                    style: {
                        margin: '5px',
                        background:'#1bc98e'
                    }
                },
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-eye',
                    text: 'Подробно',
                    right:0,
                    handler: 'onShowImportFiles',
                    style: {
                        margin: '5px'
                    }
                },
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-undo',
                    text: 'Сбросить дату импорта',
                    handler: 'onResetImportDate',
                    style: {
                        margin: '5px',
                        background:'#e5445a'
                    }
                }
            ]
        }
    ],
    listeners: {
        beforerender: 'onFormRender'
    }
});