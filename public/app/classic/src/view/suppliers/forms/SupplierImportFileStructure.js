/**
 * Created by swds.dll on 24.04.2017.
 */
Ext.define('Pricer.view.suppliers.grid.SupplierImportFile', {
    extend: 'Ext.grid.Panel',
    xtype: 'supplierImportFile',
    requires: [
        'Ext.grid.Panel',
        'Pricer.store.suppliers.SuppliersStore',
        'Pricer.view.suppliers.grid.SuppliersGridController'
    ],
    controller: 'suppliers-grid-suppliersgrid',
    title: 'Список поставщиков',

    initComponent: function () {

        // create the Data Store
        var store = Ext.create('Pricer.store.suppliers.SuppliersStore');

        Ext.apply(this, {
            store: store,
            viewConfig: {
                trackOver: false,
                stripeRows: false
            },

            // grid columns
            columns: [
                {
                    text: '#',
                    dataIndex: 'id',
                    align: 'center',
                    width: 50
                },
                {
                    text: 'Город',
                    dataIndex: 'city',
                    align: 'left',
                    width: 150
                },
                {
                    text: 'Группа',
                    dataIndex: 'provider_group',
                    align: 'center',
                    width: 100
                },
                {
                    text: 'Провайдер',
                    dataIndex: 'provider',
                    align: 'left',
                    width: 550
                },
                {
                    text: 'Организация',
                    dataIndex: 'org_name',
                    align: 'left',
                    width: 350
                },
                {
                    text: 'Номенклатура',
                    tooltip: 'Номенклатура',
                    dataIndex: 'price_cnt',
                    align: 'center',
                    width: 80
                },
                {
                    text: 'Номенклатура с ценой и складом',
                    tooltip: 'Номенклатура с ценой и складом',
                    dataIndex: 'price_full_cnt',
                    align: 'center',
                    width: 80
                },
                {
                    text: 'Дата последней загрузки прайса',
                    tooltip: 'Дата последней загрузки прайса',
                    dataIndex: 'dt_price_load',
                    align: 'center',
                    width: 100
                },
                {
                    text: 'Статус',
                    dataIndex: 'status',
                    align: 'center',
                    renderer: 'renderStatus',
                    width: 100
                },
                {
                    text: 'Дeйствия',
                    xtype:'actioncolumn',
                    dataIndex: 'actions',
                    width: 100,
                    items: [{
                        iconCls: 'x-fa fa-edit',
                        tooltip: 'Редактировать',
                        handler: 'onItemEdit'
                    },{
                        iconCls: 'x-fa fa-trash',
                        tooltip: 'Удалить',
                        handler: 'onItemDelete'
                    }]
                }
            ],
            // paging bar on the bottom
            bbar: [{
                xtype:'pricertoolbar',
                store: store
            }]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    }
});