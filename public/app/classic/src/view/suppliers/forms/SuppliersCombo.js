/**
 * Created by k.kochinyan on 05.06.2017.
 */
Ext.define('Pricer.view.suppliers.forms.SuppliersCombo', {
    extend: 'Ext.form.field.Tag',
    requires: [
        'Ext.form.field.Tag'
    ],
    xtype: 'suppliersCombo',
    store: null,
    initComponent: function () {
        this.store = Ext.create('Pricer.store.suppliers.SuppliersStore', {});
        this.callParent();
    }
});
