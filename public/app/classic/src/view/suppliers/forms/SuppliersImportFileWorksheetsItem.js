/**
 * Created by k.kochinyan on 05.05.2017.
 */

Ext.define('Pricer.view.suppliers.forms.SuppliersImportFileWorksheetsItem', {
    extend: 'Ext.form.Panel',
    xtype: 'suppliersimportfileworksheetsitem',
    reference: 'suppliersimportfileworksheetsitem',
    controller: 'suppliers.form.importfileworksheets',
    title: null,
    trackResetOnLoad: true,
    importFileStructureId: null,
    fieldsSaved: false,
    viewModel:{
        formulas: {
            headersRowTitle: {
                bind: {
                    noHeaders: '{!noHeaders.value}'
                },
                get: function (data) {
                    return data.noHeaders ? 'Строка заголовков' : 'Строка данных';
                }
            },
            readHeadersText: {
                bind: {
                    noHeaders: '{!noHeaders.value}'
                },
                get: function (data) {
                    return data.noHeaders ? 'Прочитать заголовки' : 'Прочитать данные';
                }
            }
        }
    },
    initComponent: function () {
        Ext.apply(this, {
            referenceHolder: true,
            items: [
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            layout: {
                                type: 'hbox'
                            },
                            anchor: '100%',
                            defaults: {
                                margin: '0 10 10 10'
                            },
                            items: [
                                {
                                    name: 'noHeaders',
                                    reference: 'noHeaders',
                                    boxLabel: 'Заголовков нет',
                                    xtype: 'checkbox',
                                    labelAlign: 'right',
                                    margin: '10',
                                    publishes: 'value'
                                },
                                {
                                    name: 'noUpload',
                                    reference: 'noUpload',
                                    boxLabel: 'Не выгружать',
                                    xtype: 'checkbox',
                                    labelAlign: 'right',
                                    margin: '10',
                                    listeners:{
                                        change: 'saveNoUpload'
                                        // focus: function (obj) {
                                        //     obj.focused = true;
                                        // }
                                    }
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox'
                            },
                            anchor: '100%',
                            defaults: {
                                margin: '0 10 10 10'
                            },
                            items: [
                                {
                                    name: 'worksheet',

                                    xtype: 'hiddenfield',
                                    value: this.title
                                },
                                {
                                    name: 'headersRow',
                                    xtype: 'numberfield',
                                    labelWidth: 180,
                                    width: '65%',
                                    bind: {
                                        fieldLabel: '{headersRowTitle}'
                                    }
                                },
                                {
                                    name: 'readHeaders',
                                    xtype: 'button',
                                    width: '35%',
                                    handler: 'saveWorksheetHeaders',
                                    bind: {
                                        text: '{readHeadersText}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    importFileStructureId: this.importFileStructureId,
                    xtype: 'supplierimportfilestructureList'
                }
            ],
            buttons: [
                {
                    text: 'Сохранить данные импорта',
                    xtype: 'button',
                    handler: 'onUpdateHeadersData'
                }
            ]
        });
        this.items.unshift();
        this.callParent();
    },
    listeners : {
        afterrender: function () {
            this.getForm().setValues(this.allData);
        }
    }
});