/**
 * Created by swds.dll on 24.04.2017.
 */
Ext.define('Pricer.view.suppliers.forms.SuppliersFormLoadFileImport', {
    extend: 'Ext.form.Panel',
    requires: [
        'Pricer.store.groups.GroupsStore',
        'Pricer.store.cities.CitiesStore',
        'Pricer.view.suppliers.grid.SuppliersGrid'
    ],
    xtype: 'suppliersformloadfileimport',
    controller: 'suppliers.form.edit',
    reference: 'suppliersformloadfileimport',
    bodyPadding: 10,
    autoScroll:true,
    url: '/import/loadFilesImport.json',
    supplierId: null,
    supplierPriceImportId: null,
    items:[
        {
            xtype : 'combo',
            name: 'importType',
            fieldLabel: 'Тип импорта.',
            store: Ext.create('Ext.data.Store', {
                fields: ['id', 'name'],
                data : [
                    {id: 1, name: 'url'},
                    {id: 2, name: 'Почта'}
                ]
            }),
            allowBlank: false,
            triggerAction: 'all',
            editable: true,
            mode : 'local',
            valueField : 'id',
            displayField: 'name',
            forceSelection: true,
            lazyRender: true

        },
        {
            xtype:'textfield',
            fieldLabel: 'url или email',
            name: 'importInfo'
        }

    ],
    buttons: [
        {
            xtype:'button',
            text: 'Сохранить',
            handler: 'onLoadFileImport'
        },
        {
            text: 'Отмена',
            handler: function() {
                this.up('window').destroy();
            }
        }
    ]
});
