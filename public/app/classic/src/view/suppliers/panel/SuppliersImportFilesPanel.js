Ext.define('Pricer.view.suppliers.panel.SuppliersImportFilesPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'suppliersimportfilespanel',
    requires: [
        'Pricer.view.suppliers.panel.SuppliersImportFilesPanelController',
        'Pricer.view.suppliers.panel.SuppliersImportFilesPanelModel'
    ],

    controller: 'suppliers-panel-suppliersimportfilespanel',
    viewModel: {
        type: 'suppliers-panel-suppliersimportfilespanel'
    },
    width: '800',
    afterRender: function () {
        this.callParent();
        var supplierPriceImportId = this.supplierPriceImportId;
        var worksheetsCmp = this;
        Ext.Ajax.request({
            url: '/import/getSupplierPriceImportFiles.json',
            params: {
                supplierPriceImportId: supplierPriceImportId
            },
            success: function (response) {
                var data = Ext.decode(response.responseText).data;
                Ext.Array.forEach(data, function (item) {
                    worksheetsCmp.add(
                        {
                            title: 'Файл :' + item.filename_mask,
                            xtype: 'form',
                            margin: '0 0 20px 0',
                            supplierPriceImportFileId: item.id,
                            closable: true,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    combineErrors: true,
                                    defaults: {
                                        margin: '10px 10px 0 0'
                                    },
                                    items: [
                                        {
                                            name: 'noUploadFile',
                                            reference: 'noUploadFile',
                                            boxLabel: 'Не выгружать',
                                            xtype: 'checkbox',
                                            publishes: 'value',
                                            value: item.no_upload
                                        },
                                        {
                                            xtype: 'dictionary',
                                            fieldLabel: 'Разделитель',
                                            labelAlign: 'right',
                                            name: 'delimiter',
                                            dictionaryName: 'Delimiter',
                                            valueField: 'code',
                                            value: item.delimiter
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    combineErrors: true,
                                    defaults: {
                                        labelAlign: 'top',
                                        margin: '0 10px 0 0'
                                    },
                                    margin: '0 0 10px 0',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Маска имени файла',
                                            name: 'filename_mask',
                                            value: item.filename_mask
                                        },
                                        {
                                            xtype: 'dictionary',
                                            fieldLabel: 'Кодировка',
                                            name: 'encoding',
                                            dictionaryName: 'Encoding',
                                            valueField: 'code',
                                            value: item.encoding
                                        },
                                        {
                                            xtype: 'button',
                                            name: 'save',
                                            text: 'Сохранить',
                                            handler: 'saveSupplierFileImportInfo',
                                            margin: '30px 0 0 0'
                                        },
                                        {
                                            xtype: 'button',
                                            name: 'save',
                                            style: {
                                                background: 'red'
                                            },
                                            text: 'Выгрузить данные из текущего файла',
                                            handler: 'importOneFile',
                                            margin: '30px 0 0 0'
                                        }

                                    ]
                                },
                                {
                                    xtype: 'suppliersimportworksheetspanel',
                                    supplierPriceImportFileId: item.id
                                }
                            ],
                            listeners: {
                                beforeclose: 'onRemoveImportOneFile'
                            }
                        }
                    );
                });
                worksheetsCmp.setActiveTab(0);
                worksheetsCmp.up('window').toggleMaximize();
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }
});
