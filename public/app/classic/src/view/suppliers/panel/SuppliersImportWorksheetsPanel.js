Ext.define('Pricer.view.suppliers.panel.SuppliersImportWorksheetsPanel', {
    extend: 'Ext.Panel',
    xtype: 'suppliersimportworksheetspanel',
    requires: [
        'Pricer.view.suppliers.panel.SuppliersImportWorksheetsPanelController',
        'Pricer.view.suppliers.panel.SuppliersImportWorksheetsPanelModel'
    ],

    controller: 'suppliers-panel-suppliersimportworksheetspanel',
    viewModel: {
        type: 'suppliers-panel-suppliersimportworksheetspanel'
    },
    width:800,
    afterRender: function () {
        this.callParent();
        var supplierPriceImportFileId = this.supplierPriceImportFileId,
            worksheetsCmp = this;

        Ext.Ajax.request({
            url: '/import/getFileImportStructureBySupplierImportId.json',
            params: {
                supplierPriceImportFileId: supplierPriceImportFileId
            },
            success: function (response) {
                var data = Ext.decode(response.responseText).data;
                var tabs = [];
                if (data.length > 0) {
                    Ext.Array.forEach(data, function (item) {
                        tabs.push({
                            title: item.worksheet,
                            allData: item,
                            fieldsSaved: item.fieldsSaved,
                            importFileStructureId: item.importFileStructureId,
                            xtype: 'suppliersimportfileworksheetsitem'
                        });
                    });
                    worksheetsCmp.add(
                        {
                            xtype: 'tabpanel',
                            items: tabs
                        }
                    );
                }
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
});
