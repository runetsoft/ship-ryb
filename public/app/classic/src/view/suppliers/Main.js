/**
 * Created by swds.dll on 24.04.2017.
 */
Ext.define('Pricer.view.suppliers.Main', {
    extend: 'Ext.Panel',
    xtype: 'suppliers',
    viewModel: 'suppliers.main',
    requires: [
        'Pricer.view.suppliers.MainController',
        'Pricer.view.suppliers.grid.SuppliersGrid'
    ],

    controller: 'suppliers.main',
    items:[
        {
            xtype:'suppliersList'
        },
        {
            xtype:'panel',
            layout: {
                type: 'vbox',
                align : 'right',
                pack  : 'start'
            },
            items:[
                {
                    xtype: 'button',
                    text: "Добавить поставщика",
                    iconCls: 'x-fa fa-plus',
                    style: {
                        marginBottom: '10px'
                    },
                    handler: 'addSupplier'
                }
            ]
        }
    ]
});
