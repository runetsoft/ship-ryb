Ext.define('Pricer.view.dictionary.grid.DictionaryCommonContainer', {
    extend: 'Ext.container.Container',
    xtype: 'dictionaryContainer',
    reference: 'dictionaryContainer',
    requires: [
        'Pricer.view.dictionary.grid.DictionaryGrid',
        'Pricer.view.dictionary.grid.DictionaryValuesGrid'
    ],
    urlData: {},
    controller: 'dictionary.main',
    dictionaryBind: null,

    getDictionaryBind: function (tableName) {
        var me = this;
        if (tableName) {
            Ext.Ajax.request({
                url: '/dictionary/getDictionaryBind.json',
                method: 'post',
                params: {
                    table: JSON.stringify(tableName)
                },
                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    var dictionaryName = obj.data.parent_table_name;
                    if (dictionaryName) {
                        dictionaryName = dictionaryName.match(/(cms_dictionary_)(.*)/)[2];
                        dictionaryName = Pricer.view.extention.Render.renderDictionary(dictionaryName);
                    }
                    else {
                        dictionaryName = null;
                    }
                    me.fireEvent('setDictionaryBind', dictionaryName);
                },
                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    },

    updateItems: function (urlData) {
        if (urlData.params['table_name']) {
            var res = Pricer.view.extention.Render.renderDictionary(urlData.params['table_name']);
            this.getDictionaryBind(urlData.params['table_name']);
        }


        if (urlData.action === "edit_table") {
            this.on('setDictionaryBind', function (data) {
                this.add({
                        xtype: 'dictionaryValueList',
                        reference: 'dictionaryValueList',
                        dictionary: res,
                        dictionaryTable: urlData.params['table_name'],
                        dictionaryBind: data
                    },
                    {
                        xtype: 'button',
                        text: "Список справочников",
                        iconCls: 'fa fa-arrow-left',
                        style: {
                            marginBottom: '10px',
                            marginLeft: '10px'
                        },
                        handler: 'back'
                    });
            });
        }
        else {
            this.add({
                xtype: 'dictionaryList',
                reference: 'dictionaryList',
                dictionary: res,
                dictionaryTable: urlData.params['table_name'],
                dictionaryBind: null
            });
        }

    },

    listeners: {
        beforerender: function () {
            this.updateItems(this.urlData);
        },
        setDictionaryBind: function (tableName) {
            this.dictionaryBind = tableName;
        }
    }
});
