Ext.define('Pricer.view.dictionary.grid.DictionaryValuesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'dictionaryValueList',
    module: 'dictionary',
    dictionary: null,
    dictionaryBind: null,
    dictionaryTable: null,
    showAddNewBtn: true,
    controller: 'dictionary-grid-dictionaryvaluesgrid',
    viewModel: {
        type: 'dictionary-grid-dictionarvaluesygrid'
    },
    requires: [
        'Ext.grid.plugin.CellEditing',
        'Pricer.store.dictionary.DictionaryStore',
        'Pricer.view.extention.QueryDispatcher',
        'Pricer.view.dictionary.forms.dictionaryBinderCombo'
    ],
    initComponent: function () {

        var store = Ext.create('Pricer.store.dictionary.DictionaryValuesGridStore', {
            proxy: {
                type: 'ajax',
                url: '/dictionary/getDictionaryList.json',
                method: "POST",
                extraParams: {
                    dictionary: this.dictionary
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    totalProperty: 'totalCount'
                }
            }
        });

        Ext.apply(this, {
            store: store,
            minHeight: 600,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1,
                    listeners: {
                        beforeedit: 'beforeEdit',
                        afteredit: 'afterEdit'
                    }
                })
            ],
            viewConfig: {
                enableTextSelection: true
            },
            columns:
                {
                    items: [
                        {
                            text: '#',
                            dataIndex: 'id',

                            align: 'center',
                            width: '5%',
                            isEditable: false,
                            filterField: 'textfield'
                        },
                        {
                            text: 'Наименование',
                            dataIndex: 'name',
                            isEditable: true,
                            align: 'center',
                            width: '25%',
                            editor: {
                                width: 350,
                                textAlign: 'left',
                                xtype: 'textfield',
                                allowBlank: true
                            }
                        },
                        {
                            text: 'Код',
                            dataIndex: 'code',
                            isEditable: true,
                            align: 'center',
                            width: '25%',
                            editor: {
                                width: 350,
                                textAlign: 'left',
                                xtype: 'textfield',
                                allowBlank: true
                            }
                        },
                        {
                            text: 'Привязка',
                            dataIndex: 'parent_id',
                            renderer: Pricer.view.extention.Render.renderBind,
                            reference: 'bind',
                            align: 'center',
                            width: '25%',
                            isEditable: true,
                            editor: {
                                xtype: 'dictionary',
                                reference: 'parent_id',
                                name: 'parent_id',
                                width: '100%',
                                align: 'right',
                                dictionaryName: this.dictionaryBind,
                                valueField: 'id',
                                displayField: 'name',
                                typeAhead: false,
                                editorMode: true,
                                forceSelection: false,
                                listeners: {
                                    addValueToDictionary: function (component, savingValue) {
                                        component.saveData = {};
                                        component.saveData.name = savingValue;
                                    },
                                    afterrender: function () {
                                        if (!this.dictionaryName)
                                        {
                                            this.disable();
                                        }
                                    }
                                }
                            }
                        },
                        {
                            text: 'Дeйствия',
                            xtype: 'actioncolumn',
                            dataIndex: 'actions',
                            width: '10%',
                            ref: 'actions',

                            items: [
                                {
                                    tooltip: 'Сохранить',
                                    handler: 'onItemSave',
                                    getClass: function (v, m, r) {
                                        if (store.lastInsertedRecord !== null){
                                            return 'x-fa fa-floppy-o';
                                        }
                                        else{
                                            return 'x-hidden';
                                        }
                                    }
                                },
                                {
                                    tooltip: 'Редактировать',
                                    handler: 'onItemEdit',
                                    getClass: function (v, m, r) {
                                            return 'x-fa fa-edit';
                                    }
                                },
                                {
                                    tooltip: 'Удалить',
                                    handler: 'onItemDelete',
                                    getClass: function (v, m, r) {
                                        return 'x-fa fa-trash';
                                    }
                                }
                            ]
                        }
                    ]
                },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                cls: 'grid-tool-bar',
                layout: 'anchor',
                items: [
                    // {
                    //     xtype: 'gridfilter',
                    //     grid: this
                    // }//,
                    {
                        xtype: 'dictionaryBinderCombo',
                        table: this.dictionaryTable
                    }
                ]
            }],
            bbar: [{
                xtype: 'button',
                text: 'Добавить значение',
                handler: function () {
                    store.add({id: '', name: '', code: '', parent_id: ''});
                    store.lastInsertedRecord[0].set('isNewRow',true);
                },
                hidden: !this.showAddNewBtn
            },
            {
                xtype: 'pricertoolbar',
                store: store,
                autoScroll: true,
                items: [{
                    xtype: 'prepage',
                    grid: this
                }]
            }]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});