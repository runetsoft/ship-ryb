Ext.define('Pricer.view.dictionary.grid.DictionaryGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'dictionaryList',
    module: 'dictionary',
    controller: 'dictionary-grid-dictionarygrid',
    // urlParams: null,
    viewModel: {
        type: 'dictionary-grid-dictionarygrid'
    },
    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Pricer.store.dictionary.DictionaryGridStore',
        'Pricer.view.dictionary.grid.DictionaryGridController',
        'Pricer.view.dictionary.grid.DictionaryGridModel'
    ],
    initComponent: function () {
        // create the Data Store
        var store = Ext.create('Pricer.store.dictionary.DictionaryGridStore', {remoteSort: true});
        Ext.apply(this, {
            store: store,
            minHeight: 600,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1,
                    listeners: {
                        beforeedit: 'beforeEdit',
                        afteredit: 'afterEdit'
                    }
                })
            ],
            viewConfig: {
                enableTextSelection: true
            },
            columns:
                {
                    items: [
                        {
                            text: 'Наименование',
                            dataIndex: 'TABLE_NAME',
                            align: 'center',
                            width: '25%',
                            isEditable: false,
                            filterField: {
                                xtype: 'combobox',
                                minChars: 3,
                                store: Ext.create('Pricer.store.dictionary.DictionaryGridStore', {
                                    pageSize: 150,
                                    proxy: {
                                        type: 'ajax'
                                    },
                                    autoLoad: true
                                }),
                                valueField: 'TABLE_NAME',
                                displayField: 'TABLE_COMMENT'
                            },
                            editor: {
                                width: 350,
                                textAlign: 'left',
                                xtype: 'textfield',
                                allowBlank: false
                            }
                        },
                        {
                            text: 'Описание',
                            dataIndex: 'TABLE_COMMENT',
                            isEditable: true,
                            align: 'center',
                            width: '25%',
                            editor: {
                                width: 350,
                                textAlign: 'left',
                                xtype: 'textfield',
                                allowBlank: true
                            }
                        },
                        {
                            text: 'Дeйствия',
                            xtype: 'actioncolumn',
                            dataIndex: 'actions',
                            width: '10%',
                            ref: 'actions',

                            items: [
                                {
                                    tooltip: 'Подробно',
                                    handler: 'onDetail',
                                    getClass: function (v, m, r) {
                                        if (store.lastInsertedRecord === null){
                                            return 'x-fa fa-eye';
                                        }
                                        else{
                                            return 'x-hidden';
                                        }
                                    }
                                },
                                {
                                    tooltip: 'Сохранить',
                                    handler: 'onSave',
                                    getClass: function (v, m, r) {
                                        if (store.lastInsertedRecord !== null){
                                            return 'x-fa fa-floppy-o';
                                        }
                                        else{
                                            return 'x-hidden';
                                        }
                                    }
                                },
                                {
                                    tooltip: 'Удалить',
                                    handler: 'onItemDelete',
                                    getClass: function (v, m, r) {
                                        if (store.lastInsertedRecord !== null){
                                            return 'x-fa fa-trash';
                                        }
                                        else{
                                            return 'x-hidden';
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                cls: 'grid-tool-bar',
                items: [
                    {
                        xtype: 'gridfilter',
                        grid: this
                    }
                ]
            }],
            bbar: [{
                xtype: 'button',
                text: 'Добавить справочник',
                handler: function () {
                    store.add({TABLE_NAME: '', TABLE_COMMENT: ''});
                    store.lastInsertedRecord[0].set('isEditable',true);
                }
            },
                {
                    xtype: 'pricertoolbar',
                    store: store,
                    autoScroll: true,
                    items: [{
                        xtype: 'prepage',
                        grid: this
                    }]
                }]
        });
        this.callParent();
    },
    afterRender: function () {
        this.callParent(arguments);
        this.getStore().loadPage(1);
    },
    listeners: {
        search: function (data) {
            this.getStore().getProxy().extraParams = [];
            for (var field in data) {
                if (data[field] != '') {
                    this.getStore().getProxy().extraParams[field] = data[field];
                }
            }
            this.getStore().reload();
        },
        reset: function () {
            this.getStore().getProxy().extraParams = [];
            this.getStore().reload();
        }
    }
});