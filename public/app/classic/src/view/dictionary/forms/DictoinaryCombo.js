/**
 * Created by yu.khristodorov on 17.05.2017.
 */

Ext.define('Pricer.view.dictionary.forms.dictionaryCombo', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'dictionary',
    dictionaryName: null,
    valueField: 'id',
    allowBlank: true,
    triggerAction: 'all',
    editable: true,
    mode: 'local',
    displayField: 'name',
    forceSelection: true,
    typeAhead: true,
    lazyRender: true,
    savedText: null,
    minChars : 2,
    saveData: {}, //запишите сюда все те параметры которые хотите передавать во время сохранения
    filter: {
        id: null,
        code: null,
        name: null,
        parent_id: null
    },
    editorMode: false,
    initComponent: function () {
        var store = Ext.create('Pricer.store.dictionary.DictionaryStore', {
            proxy: {
                type: 'ajax',
                url: '/dictionary/getList.json',
                method: "POST",
                extraParams: {
                    dictionary: this.dictionaryName,
                    name: null,
                    filter: JSON.stringify(this.filter)
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            }
        });
        store.load();
        Ext.apply(this, {
            store: store
        });
        this.callParent();
    },
    listeners: {
        blur: function (component) {
            var savingValue = component.getRawValue();
            var comboRecordByText = component.getStore().findRecord('name', savingValue);
            if (!Ext.isDefined(component.displayTplData) && component.editorMode === true && Ext.String.trim(savingValue) !=="" && comboRecordByText === null) {
                component.addWindow(component, savingValue);
            }
            else if(comboRecordByText !== null && component.editorMode === true){
                if (savingValue.toLowerCase() === comboRecordByText.data.name.toLowerCase())
                {
                    component.setValue(comboRecordByText.id);
                }
                else {
                    component.addWindow(component, savingValue);
                }
            }
        },
        addValueToDictionary: function (component, savingValue) {
            component.saveData.name = savingValue;
        }
    },
    /**
     * рисует окно и передает параметры сохранения в необходимые для этого функции
     *
     * @param component
     * @param savingValue
     */
    addWindow: function (component, savingValue) {
        var text = component.savedText || 'новое значение';
        Ext.Msg.confirm(
            '',
            Ext.String.format('Желаете добавить {0} ({1}) в справочник?',text,savingValue),
            function (buttonId, value, opt) {
                if (buttonId === 'yes') {
                    component.fireEvent('addValueToDictionary',component, savingValue);
                    component.addValueToDictionary(component);
                }
            });
    },

    /**
     * получает данные для отправки на сервер из свойств компонента и отправляет их, свойства устанавливаются в событии addValueToDictionary
     *
     * @param component
     */
    addValueToDictionary: function (component) {
        Pricer.view.extention.Ajax.request({
            targetMask: component.up('form'),
            msgMask: 'Добавление значения ...',
            url: '/dictionary/saveNewDictionaryValue.json',
            method: 'POST',
            params: {
                dictionary: component.dictionaryName,
                data: JSON.stringify(component.saveData)
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                component.saveData = {};
                component.store.reload({
                    callback: function () {
                        component.setValue(obj.request);
                    }
                });
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    }
});