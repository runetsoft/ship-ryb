Ext.define('Pricer.view.dictionary.forms.dictionaryBinderCombo', {
    extend: 'Ext.form.Panel',
    xtype: 'dictionaryBinderCombo',
    editorMode: false,
    table: null,
    dataFields: null,
    layout: 'hbox',
    style: 'margin: 5px 0px 5px 0px',
    // url: '/dictionary/addDictionaryBind.json',
    items: [
        {
            xtype: 'hidden',
            name: 'table_name'
        },
        {
            fieldLabel: 'Привязка',
            xtype: 'combobox',
            minChars: 3,
            allowBlank: false,
            store: Ext.create('Pricer.store.dictionary.DictionaryGridStore', {
                pageSize: 150,
                proxy: {
                    type: 'ajax'
                },
                autoLoad: true
            }),
            name: 'parent_table_name',
            valueField: 'TABLE_NAME',
            displayField: 'TABLE_COMMENT'
        },

        {
            xtype: 'button',
            text: 'Сохранить привязку',
            handler: function () {
                var form = this.up('form').getForm();
                var panel = this.up('form');
                var bindEditor = panel.up('dictionaryValueList').getReferences().bind.getEditor();
                if (form.isValid()) {
                    form.submit({
                        url: '/dictionary/addDictionaryBind.json',
                        method: 'POST',
                        params: {
                            data: JSON.stringify(form.getValues())
                        },
                        waitMsg: 'Сохранение...',
                        reset: false,
                        success: function (response, opts) {
                            bindEditor.dictionaryName = Pricer.view.extention.Render.renderDictionary(form.getValues().parent_table_name);
                            bindEditor.getStore().getProxy().extraParams.dictionary = Pricer.view.extention.Render.renderDictionary(form.getValues().parent_table_name);
                            bindEditor.getStore().reload();

                            panel.down('combobox').setDisabled(true);
                            panel.down('button').setDisabled(true);

                            bindEditor.setDisabled(false);
                            panel.up('dictionaryValueList').store.reload();
                        },
                        failure: function (me, opts) {
                            var response = Ext.decode(opts.response.responseText);
                            console.log(response);
                        }
                    });
                }
            }
        }],
    setDataFields: function () {
        if (this.dataFields !== null)
        {
            this.getForm().setValues(this.dataFields);
            var combo = this.down('combobox');
            combo.setDisabled(true);
            var btn = this.down('button');
            btn.setDisabled(true);
        }
        else{
            this.getForm().setValues({'table_name': this.table});
        }
    },
    listeners: {
        afterrender: function () {
            Ext.Ajax.request({
                scope: this,
                url: '/dictionary/getDictionaryBind.json',
                method : 'post',
                params : {
                    table: JSON.stringify(this.table)
                },
                success: function(response, opts) {
                    var obj = Ext.decode(response.responseText);
                    if (obj.data !== false)
                    {
                        this.dataFields = obj.data;
                    }
                    else {
                        this.dataFields = null;
                    }
                    this.setDataFields();
                },
                failure: function(response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    }
});