Ext.define('Pricer.view.dictionary.forms.dictionaryComboMultiSelect', {
    extend: 'Ext.form.field.Tag',
    xtype: 'dictionarymultiselect',
    dictionaryName: null,
    valueField: 'id',
    mode: 'local',
    displayField: 'name',
    editorMode: true,
    initComponent: function () {
        var store = Ext.create('Pricer.store.dictionary.DictionaryStore', {
            proxy: {
                type: 'ajax',
                url: '/dictionary/getList.json',
                extraParams: {
                    dictionary: this.dictionaryName,
                    name: null
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            }
        });
        store.load();
        Ext.apply(this, {
            store: store
        });
        this.callParent();
    }
});