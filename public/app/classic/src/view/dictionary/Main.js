Ext.define('Pricer.view.dictionary.Main', {
    extend: 'Ext.Panel',
    xtype: 'dictionaryGrid',
    viewModel: 'dictionary.main',
    reference: 'dictionaryGrid',
    requires: [
        'Pricer.view.dictionary.grid.DictionaryCommonContainer'
    ],
    urlParams: {},
    controller: 'dictionary.main',
    dictionaryBind: null,

    updateItems: function (urlData) {
        Pricer.view.extention.QueryDispatcher.setUrl(urlData.action, urlData.params, urlData.module);
        if (this.lookup('dictionaryContainer'))
        {
            this.lookup('dictionaryContainer').destroy();
        }
        this.add({
            xtype: 'dictionaryContainer',
            reference: 'dictionaryContainer',
            urlData: urlData
        });
    },

    listeners: {
        beforerender: function () {
            Pricer.view.extention.QueryDispatcher.getUrlData()
                .then(
                    function (result) {
                        this.updateItems(result);
                    }.bind(this)
                );
        }
    }
});
