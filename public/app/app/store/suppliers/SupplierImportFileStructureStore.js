/**
 * Created by k.kochinyan on 11.05.2017.
 */
Ext.define('Pricer.store.suppliers.SupplierImportFileStructureStore', {
    extend: 'Ext.data.Store',
    alias: 'store.supplierimportfilestructureStore',
    model: 'Ext.data.Model',
    pageSize: 10,
    proxy: {
        type: 'ajax',
        url: '/import/getFileImportStructure.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});
