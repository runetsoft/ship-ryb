/**
 * Created by babkin on 31.03.2017.
 */

Ext.define('Pricer.store.suppliers.SuppliersStore', {
    extend: 'Ext.data.Store',
    alias: 'store.suppliersStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    proxy: {
        type: 'ajax',
        url: '/supplier/list.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

