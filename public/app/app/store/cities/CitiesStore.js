/**
 * Created by swds.dll on 25.04.2017.
 */

Ext.define('Pricer.store.cities.CitiesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.citiesStore',
    model: 'Ext.data.Model',
    pageSize: 10,
    proxy: {
        type: 'ajax',
        url: '/supplier/citiesList.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

