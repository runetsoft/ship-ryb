/**
 * Created by babkin on 31.03.2017.
 */

Ext.define('Pricer.store.goods.GoodInfoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.goodInfoStore',
    model: 'Ext.data.Model',
    xtype: 'goodinfo',
    pageSize: 10,

    // toDo
    baseParams: {
        goodId: null
    },
    proxy: {
        type: 'ajax',
        url: '/good/goodInfo.json',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    autoLoad: true
});
