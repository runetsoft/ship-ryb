/**
 * Created by babkin on 31.03.2017.
 */

Ext.define('Pricer.store.goods.GoodsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.goodsStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    storeId: 'GoodStore',
    proxy: {
        type: 'ajax',
        url: '/good/list.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

