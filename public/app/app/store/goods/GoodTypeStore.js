/**
 * Created by babkin on 31.03.2017.
 */

Ext.define('Pricer.store.goods.GoodTypeStore', {
    extend: 'Ext.data.Store',
    alias: 'store.goodtypestore',
    model: 'Ext.data.Model',
    pageSize: 50,
    storeId: 'goodtypestore',
    proxy: {
        type: 'ajax',
        url: '/good/getGoodTypeStructure.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: false
});

