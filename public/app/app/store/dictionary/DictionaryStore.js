/**
 * Created by swds.dll on 25.04.2017.
 */

Ext.define('Pricer.store.dictionary.DictionaryStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dictionaryStore',
    model: 'Ext.data.Model',
    proxy: {
        type: 'ajax',
        url:  '/dictionary/getList.json',
        baseParams: {
            dictionary: null,
            name: null
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});

