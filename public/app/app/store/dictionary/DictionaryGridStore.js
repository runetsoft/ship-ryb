Ext.define('Pricer.store.dictionary.DictionaryGridStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dictionaryGridStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    storeId: 'DictionaryGridStore',
    proxy: {
        type: 'ajax',
        url:  '/dictionary/getDictionaries.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    lastInsertedRecord: null,
    autoLoad: true,
    listeners: {
        add : function (store, record, index, eOpts) {
            store.lastInsertedIndex = index;
            store.lastInsertedRecord = record;
        }
    }
});