Ext.define('Pricer.store.dictionary.DictionaryValuesGridStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dictionaryValuesGridStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    storeId: 'DictionaryValuesGridStore',
    proxy: {
        type: 'ajax',
        url:  '/dictionary/getDictionaryList.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    lastInsertedRecord: null,
    autoLoad: true,
    listeners: {
        add : function (store, record, index, eOpts) {
            store.lastInsertedIndex = index;
            store.lastInsertedRecord = record;
        }
    }
});