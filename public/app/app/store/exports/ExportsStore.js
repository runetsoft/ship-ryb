Ext.define('Pricer.store.export.ExportsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.exportgridStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    proxy: {
        type: 'ajax',
        url: '/export/list.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

