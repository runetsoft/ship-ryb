Ext.define('Pricer.store.log.LogStore', {
    extend: 'Ext.data.Store',
    alias: 'store.logStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    proxy: {
        type: 'ajax',
        url: '/log/list.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

