/**
 * Created by swds.dll on 25.04.2017.
 */

Ext.define('Pricer.store.groups.GroupsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.groupsStore',
    model: 'Ext.data.Model',
    pageSize: 10,
    proxy: {
        type: 'ajax',
        url: '/supplier/groupsList.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

