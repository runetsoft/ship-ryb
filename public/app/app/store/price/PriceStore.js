/**
 * Created by babkin on 31.03.2017.
 */

Ext.define('Pricer.store.Price.PriceStore', {
    extend: 'Ext.data.Store',
    alias: 'store.priceStore',
    model: 'Ext.data.Model',
    pageSize: 50,
    proxy: {
        type: 'ajax',
        url: '/price/list.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
    autoLoad: true
});

