Ext.define('Pricer.view.suppliers.panel.SuppliersImportFilesPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.suppliers-panel-suppliersimportfilespanel',
    saveSupplierFileImportInfo: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form'),
            formData = form.getForm().getValues();

        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: '/import/saveSupplierFileImportInfo.json',
            method: 'GET',
            params: {
                supplierPriceImportFile: form.supplierPriceImportFileId,
                encoding: formData.encoding,
                noUploadFile: formData.noUploadFile,
                delimiter: formData.delimiter,
                filenameMask: formData.filename_mask
            },
            success: function (response, opts) {
                var result = Ext.decode(response.responseText);
                if (result.success) {
                    Ext.Msg.alert('Сохранение', result.message);
                } else {
                    Ext.Msg.alert('Сохранение', "Не удалось сохранить данные!");
                    console.log('ERROR: ' + result.message);
                }
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    importOneFile: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form');

        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: '/import/importOneFile.json',
            method: 'GET',
            params: {
                supplierPriceImportFile: form.supplierPriceImportFileId
            },
            success: function (response, opts) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.success) {
                    Ext.Msg.alert('Успешно', 'Информация успешно обновлена');
                } else {
                    Ext.Msg.alert('Ошибка', responseData.message);
                }
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    onRemoveImportOneFile: function (tab) {
        Ext.Msg.confirm("Удаление файла", "Вы уверены что хотите удалить файл?", function (btnText) {
            if (btnText === "no") {
                return false;
            }
            else if (btnText === "yes") {
                Pricer.view.extention.Ajax.request({
                    targetMask: tab.up('win'),
                    url: '/import/removeImportOneFile.json',
                    method: 'GET',
                    params: {
                        supplierPriceImportFileId: tab.supplierPriceImportFileId
                    },
                    success: function (response, opts) {
                        var result = Ext.decode(response.responseText);
                        Ext.Msg.alert('Удаление', result.message);
                        tab.destroy();
                    },
                    failure: function (response, opts) {
                        console.log('server-side failure with status code ' + response.status);
                    }
                });
            }
        }, this);
        return false;
    }
});
