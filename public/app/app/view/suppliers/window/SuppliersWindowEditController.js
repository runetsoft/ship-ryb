/**
 * Created by swds.dll on 18.04.2017.
 */
Ext.define('Pricer.view.suppliers.window.SuppliersWindowEditController', {
    extend: 'Pricer.view.lib.BaseController',
    requires: [
        'Pricer.view.extention.TemplateEngine',
        'Pricer.view.suppliers.view.SuppliersImportLog'
    ],
    alias: 'controller.suppliers.window.edit',

    /**
     *
     */
    onRemoveImportFile: function (obj) {
        Ext.MessageBox.show({
            title: 'Внимание ',
            msg: 'Вы точно хотите удалить данные!!!',
            buttons: Ext.MessageBox.OK,
            fn: function (btn) {
                if (btn == 'ok') {
                    var win = obj.up('window'),
                        form = obj.up('form');
                    Pricer.view.extention.Ajax.request({
                        targetMask: win,
                        url: '/import/removeImportFile.json',
                        method: 'GET',
                        params: {
                            supplierPriceImportId: form.supplierPriceImportId
                        },
                        success: function (response, opts) {
                            var supplierId = win.supplierId,
                                winNew = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
                                    supplierId: supplierId,
                                    items: [{
                                        supplierId: supplierId,
                                        xtype: 'suppliersform'
                                    }]
                                });
                            win.destroy();
                            winNew.show();
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                }
            }
        });
    },

    /**
     * Окошко с формой загрузки файла.
     */
    createLoadFileImportWindow: function (obj) {
        var winSuppliersFormEdit = obj.up('window'),
            form = winSuppliersFormEdit.getReferences().suppliersform,
            data = form.getForm().getValues(),
            supplierId = winSuppliersFormEdit.supplierId;
        winSuppliersFormEdit.destroy();
        var winLoadFileImport = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsLoadFileImport', {
            items: [{
                supplierId: supplierId,
                xtype: 'suppliersformloadfileimport'
            }]
        });
        winLoadFileImport.show();
    },

    /**
     * Заполнение формы редактирования.
     *
     * @param form
     **/
    onFormRender: function (form) {
        var win = form.up('window');
        Ext.Ajax.request({

            url: form.url,
            method: 'GET',
            params: {
                supplierPriceImportId: form.supplierPriceImportId
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.data) {
                    form.getForm().setValues(obj.data);
                } else {
                    console.log(obj);
                }
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * Импорт файла.
     *
     * @param form
     **/
    onImport: function (obj) {
        var win = obj.up('window');
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Загружаю...',
            url: '/import/importPrice.json',
            method: 'GET',
            params: {
                supplierId: win.supplierId
            },
            success: function (response, opts) {
                Ext.Msg.alert('Успешно', 'Данные импортированы');
            },
            failure: function (me, opts) {
                Ext.Msg.alert('Ошибка', 'Возникли проблемы с сервером. Обратитесь к администратору!');
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * Импорт всех файлов.
     *
     * @param form
     **/
    onAllImport: function (obj) {
        var win = obj.up('window');
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Загружаю...',
            url: '/import/importPrice.json',
            method: 'GET',
            params: {
                supplierId: win.supplierId
            },
            success: function (response, opts) {
                Ext.Msg.alert('Успешно', 'Данные импортированы');
            },
            failure: function (me, opts) {
                Ext.Msg.alert('Ошибка', 'Возникли проблемы с сервером. Обратитесь к администратору!');
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * Видит информацию о последнем импорте.
     *
     * @param obj
     **/
    onLastImportInfo: function (obj) {
        var win = obj.up('window'),
            importFileForm = obj.up('form');
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Загрузка информации о последнем импорте...',
            url: '/import/lastImportInfo.json',
            method: 'GET',
            params: {
                supplierPriceImportId: importFileForm.supplierPriceImportId
            },
            success: function (response, opts) {
                var responseText = Ext.decode(response.responseText).data;
                var template = '<strong>Дата импорта</strong>: {date_import} <br> ' +
                    '<strong>Состояние импорта</strong>: {success} <br> ' +
                    '<strong>Изменения в файле</strong>: {warning} <br> ' +
                    '<strong>Детали</strong>: {detail}';
                var info = Pricer.view.extention.TemplateEngine.convert(template, {
                    date_import: responseText.date_import,
                    warning: (responseText.warning == 1) ? 'Да' : 'Нет',
                    success: (responseText.success == 1) ? 'Успешный' : 'Не успешный',
                    detail: Ext.decode(responseText.detail)
                });
                Ext.Msg.alert('Информация об импорте', info);
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },
    onShowImportFiles: function (obj) {
        var form = obj.up('form'),
            winWorksheets = Ext.create('Pricer.view.suppliers.window.SuppliersImportWorksheetsWindow', {
                title: 'Экспортные файлы',
                items: [{
                    supplierPriceImportId: form.supplierPriceImportId,
                    xtype: 'suppliersimportfilespanel'
                }]
            });
        winWorksheets.show()
    },
    onUploadImportFiles: function (obj) {
        Ext.Msg.confirm('Загрузить и импортировать', 'Будут перезагружены и импортированы все файлы, структура' +
            ' файлов должна быть подготовлена. Вы уверены?',
            function (choice) {
                if (choice === 'yes') {
                    var win = obj.up('window'),
                        form = obj.up('form');
                    Pricer.view.extention.Ajax.request({
                        targetMask: win,
                        url: '/import/loadFilesImport.json',
                        method: 'GET',
                        params: {
                            supplierPriceImportId: form.supplierPriceImportId
                        },
                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            Ext.getBody().unmask();
                            if (obj.success) {
                                var supplierId = win.supplierId,
                                    winNew = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
                                        supplierId: supplierId,
                                        items: [{
                                            supplierId: supplierId,
                                            xtype: 'suppliersform'
                                        }]
                                    });
                                win.destroy();
                                winNew.show();
                            } else {
                                Ext.MessageBox.alert(
                                    'Ошибка',
                                     obj.message,
                                    this
                                );
                            }
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                }
            }
        );
    },
    onLogImport: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form');

        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit',
            {
                title: 'Имформация об импорте',
                width: 350,
                bodyPadding: 20,
                items: [
                    {
                        xtype: 'suppliersimportlog',
                        supplierPriceImportId: form.supplierPriceImportId
                    }
                ],
                buttons: [
                    {
                        text: 'Отмена',
                        handler: function () {
                            var win = this.up('window');
                            win.destroy();
                        }
                    }
                ]
            });
        win.show();
    },
    onCronSave: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form'),
            data = form.getForm().getValues();
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: '/import/cronSave.json',
            method: 'GET',
            params: {
                supplierPriceImportId: form.supplierPriceImportId,
                cronPeriodicity: data.cron_periodicity,
                isCron: data.is_cron,
                importInfo: data.import_info
            },
            success: function (response, opts) {
                Ext.Msg.alert('Успешно', 'Данные сохранены!');
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    onResetImportDate: function (obj) {
        Ext.Msg.confirm('Сбросить дату импорта','Вы уверены что хотите сбросить дату импорта?',
            function (choice) {
                if (choice === 'yes') {
                    var win = obj.up('window'),
                        form = obj.up('form');
                    Pricer.view.extention.Ajax.request({
                        targetMask: win,
                        url: '/import/resetImportDate.json',
                        method: 'GET',
                        params: {
                            supplierPriceImportId: form.supplierPriceImportId
                        },
                        success: function (response, opts) {
                            var data = Ext.decode(response.responseText);
                            Ext.Msg.alert('Успешно', data.message);
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                }
            }
        );
    }
});
