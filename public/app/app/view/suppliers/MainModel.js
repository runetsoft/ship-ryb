/**
 * This class is the view model for the Suppliers view of the application.
 */
Ext.define('Pricer.view.suppliers.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.suppliers.main',
    data: {
        moduleName: 'suppliers',
        useNumber: 0
    }
});
