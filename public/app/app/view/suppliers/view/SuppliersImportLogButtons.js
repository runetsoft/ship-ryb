/**
 * Created by yu.khristodorov on 04.07.2017.
 */

Ext.define('Pricer.view.suppliers.view.SuppliersImportLogButtons', {
    extend: 'Ext.Container',
    xtype: 'supplierlogbuttons',
    style: {
        border: '0px'
    },
    listeners:{
        render: function(combo, value, eOpts){
            var record = combo.getWidgetRecord().getData();
            var imports_name;
            if (record.imports) {
                Ext.each(record.imports.split(','), function(value, i)
                {
                    imports_name = record.imports_name.split(',');
                    combo.add(
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-list',
                            tooltip: imports_name[i],
                            style: {
                                margin: '0px 0px 0px 5px',
                                border: '0px',
                                background: '#e64759'
                            },
                            handler: 'onLogStatusImport',
                            supplierPriceImportId: value
                        }
                    )
                });
            }
        }
    }


});

