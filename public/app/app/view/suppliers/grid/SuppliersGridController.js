Ext.define('Pricer.view.suppliers.grid.SuppliersGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.suppliers-grid-suppliersgrid',

    /**
     * Показываем окно редактирования
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */

    onItemEdit: function ( grid , record , element , rowIndex , e , eOpts ) {
        var supplierId = parseInt(eOpts.get('id'));
        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
            supplierId: supplierId, // @TODO надо удалить.
            items:[{
                supplierId: parseInt(eOpts.get('id')),
                xtype: 'suppliersform'
            }]
        });
        win.on('dataSave',
            function (){
                grid.getStore().load();
            });
        win.show();
    },

    /**
     * Показываем окно редактирования клику на ИД
     * @param iView
     * @param iCellEl
     * @param iColIdx
     * @param iRecord
     * @param iRowEl
     * @param iRowIdx
     * @param iEvent
     */

    onCellClick: function(iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx, iEvent) {
        var fieldName = iView.getGridColumns()[iColIdx].dataIndex;
        if(fieldName == 'id'){
            this.onItemEdit(iView, null, null, null, null, iRecord);
        }
    },

    /**
     * Показываем окно удаления
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */
    onItemDelete: function ( grid , record , element , rowIndex , e , eOpts ) {
        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
            height: 150,
            title: 'Удаление товара',
            html: 'Вы уверены что хотите удалить товар?',
            width: 350,
            bodyPadding: 20,
            buttons: [
                {
                    text: 'Удалить',
                    handler: function() {
                        var win = this.up('window');
                        Ext.Ajax.request({
                            url: '/supplier/supplierDelete.json?supplierId=' + eOpts.get('id'),
                            waitMsg:'Удаление...',
                            reset: false,
                            success: function(response, opts) {

                                // Закрыть окно
                                win.destroy();

                                // перезагрузить таблицу
                                grid.getStore().load();
                            },
                            failure: function(response, opts) {
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    }
                },
                {
                    text: 'Отмена',
                    handler: function() {
                        this.up('window').destroy();
                    }
                }
            ]
        });
        win.show();
    },

    /**
     * Рендер статуса
     * @param value
     * @param p
     * @param model
     * @returns {string}
     */
    renderStatus: function (value, p, model) {
        var status = (value == Pricer.view.extention.Options.SUPPLIERS_STATUS_ACTIVE) ? '<b>Активен</b>' : '-';
        return status;
    },

    /**
     * Выводит информацию о импортах.
     *
     * @param obj
     **/
    onLastImportInfoById: function (grid, record, element, rowIndex, e, eOpts) {
        console.log(rowIndex.supplierPriceImportId);
        Pricer.view.extention.Ajax.request({
            targetMask: grid,
            msgMask: 'Загрузка информации об импорте...',
            url: '/import/lastImportInfo.json',
            method: 'GET',
            params: {
                supplierPriceImportId: rowIndex.supplierPriceImportId
            },
            success: function (response, opts) {
                var responseText = Ext.decode(response.responseText).data;
                var template = '<strong>Дата импорта</strong>: <%date_import%> <br> ' +
                    '<strong>Состояние импорта</strong>: <%success%> <br> ' +
                    '<strong>Изменения в файле</strong>: <%warning%> <br> ' +
                    '<strong>Детали</strong>: <%detail%>';
                var info = Pricer.view.extention.TemplateEngine.convert(template, {
                    date_import: responseText.date_import,
                    warning: (responseText.warning == 1) ? 'Да' : 'Нет',
                    success: (responseText.success == 1) ? 'Успешный' : 'Не успешный',
                    detail: Ext.decode(responseText.detail)
                });
                Ext.Msg.alert('Информация об импорте', info);
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },
    
    /**
     * Показать окно с логами импорта по его ИД
     * @param obj
     */
    onLogStatusImport: function (obj) {
        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit',
            {
                title: 'Имформация об импорте',
                width: 350,
                bodyPadding: 20,
                items: [
                    {
                        xtype: 'suppliersimportlog',
                        supplierPriceImportId: obj.supplierPriceImportId
                    }
                ],
                buttons: [
                    {
                        text: 'Отмена',
                        handler: function () {
                            var win = this.up('window');
                            win.destroy();
                        }
                    }
                ]
            });
        win.show();
    }
});