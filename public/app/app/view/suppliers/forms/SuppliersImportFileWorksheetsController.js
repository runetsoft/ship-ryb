/**
 * Created by swds.dll on 30.03.2017.
 */
Ext.define('Pricer.view.suppliers.forms.SuppliersImportFileWorksheetsController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.suppliers.form.importfileworksheets',
    requires: [
        'Pricer.view.lib.BaseController',
        'Pricer.view.extention.Ajax'
    ],

    /**
     * Обновляет структуру файла.
     */
    onUpdateHeadersData: function (obj) {

        var win = obj.up('window'),
            form = obj.up('form'),
            grid = form.lookupReference('supplierimportfilestructureList'),
            gridData = grid.getStore().getData().items;
        var fields = [];
        Ext.Array.forEach(gridData, function (item) {
            fields.push(item.data);
        });
        data = {
            importFileStructureId: grid.importFileStructureId,
            fields: fields
        }
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Сохранение полей...',
            url: '/import/updateImportFileStructure.json',
            params: {
                data: Ext.util.JSON.encode(data)
            },
            success: function (response, opts) {
                grid.getStore().load({
                    params: {
                        importFileStructureId: grid.importFileStructureId
                    }
                });
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });


        // var importFileform = win.lookupReference('suppliersimportfileworksheets'),
        //     formData = importFileform.getForm().getValues(),
        //     data = {
        //         supplierPriceImportId: importFileform.supplierPriceImportId,
        //         is_cron: formData.is_cron,
        //         cron_periodicity: formData.cron_periodicity
        //     };
        //
        // Pricer.view.extention.Ajax.request({
        //     targetMask: win,
        //     msgMask: 'Сохранение импорта...',
        //     url: '/import/updateSupplierPriceImport.json',
        //     params: {
        //         data: Ext.util.JSON.encode(data)
        //     },
        //     success: function (response, opts) {
        //         console.log(response);
        //     },
        //     failure: function (me, opts) {
        //         var response = Ext.decode(opts.response.responseText);
        //         console.log(response);
        //     }
        // });
    },
    saveNoUpload: function (obj,  newValue, oldValue) {
        var win = obj.up('window'),
            form = obj.up('form'),
            formData = form.getForm().getValues(),
            grid = form.lookupReference('supplierimportfilestructureList');
        if (obj.hasFocus) {
            Pricer.view.extention.Ajax.request({
                targetMask: win,
                url: '/import/saveNoUpload.json',
                method: 'GET',
                params: {
                    importFileStructureId: grid.importFileStructureId,
                    noUpload: formData.noUpload
                },
                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    },
    /**
     * Сохраняет заголовки таблицы.
     */
    saveWorksheetHeaders: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form'),
            formData = form.getForm().getValues(),
            grid = form.lookupReference('supplierimportfilestructureList');

        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: '/import/saveWorksheetHeaders.json',
            method: 'GET',
            params: {
                importFileStructureId: grid.importFileStructureId,
                headersRow: formData.headersRow,
                noHeaders: formData.noHeaders
            },
            success: function (response, opts) {
                grid.getStore().load({
                    params: {
                        importFileStructureId: grid.importFileStructureId
                    }
                });
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
});
