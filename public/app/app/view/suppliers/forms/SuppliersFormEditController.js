/**
 * Created by swds.dll on 30.03.2017.
 */
Ext.define('Pricer.view.suppliers.forms.SuppliersFormEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.suppliers.form.edit',
    requires: [
        'Pricer.view.lib.BaseController',
        'Pricer.view.extention.Ajax'
    ],

    /**
     * Заполнение формы редактирования.
     *
     * @param form
     **/
    onFormRender: function (form) {
        var win = form.up('window');
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: form.url,
            method: 'GET',
            params: {
                supplierId: win.supplierId
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.data) {
                    form.getForm().setValues(obj.data);
                } else {
                    form.getForm().setValues({'supplier_id': win.supplierId});
                }
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    onSupplierImport: function (obj) {
        var win = obj.up('window'),
            form = win.getReferences().suppliersform;
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: '/import/supplierImport.json',
            method: 'GET',
            params: {
                supplierId: form.supplierId
            },
            waitMsg: 'Импортирую ...',
            reset: false,
            success: function (response, opts) {
                Ext.Msg.alert('Успешно', 'Данные импортированы!');
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },
    /**
     * Сохренение формы.
     *
     * @param obj
     * @param value
     *
     **/
    onSaveData: function (obj, value) {
        var win = obj.up('window'),
            form = win.getReferences().suppliersform,
            data = form.getForm().getValues();
        data = Ext.util.JSON.encode(data);
        form.submit({
            url: '/supplier/supplierUpdate.json',
            method: 'POST',
            params: {
                request: 'save',
                data: data
            },
            waitMsg: 'Сохранение...',
            reset: false,
            success: function (response, opts) {
                win.fireEvent('dataSave');
                win.destroy();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * Загрузка файла.
     */
    onLoadFileImport: function (obj) {
        var winLoadFileImport = obj.up('window'),
            form = winLoadFileImport.getReferences().suppliersformloadfileimport,
            data = form.getForm().getValues(),
            supplierId = form.supplierId;
        data.supplierId = supplierId;
        form.submit({
            url: '/import/loadFilesImport.json',
            method: 'POST',
            params: {
                request: 'loadFileImport',
                data: Ext.util.JSON.encode(data)
            },
            waitMsg: 'Загрузка',
            reset: false,
            success: function (form, action) {
                form.destroy();
                winLoadFileImport.destroy();
                var winNew = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
                    supplierId: supplierId,
                    items: [{
                        supplierId: supplierId,
                        xtype: 'suppliersform'
                    }]
                });
                winNew.show();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                Ext.MessageBox.show({
                    title: 'Не удалось загрузить файл',
                    msg: response.message,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
                console.log(response);
            }
        });
    }
});
