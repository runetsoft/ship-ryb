/**
 * Created by babkin on 30.03.2017.
 */
Ext.define('Pricer.view.suppliers.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.suppliers.main',
    requires: [
        'Pricer.view.suppliers.MainModel',
        'Pricer.view.suppliers.forms.SuppliersFormEditController',
        'Pricer.view.suppliers.forms.SuppliersFormEdit',
        'Pricer.view.suppliers.window.SuppliersWindowEditController',
        'Pricer.view.suppliers.windows.SuppliersWindowsEdit'
    ],
    /**
     * Показываем окно добавления нового поставщика
     */
    addSupplier: function(){
        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit', {
            supplierId: null,
            items:[{
                xtype: 'suppliersform'
            }]
        });
        win.show();
    }
});
