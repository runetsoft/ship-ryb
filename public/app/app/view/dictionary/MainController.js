Ext.define('Pricer.view.dictionary.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.dictionary.main',
    requires: [
        'Pricer.view.dictionary.MainModel',
    ],
    back: function (btn, conf) {
        var grid = btn.up('app-main').getReferences().dictionaryGrid;
        // btn.up().removeAll();
        grid.removeDocked(btn.up().down('dictionaryBinderCombo'), true, false);
        grid.updateItems({action: null, params: {}, module: 'dictionary'});
        // console.log();
    }
});
