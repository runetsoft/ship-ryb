Ext.define('Pricer.view.dictionary.grid.DictionaryValuesGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dictionary-grid-dictionaryvaluesgrid',
    requires: [
        'Pricer.view.main.gridfilter.GridFilterModel'
    ],

    onItemSave: function (grid, record, element, rowIndex, e, eOpts) {
        console.log(eOpts);
        Pricer.view.extention.Ajax.request({
            targetMask: grid,
            params: {
                dictionary: grid.grid.dictionary,
                data: JSON.stringify({
                    code: eOpts.data.code,
                    name: eOpts.data.name,
                    parent_id: eOpts.data.parent_id
                })
            },
            method: 'POST',
            url: '/dictionary/saveNewDictionaryValue.json',
            success: function (response, opts) {
                grid.store.lastInsertedRecord = null;
                grid.store.reload();
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    onItemDelete: function (grid, record, element, rowIndex, e, eOpts) {
        if (e.record.get('isNewRow') !== true) {
            Pricer.view.extention.Ajax.request({
                targetMask: grid,
                params: {
                    data: JSON.stringify({
                        dictionary: grid.grid.dictionary,
                        id: eOpts.id
                    })
                },
                method: 'POST',
                url: '/dictionary/deleteItem.json',
                success: function (response, opts) {
                    grid.store.reload();
                },
                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        else {
            grid.store.remove(eOpts);
        }

    },
    onItemEdit: function (grid, record, element, rowIndex, e, eOpts) {
        var recordData = grid.getStore().getAt(record);
        var fullScreenWindow = Ext.create('Pricer.view.export.window.EditGoodTypeWindow', {
            title: 'Редактировать новый тип товара',
            goodType: recordData.data.code,
            goodTypeId: recordData.data.id
        });
        fullScreenWindow.show();
    },
    beforeEdit: function (editor, e) {

    },
    afterEdit: function (editor, e) {
        var isNewValue = e.record.get('isNewRow');
        if (Ext.util.Format.trim(e.record.get(e.field)) && isNewValue !== true)
        {
            Pricer.view.extention.Ajax.request({
                targetMask: editor.grid,
                params: {
                    dictionary: JSON.stringify(editor.grid.dictionary),
                    data: JSON.stringify({
                        fieldName: e.field,
                        fieldValue: e.record.get(e.field),
                        id: e.record.get('id')
                    })
                },
                method: 'POST',
                url: '/dictionary/updateValue.json',
                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    editor.grid.store.reload();
                },
                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    }
});