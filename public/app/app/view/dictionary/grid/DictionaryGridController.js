Ext.define('Pricer.view.dictionary.grid.DictionaryGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dictionary-grid-dictionarygrid',
    requires: [
        'Pricer.view.main.gridfilter.GridFilterModel',
        'Pricer.view.extention.QueryDispatcher'
    ],

    onDetail: function (grid, record, element, rowIndex, e, eOpts) {
        Pricer.view.extention.QueryDispatcher.setUrl('edit_table', {"table_name" : eOpts.data.TABLE_NAME}, 'dictionary');
        grid.up('app-main').getReferences().dictionaryGrid.updateItems({action: 'edit_table', params: {"table_name" : eOpts.data.TABLE_NAME}, module: 'dictionary'});
    },
    onItemDelete: function (grid, record, element, rowIndex, e, eOpts) {
        grid.store.remove(eOpts);
    },
    beforeEdit: function (editor, e) {
        var isEditableByColumn = e.column.isEditable;
        var isEditableByRecord = e.record.get('isEditable');
        if (isEditableByColumn === false && !isEditableByRecord) {
            return false;
        }
    },
    afterEdit: function (editor, e) {
        var isEditableByColumn = e.column.isEditable;
        var isEditableByRecord = e.record.get('isEditable');
        var tableComment = e.record.get('TABLE_COMMENT');
        var tableName = e.record.get('TABLE_NAME');
        if (isEditableByColumn === true && !isEditableByRecord){
            Pricer.view.extention.Ajax.request({
                targetMask: editor.grid,
                params: {
                    data: JSON.stringify({
                        tableName: tableName,
                        tableComment: tableComment
                    })
                },
                method: 'POST',
                url: '/dictionary/updateTableComment.json',
                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    editor.grid.store.reload();
                },
                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    },
    onSave: function (grid, record, element, rowIndex, e, eOpts) {
        Pricer.view.extention.Ajax.request({
            targetMask: grid,
            params: {
                data: JSON.stringify({
                    tableName: eOpts.data.TABLE_NAME,
                    tableComment: eOpts.data.TABLE_COMMENT
                })
            },
            method: 'POST',
            url: '/dictionary/addTable.json',
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                grid.store.lastInsertedRecord = null;
                grid.store.reload();
                console.log(obj);
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }
});