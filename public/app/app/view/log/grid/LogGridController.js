Ext.define('Pricer.view.log.grid.LogGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.log-grid-loggrid',
    /**
     * Показываем окно редактирования
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */

    onDetail: function (grid, record, element, rowIndex, e, eOpts) {
        var rec = grid.getStore().getAt(record);
        var win = Ext.create('Pricer.view.suppliers.windows.SuppliersWindowsEdit',
            {
                title: 'Имформация об импорте',
                width: 600,
                bodyPadding: 20,
                items: [
                    {
                        xtype: 'viewimportlog',
                        importLogId: rec.get('id')
                    }
                ],
                buttons: [
                    {
                        text: 'Отмена',
                        handler: function () {
                            var win = this.up('window');
                            win.destroy();
                        }
                    }
                ]
            });
        win.show();
    }
});
