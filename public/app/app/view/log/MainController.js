Ext.define('Pricer.view.log.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.log.main',
    requires: [
        'Pricer.view.log.MainModel',
    ]
});
