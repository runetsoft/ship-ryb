Ext.define('Pricer.view.export.form.ExportsFormEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.export.form.edit',

    /**
     * Заполнение формы редактирования.
     *
     * @param form
     **/
    onFormRender: function (form) {
        var win = form.up('window');
        if (!win.exportId) {
            return;
        }
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: form.url,
            method: 'GET',
            params: {
                exportId: win.exportId
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.data) {
                    form.setFormValues(obj.data);
                }
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    onExportData: function (obj) {
        var win = obj.up('window'),
            form = obj.up('form'),
            grid = form.getReferences().exportPriceColumnsList,
            requestData = {data: {columns: {}, id: win.exportId}, filter: {}},
            formData = form.getValues(),
            gridData = grid.getStore().getData().items;

        // Prepare  request data
        Ext.Array.forEach(gridData, function (item) {
            if (item.data.columnKey != '') {
                requestData.data.columns[item.data.columnKey] = item.data.columnName;
            }
        });
        requestData.data.name = formData.name;
        requestData.data.min_size = formData.min_size;
        if (formData.hasOwnProperty('is_cron')) {
            requestData.data.is_cron = formData.is_cron;
            requestData.data.cron_periodicity = formData.cron_periodicity;
        }
        if (formData.hasOwnProperty('all_good')) {
            requestData.data.all_good = formData.all_good;
        }

        requestData.filter = {
            provider_id: (formData.suppliers != '') ? formData.suppliers : [],
            good_type: formData.good_type
        };

        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Экспорт прайсов ...',
            url: '/export/uploadPrice.json',
            params: {
                data: Ext.util.JSON.encode(requestData)
            },
            success: function (response, opts) {
                win.fireEvent('onFileAdded');
                win.destroy();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    }

});
