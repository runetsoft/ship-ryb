/**
 * Created by k.kochinyan on 05.06.2017.
 */
Ext.define('Pricer.view.export.grid.ExportsGridController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.export.grid.exportgrid',

    onItemDelete: function (grid, record, element, rowIndex, e, eOpts) {
        Pricer.view.extention.Ajax.request({
            targetMask: grid,
            msgMask: 'Удаление экспорта',
            url: '/export/exportDelete.json',
            method: 'GET',
            params: {
                exportId: eOpts.get('id')
            },
            success: function (response, opts) {
                grid.getStore().load();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
        grid.getStore().removeAt(rowIndex);
    },

    /**
     * Показываем окно редактирования
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */

    onItemEdit: function (grid, record, element, rowIndex, e, eOpts) {
        var exportId = parseInt(eOpts.get('id'));
        var win = Ext.create('Pricer.view.export.window.ExportsWindowEdit', {
            exportId: exportId,
            items: [{
                exportId: parseInt(eOpts.get('id')),
                xtype: 'exportform'
            }]
        });
        win.on('onFileAdded',
            function () {
                grid.getStore().load();
            });
        win.show();
    }
});
