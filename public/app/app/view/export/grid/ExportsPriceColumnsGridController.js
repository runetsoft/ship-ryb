/**
 * Created by k.kochinyan on 05.06.2017.
 */
Ext.define('Pricer.view.export.grid.ExportsPriceColumnsGridController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.export.grid.priceColumnsGrid',

    onItemDelete: function (grid, rowIndex) {
        if (rowIndex == undefined) {
            return;
        }
        grid.getStore().removeAt(rowIndex);
    },
    onGridEdit: function( editor, context, eOpts) {
         if (context.field == 'columnKey') {
             context.record.set('columnName', this.getViewModel().get('columnName'));
         }
    },
    onColumnKeySelect: function(combo, record, index) {
        this.getViewModel().set('columnName',record.get('name'));
    }
});
