Ext.define('Pricer.view.export.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.export.main',
    requires: [
        'Pricer.view.export.MainModel',
        'Pricer.view.export.window.ExportsWindowEdit',
        'Pricer.view.export.form.ExportsFormEdit'
    ],
    /**
     * Показываем окно добавления нового поставщика
     */
    addExport: function (btn) {

        var win = Ext.create('Pricer.view.export.window.ExportsWindowEdit', {
            items: [{
                xtype: 'exportform'
            }]
        });
        win.on('onFileAdded',
            function () {
                this.fireEvent('onFileAdded');
            },
            btn
        )
        win.show();
    }
});
