/**
 * This class is the view model for the price view of the application.
 */
Ext.define('Pricer.view.price.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.price.main',
    data: {
        moduleName: 'price',
        useNumber: 0
    }
});
