Ext.define('Pricer.view.price.form.PriceFormLoadcsvController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.price.form.loadcsv',


    sendCsv: function (btn) {

        var form = btn.up('form').getForm();
        var win = btn.up('window');
        if(form.isValid()) {
            form.submit({
                url: '/price/upload.json',
                waitMsg: 'Загрузка...',
                success: function(fp, opts) {
                    var obj = Ext.decode(opts.response.responseText);
                    var localstore =  Ext.create('Ext.data.Store', {
                        fields:['id', 'good'],
                        data: obj.data,
                        groupField: 'state'
                    });
                    var windowContent = Ext.create('Pricer.view.price.form.PriceAfterFormLoad', {
                        store:localstore
                    });
                    var window = Ext.create('Pricer.view.price.window.PriceWindowAfterLoad', {
                        items: windowContent
                    });
                    window.show();
                    win.destroy();
                },
                failure: function (fp, opts) {
                    console.log(Ext.decode(opts.response.responseText).message);
                }
            });
        }
    }

});