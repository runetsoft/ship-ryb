Ext.define('Pricer.view.price.form.PriceFormEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.price.form.edit',

    /**
     * Заполнение формы редактирования.
     *
     * @param form
     **/
    onFormRender: function (form) {
        var win = form.up('window');
        if (!win.priceId) {
            return;
        }
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            url: form.url,
            method: 'GET',
            params: {
                id: win.priceId
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.data) {
                    form.getForm().setValues(obj.data);
                }
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * сохранение данных формы редактирования
     *
     * @param win obj
     * @param price_id int
     * @param good_id int
     */
    onSaveData: function (win, price_id, good_id) {
        Pricer.view.extention.Ajax.request({
            targetMask: win,
            msgMask: 'Обновление позиции ...',
            url: '/price/saveConformInfo.json',
            method: 'POST',
            params: {
                data: JSON.stringify({price_id: price_id, good_id: good_id})
            },
            success: function (response, opts) {
                win.fireEvent('dataSave');
                win.destroy();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },
    /**
     * проверка и выбор действия перед сохраненнием соответствия
     *
     * @param obj object
     */
    beforeSaveData: function (obj) {
        var win = obj.up('window');
        var form = obj.up('form'),
            formData = form.getValues();

        var reference = form.getReferences(),
            combo = reference.goodsCombo;

        if (combo.getRawValue() !== 'ненайдено соответствий') {
            var comboRecordById = combo.getStore().findRecord('id', combo.getValue());
            var comboRecordByText = combo.getStore().findRecord('good', combo.getRawValue());
            if (comboRecordById || comboRecordByText || Ext.util.Format.trim(combo.getValue()) === "") {
                this.onSaveData(win, formData.id, formData.good_id);
            } else if (
                combo.dirty &&
                combo.getValue() != null && combo.getValue().replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' ') != ''
            ) {
                var modal = Ext.create('Pricer.view.price.grid.PriceAddGoodComponent');
                var controller = this;
                modal.on('afterSucces',function () {
                    Pricer.view.extention.Ajax.request({
                        url: '/good/explodeGood.json',
                        method: 'GET',
                        params: {
                            goodName: combo.getValue(),
                            zarticle: formData.zarticle,
                            rprice: formData.rprice
                        },
                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            if (obj.success) {
                                combo.getStore().getProxy().extraParams.id = obj.data.id;
                                combo.getStore().reload({
                                    callback: function () {
                                        combo.setValue(obj.data.id);
                                        combo.getStore().getProxy().extraParams = [];
                                    }
                                });
                                if (obj.data.isNotNew === true) {
                                    controller.onSaveData(win, formData.id, obj.data.id);
                                }
                                else{
                                    var editingWindow = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit', {
                                        goodType: obj.data.goodType,
                                        goodId: obj.data.id,
                                        items: [{
                                            xtype: 'goodsformtype'
                                        }]
                                    });
                                    editingWindow.show();
                                }

                            } else {
                                console.log(response.responseText);
                                Ext.Msg.alert('Ошибка!', 'Ошика сохранения');
                            }
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                            Ext.Msg.alert('Ошибка!', 'Ошика сохранения');
                        }
                    });
                });
                modal.addGood(combo, formData.zarticle, formData.rprice);
            }
        }

    },

    /**
     * добавление текущего названия в поле для соответствий
     *
     * @param component obj
     */
    onSetTextInCombo: function(component) {
        component.getEl().on('click', function() {
            var form = component.up('form');

            var reference = form.getReferences(),
                goodsCombo = reference.goodsCombo;

            goodsCombo.select(component.value);

        });
    },

    /**
     * перезагрузка стора по артикулу текущего прайса
     *
     * @param component obj
     */
    onFindConformByArticle: function (component) {
        component.getEl().on('click', function() {
            var form = component.up('form');

            var reference = form.getReferences(),
                goodsCombo = reference.goodsCombo;
            goodsCombo.store.getProxy().extraParams['zarticle'] = component.value;
            goodsCombo.store.reload({
                callback: function () {
                    goodsCombo.select(goodsCombo.getStore().getAt(0)||'ненайдено соответствий');
                    if (!goodsCombo.getStore().getAt(0))
                    {
                        goodsCombo.store.getProxy().extraParams = [];
                    }
                }
            });

        });
    }


});