/**
 * Created by babkin on 30.03.2017.
 */
Ext.define('Pricer.view.price.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.price.main',
    requires: [
        'Pricer.view.price.MainModel'
    ],
    uploadPrice: function () {
        Ext.Ajax.request({
            url: '/export/uploadPrice.json',
            method: 'GET',
            params: {},
            success: function (response, opts) {
                var data = Ext.decode(response.responseText);
                console.log(data);
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    },

    /**
     * Удаление всех товаров без соответствия.
     * @param grid
     */
    onDeleteAllItemsWithoutConform: function (grid ) {

        var targetMask = grid.up('price');

        Ext.Msg.confirm('Удаление товаров', 'Вы действительно хотите удалить товары без соответствия?',
            function (choice) {
                if (choice === 'yes') {
                    Pricer.view.extention.Ajax.request({
                        targetMask: targetMask,
                        url: '/price/deleteAllItemsWithoutConform.json',
                        waitMsg: 'Удаление...',
                        reset: false,
                        success: function (response, opts) {
                            // перезагрузить таблицу
                            targetMask.getReferences().priceList.getStore().load();
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                }
            }
        );
    },
    onSaveConform: function (btn) {
        var panel = btn.up('price'),
            grid = panel.getReferences().priceList,
            s = grid.getSelectionModel().getSelection(),
            selected = [];
        Ext.each(s, function (item) {
            selected.push({
                'price_id': item.data.id,
                'good_id': item.data.good_orig_id
            });
        });
        if (selected.length == 0) {
            Ext.MessageBox.alert(
                'Ошибка',
                'Не выбраны товары',
                this
            );
            return;
        }

        Ext.MessageBox.confirm(
            'Проставить соответствия',
            'У выбранных товаров будут сохранены соответствия, а товары без соответсвий удялятся! Вы уверены?',
            function (btn) {
                if (btn === 'yes') {
                    Pricer.view.extention.Ajax.request({
                        targetMask: panel,
                        params: {
                            data: JSON.stringify(selected)
                        },
                        url: '/price/saveConform.json',
                        waitMsg: 'Удаление...',
                        reset: false,
                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            if (obj.success) {
                                Ext.MessageBox.alert(
                                    'Успех',
                                    'Соответствия проставлены и товары без соответсвий удалены',
                                    this
                                );
                                grid.getSelectionModel().deselectAll();
                                grid.getStore().reload();
                            }
                            panel.getReferences().onSaveConform.hide();
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                }
            },
            this
        );

    }
});
