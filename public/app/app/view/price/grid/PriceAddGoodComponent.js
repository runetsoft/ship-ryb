Ext.define('Pricer.view.price.grid.PriceAddGoodComponent', {
    extend: 'Pricer.view.lib.BaseController',
    params: {},
    /**
     * рисует модальное окно с запросом на добавление нового товара
     *
     * @param combo
     * @param zarticle
     * @param rprice
     */
    addGood: function (combo, zarticle, rprice) {
        var component = this;
        Ext.Msg.confirm('', 'Желаете добавить новый товар (' + combo.getValue() + ') в качестве соответствия?',
            function (choice) {
                if (choice === 'yes') {
                    component.fireEvent('afterSucces', combo, zarticle, rprice);
                }
            })
    }
});