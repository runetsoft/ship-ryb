Ext.define('Pricer.view.price.grid.PriceGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.price-grid-pricegrid',
    requires: [
        'Pricer.view.price.grid.PriceAddGoodComponent'
    ],
    /**
     * переход по кликам на ячейки грида
     *
     * @param iView
     * @param iCellEl
     * @param iColIdx
     * @param iRecord
     * @param iRowEl
     * @param iRowIdx
     */
    onCellClick: function (iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx) {
        var fieldName = iView.getGridColumns()[iColIdx].dataIndex;
        if(fieldName === 'id'){
            this.onItemEdit(iView, iRowIdx);
        }
    },

    /** кнопока редактирования.
     *
     * @param grid
     * @param record
     * @param element
     *
     */
    onItemEdit: function (grid, record) {
        var rec = grid.getStore().getAt(record);
        var win, rec = grid.getStore().getAt(record);

        win = Ext.create('Pricer.view.price.window.PriceWindowEdit', {
            priceId: parseInt(rec.get('id')),
            items:[{
                xtype: 'priceform'
            }]
        });

        win.show();
        win.on('dataSave',
            function (){
                grid.getStore().load();
            });
    },

    /** кнопока редактирования.
     *
     * @param grid
     * @param record
     * @param element
     *
     */

    onItemDelete: function (grid, record, element, rowIndex, e, eOpts) {
        var win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit',
            {
                height: 150,
                title: 'Удаление предложения',
                html: 'Вы уверены что хотите удалить предложение?',
                width: 350,
                bodyPadding: 20,
                buttons: [
                    {
                        text: 'Удалить',
                        handler: function () {
                            var win = this.up('window');
                            Ext.Ajax.request({
                                url: '/price/itemDelete.json?itemId=' + eOpts.get('id'),
                                waitMsg: 'Удаление...',
                                reset: false,
                                success: function (response, opts) {
                                    // Закрыть окно
                                    win.hide();

                                    // перезагрузить таблицу
                                    grid.getStore().load();
                                },
                                failure: function (response, opts) {
                                    console.log('server-side failure with status code ' + response.status);
                                }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        handler: function () {
                            var win = this.up('window');
                            win.destroy();
                        }
                    }
                ]
            });
        win.show();
    },
    /**
     * Проставляем соответсвия
     * @param btn
     */
    onAddConform: function (btn) {
        var panel = btn.up('price'),
            name = '', id = 0,
            ids = {}, onSaveConform = false;
        var store = panel.getReferences().priceList.getStore();
        Ext.Array.each(store.getData().items, function (dataObj, index) {
            if (!dataObj.get('good_orig')) {
                ids[dataObj.get('id')] = index;
                onSaveConform = true;
            }
        });
        Pricer.view.extention.Ajax.request({
            targetMask: panel,
            params: {
                priceIds: JSON.stringify(ids)
            },
            url: '/price/getConform.json',
            waitMsg: 'Загрузка ...',
            reset: false,
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText),
                    itemFirst = {}, name = '', id = 0, expectConform = false;
                if (obj.success) {
                    if (obj.data) {
                        if (obj.data.length > 0) {
                            Ext.Array.each(obj.data, function (item, index) {
                                itemFirst = item[0];
                                if (item.length > 1) {
                                    store.getAt(ids[itemFirst.price_id]).set('good_orig_many', true);
                                }
                                expectConform = itemFirst.good == null;
                                name = expectConform ? 'ненайдено соответствий' : itemFirst.good;
                                id = itemFirst.good_id == null ? 0 : itemFirst.good_id;
                                store.getAt(ids[itemFirst.price_id]).set('good_orig', name);
                                store.getAt(ids[itemFirst.price_id]).set('good_orig_id', id);
                                store.getAt(ids[itemFirst.price_id]).set('expectConform', expectConform);
                                store.getAt(ids[itemFirst.price_id]).set('isEditable', true);
                            });
                            if (onSaveConform) {
                                panel.getReferences().onSaveConform.show()
                            }
                        } else {
                            Ext.Msg.alert('Проставить соответсвия', 'У всех Прайсов есть соответствия');
                        }
                    }
                }
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    /**
     * Обработчик события перед редактированием соответсвия в гриде
     *
     * @param editor
     * @param e
     * @return {boolean}
     */
    beforeEdit: function (editor, e) {
        var combo = e.column.getEditor(),
            orig_many = e.record.get('good_orig_many'),
            zarticle = e.record.get('zarticle'),
            goodName = e.record.get('good'),
            isEditable = e.record.get('isEditable'),
            expectConform = e.record.get('expectConform'),
            proxy = combo.getStore().getProxy();
        if (!isEditable) {
             return false
        }
        proxy.extraParams['zarticle'] = expectConform ? null : zarticle;
        if (expectConform) {
            e.record.set('good_orig', '');
        }
        combo.getStore().load();
    },
    /**
     * Обработчик события после редактирования соответсвия в гриде
     *
     * @param editor
     * @param e
     */
    afterEdit: function (editor, e) {
        var parentGrid = e.grid,
            combo = e.column.getEditor(),
            record = e.record,
            zarticle = record.get('zarticle'),
            rprice = record.get('rprice'),
            comboStore = combo.getStore();
        e.record.set('expectConform', false);
        if (combo.getValue() != 'ненайдено соответствий') {
            var comboRecord = comboStore.findRecord('good', combo.getValue());
            if (comboRecord) {
                e.record.set('good_orig_id', comboRecord.get('id'));
            } else if (
                combo.dirty &&
                combo.getValue() != null && combo.getValue().replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' ') != ''
            ) {
                var modal = Ext.create('Pricer.view.price.grid.PriceAddGoodComponent');
                modal.on('afterSucces',function () {
                    Pricer.view.extention.Ajax.request({
                        url: '/good/explodeGood.json',
                        method: 'GET',
                        params: {
                            goodName: combo.getValue(),
                            zarticle: zarticle,
                            rprice: rprice
                        },
                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            if (obj.success) {
                                record.set('good_orig_id', obj.data.id);
                                record.set('expectConform', false);
                                var win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit', {
                                    goodType: obj.data.goodType,
                                    goodId: obj.data.id,
                                    parentGrid: null,
                                    items: [{
                                        xtype: 'goodsformtype'
                                    }]
                                });
                                win.on('onMRCUpdated',
                                    function () {
                                        grid.getStore().load();
                                    });
                                win.show();
                            } else {
                                Ext.Msg.alert('Ошибка!', '<strong>Ошика сохранения!!!</strong><p>' + obj.message + '</p>');
                            }
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                            Ext.Msg.alert('Ошибка!', 'Ошика сохранения');
                        }
                    });
                });
                modal.addGood(combo, zarticle, rprice);
            }
        }
    },

    /**
     * Выгрузить в csv
     *
     * @param btn
     */

    unloadInCsv: function (btn) {
        var filterParams = btn.up('grid').getStore().getProxy().extraParams;
        var str = '/price/download.json?data='+Ext.JSON.encode(Object.assign({},filterParams));
        window.location = str;
    },

    downloadFromCsv: function (btn) {
        win = Ext.create('Pricer.view.price.window.PriceWindowDownloadfromcsv', {
            items:[{
                xtype: 'priceloadcsv'
            }]
        });

        win.show();
    }
});
