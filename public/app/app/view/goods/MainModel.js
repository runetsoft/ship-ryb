/**
 * This class is the view model for the Goods view of the application.
 */
Ext.define('Pricer.view.goods.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.goods.main',
    data: {
        moduleName: 'goods',
        useNumber: 0
    }
});
