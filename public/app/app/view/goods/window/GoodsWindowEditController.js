/**
 * Created by swds.dll on 18.04.2017.
 */
Ext.define('Pricer.view.goods.window.GoodsWindowEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.window.edit',
    requires: [
        'Pricer.view.goods.forms.GoodsFormEdit',
        'Pricer.view.goods.forms.GoodsFormTypesEdit'
    ],

    /**
     * Какую форму показать?
     *
     */
    onShowForm: function (combo, records, eOpts) {
        var win = this.view,
            goodId = win.goodId,
            goodType = win.goodType,
            attributes = [],
            newForm;
        if (win.lookupReference('goodform')) {
            win.lookupReference('goodform').destroy();
        }
        Ext.Ajax.request({
            url: '/good/getGoodTypeFormData.json',
            method: 'get',
            params: {
                goodId: goodId,
                goodTypeId: goodType
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                attributes = JSON.parse(obj.data.structure);
                newForm = Ext.create('Pricer.view.goods.forms.GoodsFormEdit', {
                    url: '/good/getGoodInfo.json',
                    goodId: goodId,
                    goodType: goodType,
                    attributes: attributes,
                    brandClass: obj.data.brandClass,
                    modelClass: obj.data.modelClass
                });
                win.add(newForm);
            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }
});
