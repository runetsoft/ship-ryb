/**
 * Created by babkin on 30.03.2017.
 */
Ext.define('Pricer.view.goods.MainController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.main',
    requires: [
        'Pricer.view.goods.MainModel'
    ],
    /**
     * Показ инфо о товаре при двойном клике по строчке.
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */
    onItemSelected: function ( grid , record , element , rowIndex , e , eOpts ) {
        var win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit', {
            title: 'Список прайсов',
            width: 1320,

            viewConfig: {
                trackOver: false,
                stripeRows: false,
                enableTextSelection: true
            },
                items:[{
                    xtype: 'goodInfoList',
                    goodId: record.get('id')
                }]
        });
        win.show();
    },
    /**
     * Очистить все МРЦ
     */
    clearMRC: function() {
        Ext.MessageBox.confirm(
            'Очистка МРЦ',
            'Вы уверены',
            'onClearMRCConfirm',
            this
        );
    },
    /**
     * Удалить все товары без соответствий
     */
    dellAllGoods: function() {
        Ext.MessageBox.confirm(
            'Удаление всеx товары без соответствий',
            'Вы уверены',
            'onDellAllGoodsConfirm',
            this
        );
    },
    /**
     * Обработчик кнопки "Очистить МРЦ"
     *
     * @param choice
     *
     * @todo Переделать на промисы
     */
    onClearMRCConfirm: function (choice) {
        if (choice === 'yes') {
            Ext.getBody().mask('Загрузка ...');
            Ext.Ajax.request({
                url: '/good/clearMrc.json',
                success: function (response){
                    var obj = Ext.decode(response.responseText);
                    Ext.getBody().unmask();
                    if (obj.success) {
                        Ext.MessageBox.alert(
                            'Очистка МРЦ',
                            'Успешно очищено!',
                            this
                        );
                    } else {
                        Ext.MessageBox.alert(
                            'Очистка МРЦ',
                            'Ошибка сервера!',
                            this
                        );
                    }

                },
                failure: function (response) {
                    Ext.getBody().unmask();
                    Ext.MessageBox.alert(
                        'Ошибка сервера',
                        response.status,
                        this
                    );
                }
            });
        }
    },
    /**
     * Обработчик кнопки "Обновить МРЦ из файла"
     */
    updateMRC: function (btn) {
        var win;
        win = Ext.create('Pricer.view.goods.windows.GoodsWindowsUpdateMRC', {
            title: 'Обновить МРЦ из файла',
            items:[{
                xtype: 'goodsformupdatemrc'
            }]
        });
        win.on('onMRCUpdated',
            function () {
                this.fireEvent('onMRCUpdated');
            },
            btn
        ),
        win.show();
    },
    /**
     * Обработчик кнопки "Удалить все товары без соответствий"
     *
     * @param choice
     */
    onDellAllGoodsConfirm: function (choice) {
        if (choice === 'yes') {
            Ext.getBody().mask('Загрузка ...');
            Ext.Ajax.request({
                url: '/adm/cmd.php?mod=3&do=del_clear',
                success: function (response, opts) {
                    Ext.getBody().unmask();
                    Ext.MessageBox.alert(
                        'Удалить все товаров без соответствий',
                        'Успешно удалено!',
                        this
                    );
                },
                failure: function (response, opts) {
                    Ext.getBody().unmask();
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    },
    onAddGood: function (btn){
        var win;
        win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit', {
            title: 'Добавить новый товар',
            goodId: '0',
            items:[{
                xtype: 'goodsformtype'
            }]
        });
        win.on('onMRCUpdated',
            function () {
                this.fireEvent('onMRCUpdated');
            },
            btn
        ),
        win.show();

    }
});
