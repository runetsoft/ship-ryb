Ext.define('Pricer.view.goods.grid.GoodsGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.goods-grid-goodsgrid',

    /**
     * переход по кликам на ячейки грида GoodGrid
     *
     * @param iView
     * @param iCellEl
     * @param iColIdx
     * @param iRecord
     * @param iRowEl
     * @param iRowIdx
     */
    onCellClick: function (iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx) {
        var fieldName = iView.getGridColumns()[iColIdx].dataIndex;
        if (fieldName === 'id' && iView.grid.getXType() === "goodsList") {
            this.onItemEdit(iView, iRowIdx);
        }
    },

    /**
     * переход по кликам на ячейки грида GoodInfoGrid
     *
     * @param iView
     * @param iCellEl
     * @param iColIdx
     * @param iRecord
     * @param iRowEl
     * @param iRowIdx
     */
    onInfoGridCellClick: function (iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx) {
        if (iView.grid.getXType() === "goodInfoList") {
            switch (iColIdx) {
                case 0:
                    window.open("#price/filter/id:" + iRecord.id, '_blank');
                    break;
                case 1:
                    if (iRecord.data.photo) {
                        Ext.create('Ext.window.Window', {
                                height: 600,
                                cls: 'price-photo-win',
                                html: '<div class="price-grid-popup-photo"><img src="' + iRecord.data.photo + '" /></div>',
                            }
                        ).show();
                    } else {
                        Ext.Msg.show({
                            title: 'Фото прайса.',
                            message: 'У прайса нет фото!',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                    }
                    break;
            }
        }
    },

    /**
     * Получаем данные о товаре
     * @param grid
     * @param record
     * @param element
     */

    onItemEdit: function (grid, record) {
        var rec = grid.getStore().getAt(record),
            win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit', {
                goodType: parseInt(rec.get('type')),
                goodId: parseInt(rec.get('id')),
                parentGrid: grid,
                items: [{
                    xtype: 'goodsformtype'
                }]
            });
        win.on('onMRCUpdated',
            function () {
                grid.getStore().load();
            });
        win.show();
    },

    /**
     *  Удаляем товар.
     * @param grid
     * @param record
     * @param element
     * @param rowIndex
     * @param e
     * @param eOpts
     */
    onItemDelete: function ( grid , record , element , rowIndex , e , eOpts ) {
        var win = Ext.create('Pricer.view.goods.windows.GoodsWindowsEdit',
            {
                height: 150,
                title: 'Удаление товара',
                html: 'Вы уверены что хотите удалить товар?',
                width: 350,
                bodyPadding: 20,
	            buttons: [
                    {
                        text: 'Удалить',
                        handler: function() {
                            var win = this.up('window');
                            Ext.Ajax.request({
                                 url: '/good/itemDelete.json?goodId=' + eOpts.get('id'),
                                 waitMsg:'Удаление...',
                                 reset: false,
                                 success: function(response, opts) {
                                     // Закрыть окно
                                        win.hide();

                                     // перезагрузить таблицу
                                        grid.getStore().load();
                                 },
                                 failure: function(response, opts) {
                                    console.log('server-side failure with status code ' + response.status);
                                 }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        handler: function() {
                            var win = this.up('window');
                            win.destroy();
                        }
                    }
                ]
            });
        win.show();
    },

    updateGoodImageByPrice: function (grid, record, element, rowIndex, e, eOpts) {
        Ext.Msg.show({
            title:'Использовать как картинку товара',
            message: 'Вы точно хотите изменить картинку товара?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Pricer.view.extention.Ajax.request({
                        targetMask: grid,
                        url: '/good/updateGoodPhotoByPrice.json',
                        method: 'POST',
                        params: {
                            priceId: eOpts.get('id')
                        },
                        waitMsg: 'Сохранение...',
                        success: function (response, opts) {
                            var result = JSON.parse(response.responseText);
                            if (!result.success) {
                                Ext.Msg.show({
                                    title:'Не удалось обновить фото товара',
                                    message: result.message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.ERROR
                                });
                            } else {
                                Ext.Msg.show({
                                    title:'Обновление тото товара',
                                    message: 'Фото товара успешно обновлено. ',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.INFO
                                });
                            }
                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                } else {
                    console.log('No pressed');
                }
            }
        });
    }
});
