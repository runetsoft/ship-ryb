/**
 * Created by swds.dll on 18.04.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsTypeEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.type.edit',

    onChange: function (obj, value) {
        var win = obj.up('window');
        win.goodType = value;
        win.fireEvent('formShow');
    },

    onBeforeRenderer: function (obj, value) {
        var win = obj.up('window');
        var radioObj = win.lookupReference('typeform');
        var newValue = win.goodType;
        radioObj.getViewModel().set('type', newValue);
        var goodTypes = radioObj.getReferences().goodType;
        goodTypes.store.reload({
            callback: function () {
                goodTypes.setValue(win.goodType);
            }
        });
    }
});
