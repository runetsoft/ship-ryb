/**
 *
 */
Ext.define('Pricer.view.goods.forms.GoodsFormUpdateMRCController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.form.update.mrc',

    /**
     *
     */
    onUpdateMRC: function (btn) {
        var win = btn.findParentByType('window'),
            form = btn.findParentByType('form');
        if (form.isValid()) {
            form.submit({
                url: '/good/updateMrc.json',
                method: 'POST',
                waitMsg: 'Обновление МРЦ ...',
                success: function (fp, o) {
                    win.fireEvent('onMRCUpdated');
                    win.destroy();
                    Ext.MessageBox.alert(
                        'Обновление МРЦ',
                        'Успешно обновлено!',
                        this
                    );
                },
                failure: function(response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    }
});