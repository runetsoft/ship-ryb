/**
 * Created by swds.dll on 30.03.2017.
 */
Ext.define('Pricer.view.goods.forms.GoodsFormEditController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.form.edit',

    /**
     * Заполнение формы редактирования.
     *
     * @param form
     **/
    onFormRender: function ( form ) {
        Ext.Ajax.request({
            url: form.url,
            method: 'get',
            params: {
                goodId: form.goodId,
                goodType: form.goodType
            },
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.data) {
                    form.getForm().setValues(obj.data);
                }else{
                    form.getForm().setValues({'good_id': form.goodId});
                }

            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    onSaveData:function(obj, value) {
        var win = obj.up('window'),
            form = obj.up('form');

        var data = form.getForm().getValues();
        data.type = win.goodType;
        form.submit({
            url: '/good/updateGood.json',
            method: 'POST',
            params: {request: 'save', data: Ext.util.JSON.encode(data)},
            waitMsg: 'Сохранение...',
            reset: false,
            success: function (response, opts) {
                if (win.parentGrid) {
                    win.parentGrid.getStore().load();
                }
                win.destroy();
            },
            failure: function (me, opts) {
                var response = Ext.decode(opts.response.responseText);
                console.log(response);
            }
        });
    }
});
