/**
 * Created by yu.khristodorov on 19.04.2017.
 */

Ext.define('Pricer.view.goods.form.GoodsTypeEditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.goods.type.edit',
    data: {
        type: Pricer.view.extention.Goods.GOODS_DISK_TYPE
    }
});