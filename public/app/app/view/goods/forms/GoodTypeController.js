Ext.define('Pricer.view.goods.forms.GoodTypeController', {
    extend: 'Pricer.view.lib.BaseController',
    alias: 'controller.goods.form.goodtype',

    onItemDelete: function (grid, rowIndex) {
        if (rowIndex == undefined) {
            return;
        }
        grid.getStore().removeAt(rowIndex);
    },

    onAddNewGoodType: function (btn) {
        var isValid = true,
            win = btn.up('window'),
            form = win.lookupReference('addgoodtypeform'),
            grid = win.lookupReference('addgoodtypegrid'),
            formData = form.getValues(),
            gridData = grid.getStore().getData().items,
            requestData;

        // Form data validation
        if (formData.name != '' && formData.title != '') {
            requestData = formData;
        } else {
            isValid = false;
        }

        // Grid data validation
        requestData.columns = []
        Ext.Array.forEach(gridData, function (item) {
            if ((item.data.field_type == "dictionary" || item.data.field_type == "dictionarymultiselect")
                && item.data.dictionary_name) {
                item.data.column = item.data.field_type + '_'
                    + item.data.dictionary_name.replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
                        return "_" + y.toLowerCase()
                    }).replace(/^_/, "");

            }
            if (item.data.column != '' && item.data.title != '' && item.data.field_type != '') {
                requestData.columns.push(item.data);
            } else {
                isValid = false;
            }
        });

        if (!isValid) {
            Ext.Msg.alert('Ошибка', 'Надо заполнить все поля!');
        } else {
            Pricer.view.extention.Ajax.request({
                targetMask: win,
                msgMask: 'Добавляется новый тип товара ...',
                url: '/good/addNewGoodType.json',
                method: 'POST',
                params: {
                    data: Ext.encode(requestData)
                },
                success: function (response, opts) {
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.success) {
                        Ext.Msg.alert('Успешно', responseData.message);
                        win.destroy();
                    } else {
                        Ext.Msg.alert('Ошибка', responseData.message);
                    }
                },
                failure: function (me, opts) {
                    Ext.Msg.alert('Ошибка', 'Возникли проблемы с сервером. Обратитесь к администратору!');
                    var response = Ext.decode(opts.response.responseText);
                    console.log(response);
                }
            });
        }
    },

    onEditGoodType: function (btn, context) {
        var isValid = true,
            win = btn.up('window'),
            grid = win.lookupReference('editgoodtypegrid'),
            gridData = grid.getStore().getData().items,
            requestData = {
                columns: [],
                good_type: win.goodType,
                good_type_id: win.goodTypeId
            };

        // Grid data validation
        Ext.Array.forEach(gridData, function (item) {
            if ((item.data.field_type == "dictionary" || item.data.field_type == "dictionarymultiselect")
                && item.data.dictionary_name) {
                item.data.column = item.data.field_type + '_'
                    + item.data.dictionary_name.replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
                        return "_" + y.toLowerCase()
                    }).replace(/^_/, "");

            }
            if (item.data.column != '' && item.data.title != '' && item.data.field_type != '') {
                requestData.columns.push(item.data);
            } else {
                isValid = false;
            }
        });

        if (!isValid) {
            Ext.Msg.alert('Ошибка', 'Надо заполнить все поля!');
        } else {
            Pricer.view.extention.Ajax.request({
                targetMask: win,
                msgMask: 'Редактирование типа товара ...',
                url: '/good/updateGoodType.json',
                method: 'POST',
                params: {
                    data: Ext.encode(requestData)
                },
                success: function (response, opts) {
                    var responseData = Ext.decode(response.responseText);
                    if (responseData.success) {
                        Ext.Msg.alert('Успешно', responseData.message);
                        win.destroy();
                    } else {
                        Ext.Msg.alert('Ошибка', responseData.message);
                    }
                },
                failure: function (me, opts) {
                    Ext.Msg.alert('Ошибка', 'Возникли проблемы с сервером. Обратитесь к администратору!');
                    var response = Ext.decode(opts.response.responseText);
                    console.log(response);
                }
            });
        }
    },

    onGoodTypeChanged: function (select) {
        var selectedModel = select.up('grid').getSelectionModel().getSelection()[0];

        if ((select.value == "dictionary" || select.value == "dictionarymultiselect")
            && selectedModel.data.dictionary_name) {
            selectedModel.data.column = select.value + '_'
                + selectedModel.data.dictionary_name.replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
                    return "_" + y.toLowerCase()
                }).replace(/^_/, "");
        }
    },

    onFieldTypeSelect: function (select) {
        var selectedModel = select.up('grid').getSelectionModel().getSelection()[0];
        if ((selectedModel.data.field_type == "dictionary" || selectedModel.data.field_type == "dictionarymultiselect")
            && select.value) {
            selectedModel.data.column = selectedModel.data.field_type + '_'
                + select.value.replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
                    return "_" + y.toLowerCase()
                }).replace(/^_/, "");
        }
    }

});
