/**
 * Created by yu.khristodorov on 25.05.2017.
 */
/**
 * Created by yu.khristodorov on 17.05.2017.
 */

Ext.define('Pricer.view.main.forms.FormTextfieldNumber', {
    extend: 'Ext.form.field.Text',
    xtype: 'textfieldnumber',

    setValue : function (value) {
        var renderValue;
        if (value) {
            renderValue = value.replace(".00", "");
        }
        this.callParent([renderValue]);
    }
});