/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 */
Ext.define('Pricer.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',
    requires: [
        'Pricer.view.main.MainModel',
        'Pricer.view.extention.QueryDispatcher'
    ],
    routes: {
        ':module': 'onRoute',
        ':module/:action': 'onRoute',
        ':module/:action/:param': {
            action: 'onRoute',
            conditions: {
                ':param': '(.+)'
            }
        }
    },

    /**
     * Обрабатывает маршрут, передает данные в QueryDispatcher
     *
     * tmp: http://ship/app/#goods/filter/id:1;good:213 адреса делать примерно такого вида
     *
     * @param module Имя модуля (вкладки)
     * @param action Экшен
     * @param param Параметр
     */
    onRoute: function (module, action, param) {
        Pricer.view.extention.QueryDispatcher.initUrl(module, action, decodeURI(param));
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            this.getViewModel().set('name', 'Test');
        }
    },

    onAddGoodType: function (btn) {
        var fullScreenWindow = Ext.create('Pricer.view.export.window.AddGoodTypeWindow', {
            title: 'Добавить новый тип товара'
        });
        fullScreenWindow.show();
    }
});
