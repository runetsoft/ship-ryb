/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 */
Ext.define('Pricer.view.main.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',
    onLoginClick: function (form){
        var values = form.up('form').getForm().getValues();
        Ext.Ajax.request({
            url: '/api/login.json?login=' + values.login +'&password='+values.password+'&time24='+values.time24,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                // Если пришли данные о пользователе (указаны верные данные при авторизации)
                if (obj.success){
                    // выставляем флаг авторизации и перезагружаем страницу
                    localStorage.setItem("LoggedIn", true);
                    window.location.reload();
                }else{
                    // Выводим окно с ошибкой
                    Ext.Msg.alert('Ошибка авторизации','Авторизация не удалась: </br>Возможно вы указали неверный пароль.');
                }
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    }
});
