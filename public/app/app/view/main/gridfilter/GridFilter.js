Ext.define('Pricer.view.main.gridfilter.GridFilter', {
    extend: 'Ext.form.Panel',
    xtype: 'gridfilter',
    width: '100%',
    columnsCount: 4,
    grid: null,
    cls: 'grid-filter',
    action: 'filter',
    requires: [
        'Pricer.view.main.gridfilter.GridFilterController',
        'Pricer.view.main.gridfilter.GridFilterModel',
        'Pricer.view.extention.QueryDispatcher'
    ],
    controller: 'main-gridfilter-gridfilter',

    viewModel: {
        type: 'main-gridfilter-gridfilter'
    },
    fieldDefaults: {
        labelAlign: 'top'
    },
    initComponent: function () {
        Ext.apply(this, {
            items: this.generateFormFields()
            // ,
            // listeners: {}
        });
        this.callParent();
    },
    /**
     * Generate form fields by grid columns
     *
     * @returns {Array}
     */
    generateFormFields: function () {
        var columns = this.grid.getColumns(),
            columnsCount = this.columnsCount,
            formItems = [],
            row = [],
            formFieldIndex = 0;

        // Add input fields.
        for (var index in columns) {
            var column = columns[index];
            if (!column.hasOwnProperty('filterField')) {
                continue;
            }

            if (column.period) {
                var baseField = [
                    {
                        name: '>=' + column.dataIndex,
                        xtype: 'textfield',
                        width: (50 / columnsCount) + '%',
                        fieldLabel: column.text + ' с',
                        margin: 10
                    },
                    {
                        name: '<' + column.dataIndex,
                        xtype: 'textfield',
                        width: (50 / columnsCount) + '%',
                        fieldLabel: column.text + ' по',
                        margin: 10
                    }
                ]
            } else {
                var baseField = [{
                    name: (column.condition) ? column.condition : column.dataIndex,
                    xtype: 'textfield',
                    width: (100 / columnsCount) + '%',
                    fieldLabel: column.text,
                    margin: 10
                }]
            }

            if (typeof(column.filterField) == 'string') {
                row = row.concat(Ext.apply(baseField[0], {xtype: column.filterField}));
                if (column.period) {
                    row = row.concat(Ext.apply(baseField[1], {xtype: column.filterField}));
                }
            } else if (typeof(column.filterField) == 'object') {
                row = row.concat(Ext.apply(baseField[0], column.filterField));
                if (column.period) {
                    row = row.concat(Ext.apply(baseField[1], column.filterField));
                }
            } else {
                row = row.concat(baseField);
            }
            // Add row and go to next row.
            if (formFieldIndex % columnsCount == columnsCount - 1) {
                formItems[formItems.length] = {
                    layout: 'hbox',
                    items: row
                };
                row = [];
            }
            ++formFieldIndex;
        }
        // Add last row if is not full.
        if (row.length != 0) {
            formItems[formItems.length] = {
                layout: 'hbox',
                items: row
            };
        }
        // Add buttons.
        formItems[formItems.length] = {
            items: [
                {
                    xtype: 'button',
                    text: 'Фильтровать',
                    scale: 'medium',
                    margin: 10,
                    scope: this,
                    handler: this.search
                },
                {
                    xtype: 'button',
                    text: 'Сбросить фильтр ',
                    scale: 'medium',
                    margin: 10,
                    scope: this,
                    handler: this.reset
                }]
        };
        return formItems;
    },
    /**
     * Call Grid search.
     */
    search: function () {
        Pricer.view.extention.QueryDispatcher.setUrl(this.action, this.getForm().getValues(), this.grid.module);

        this.grid.fireEvent('search', this.getForm().getValues());
    },
    /**
     * Reset form and call Grid reset.
     */
    reset: function () {
        Pricer.view.extention.QueryDispatcher.setModule(this.grid.module);
        this.getForm().reset();
        this.grid.fireEvent('reset');
    },
    listeners:{
        afterrender:function () {
            Pricer.view.extention.QueryDispatcher.getUrlData()
            .then(
                function (result) {
                    if (result.action === this.action && result.module === this.grid.module) {
                        var values = Object.assign({}, result.params);
                        this.getForm().setValues(values);
                        this.search();
                    }
                }.bind(this)
            );
        }
    }
});
