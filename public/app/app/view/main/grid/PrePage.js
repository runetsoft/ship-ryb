/**
 * Created by yu.khristodorov on 25.05.2017.
 */

Ext.define('Pricer.view.main.grid.PrePage', {
    extend: 'Ext.form.ComboBox',
    xtype: 'prepage',
    width: 100,
    grid: null,
    store: new Ext.data.ArrayStore({
        fields: ['id'],
        data: [
            ['10'],
            ['30'],
            ['50'],
            ['100'],
            ['500']
        ]
    }),
    mode: 'local',
    value: '50',
    listWidth: 100,
    triggerAction: 'all',
    displayField: 'id',
    valueField: 'id',
    editable: false,
    forceSelection: true,
    listeners: {
        change: function (combo) {
            this.grid.getStore().pageSize = combo.value;
            this.grid.getStore().load();
        }
    }
});
