Ext.define('Pricer.view.main.grid.PagingToolbar', {
    extend: 'Ext.PagingToolbar',
    xtype: 'pricertoolbar',
    requires: [
        'Pricer.view.main.grid.PagingToolbarController',
        'Pricer.view.main.grid.PagingToolbarModel'
    ],

    controller: 'main-grid-pagingtoolbar',
    viewModel: {
        type: 'main-grid-pagingtoolbar'
    },
    initComponent: function () {
        Ext.apply(this, {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Показано {0} - {1} из {2}'
        });
        this.callParent();
    }
});
