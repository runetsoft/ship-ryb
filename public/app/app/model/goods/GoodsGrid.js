Ext.define('Pricer.model.goods.GoodsGrid', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'int' },
        { name: 'dt_insert', type: 'date' },
        { name: 'dt_update', type: 'date' },
        { name: 'adm_id', type: 'int' },
        { name: 'good', type: 'auto' },
        { name: 'rprice', type: 'number' },
        { name: 'zarticle', type: 'auto' },
        { name: 'not_load', type: 'int' },
        { name: 'price_cnt', type: 'int' },
        { name: 'info', type: 'string' },
        { name: 'good_url', type: 'string' }

    ]
});
