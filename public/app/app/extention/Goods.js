/**
 * Created by yu.khristodorov on 20.04.2017.
 */
Ext.ns('Pricer.view.extention');
Ext.define('Pricer.view.extention.Goods',{
    singleton:true,
    /**
     * var GOODS_TIRES_TYPE - значение типа Шин
     * var GOODS_DISK_TYPE - значение типа Дисков
     */
    GOODS_DISK_TYPE: 1,
    GOODS_TIRES_TYPE: 2,
    GOODS_TIRES_MOTO_TYPE: 3
});
