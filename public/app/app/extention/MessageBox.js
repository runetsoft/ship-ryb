Ext.ns('Ext.extension.MessageBox');

/**
 * @Ext.extension.Promise
 * Promise for ExtJs.
 * @singleton
 */
Ext.extension.MessageBox = function() {
    // Обход бага Ext JS 3.x - при вызове из модального окна, диалог оказывается под ним.
    var BRING_TO_FRONT_DELAY = 50;
    return {
        /**
         * Обещание стандартного диалога.
         *
         * @param {string} title Заголовок окна.
         * @param {string} msg   Сообщение.
         * @param {Object} value Что передать дальше по цепи?
         *
         * @returns {*|Promise|c} Объект обещания.
         */
        confirmPromise: function (title, msg, value) {
            return new Promise(function(resolve, reject) {
                var mdConfirm = Ext.Msg.confirm(title, msg, function(button) {
                    if (button == 'yes') {
                        resolve(value);
                    } else {
                        reject(value);
                    }
                });
                // Обход бага Ext JS 3.x - при вызове из модального окна, диалог оказывается под ним.
                Ext.defer(function() {
                    Ext.WindowMgr.bringToFront(mdConfirm.getDialog());
                }, BRING_TO_FRONT_DELAY);
            });
        },

        /**
         * Показать предупреждающее сообщение.
         * Завёрнуто в промис для использования в цепочке - код асинхронный.
         *
         * @param {string} mess           Предупреждающее сообщение.
         * @param {string} title          Заголовок окна, по-умолчанию "Ошибка"
         * @param {string} icon           Название иконки из Ext.MessageBox - ERROR|INFO|WARNING, ERROR по-умолчанию.
         * @param {string} defaultAction  Дальше по цепочке пойдёт resolve или reject, по-умолчанию reject.
         * @param {Object} value          Что передать дальше по цепи?
         *
         * @return {*|Promise|c} Обещание.
         */
        alertPromise: function (mess, title, icon, defaultAction, value) {
            title = title || 'Ошибка';
            icon = icon || 'ERROR';
            defaultAction = defaultAction || 'reject';
            return new Promise(function(resolve, reject) {
                var md = Ext.MessageBox.show({
                    title: title,
                    msg: mess,
                    icon: Ext.MessageBox[icon],
                    buttons: Ext.Msg.OK,
                    fn: function() {
                        if (defaultAction === 'reject') {
                            reject(value);
                        } else {
                            resolve(value);
                        }
                    }
                });
                Ext.defer(function() {
                    Ext.WindowMgr.bringToFront(md.getDialog());
                }, BRING_TO_FRONT_DELAY);
            });
        },

        /**
         * Обещание стандартного диалога c ответом.
         * Завёрнуто в промис для использования в цепочке - код асинхронный.
         * @param {string} title          Заголовок окна, по-умолчанию "Ошибка"
         * @param {string} mess           Предупреждающее сообщение.
         * @param {Object} value          Что передать дальше по цепи?
         *
         * @return {*|Promise|c} Обещание.
         */
        promptPromise: function (title, mess, value) {
            return new Promise(function(resolve, reject) {
                var mdPrompt = Ext.MessageBox.prompt(
                    title,
                    mess,
                    function (btn, promptText) {
                        if (btn == 'ok') {
                            resolve(promptText, value);
                        } else {
                            reject(value);
                        }
                    }, this, true);
                // Обход бага Ext JS 3.x - при вызове из модального окна, диалог оказывается под ним.
                Ext.defer(function () {
                    Ext.WindowMgr.bringToFront(mdPrompt.getDialog());
                }, BRING_TO_FRONT_DELAY);
            });
        }
    }
}();
