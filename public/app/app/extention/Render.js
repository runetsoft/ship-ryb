/**
 * Класс для вспомогательных функций рендеров.
 */
Ext.ns('Pricer.view.extention');
Ext.define('Pricer.view.extention.Render',{
    singleton:true,
    /**
     * Рендер колонки с номером, если пустойе то выводим 0.
     *
     * @param value
     * @param p
     * @param model
     * @returns {*|number}
     */
    renderNumber: function (value, p, model) {
        return value || 0;
    },
    renderString: function (value, p, model) {
        return (value) ? ((value == 0 ) ? "" : value) : "";
    },
    renderBind: function (value, p, model) {
        return value;
    },
    renderDictionary: function (dictionaryName) {
        var res = "";
        Ext.each(dictionaryName.split("_"), function (value, key) {
            res += value.charAt(0).toUpperCase() + value.slice(1);
        });
        return res;
    }
});