/**
 * Created by k.kochinyan on 26.05.2017.
 */

Ext.ns('Pricer.view.extention');
Ext.define('Pricer.view.extention.TemplateEngine', {
    singleton:true,
    /**
     * Меняет текст по шаблону и возвращает его.
     *
     * @param tpl
     * @param data
     * @returns {*}
     */
    convert: function (tpl, data) {
        var re = /<%([^%>]+)?%>/g, match;
        while (match = re.exec(tpl)) {
            tpl = tpl.replace(match[0], data[match[1]])
        }
        return tpl;
    }
});