Ext.ns('Ext.extension.Promise');

/**
 * @Ext.extension.Promise
 * Promise for ExtJs.
 * @singleton
 */
Ext.extension.Promise = function() {
    return {
        /**
         * Отображение сообщения об ошибке или успехе операции.
         *
         * @param {object} resp      Ответ сервера.
         * @param {string} [success] Заголовок по умолчанию в случае успешной операции.
         * @param {string} [fail]    Заголовок по умолчанию в случае ошибки.
         *
         * @returns {*|Promise|c} Объект обещания.
         */
        showMessage: function (resp, success, fail) {
            if (!resp) {
                resp = {};
            }
            if (resp.failureType == 'server' && Ext.MessageBox.isVisible()) {
                // Уже отображается ошибка директа, не будем ее перебивать
                return;
            }
            if ( typeof resp.success == 'function' ) {
                resp = resp.result || {};
            }
            if (resp.message && resp.stack) {
                return Ext.extension.Promise.showException(resp);
            }

            Ext.extension.Promise.showResponse(resp, success, fail);
        },
        /**
         * Отображает сообщение о ошибке.
         *
         * @param {object} e Объект исключение.
         *
         * @returns {*|Promise|c} Объект обещания.
         */
        showException: function(e) {
            var message = (e.stack || e.message) || 'Неизвестная ошибка';

            return Ext.extension.MessageBox.alertPromise(t(message), 'Ошибка');
        },
        /**
         * Отображение сообщения о результате операции.
         *
         * @param {object} resp      Ответ сервера.
         * @param {string} [success] Заголовок по умолчанию в случае успешной операции.
         * @param {string} [fail]    Заголовок по умолчанию в случае ошибки.
         *
         * @returns {*|Promise|c} Объект обещания.
         */
        showResponse: function(resp, success, fail) {
            success = success || 'Документы и сведения направлены успешно';
            fail = fail || 'Ошибка';
            var title = resp.success ? success : fail;
            var icon = resp.success ? 'INFO' : 'ERROR';
            var msg = resp.message || resp.msg;
            var warning = resp.warnings || null;
            if (!msg) {
                msg = resp.success ? success : 'Неизвестная ошибка';
            }
            if (warning && warning.length) {
                msg += '<br/>\n<b>Внимание:</b> ' + warning.join('<br/>\n');
                icon = 'WARNING';
            }

            // Замена перевода на новую строку, на <br>.
            msg = msg.replace(/(?:\r\n|\r|\n)/g, '<br />');

            return Ext.extension.MessageBox.alertPromise(t(msg), t(title), icon);
        },

        /**
         * @deprecated Только для обратной совместимости. Метод будет удален.
         * Используйте: Ext.extension.MessageBox.confirmPromise
         *
         * Обещание стандартного диалога.
         *
         * @param {string} title Заголовок окна.
         * @param {string} msg   Сообщение.
         * @param {Object} value Что передать дальше по цепи?
         *
         * @returns {*|Promise|c} Объект обещания.
         */
        message: Ext.extension.MessageBox.confirmPromise,

        /**
         * @deprecated Только для обратной совместимости. Метод будет удален.
         * Используйте: Ext.extension.MessageBox.alertPromise
         *
         * Показать предупреждающее сообщение.
         * Завёрнуто в промис для использования в цепочке - код асинхронный.
         *
         * @param {string} mess           Предупреждающее сообщение.
         * @param {string} title          Заголовок окна, по-умолчанию "Ошибка"
         * @param {string} icon           Название иконки из Ext.MessageBox - ERROR|INFO|WARNING, ERROR по-умолчанию.
         * @param {string} defaultAction  Дальше по цепочке пойдёт resolve или reject, по-умолчанию reject.
         * @param {Object} value          Что передать дальше по цепи?
         *
         * @return {*|Promise|c} Обещание.
         */
        alert: Ext.extension.MessageBox.alertPromise
    };
}();
