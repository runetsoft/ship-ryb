Ext.ns('Pricer.view.extention');
Ext.define('Pricer.view.extention.QueryDispatcher', {
    extend: 'Ext.app.Controller',
    urlData: {},
    singleton: true,
    listeners: {
        returnURL: function (urlData) {
            this.urlData = urlData;
        }
    },

    /**
     * возвращает параметры url запроса, если их нет, то обращается к событию
     *
     * @return promise
     */
    getUrlData: function () {
        var me = this;
        return new Promise(function(resolve, reject) {
            if (Ext.Object.isEmpty(me.urlData)){
                me.on('returnURL',function(data){
                    var actionWithParams = {"module": data.module, "action": data.action, "params": data.params};
                    resolve(actionWithParams);
                });
            }
            else {
                resolve(me.urlData);
                // me.urlData = {};
            }
        });
    },

    /**
     * getter method, используйте его для установки значений url
     *
     * @param module
     * @param action
     * @param param
     */
    initUrl: function (module, action, param) {
        var params = this.splitParam(param);
        var urlData = {"module": module, "action": action, "params": params};
        this.fireEvent('returnURL',urlData);
    },

    /**
     * разбивает строку на массив
     *
     * @param param
     * @returns {Array}
     */
    splitParam: function (param) {
        if (param) {
            var params = param.split(";");
            var filterParams = [];
            Ext.each(params, function (value) {
                var tmpVariables = value.split(":");
                filterParams[tmpVariables[0]] = tmpVariables[1];
            });
            return filterParams;
        }
    },

    /**
     * устанавливает модуль в адресную строку
     *
     * @param module
     */
    setModule: function (module) {
        var hash = '#'+module;
        this.redirectTo(hash);
    },

    /**
     * setter method, формирует url адресс
     *
     * @param module
     * @param action
     * @param params
     */
    setUrl: function (action, params, module) {
        var hash = '#'+module;
        if (params)
        {
            var param = this.joinParams(params);
        }
        if (action && param)
        {
            hash = hash+"/"+action+"/"+encodeURI(param);
        }
        this.redirectTo(hash);
    },

    /**
     * преобразует массив в строку с параметрами
     *
     * @param params
     * @returns {string}
     */
    joinParams: function (params) {
        var arrWithDelimetr = [];
        var arr = Object.entries(params);
        Ext.Object.each(arr, function(key, val){
            if (val[1] !== "")
            {
                arrWithDelimetr[key] = val.join(':');
            }
        });
        arrWithDelimetr = arrWithDelimetr.filter(function(entry) { return entry.trim() != ''; });
        return arrWithDelimetr.join(';');
    }
});