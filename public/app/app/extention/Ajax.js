/**
 * Created by k.kochinyan on 18.05.2017.
 */
Ext.ns('Pricer.view.extention');
Ext.define('Pricer.view.extention.Ajax', {
    extend: 'Ext.data.Connection',
    requires: [
        'Ext.data.Connection'
    ],
    singleton: true,
    msgMask: 'Загрузка',
    loadingMask: null,
    timeout: 0,
    constructor: function (config) {
        this.callParent([config]);
        this.on("beforerequest", function (me, options) {
            if (typeof options.targetMask == 'object') {
                this.targetMask = options.targetMask;
            }
            if (options.msgMask != undefined) {
                this.msgMask = options.msgMask;
            }
            if (this.loadingMask == null) {
                this.loadingMask = new Ext.LoadMask({
                    msg: this.msgMask,
                    target: this.targetMask
                });
            }
            this.loadingMask.show();
        }, this);
        this.on("requestcomplete", function () {
            this.hideMask();
        }, this);
        this.on("requestexception", function () {
            this.hideMask();
        }, this);
    },
    /**
     * Скрывает маску.
     */
    hideMask: function () {
        if (this.loadingMask != null) {
            this.loadingMask.hide();
            this.loadingMask = null;
        }
    }
});