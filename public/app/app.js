/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'Pricer',

    extend: 'Pricer.Application',

    requires: [
		'Pricer.view.extention.Render',
        'Pricer.view.extention.Goods',
        'Pricer.view.main.Main',
        'Pricer.view.main.Login',
		'Pricer.view.extention.Options',
        'Pricer.view.goods.Main',
        'Pricer.view.price.Main',
        'Pricer.view.suppliers.Main',
        'Pricer.view.main.grid.PagingToolbar',
        'Ext.form.field.Tag'
    ]

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //

    //-------------------------------------------------------------------------
    // Most customizations should be made to Pricer.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
