<?php /* Smarty version 2.6.16, created on 2017-03-24 15:52:57
         compiled from _tools/list_sort.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup', '_tools/list_sort.tpl', 13, false),array('function', 'html_image', '_tools/list_sort.tpl', 13, false),)), $this); ?>
<!-- sort -->

<?php if ($this->_tpl_vars['sort'] == $this->_tpl_vars['index']): ?>
    <span <?php echo smarty_function_popup(array('width' => '100','text' => "отсортировано по возрастанию"), $this);?>
><?php echo smarty_function_html_image(array('file' => "img/icon/s_desc.png"), $this);?>
</span><a href="<?php echo $this->_tpl_vars['url']; ?>
&sort=<?php echo $this->_tpl_vars['index']+1; ?>
"
    <?php echo smarty_function_popup(array('width' => '100','text' => "сортировать по убыванию"), $this);?>
><i><?php echo $this->_tpl_vars['text']; ?>
</i></a>
<?php elseif ($this->_tpl_vars['sort'] == $this->_tpl_vars['index']+1): ?>
    <span <?php echo smarty_function_popup(array('width' => '100','text' => "отсортировано по убыванию"), $this);?>
><?php echo smarty_function_html_image(array('file' => "img/icon/s_asc.png"), $this);?>
</span>
    <a href="<?php echo $this->_tpl_vars['url']; ?>
&sort=<?php echo $this->_tpl_vars['index']; ?>
" <?php echo smarty_function_popup(array('width' => '100','text' => "сортировать по возрастанию"), $this);?>
><i><?php echo $this->_tpl_vars['text']; ?>
</i></a>
<?php else: ?>
    <a href="<?php echo $this->_tpl_vars['url']; ?>
&sort=<?php echo $this->_tpl_vars['index']; ?>
" <?php echo smarty_function_popup(array('width' => '100','text' => "сортировать по возрастанию"), $this);?>
><?php echo $this->_tpl_vars['text']; ?>
</a>
<?php endif; ?>

<!-- /sort -->