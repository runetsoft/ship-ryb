<?php /* Smarty version 2.6.16, created on 2017-03-24 15:52:57
         compiled from _tools/action_pages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup', '_tools/action_pages.tpl', 47, false),array('function', 'html_image', '_tools/action_pages.tpl', 58, false),)), $this); ?>

<!-- pages list -->

<tr class="grid">
    <td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
">
	<table width="100%">
	<tr>
		<?php if ($this->_tpl_vars['page_url']): ?>
	    <td align="left">Всего записей: <b><?php echo $this->_tpl_vars['count']; ?>
</b></td>
	    <td align="center">Страница:
		<?php unset($this->_sections['page']);
$this->_sections['page']['name'] = 'page';
$this->_sections['page']['loop'] = is_array($_loop=$this->_tpl_vars['pages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['page']['show'] = true;
$this->_sections['page']['max'] = $this->_sections['page']['loop'];
$this->_sections['page']['step'] = 1;
$this->_sections['page']['start'] = $this->_sections['page']['step'] > 0 ? 0 : $this->_sections['page']['loop']-1;
if ($this->_sections['page']['show']) {
    $this->_sections['page']['total'] = $this->_sections['page']['loop'];
    if ($this->_sections['page']['total'] == 0)
        $this->_sections['page']['show'] = false;
} else
    $this->_sections['page']['total'] = 0;
if ($this->_sections['page']['show']):

            for ($this->_sections['page']['index'] = $this->_sections['page']['start'], $this->_sections['page']['iteration'] = 1;
                 $this->_sections['page']['iteration'] <= $this->_sections['page']['total'];
                 $this->_sections['page']['index'] += $this->_sections['page']['step'], $this->_sections['page']['iteration']++):
$this->_sections['page']['rownum'] = $this->_sections['page']['iteration'];
$this->_sections['page']['index_prev'] = $this->_sections['page']['index'] - $this->_sections['page']['step'];
$this->_sections['page']['index_next'] = $this->_sections['page']['index'] + $this->_sections['page']['step'];
$this->_sections['page']['first']      = ($this->_sections['page']['iteration'] == 1);
$this->_sections['page']['last']       = ($this->_sections['page']['iteration'] == $this->_sections['page']['total']);
?>
		    <?php if ($this->_tpl_vars['page'] > $this->_tpl_vars['offset'] && $this->_sections['page']['first']): ?>
			<a href="<?php echo $this->_tpl_vars['page_url']; ?>
&page=1">1</a>
		        <?php if ($this->_tpl_vars['page']-1 <> $this->_tpl_vars['offset']): ?>...<?php endif; ?>
		    <?php endif; ?>
		    <?php if ($this->_sections['page']['index']+1+$this->_tpl_vars['offset'] > $this->_tpl_vars['page'] && $this->_sections['page']['index']+1-$this->_tpl_vars['offset'] < $this->_tpl_vars['page']): ?>
   			        <?php if ($this->_tpl_vars['page'] == $this->_sections['page']['index']+1): ?>
			    	    <b><?php echo $this->_sections['page']['index']+1; ?>
</b>
			<?php else: ?>
		    	    <a href="<?php echo $this->_tpl_vars['page_url']; ?>
&page=<?php echo $this->_sections['page']['index']+1; ?>
">
			    <?php echo $this->_sections['page']['index']+1; ?>
</a>
			<?php endif; ?>
		    <?php endif; ?>
		    <?php if ($this->_tpl_vars['page'] < $this->_sections['page']['loop']+1-$this->_tpl_vars['offset'] && $this->_sections['page']['last']): ?>
			<?php if ($this->_tpl_vars['page'] <> $this->_sections['page']['loop']-$this->_tpl_vars['offset']): ?>...<?php endif; ?>
		        <a href="<?php echo $this->_tpl_vars['page_url']; ?>
&page=<?php echo $this->_sections['page']['loop']; ?>
"><?php echo $this->_sections['page']['loop']; ?>
</a>
		    <?php endif; ?>
		<?php endfor; else: ?>
		    <b>0</b>
		<?php endif; ?>
	    </td>
	    <td align="center">
		Строк на странице:
		<span <?php echo smarty_function_popup(array('width' => '60','text' => "Строк на странице"), $this);?>
>
		    <?php if ($this->_tpl_vars['elements'] == 20): ?><b>20</b><?php else: ?><a href="cmd.php?mod=set&elements=20">20</a><?php endif; ?>
		    <?php if ($this->_tpl_vars['elements'] == 50): ?><b>50</b><?php else: ?><a href="cmd.php?mod=set&elements=50">50</a><?php endif; ?>
		    <?php if ($this->_tpl_vars['elements'] == 300): ?><b>300</b><?php else: ?><a href="cmd.php?mod=set&elements=300">300</a><?php endif; ?>
		</span>
	    </td>
		<?php endif; ?>
	    <?php if ($this->_tpl_vars['add_url']): ?>
			<td align="right">
		    	<a href="<?php echo $this->_tpl_vars['add_url']; ?>
">
    		    <span <?php echo smarty_function_popup(array('width' => '50','left' => '10','text' => "Добавить"), $this);?>
>
    		    <?php echo smarty_function_html_image(array('file' => "img/icon/b_insrow.png",'border' => '0','align' => 'absmiddle','alt' => ""), $this);?>
</span></a>
	        </td>
	    <?php endif; ?>
	</tr>
	</table>
    </td>
</tr>

<!-- /pages list -->