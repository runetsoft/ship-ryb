<?php /* Smarty version 2.6.16, created on 2017-03-27 02:02:48
         compiled from /var/www/shipryb/public_html/adm/core/shop/edit_shop.tpl */ ?>

<!-- edit -->

<form action="cmd.php" method="post" enctype="multipart/form-data">

<p>
<b><?php echo $this->_tpl_vars['page_title']; ?>
</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="grid">
	<td class="head" width="20%">Магазинин</td>
	<td><input type="text" name="form[shop]" value="<?php echo $this->_tpl_vars['form']['shop']; ?>
" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">FTP</td>
	<td>
		ftp://
		<input type="text" name="form[ftp_user]" value="<?php echo $this->_tpl_vars['form']['ftp_user']; ?>
" style="width:15%">
		:
		<input type="text" name="form[ftp_pass]" value="**********" style="width:15%">
		@
		<input type="text" name="form[ftp_host]" value="<?php echo $this->_tpl_vars['form']['ftp_host']; ?>
" style="width:15%">
		/
		<input type="text" name="form[ftp_dir]" value="<?php echo $this->_tpl_vars['form']['ftp_dir']; ?>
" style="width:15%">
		/partner_offers.xml
	</td>
</tr>
<tr class="grid">
	<td class="head">Следующий номер для артикула</td>
	<td><input type="text" name="form[article_last]" value="<?php echo $this->_tpl_vars['form']['article_last']; ?>
" style="width:15%">
		<br>Номер будет автоматически увеличиваться при авто-создании новых артикулов
	</td>
</tr>
<tr class="grid">
	<td class="head">Кол-во знаков в артикуле</td>
	<td>
		<input type="text" name="form[article_width]" value="<?php echo $this->_tpl_vars['form']['article_width']; ?>
" style="width:15%">
		<br>Если при создании нового артикула, его длина будет меньше этого числа, то слева будут добавлены нули
	</td>
</tr>
<tr class="grid">
	<td class="head">Минимальное кол-во склада для выгрузки</td>
	<td><input type="text" name="form[sklad_min]" value="<?php echo $this->_tpl_vars['form']['sklad_min']; ?>
" style="width:5%"></td>
</tr>
<tr class="grid">
	<td class="head">Статус</td>
	<td><label name="form[status]"><input type="checkbox" name="form[status]"<?php if ($this->_tpl_vars['form']['status']): ?> checked<?php endif; ?>> активен</label></td>
</tr>

</table>

<p>
<center>
<input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>
">
<input type="hidden" name="do" value="<?php echo $_GET['do']; ?>
">
<input type="hidden" name="do_add" value="<?php echo $this->_tpl_vars['do_add']; ?>
">
<?php if ($this->_tpl_vars['do_add']): ?>
	<input type="submit" value="Добавить">
<?php else: ?>
	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
">
	<input type="submit" value="Изменить">
<?php endif; ?>
<input type="submit" name="cancel" value="Отмена">
</center>
<p>

</form>

<!-- /edit -->