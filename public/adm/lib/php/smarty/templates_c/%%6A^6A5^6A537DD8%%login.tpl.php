<?php /* Smarty version 2.6.16, created on 2017-03-24 14:38:10
         compiled from login.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_image', 'login.tpl', 26, false),)), $this); ?>
<html>
<head>
<title><?php echo $this->_tpl_vars['info']['site_name']; ?>
 - <?php echo $this->_tpl_vars['info']['cp_text']; ?>
</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="stylesheet" href="lib/css/adm.css" type="text/css">
<link rel="stylesheet" type="text/css" href="lib/js/keyboard/keyboard.css">
<script type="text/javascript" src="lib/js/keyboard/keyboard.js"></script>
</head>
<body>

<!-- login -->

<form method="post"
	<?php if ($_SERVER['SERVER_NAME'] == "dsms.ru"): ?>
		action="/adm/"
	<?php endif; ?>
	onsubmit="this.submit.disabled=true">

<table width="100%" height="100%">
<tr>
<td align="center" valign="middle">

	<table class="grid" cellpadding="2" cellspacing="1">
	<tr class="grid">
	<th colspan="2">
		<?php echo smarty_function_html_image(array('file' => "img/login.gif"), $this);?>

		Авторизация
	</th>
	</tr>
	<tr class="grid">
	<td align="right">Логин/E-mail:</td>
		<td align="center">
			<input type="text" id="login" name="login" value="<?php echo $this->_tpl_vars['login']; ?>
" class="keyboardInput">
			<?php if (! $this->_tpl_vars['login']): ?>
				<script type="text/javascript">
				document.getElementById('login').focus();
				</script>
			<?php endif; ?>
		</td>
	</tr>
	<tr class="grid">
		<td align="right">Пароль:</td>
		<td align="center">
			<input type="password" id="password" name="password" class="keyboardInput">
			<?php if ($this->_tpl_vars['login']): ?>
				<script type="text/javascript">
				document.getElementById('password').focus();
				</script>
			<?php endif; ?>
		</td>
	</tr>
	<tr class="grid">
		<td align="right">На сутки:</td>
		<td align="left"><input type="checkbox" name="time24" style="border:0" checked></td>
	</tr>
	<tr class="grid">
		<td colspan="2" align="center">
			<input type="submit" name="submit" value="Вход">
			<input type="hidden" name="on_submit" value="1">
		</td>
	</tr>
	</table>

<?php if ($_SESSION['auth']['badlogin'] == 1): ?><p><font size="-1" color="red">Пара логин/пароль не найдена</font><?php endif;  if ($_SESSION['auth']['badlogin'] == 2): ?>
	<p><font size="-1" color="red">
		Вы запросили закрытую для вас страницу на сайте<br>Ваша авторизации автоматически снята, из-за нарушения доступа
	</font>
<?php endif; ?>

</td>
</tr>
</table>

</form>

<!-- /login -->

</body>
</html>