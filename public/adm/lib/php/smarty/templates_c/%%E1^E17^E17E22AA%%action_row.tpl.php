<?php /* Smarty version 2.6.16, created on 2017-03-24 15:52:57
         compiled from _tools/action_row.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup', '_tools/action_row.tpl', 24, false),array('function', 'html_image', '_tools/action_row.tpl', 29, false),)), $this); ?>

<!-- action row -->

<td align="center" nowrap>
    <?php if ($this->_tpl_vars['checkbox']): ?>
        <input type="checkbox"
		<?php if ($this->_tpl_vars['id_prefix']): ?>
			id="<?php echo $this->_tpl_vars['id_prefix'];  echo $this->_tpl_vars['checkbox']; ?>
" 
		<?php endif; ?>
			name="update[<?php echo $this->_tpl_vars['checkbox']; ?>
]"
			<?php echo smarty_function_popup(array('width' => '50','left' => '10','text' => "Выделить"), $this);?>
 style="border:0px">
	<?php endif; ?>
    <?php if ($this->_tpl_vars['edit_url']): ?>
		<a href="<?php echo $this->_tpl_vars['edit_url']; ?>
" style="text-decoration: none;">
		<span <?php echo smarty_function_popup(array('width' => '50','left' => '10','text' => "Изменить"), $this);?>
>
		<?php echo smarty_function_html_image(array('file' => "img/icon/b_edit.png",'border' => '0','align' => 'absmiddle','alt' => ""), $this);?>
</span></a>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['del_url']): ?>
		<a href="javascript:void(0)" <?php echo smarty_function_popup(array('sticky' => 'true','trigger' => 'onclick','height' => '10','above' => 'true','caption' => "Подтверждение удаления",'text' => ($this->_tpl_vars['del_text'])), $this);?>
 style="text-decoration: none;">
		<span <?php echo smarty_function_popup(array('width' => '50','left' => '10','text' => "Удалить"), $this);?>
>
		<?php echo smarty_function_html_image(array('file' => "img/icon/b_drop.png",'border' => '0','align' => 'absmiddle','alt' => ""), $this);?>
</span></a>
    <?php endif; ?>
</td>

<!-- /action row -->