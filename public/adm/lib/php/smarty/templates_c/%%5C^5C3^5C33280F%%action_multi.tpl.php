<?php /* Smarty version 2.6.16, created on 2017-03-27 13:09:03
         compiled from _tools/action_multi.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup', '_tools/action_multi.tpl', 37, false),)), $this); ?>

<!-- action multi -->

<tr class="grid">
    <td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
" align="right">

<?php if ($this->_tpl_vars['update_ids']):  echo '
<script language="JavaScript">
<!-- Begin
function checkAll';  echo $this->_tpl_vars['id_prefix'];  echo '(b) {
    var ids = new Array(';  echo $this->_tpl_vars['update_ids'];  echo ');
    var x = 0;
    for (i in ids) {
	document.getElementById(\'';  echo $this->_tpl_vars['id_prefix'];  echo '\'+ids[x]).checked = b ;
        x++;
    }
}
// End -->
</script>
'; ?>

[
<a href="javascript:void(0)" onclick="checkAll<?php echo $this->_tpl_vars['id_prefix']; ?>
(true);" style="text-decoration:none"
    <?php echo smarty_function_popup(array('left' => '10','width' => '80','text' => "Выделить все"), $this);?>
>+</a>
<a href="javascript:void(0)" onclick="checkAll<?php echo $this->_tpl_vars['id_prefix']; ?>
(false);" style="text-decoration:none"
    <?php echo smarty_function_popup(array('left' => '10','width' => '150','text' => "Снять выделение со всех"), $this);?>
>&ndash;</a>
]

<?php endif; ?>	

        <input type="hidden" name="mod" value="<?php echo $this->_tpl_vars['mod']; ?>
">
	<?php if ($this->_tpl_vars['mod2']): ?>
	    <input type="hidden" name="mod2" value="<?php echo $this->_tpl_vars['mod2']; ?>
">
	<?php endif; ?>
        <input type="hidden" name="do" value="multi">
	<?php if ($this->_tpl_vars['button_edit']): ?>
    	    <input type="image" src="img/icon/b_edit.png" align="absmiddle"
	    name="multi_edit" style="border:0px"
	    <?php echo smarty_function_popup(array('left' => '10','width' => '140','text' => "Изменить выделенное"), $this);?>
>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['button_del']): ?>
    	    <input type="image" src="img/icon/b_drop.png" align="absmiddle"
	    name="multi_del" style="border:0px"
	    <?php echo smarty_function_popup(array('left' => '10','width' => '140','text' => "Удалить выделенное"), $this);?>
>
	<?php endif; ?>
    </td>
</tr>

<!-- /action multi -->