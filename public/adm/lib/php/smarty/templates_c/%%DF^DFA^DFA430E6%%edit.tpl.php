<?php /* Smarty version 2.6.16, created on 2017-03-27 13:14:05
         compiled from /var/www/shipryb/public_html/adm/core/provider/edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', '/var/www/shipryb/public_html/adm/core/provider/edit.tpl', 13, false),)), $this); ?>

<!-- edit -->

<form action="cmd.php" method="post" enctype="multipart/form-data">

<p>
<b><?php echo $this->_tpl_vars['page_title']; ?>
</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="grid">
	<td class="head" width="10%">Город</td>
	<td><?php echo smarty_function_html_options(array('name' => "form[city_id]",'options' => $this->_tpl_vars['form']['city_tree'],'selected' => $this->_tpl_vars['form']['city_id']), $this);?>
</td>
</tr>
<tr class="grid">
	<td class="head">Группа</td>
	<td><?php echo smarty_function_html_options(array('name' => "form[provider_group_id]",'options' => $this->_tpl_vars['form']['group_tree'],'selected' => $this->_tpl_vars['form']['provider_group_id']), $this);?>
</td>
</tr>
<tr class="grid">
	<td class="head">Провайдер</td>
	<td><input type="text" name="form[provider]" value="<?php echo $this->_tpl_vars['form']['provider']; ?>
" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Организация</td>
	<td><input type="text" name="form[org_name]" value="<?php echo $this->_tpl_vars['form']['org_name']; ?>
" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Статус</td>
	<td><label name="form[status]"><input type="checkbox" name="form[status]"<?php if ($this->_tpl_vars['form']['status']): ?> checked<?php endif; ?>> активен</label></td>
</tr>

</table>

<p>
<center>
<input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>
">
<input type="hidden" name="do" value="<?php echo $_GET['do']; ?>
">
<input type="hidden" name="do_add" value="<?php echo $this->_tpl_vars['do_add']; ?>
">
<?php if ($this->_tpl_vars['do_add']): ?>
	<input type="submit" value="Добавить">
<?php else: ?>
	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
">
	<input type="submit" value="Изменить">
<?php endif; ?>
<input type="submit" name="cancel" value="Отмена">
</center>
<p>

</form>

<!-- /edit -->