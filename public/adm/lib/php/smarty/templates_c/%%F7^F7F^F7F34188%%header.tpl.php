<?php /* Smarty version 2.6.16, created on 2017-03-24 15:52:54
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup_init', 'header.tpl', 17, false),array('function', 'html_options', 'header.tpl', 29, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><?php echo $this->_tpl_vars['info']['site_name']; ?>
 - <?php echo $this->_tpl_vars['info']['cp_text']; ?>
</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<script type="text/javascript" src="lib/js/jquery/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="lib/js/dtree/dtree.js"></script>
	<script type="text/javascript" src="lib/php/JsHttpRequest/JsHttpRequest.js"></script>
	<link rel="StyleSheet" href="lib/js/dtree/dtree.css" type="text/css" />
	<link rel="stylesheet" href="lib/css/adm.css" type="text/css">
	<!--
	<link rel="stylesheet" type="text/css" href="lib/css/kube/css/kube.css" /> 	
	-->
	

</head>
<?php echo smarty_function_popup_init(array('src' => "lib/js/overlib/Mini/overlib_mini.js"), $this);?>



<!-- header -->

<table width='100%' style="border-bottom: 1px dashed #808080;">
<tr>
<td><b><?php echo $this->_tpl_vars['info']['site_name']; ?>
 - <?php echo $this->_tpl_vars['info']['cp_text']; ?>
</b></td>
<?php if ($this->_tpl_vars['lang_tree']): ?>
<form method="post" action="cmd.php">
<td>
    <select name="lang" onchange='this.form.submit()'>
    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lang_tree'],'selected' => $_SESSION['sess']['lang']), $this);?>

    </select>
    <input type="hidden" name="mod" value="set">
</td>
</form>
<?php endif;  if ($this->_tpl_vars['price_low']): ?>
<td>Найдено <?php echo $this->_tpl_vars['price_low']; ?>
 товаров с ценой ниже 500 руб.</td>
<?php endif; ?>
<td align="right">
    Пользователь: <?php echo $this->_tpl_vars['auth']['login']; ?>
 (<?php echo $this->_tpl_vars['auth']['fio']; ?>
)
    | Уровень доступа: <?php echo $this->_tpl_vars['auth']['access']; ?>

    | <a href="?logout=1">Выход</a>
</td>
</tr>
</table>

<!-- /header -->

<table>
<tr valign="top">
