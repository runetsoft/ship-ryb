<?php /* Smarty version 2.6.16, created on 2017-03-27 13:04:26
         compiled from /var/www/shipryb/public_html/adm/core/adm/edit_adm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'popup', '/var/www/shipryb/public_html/adm/core/adm/edit_adm.tpl', 17, false),array('function', 'html_options', '/var/www/shipryb/public_html/adm/core/adm/edit_adm.tpl', 34, false),)), $this); ?>

<!-- edit -->

<form action="cmd.php" method="post" enctype="multipart/form-data">

<p>
<b><?php echo $this->_tpl_vars['page_title']; ?>
</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="grid">
	<td class="head" width="10%">Логин</td>
	<td><input type="text" name="form[login]" value="<?php echo $this->_tpl_vars['form']['login']; ?>
" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Пароль</td>
	<td <?php if (! $this->_tpl_vars['do_add']):  echo smarty_function_popup(array('text' => "Если не желаете менять пароль, оставьте это поле пустым."), $this); endif; ?>
	><input type="text" name="form[password]" value="<?php echo $this->_tpl_vars['form']['password']; ?>
" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Ф.И.О.</td>
	<td><input type="text" name="form[fio]" value="<?php echo $this->_tpl_vars['form']['fio']; ?>
" style="width:95%"></td>
</tr>
<tr class="grid">
	<td class="head">Email для связи</td>
	<td><input type="text" name="form[email]" value="<?php echo $this->_tpl_vars['form']['email']; ?>
" style="width:95%"></td>
</tr>
<tr class="grid">
	<td class="head">Статус</td>
	<td><label name="form[status]"><input type="checkbox" name="form[status]"<?php if ($this->_tpl_vars['form']['status']): ?> checked<?php endif; ?>> активен</label></td>
</tr>
<tr class="grid">
	<td class="head">Группа</td>
	<td><?php echo smarty_function_html_options(array('name' => "form[access_id]",'options' => $this->_tpl_vars['form']['access_tree'],'selected' => $this->_tpl_vars['form']['access_id']), $this);?>
</td>
</tr>
</table>

<p>
<center>
<input type="hidden" name="mod" value="<?php echo $this->_tpl_vars['mod']; ?>
">
<input type="hidden" name="do" value="<?php echo $this->_tpl_vars['do']; ?>
">
<input type="hidden" name="do_add" value="<?php echo $this->_tpl_vars['do_add']; ?>
">
<?php if ($this->_tpl_vars['do_add']): ?>
	<input type="submit" value="Добавить">
<?php else: ?>
	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
">
	<input type="submit" value="Изменить">
<?php endif; ?>
<input type="submit" name="cancel" value="Отмена">
</center>
<p>

</form>

<!-- /edit -->