<?

class cms_sql_tool
{


	function memcache_test() {
		global $db;
		$r = $db->selectRow("-- CACHE: 1m
				SELECT now()");
		print_r($r);
		exit;
	}

	function cmd_good_new($a = null) {
		global $db;
		
		$a['good'] = trim($a['good']);
		if (!$a['good']) return false;		
		if ($good_id = $db->selectRow("SELECT id FROM ?_good WHERE good=? LIMIT 1",$a['good']))
			return $good_id;
		
		$good_id = $db->query("INSERT INTO ?_good
					(dt_insert,adm_id,article,good)
					VALUES (now(),?d,?,?)",
					$_SESSION['auth']['id'],
					'aaaaaaaa',
					trim($a['good'])
		);
		$r = $db->query("SELECT * FROM ?_shop");
		foreach ($r as $i=>$v) {
			$article = $v['article_last'];
			$article++;
			$db->query("UPDATE ?_shop SET article_last=?d WHERE id=?d LIMIT 1", $article, $v['id']);
			$db->query("INSERT INTO ?_good_shop
					(dt_insert,shop_id,article,good_id)
					VALUES (now(),?d,?,?d)",
					$v['id'],
					$article,
					$good_id
			);
		}
		return $good_id;
	}

################################################################################################


	/**************************************************
	определение города по IP
	****************************************************/
	function mod_city_geo($a = null) {
		global $db,$sql;
		$a['long1'] = ip2long($a['ip']);
		$a['long2'] = sprintf("%u\n", ip2long($a['ip']));
		if ($a['debug_']) {
			$sql->debug_db(1);
		}
		if ($city_id = $db->selectCell("-- CACHE: 15m
				SELECT city_id FROM ?_geoip
				WHERE ip_from<=".sprintf("%u", ip2long($a['ip']))."
				AND     ip_to>=".sprintf("%u", ip2long($a['ip']))." LIMIT 1")) {
			$log['city_found']++;
			$log['city_id'] = $city_id;
			$r = $db->selectRow("SELECT id,city,url,lat,lng FROM ?_city WHERE id=?d LIMIT 1",$city_id);
		} else {
			$log['city_not_found']++;
			$r = $db->selectRow("-- CACHE: 15m
					SELECT id,city,url FROM ?_city LIMIT 1");
		}
		//mail('support@adsmedia.ru','debug mod_city_geo',print_r($a,1).print_r($log,1).print_r($r,1));
		if ($a['debug']) {
			print_r($a);
			print_r($log);
		}
		return $r;
	}

	/**************************************************
	город по id
	****************************************************/
	function mod_city_one($a = null) {
		global $db;
		if (isset($a['city_id']) && !empty($a['city_id'])) {
			$r = $db->selectRow("-- CACHE: 15m
				SELECT id,city,only_reg,url,lat,lng FROM ?_city WHERE id=?d LIMIT 1",$a['city_id']);
			return $r;
		} else {
			return array();
		}
	}



	//--------------------------------------------------------
	function mod_sysctl_list() {
		global $db, $cfg, $cms;
		$r = $db->select("-- CACHE: 1m
				SELECT param,value,name FROM ?_sysctl");
		return $r;
	}

	//---------------------------------------------------------------
	function mod_logo_list() {
		global $db, $cfg, $cms;
		$r = $db->select("-- CACHE: 15m
				SELECT id,img_name,url FROM ?_logo ORDER BY orderby");
		foreach ($r as $i=>$v) {
			$r[$i]['img'] = $cfg['files']['logo'].$v['id'].'_'.$v['img_name'];
			unset($r[$i]['img_name']);
		}
		return $r;
	}

	//--------------------------------------------------------
	function update_tags($a=null) {
		global $db;

		//$db->transaction();

		$log['a'] = $a;
		if (!$a['table'] || !$a['field'] || !$a['field_id'] || !is_array($a['ids'])) return false;
		// получаем список старых
		if ($r = $db->selectCol("SELECT tag_id FROM ?_".$a['table']." WHERE ".$a['field']."=".$a['field_id'])) {
		} else {
			$r = array();
		}
		$log['exist'] = $r;
		// проверяем и удаляем лишние
		foreach ($r as $v) {
			if (!in_array($v,$a['ids'])) {
				$db->query("DELETE FROM ?_".$a['table']." WHERE ".$a['field']."=".$a['field_id']." AND tag_id=?d LIMIT 1",$v);
				$log['delete'][] = $v;
			}
		}
		// проверяем и добавляем новые
		foreach ($a['ids'] as $v) {
			if (!in_array($v,$r)) {
				$db->query("INSERT INTO ?_".$a['table']." (dt_insert,".$a['field'].",tag_id) VALUES (now(),?d,?d)",$a['field_id'],$v);
				$log['insert'][] = $v;
			}
		}
		//print"<pre>";print_r($log);exit;
		//mail('dewil.ru@gmail.com','update_tags',print_r($log,1));

		//$db->commit();
	}
	//--------------------------------------------------------
	function update_sub_tags($a=null) {
		global $db;

		//$db->transaction();

		$log['a'] = $a;
		if (!$a['tag_id'] || !is_array($a['tag_ids'])) return false;
		foreach($a['tag_ids'] as $i=>$v)
			$tag_ids[] = $i;

		// получаем список старых
		if ($r = $db->selectCol("SELECT tag_id2 FROM ?_tag_sub WHERE tag_id1=?",$a['tag_id'])) {
		} else {
			$r = array();
		}
		$log['exist'] = $r;
		// проверяем и удаляем лишние
		foreach ($r as $v) {
			if (!in_array($v,$tag_ids)) {
				$db->query("DELETE FROM ?_tag_sub WHERE tag_id1=?d AND tag_id2=?d LIMIT 1",$a['tag_id'],$v);
				$log['delete'][] = $v;
			}
		}
		// проверяем и добавляем новые
		foreach ($tag_ids as $v) {
			if (!in_array($v,$r)) {
				$db->query("INSERT IGNORE INTO ?_tag_sub (dt_insert,tag_id1,tag_id2) VALUES (now(),?d,?d)",$a['tag_id'],$v);
				$log['insert'][] = $v;
			}
		}
		//print"<pre>";print_r($log);exit;
		//mail('dewil.ru@gmail.com','update_tags',print_r($log,1));

		//$db->commit();
	}
	//-----------------------------------------------------------
	function update_tags_cnt() {
		global $db;
		$db->query("UPDATE ?_tags s SET cnt=(SELECT count(*) FROM ?_act_tags WHERE tag_id=s.id)");
	}
	//-----------------------------------------------------------
	function update_xml($a=null) {
		global $db;
		$log['a'] = $a;
		// получаем список старых
		$r = $db->selectCol("SELECT xml_id FROM ?_act_xml WHERE act_id=?d", $a['act_id']);
		$log['exist'] = $r;
		// проверяем и удаляем лишние
		foreach ($r as $v) {
			if (!in_array($v,$a['ids'])) {
				$db->query("DELETE FROM ?_act_xml WHERE act_id=?d AND xml_id=?d AND xml_act=0 LIMIT 1", $a['act_id'], $v);
				$log['delete'][] = $v;
			}
		}
		// проверяем и добавляем новые
		foreach ($a['ids'] as $v) {
			if (!in_array($v,$r)) {
				$db->query("INSERT INTO ?_act_xml (dt_insert,act_id,xml_id) VALUES (now(),?d,?d)",$a['act_id'],$v);
				$log['insert'][] = $v;
			}
		}
		//print"<pre>";print_r($log);exit;
		//mail('dewil.ru@gmail.com','update_tags',print_r($log,1));
	}
	//-----------------------------------------------------------
	function update_xml_cnt() {
		global $db;
		//$db->query("UPDATE ?_act_xml x SET cnt=(SELECT count(*) FROM ?_act_tags WHERE tag_id=s.id)");
	}

	//-----------------------------------------------------------
	function uuid_new($a = null) {
		global $db;
		if (!$a['table'] || !$a['where']) return false;
		do {
			$uuid = $db->selectCell("SELECT upper(left(uuid(),8))");
			/*
			if ($a['table'] == 'act_kupon' && $a['act_id']) {
				$count = $db->selectCell("SELECT count(*)+1 FROM ?_act_kupon WHERE act_id=?d",$a['act_id']);
				//$count = sprintf("%04d",$count);
				//mail('support@adsmedia.ru','new kupon debug act_id='.$a['act_id'],$count.'-'.$uuid);
			}
			*/
		} while ($db->select("SELECT id FROM ?_".$a['table']." ".$a['where']."=? LIMIT 1",$uuid));
		return $uuid;
	}


	/****************************************************************************
	логирование действий пользователей
	*****************************************************************************/
	function log_adm($a = null) {
		global $db;
		if (is_array($a['info']) && count($a['info'])) {
			foreach ($a['info'] as $v) {
				$info_arr[] = "<u>".$v['name']."</u>: ".$v['value'];
			}
			$info = implode("\n",$info_arr);
		} else {
			return false;
		}
		$db->query("INSERT INTO ?_log_adm (dt_insert,access_id,user_id,class,info)
			VALUES (now(),?d,?d,?,?)",
			($_SESSION['auth']['access'] == 'adm'
				? 1
				: ($_SESSION['auth']['access'] == 'moder'
					? 2 : 3)),
			$_SESSION['auth']['id'],
			$db->selectCell("SELECT class FROM ?_site WHERE id=?d LIMIT 1", $_GET['mod'] ? $_GET['mod'] : $_POST['mod']),
			$info
		);
	}


	/********************************************************************/
	function curl() {

		if (!isset($this->curl) || !is_array($this->curl)) return;
		$ch = curl_init($this->curl['url']);

		if (isset($this->curl['postfields']) && is_array($this->curl['postfields']))
		{
			$postfields = '';
			foreach($this->curl['postfields'] as $i=>$v)
				$postfields .= urlencode($i)."=".urlencode($v)."&";
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		}
		elseif (isset($this->curl['postfield']) && is_string($this->curl['postfield']))
		{
			$postfields = urlencode($this->curl['postfield']);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		}

		if (isset($this->curl['referer']) && !empty($this->curl['referer'])) {
			curl_setopt($ch, CURLOPT_REFERER, $this->curl['referer']);
		}

		if (isset($this->curl['cookie']) && is_array($this->curl['cookie'])) {
			$cookie = '';
			foreach($this->curl['cookie'] as $i=>$v)
				$cookie .= "$i=$v; ";
			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}

		if (isset($this->curl['useragent']) && !empty($this->curl['useragent'])) {
			curl_setopt($ch, CURLOPT_USERAGENT, $this->curl['useragent']);
		} else {
			curl_setopt($ch, CURLOPT_USERAGENT, "API MyFant.RU v.1.0");
		}

		if (isset($this->curl['httpheader']) && is_array($this->curl['httpheader']) && count($this->curl['httpheader'])) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl['httpheader']);
		} else {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
				"Accept-Language: en-us,en;q=0.5",
				"Accept-Charset: KOI8-R,utf-8;q=0.7,*;q=0.7",
				"Keep-Alive: 30",
				"Connection: close"));
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_ENCODING, 0);
		if (isset($this->curl['header']) && !empty($this->curl['header'])) {
			curl_setopt($ch, CURLOPT_HEADER, $this->curl['header']);
		} else {
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$this->curl['result'] = curl_exec($ch);
		$this->curl['getinfo'] = curl_getinfo($ch);

		if (curl_errno($ch) != 0) {
			$this->curl['getinfo']['curl_error'] = curl_error($ch);
			$this->curl['getinfo']['curl_errno'] = curl_errno($ch);
			//mail('dewil.ru@gmail.com','curl error',"ERROR (".curl_errno($ch).") ".curl_error($ch)."\n".print_r($this->curl,1));
		}
	} // function curl()

	/*************************************************************************************
	 *	Получить список номеров страниц
	 *
	 *	@param	integer	$page		текущая страница
	 *	@param	integer	$pages		всего страниц
	 *	@return	array				список страниц
	 *
	 **************************************************************************************/
	function page_nums ($page, $pages, $offset = 2)
	{
		$offset ++;
		for ($i = 1; $i <= $pages; $i ++)
		{
			if (1 == 0)
			{
				// page first
			}
			elseif ($i == 1 && $page == 1)
			{
				$a[] = array(
					'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i == 1 && $page != 1)
			{
				// page last
				$a[] = array(
					'num' => $i, 'type' => 'page'
				);
			}
			elseif ($i == $pages && $page == $pages)
			{
				$a[] = array(
					'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i == $pages && $page != $pages)
			{
				// page offset
				$a[] = array(
					'num' => $i, 'type' => 'page'
				);
			}
			elseif ($i == $page)
			{
				$a[] = array(
					'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i + $offset > $page && $i - $offset < $page)
			{
				// space '...'
				$a[] = array(
					'num' => $i, 'type' => 'page'
				);
			}
			elseif (! $l && $i < $page)
			{
				$a[] = array(
					'type' => 'space'
				);
				$l = true;
			}
			elseif (! $r && $i > $page)
			{
				$a[] = array(
					'type' => 'space'
				);
				$r = true;
			}
		}
		return $a;
	}

}
