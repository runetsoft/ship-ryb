<?
/******************************************************************
*
* @category ADS
* @package	API_SQL
* @author	dwl <dewil@dewil.ru>
* @copyright	2008 ADS Media
* @license	http://www.php.net/license/  PHP License 3.01
* @version	$Id: class.php 10 2007-12-12 21:34:37Z dwl $
*
*******************************************************************/

if (file_exists(@$cfg['dir_adm'].'/inc/const.php'))
include_once($cfg['dir_adm'].'/inc/const.php');

include dirname(__FILE__).'/class__tool.php';
include dirname(__FILE__).'/class_export.php';


/*** BEGIN CLASS ***/


class cms_sql extends cms_sql_export {
	var $time_start;
	var $time_end;
	var $lang_id = 1;
	var $tree;

	function __construct ()	{
		global $db;
		unset($db); // зарезервированная переменная для работы с базой
		$this->init_db();
	}
	/*********************************************************************
	Init DbSimple Class (all schema)
	**********************************************************************/
	function init_db () {
		global $cfg, $db;
		// check MySQL connect --------------------------------------------------------
		$mysql_link = @mysql_connect($cfg['db_host'], $cfg['db_user'], $cfg['db_pass']);
		if (! $mysql_link) return array(
		'code' => 500, 'message' => mysql_error()
		);
		// check MySQL db access -----------------------------------------------------
		if (! mysql_select_db($cfg['db_base'], $mysql_link)) return array(
		'code' => 500, 'message' => mysql_error()
		);
		// init sql ------------------------------------------------------------------
		require_once dirname(__FILE__) . '/../cms_adm/dbsimple.php';
		$db = DbSimple_Generic::connect($cfg['db_dsn']);
		if (isset($cfg['db_prefix']) && !empty($cfg['db_prefix']))
		{
			@define(TABLE_PREFIX, $cfg['db_prefix']);
			$db->setIdentPrefix(TABLE_PREFIX);
		}
		$db->setErrorHandler('dbErrorHandler');
		set_sql_codepage($db);

		if ($cfg['memcache_enable']) {
			$db->setCacher('dbCacher');
		}

		return array(
		'code' => 200
		);
	}
	//--------------------------------------------------------------------------
	function debug_db ($f) {
		global $db;
		if ($f) $db->setLogger('dbLogger');
		else $db->setLogger('');
	}




#####################################################################################################################
	/*************************************************************************************
	*	Получить данные страницы по ее урлу
	*
	*	@param	string		$url		началов дерева
	*	@return	array				найденный раздел
	*
	**************************************************************************************/
	function get_page_url ($a = null)
	{
		global $db,$cms;

		$params = array();

		$log['input'] = $a;
		$url = trim($a['url'], '/');
		$log['urls'] = $urls = explode('/', $url);
		

		// old rewrite
		if ($urls[0][0] == '?') {
			$url = trim($urls[0], '?');
			$urls = explode('&', $url);
			foreach ($urls as $v) {
				$u = explode('=',$v);
				$val[$u[0]] = $u[1];
			}
			if ($val['show'] == 'article' && $val['id']) {
				if ($r = $db->selectRow("-- CACHE: 15m
					SELECT id,dt,url FROM ?_news WHERE id=?d LIMIT 1",intval($val['id']))) {
					preg_match('/^(\d{4}-\d{2}-\d{2})/', $r['dt'], $u);
					$url = '/news/'.$u[1].'/'.$r['url'].'/';
				} else {
					$url = '/';
				}
				header("Location: $url");
				exit;
			}
		}



		// вычислени языка
		if ($r = $db->selectRow("-- CACHE: 15m
				SELECT id FROM ?_lang WHERE url=? LIMIT 1", $urls[0])) {
			$this->lang_id = $r['id'];
			array_shift($urls);
		} else {
			$this->lang_id = 1;
			$class = 'main'; // главная страница
			$log['default_lang'] = 1;
		}
		// проверка тэгов
		if (	$urls[0] != '' &&
				$r = $db->selectRow("-- CACHE: 15m
				SELECT id FROM ?_tag WHERE url=? LIMIT 1", $urls[0])) {
			$tag_id = $r['id'];
			array_shift($urls);
			$log['tag_found'] = 1;


			$id = -1;
			$class = 'tag';
			$page = $db->selectRow("-- CACHE: 15m
						SELECT page_title,page_desc,page_keyword,name,notice,info
						FROM ?_site_page WHERE class='tag' LIMIT 1");
			// собирем параметры
			foreach ($urls as $v) {
				$params[] = $v;
			}
			$urls = array();

		} else {
			$class = 'main'; // главная страница
		}

		$id = 0;
		$pid = 0;
		$level = 0;
		// проход по дереву
		foreach ($urls as $i => $url)
		{
			$log['foreach_pass'] = 1;
			if ($r = $db->selectRow("-- CACHE: 15m
					SELECT id,pid,class FROM ?_site_page WHERE
						url=? AND pid=?d LIMIT 1", $url, $pid))
			{
				$log['page_found'] = 1;
				$id = $r['id'];
				$pid = $id;
				$level++;
				$class = $r['class'];
				$url_valid = $url;
				if ($class != 'page')
				{
					{
						// собирем параметры
						for ($y = $i + 1; $y < count($urls); $y++)
						{
							$params[] = $urls[$y];
						}
						$log['param'] = 1;
					}
					$page = $db->selectRow("-- CACHE: 15m
								SELECT page_title,page_desc,page_keyword,name,notice,info
								FROM ?_site_page WHERE id=?d LIMIT 1", $id);
					break;
				}
			}
			else
			{
				//print_r($a);
				$log['page_not_fount'] = 1;
				if ($a['url'] == '/') {
					$id = 0;
					$page = $db->selectRow("-- CACHE: 15m
								SELECT page_title,page_desc,page_keyword,name,notice,info
								FROM ?_site_page WHERE class='page' AND url IS NULL LIMIT 1");
					$log['main'] = 1;					
				} elseif ($tag_id) {
					$id = -1;
					$class = 'tag';
					$page = $db->selectRow("-- CACHE: 15m
								SELECT page_title,page_desc,page_keyword,name,notice,info
								FROM ?_site_page WHERE class='tag' LIMIT 1");
					// собирем параметры
					for ($y=0; $y < count($urls); $y++) {
						$params[] = $urls[$y];
					}
					$log['tag_id'] = 1;
				} else {
					$id = 0;
					$class = '404';
				}
				break;
			}
		}
		if ($class == 'page')
		{
			$page = $db->selectRow("-- CACHE: 15m
						SELECT page_title,page_desc,page_keyword,name,notice,info
							FROM ?_site_page WHERE id=?d LIMIT 1",$id);
			if ($url_valid == '') $class = 'main';
			$log['class_page'] = 1;
		}
		if ($class == 'news') {
			$news_url_valid = 0;
			if ($params[1] && preg_match('/^(\d{4}-\d{2}-\d{2})/', $params[0], $v)) {
				$dt = $v[1];
				$url = $params[1];
				if ($news_id = $db->selectCell("-- CACHE: 15m
							SELECT id FROM ?_news WHERE date(dt)=? AND url=? LIMIT 1",$dt,$url))
				$news_url_valid = 1;
			}
			// YYYY-MM-DD
			if ($params[0] && !$params[1] && !$news_url_valid && preg_match('/^(\d{4})-(\d{2})-(\d{2})/', $params[0], $v)) {
				if ($v[1] >= 2006 && $v[1] <= 2035 &&
						$v[2] >= 1 && $v[2] <= 12 &&
						$v[3] >= 1 && $v[3] <= 31)
				$news_url_valid = 1;
			}
			// YYYY-MM
			if ($params[0] && !$params[1] && !$news_url_valid && preg_match('/^(\d{4})-(\d{2})/', $params[0], $v)) {
				if ($v[1] >= 2006 && $v[1] <= 2035 &&
						$v[2] >= 1 && $v[2] <= 12)
				$news_url_valid = 1;
			}
			// YYYY
			if ($params[0] && !$params[1] && !$news_url_valid && preg_match('/^(\d{4})/', $params[0], $v)) {
				if ($v[1] >= 2006 && $v[1] <= 2035)
				$news_url_valid = 1;
			}
			if ($params[0] && !$news_url_valid && preg_match('/^\d+$/', $params[0])) {
				if ($news_id = $db->selectCell("-- CACHE: 15m
							SELECT id FROM ?_news WHERE id=?d LIMIT 1",intval($params[0])))
				$news_url_valid = 1;
			}
			if ($news_url_valid)	$class = 'news';
			else			$class = '404';
		}
		//mail('support@adsmedia.ru','debug get_page_url',print_r($GLOBALS,1));
		$page['name'] = $cms->print_html($page['name']);
		return array(
		'id'		=> $id,
		'class'		=> $class,
		'url'		=> $url_valid,
		'level'		=> $level,
		'params'	=> $params,
		'page'		=> $page,
		'tag_id'	=> $tag_id,
		'news_id'	=> $news_id,
		'log'		=> $log,
		);
	}
	/*************************************************************************************
	*	Список разделов сайта
	*
	*	@param	integer	$pid		начало дерева
	*	@param	integer	$level		глубина уровней (optional)
	*	@return	array			список найденных разделов
	*
	**************************************************************************************/
	function site_tree ($a = null)
	{
		global $db;
		// установить переменные
		$this->tree = array();
		$level = $a['level'] ? $a['level'] : 99999;
		if (isset($a['up']) && $a['up'] > 0) {
			while ($a['pid'] != 0 && $a['up'] > 0) {
				$pid = $db->selectCell("-- CACHE: 1m
						SELECT pid FROM ?_site WHERE id=?d",$a['pid']);
				$a['pid'] = $pid;
				$a['up']--;
				$a['level']++;
			}
		}
		// вызываем рекурсию
		$this->site_tree_recur(array(
		'pid'		=> $a['pid'],
		'level'		=> 0,
		'level_end'	=> $level));
		return $this->tree;
	}
	function site_tree_recur ($a = null)
	{
		global $db,$cms;
		if ($a['level_end'] <= 0) return;
		$r = $db->select("-- CACHE: 1m
				SELECT id,pid,name,url FROM ?_site WHERE pid=?d AND hidden=0 ORDER BY orderby", $a['pid']);
		foreach ($r as $page)
		{
			$tree = array();
			$tree['id'] = $page['id'];
			//$tree['pid'] = $page['pid'];
			$tree['level'] = $a['level'];
			$tree['name'] = $cms->print_html($page['name']);

			// собрать полный урл товара
			$full_url = '/'.($page['url'] ? $page['url'].'/' : '');
			$pid = $page['pid'];
			while ($pid != 0)
			{
				$r2 = $db->selectRow("-- CACHE: 1m
					SELECT pid,url FROM ?_site WHERE id=?d LIMIT 1",$pid);
				$pid = $r2['pid'];
				$full_url = '/'.$r2['url'].$full_url;
			}
			$tree['url'] = $full_url;

			$this->tree[] = $tree;

			if ($db->select("-- CACHE: 1m
				SELECT id FROM ?_site WHERE pid=?d AND hidden=0 LIMIT 1", $page['id']))
			{
				$this->site_tree_recur(array(
				'pid'		=> $page['id'],
				'level'		=> $a['level']+1,
				'level_end'	=> $a['level_end']-1));
			}
		}
	}


###############################################################################################################

	/*************************************************************************************
	*	Получить список номеров страниц
	*
	*	@param	integer	$page		текущая страница
	*	@param	integer	$pages		всего страниц
	*	@return	array				список страниц
	*
	**************************************************************************************/
	function page_nums ($page, $pages, $offset = 2)
	{
		$offset ++;
		for ($i = 1; $i <= $pages; $i ++)
		{
			if (1 == 0)
			{
				// page first
			}
			elseif ($i == 1 && $page == 1)
			{
				$a[] = array(
				'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i == 1 && $page != 1)
			{
				// page last
				$a[] = array(
				'num' => $i, 'type' => 'page'
				);
			}
			elseif ($i == $pages && $page == $pages)
			{
				$a[] = array(
				'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i == $pages && $page != $pages)
			{
				// page offset
				$a[] = array(
				'num' => $i, 'type' => 'page'
				);
			}
			elseif ($i == $page)
			{
				$a[] = array(
				'num' => $i, 'type' => 'current'
				);
			}
			elseif ($i + $offset > $page && $i - $offset < $page)
			{
				// space '...'
				$a[] = array(
				'num' => $i, 'type' => 'page'
				);
			}
			elseif (! $l && $i < $page)
			{
				$a[] = array(
				'type' => 'space'
				);
				$l = true;
			}
			elseif (! $r && $i > $page)
			{
				$a[] = array(
				'type' => 'space'
				);
				$r = true;
			}
		}
		return $a;
	}

	/*******************************************************************************/
	/*******************************************************************************/

	function get_api($a=null) {
		global $cfg;

		if ($cfg['api']['enable'])
		{
			$fields[] = 'request='.base64_encode(serialize($a));
			$fields[] = 'auth[id]='.$cfg['api']['id'];
			$fields[] = 'auth[password]='.$cfg['api']['password'];
			$post = implode("&",$fields);

			$ch = curl_init('http://'.$cfg['api']['domain'].'/adm/db_api.php');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_ENCODING, 0);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$result = curl_exec($ch);

			$r = explode("\n",$result);
			if (isset($r[0]) && $r[0] == 'ok' && isset($r[1]))
			return unserialize(base64_decode($r[1]));
			else {
				echo $result;
			}
		}
		else
		{
			return $this->$a['function']($a['parameters']);
		}
	}




	/*************************************************************************************
	*	Время выполнения функций
	**************************************************************************************/
	function time_start()
	{
		$this->time_start = $this->microtime_float();
	}
	function time_end()
	{
		$this->time_end = $this->microtime_float();
		$time = $this->time_end - $this->time_start;
		return sprintf("%01.3f", $time);
	}
	// время выполнения кода
	function microtime_float ()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float) $usec + (float) $sec);
	}

##################################################################################################################################
##################################################################################################################################

}


?>
