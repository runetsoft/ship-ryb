<?

/******************************************************************
*
* @category ADS
* @package	API_SQL
* @author	dwl <dewil@dewil.ru>
* @copyright	2012 ADS Media
* @license	http://www.php.net/license/  PHP License 3.01
* @version	$Id: class.php 10 2012-12-12 21:34:37Z dwl $
*
*******************************************************************/

class cms_sql_export {

	function cmd_export ($a = null) {
		global $db, $cms;

		$this->time_start();
		
		$shop = $db->selectRow("SELECT * FROM ?_shop WHERE id=?d LIMIT 1", intval($a['shop_id']));
		$providers = $db->select("SELECT p.*
			FROM ?_provider p
			JOIN ?_shop_provider sp ON p.id=sp.provider_id AND sp.shop_id=?d
			WHERE p.status=1",
			$shop['id']
		);
		

		
		$log['provider_cnt'] = count($providers);
		$log['price_cnt'] = 0;

		foreach ($providers as $provider) {

			$price = $db->select("
				SELECT DISTINCT p.id,p.sklad,p.price,g.good,gs.article,g.rprice,g.zarticle
				FROM ?_price p
				JOIN ?_good_price gp ON gp.price_id=p.id
				JOIN ?_good g ON g.id=gp.good_id
				JOIN ?_good_shop gs ON gs.good_id=g.id
				WHERE gs.shop_id=?d
				AND p.provider_id=?d
				AND p.sklad>=?d AND p.price!=0
				AND g.not_load=0
				-- AND gs.article='6098939'
				-- LIMIT 10
				",
				$shop['id'],
				$provider['id'],
				$shop['sklad_min']
			);


			if (count($price)) {

				$log['price_cnt'] = $log['price_cnt'] + count($price);

				$xml .= '<ПакетПредложений СодержитТолькоИзменения="false">';
				$xml .= '<Ид>15a441a9-845b-0000-839f-0016d4c78010#'.$provider['uuid'].'</Ид>';
				$xml .= '<Наименование>Пакет предложений</Наименование>';
				$xml .= '<ИдКаталога>15a441a9-845b-11df-839f-0016d4c78010</ИдКаталога>';
				$xml .= '<ИдКлассификатора>15a441a9-845b-11df-839f-0016d4c78010</ИдКлассификатора>';
				$xml .= '<Владелец>';
				$xml .= '<Наименование>'.$cms->print_html($provider['provider']).'</Наименование>';
				$xml .= '<ОфициальноеНаименование>'.$cms->print_html($provider['org_name']).'</ОфициальноеНаименование>';
				$xml .= '</Владелец>';
				$xml .= '<ТипЦены/>';
				$xml .= '<Предложения>';

				foreach ($price as $v) {
					//$log['price_article'][] = $v;

					$xml .= '<Предложение>';
					$xml .= '<Ид>'.$v['article'].'</Ид>';
					$xml .= '<ЗаводскойАртикул>'.$v['zarticle'].'</ЗаводскойАртикул>';					
					$xml .= '<Наименование>'.$cms->print_html($v['good']).'</Наименование>';
					$xml .= '<МРЦ>'.$v['rprice'].'</МРЦ>';					
					$xml .= '<БазоваяЕдиница Код="796">шт</БазоваяЕдиница>';
					$xml .= '<Цены>';
					$xml .= '<Цена>';
					$xml .= '<Представление>'.number_format($v['price'],0,'.',' ').' руб. за шт</Представление>';
					$xml .= '<ЦенаЗаЕдиницу>'.$v['price'].'</ЦенаЗаЕдиницу>';
					$xml .= '<Валюта>руб</Валюта>';
					$xml .= '<Единица>шт</Единица>';
					$xml .= '<Коэффициент>1</Коэффициент>';
					$xml .= '</Цена>';
					$xml .= '</Цены>';
					$xml .= '<Количество>'.$v['sklad'].'</Количество>';
					$xml .= '</Предложение>';
				}
				
				$xml .= '</Предложения>';
				$xml .= '</ПакетПредложений>';
			}
		}
		
		$xml .= '</КоммерческаяИнформация>';

		$xml = '<?xml version="1.0" encoding="UTF-8"?>'.
				'<КоммерческаяИнформация ВерсияСхемы="2.04" ДатаФормирования="'.date("Y-m-d\TH:i:s").'" exectime="'.$this->time_end().'">'.
				$xml;
		

		return array(
			'xml' => $xml,
			'log' => $log,
		);
	}

}
