<?

class check_email
{
	var $server;
	var $from;

	function __construct()
	{
		global $cfg;
		$this->server  = $cfg['site_domain'];
		$this->from = "myfant@myfant.ru";

	}

	function check_email_lite($email) {
		// v1.01
		// проверить синтаксис
		if (preg_match('/^[a-zA-Z0-9_\.\+%#-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,6}$/', $email)) {
			return 1;
		} else {
			return 0;
		}
	}
//-------------------------------------------------------------------------------------------------------
	/************************************************
	level 0 - проверка по синтаксису
	level 1 - + проверка MX
	level 2 - + проверка connect
	level 3 - + проверка ящика на почтовом сервере
	***************************************************/
	function check_email($a = null)
	{
		if ($a == null)
		{
			$r = array(
				'code' => 500,
				'message' => "Нет входных данных",
				'input' => $a
			);
			return $r;
		}
		elseif (is_string($a))
		{
			$email = $a;
			$a = array();
			$a['email'] = $email;
		}
		$r = $this->check_email_regex($a);
		$r['input'] = $a;
		if ($a['debug'] || $r['code'] == 500)
			if (0)
				$cms->mail_debug('class_check_mail '.$r['code'],
					"email [".$a['email']."]".
					"\nSCRIPT_FILENAME ".print_r($_SERVER['SCRIPT_FILENAME'],1).
					"\nargv ".print_r($_SERVER['argv'],1).
					"\nPOST ".print_r($_POST,1).
					"\n ".print_r($r,1)
				);
		return $r;
	}
//-------------------------------------------------------------------------------------------------------
	function check_email_regex($a = null)
	{	// level 0
		// regex ver 1.05
		$no_reg = array(
			'/^10minutemail\.com$/i',
			'/^10x9\.com$/i',
			'/^4warding\.com$/i',
			'/^BeefMilk\.com$/i',
			'/^FatFlap\.com$/i',
			'/^FudgeRub\.com$/i',
			'/^LookUgly.com$/i',
			'/^SmellFear.com$/i',
			'/^abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk\.com$/i',
			'/^anonymail\.nl$/i',
			'/^anonymousspeech\.com$/i',
			'/^antireg\.(com|ru)$/i',
			'/^asdasd\.ru$/i',
			'/^bobmail\.info$/i',
			'/^bugmenot\.com$/i',
			'/^chammy\.info$/i',
			'/^cust\.in$/i',
			'/^discardmail\..+$/i',
			'/^disposableinbox\.com$/i',
			'/^donemail\.ru$/i',
			'/^dontreg\.com$/i',
			'/^emailtem\.com$/i',
			'/^fakedemail\.com$/i',
			'/^fr33mail\.info$/i',
			'/^guerrillamail\.com$/i',
			'/^hmamail\.com$/i',
			'/^hochsitze\.com$/i',
			'/^hulapla\.de$/i',
			'/^jetable\.org$/i',
			'/^key-mail\.net$/i',
			'/^klzlk\.com$/i',
			'/^m4ilweb\.info$/i',
			'/^maileater\.com$/i',
			'/^mailexpire\.com$/i',
			'/^mailexpire\.com$/i',
			'/^mailforspam\.com$/i',
			'/^mailinator\.com$/i',
			'/^mailinator\d+?\.com$/i',
			'/^mailmetrash\.com$/i',
			'/^mailspeed\.ru$/i',
			'/^meltmail\.com$/i',
			'/^mintemail.com$/i',
			'/^misterpinball\.de$/i',
			'/^mytrashmail\.com$/i',
			'/^nepwk\.com$/i',
			'/^no-spam\.ws$/i',
			'/^nomail2me.com$/i',
			'/^nospamfor\.us$/i',
			'/^nospamthanks\.info$/i',
			'/^odnorazovoe\.ru$/i',
			'/^rtrtr\.com$/i',
			'/^s0ny\.net$/i',
			'/^safetymail\.info$/i',
			'/^savemysoul\.ru$/i',
			'/^sharklasers\.com$/i',
			'/^shitmail\.me$/i',
			'/^slopsbox\.com$/i',
			'/^sneokemail\.com$/i',
			'/^spam\.la$/i',
			'/^spambog\..+$/i',
			'/^spambox\.us$/i',
			'/^spamherelots\.com$/i',
			'/^spamhole\.com$/i',
			'/^suremail\.info$/i',
			'/^teewars\.org$/i',
			'/^teleworm\.com$/',
			'/^tempemail\..+$/i',
			'/^tempinbox\.com$/i',
			'/^temporamail\.com$/i',
			'/^temporaryinbox\.com$/i',
			'/^thanksnospam\.info$/i',
			'/^thisisnotmyrealemail\.com$/i',
			'/^thismail\.ru$/i',
			'/^tradermail\.info$/i',
			'/^unmail\.ru$/i',
			'/^webm4il\.info$/i',
			'/^wegwerfmail\.com$/i',
			'/^wh4f\.org$/i',
			'/^yopmail\.com$/i',
			'/^yu2yu\.com$/i',
			'/^zippymail\.info$/i',
			'/mailslite\.com$/i',
		);

		if (preg_match('/^'.
				'([a-z0-9_\.\+%#-]+)'. 						// 0 box
				'@'.
				'(([a-z0-9]+[a-z0-9\.-]*)?[a-z0-9]+'.				// 1 domain 2 level
				'\.'.
				'([a-z]{2,3}|(aero|coop|info|mobi|museum|name|travel)))'.	// 2 domain 1 level
				'$/i', $a['email'], $e))
		{
			if (preg_match('/(\.\.|--|\.-|-\.)/',$e[1]))
			{
				$code = 500;
				$message = "Недопустимое имя ящика в email";
				$log[] = "syntax2 BAD";
			}
			elseif (preg_match('/(\.\.|--|\.-|-\.)/',$e[2]))
			{
				$code = 500;
				$message = "Недопустимое имя домена в email";
				$log[] = "syntax3 BAD";
			}
			elseif (preg_match('/^([a-z]+[\.][a-z]*){3,}@gmail\.com$/i',$a['email']))
			{
				$code = 500;
				$message = "Много точек для ящика на домене gmail.com";
				$log[] = "syntax4 BAD";
			}
			else
			{
				// поиск доменов с мылами без регистрации
				foreach ($no_reg as $v)
				{
					if (preg_match($v,$e[2]))
					{
						$code = 500;
						$message = "К сожалению не принимаем ящики без регистрации";
						$log[] = "syntax5 BAD";
						if (0)
							mail('support@adsmedia.ru','email domain block '.$a['email'],
								"Попытка использовать мыло без регистрации ".$a['email']."\n".
								print_r($_SERVER,1)
							);
						break;
					}
				}
				// если не нашли, то порядок	
				if ($code != 500)
				{
					$code = 200;
					$log[] = "syntax OK";
				}
			}
		} else {
			$code = 500;
			$message = "Недопустимый синтаксис email";
			$log[] = "syntax1 BAD";
		}
		if ($code == 200 && $a['level'] > 0)
		{
			$a['log'] = $log;
			$r = $this->check_email_mx($a);
		}
		else
		{
			$r = array(
				'code' => $code,
				'message' => $message,
				'log' => $log,
			);
		}
		return $r;
		
	}
//-------------------------------------------------------------------------------------------------------
	function check_email_mx($a = null)
	{	// level 1
		// отделить ящик от домена
		list($box,$host) = preg_split('/@/', $a['email']);
		// список MX
		if (getmxrr($host,$a['mx'],$a['priority']))
		{
			$code = 200;
			$log[] = "MX found: ".implode(',',$a['mx']);
		}
		else
		{
			$code = 500;
			$message = "Для домена ".$host." не указан почтовый сервер (MX запись)";
			$log[] = "MX empty";
		}
		if ($code == 200 && $a['level'] > 1)
		{
			$a['log'] = array_merge($a['log'],$log);
			$r = $this->check_email_connect($a);
		}
		else
		{
			$r = array(
				'code' => $code,
				'message' => $message,
				'log' => array_merge($a['log'],$log),
			);
		}
		return $r;

	}
//-------------------------------------------------------------------------------------------------------
	function check_email_connect($a = null)
	{	// level 2
		// сортировать MX по приоритетам
		asort($a['priority']);
		// проверка конекта
		$code = 500;
		foreach ($a['priority'] as $i=>$v)
		{
			$f = @fsockopen($a['mx'][$i], 25, $errno, $errstr, 10);
			if ($f)
			{
				fclose($f);
				$code = 200;
				$log[] = "connect ".$a['mx'][$i]." OK";
				break;
			}
		}
		if ($code == 500)
		{
			$message = "Не отвечает почтовый сервер";
			$log[] = "connect BAD";
		}

		if ($code == 200 && $a['level'] > 2)
		{
			$a['log'] = array_merge($a['log'],$log);
			$r = $this->check_email_smtp($a);
		}
		else
		{
			$r = array(
				'code' => $code,
				'message' => $message,
				'log' => array_merge($a['log'],$log),
			);
		}
		return $r;
	}
//-------------------------------------------------------------------------------------------------------
	function check_email_smtp($a = null)
	{	// level 3
		foreach ($a['priority'] as $i=>$v)
		{
			$f = @fsockopen($a['mx'][$i], 25, $errno, $errstr, 10);
			if ($f && $o = fgets($f, 1024))
			{
				$log[] = "connect smtp ".$a['mx'][$i]." OK";
				$log[] = "get: ".trim($o);
				if (preg_match('/^2\d\d/', $o))
				{
					$log[] = "put: HELO ".$this->server;
					fputs($f, "HELO ".$this->server."\r\n");
				}
				else
					continue;
				$log[] = "get: ".trim($o = fgets($f, 1024));
				if (preg_match('/^2\d\d/', $o))
				{
					$log[] = "put: MAIL FROM: ".$this->from;
					fputs($f, "MAIL FROM: <".$this->from.">\r\n");
				}
				else
					continue;
				$log[] = "get: ".trim($o = fgets($f, 1024));
				if (preg_match('/^2\d\d/', $o))
				{
					$log[] = "put: RCPT TO: ".$a['email'];
					fputs($f, "RCPT TO: <".$a['email'].">\r\n");
				}
				else
					continue;
				$log[] = "get: ".trim($o = fgets($f, 1024));
				if (!preg_match('/^5\d\d/', $o))
				{
					$r = array(
						'code' => 200,
						'log' => array_merge($a['log'],$log),
					);
					return $r;
				}
			}
			else
				$log[] = "connect smtp ".$a['mx'][$i]." BAD";
		}
		$r = array(
			'code' => 500,
			'message' => "Почтовый сервер не подтвердил ваш email",
			'log' => array_merge($a['log'],$log),
		);
		return $r;

	}

}