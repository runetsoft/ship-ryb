<?php

/*******************************************************
*	ADS CMS
*	$Id$
********************************************************/

$cfg['dir_smarty'] = $cfg['dir_adm'].'/lib/php/smarty';
require_once $cfg['dir_smarty'].'/libs/Smarty.class.php';


class ads_cms_smarty extends Smarty {

	function ads_cms_smarty()
	{
		global $cfg;

		$this->Smarty();

		$this->template_dir	= $cfg['dir_adm'].'/core/';
		$this->compile_dir	= $cfg['dir_smarty'].'/templates_c/';
		$this->config_dir	= $cfg['dir_adm'].'/inc/';
		$this->cache_dir	= $cfg['dir_smarty'].'/cache/';
		$this->plugins_dir	= array( SMARTY_DIR . 'plugins',
				  		$cfg['dir_smarty'].'/plugins/');
		$this->caching = true;
		$this->assign('app_name', 'ADS CMS');

	}
}

//-------------------------------------------------------------------------------

// главное меню

function insert_menu_tree($mod) {
	global $cfg, $smarty, $db, $cms;

	if ($_SESSION['auth']['access'] != 'adm') {

		// получить список доступа
		$list = $db->select("-- CACHE: 1m
			SELECT id,pid,name
				FROM 
				(
					SELECT s.*
					FROM ?_site s 
					JOIN ?_site_access sa ON s.id=sa.site_id
					JOIN ?_adm_access ma ON ma.id=sa.group_id
					WHERE ma.id=?d
				UNION
					SELECT s.*
					FROM ?_site s 
					JOIN ?_site_access sa ON s.id=sa.site_id
					JOIN ?_adm m ON m.id=sa.moder_id
					WHERE m.id=?d
				) foo
				WHERE lang_id=?d ORDER BY pid,orderby,name",
					$_SESSION['auth']['access_id'],
					$_SESSION['auth']['id'],
					$_SESSION['sess']['lang']
			);

	} else {

		// построение меню для админа
		$list = $db->select("-- CACHE: 1m
			SELECT id,pid,name
				FROM ?_site
				WHERE lang_id=?d
				ORDER BY pid,orderby,name",
				$_SESSION['sess']['lang']);

	}


	// access check -------------------------------------------------------------------------------
	if ($_SESSION['auth']['access'] != 'adm' && count($list)) {
		$access_ids = array('','set');
		foreach ($list as $v) {
			$access_ids[] = $v['id'];
		}
		if (!in_array($_GET['mod'],$access_ids)) {
			//echo "logout";
			$cms->mail_debug('access_denied');
			header ("Location: index.php?logout=2"); exit;
		}
	}

	//-----------------------------------------------------------------------------------------------------
	
	$class = $db->selectCell("-- CACHE: 15
			SELECT class FROM ?_site WHERE id=?d LIMIT 1",$mod);
	if ($mod == "0" ||
		(isset($cfg['class_list'][$class]['tree']) && $class <> 'site')
	) {
		if ($mod == "0") $class = 'site';
		$smarty->assign('mod_tree',$class);
	}

	// получить список ids
	foreach ($list as $v)
		$ids[] = $v['id'];

	$list2 = array();
	foreach ($list as $i=>$v) {
		$list2[] = $v;
		$pid = $v['pid'];
		while ($pid != 0) {
			// добавить звенья без возможности входить в них
			$m = $db->selectRow("-- CACHE: 15
					SELECT id,pid,name,'1' as no_url FROM ?_site
					WHERE id=? AND lang_id=?d", $pid,$_SESSION['sess']['lang']);
			if (!in_array($m['id'], $ids)) {
				$list2[] = $m;
			}
			$pid = $m['pid'];
		}
	}
	$list = $list2;

	foreach ($list as $i=>$v) {
		if ($v['no_url'])	$list[$i]['url'] = '';
		else				$list[$i]['url'] = '?mod='.$v['id'];
		$list[$i]['name'] = $cms->print_html($cms->short_name($v['name'],15));
		$list[$i]['title'] = str_replace("'","\\'",$v['name']);
	}

	$smarty->assign('list',$list);
	$smarty->caching = false;
	$smarty->display('menu.tpl');

}
function insert_menu_tree_($mod) {
	global $cfg, $smarty, $db, $cms;

	if ($_SESSION['auth']['access_id'] == 1) {

		if (!$a_list) $a_list = '-1';
		$b_list = array(-1);
		$list = $db->select("SELECT id,pid,name,concat('?mod=',id) as `mod` FROM ?_site
						WHERE id IN (".$a_list.")
						AND lang_id=?d
						ORDER BY pid,orderby,name", $_SESSION['sess']['lang']);
		// поиск недостающих звеньев меню
		foreach ($list as $v) {
			if ($v['pid']!=0 && array_search($v['pid'],$a_list)===false) {
				$id = $v['id'];
				do {
					$pid = $db->selectCell("SELECT pid FROM ?_site WHERE id=?d LIMIT 1
									AND lang_id=?d", $id, $_SESSION['sess']['lang']);
					if (!array_search($pid,$a_list) && !array_search($pid,$b_list))
						$b_list[] = $pid;
					$id = $pid;
				} while ($pid == 0);
			}
		}
		// добавить звенья без возможности входить в них
		$c_list = $db->select("SELECT id,pid,name FROM ?_site
						WHERE id IN (".implode(",",$b_list).") AND lang_id=?d
						ORDER BY pid,orderby,name", $_SESSION['sess']['lang']);
		$list = array_merge($list, $c_list);


	} elseif ($_SESSION['auth']['access'] == 'moder') {
		// получить список доступа
		$list = $db->select("-- CACHE: 1m
			SELECT id,pid,name
				FROM 
				(
					SELECT s.*
					FROM ?_site s 
					JOIN ?_site_access sa ON s.id=sa.site_id
					JOIN ?_moder_access ma ON ma.id=sa.group_id
					WHERE ma.id=?d
				UNION
					SELECT s.*
					FROM ?_site s 
					JOIN ?_site_access sa ON s.id=sa.site_id
					JOIN ?_moder m ON m.id=sa.moder_id
					WHERE m.id=?d
				) foo
				WHERE lang_id=?d ORDER BY pid,orderby,name",
					$_SESSION['auth']['access_id'],
					$_SESSION['auth']['id'],
					$_SESSION['sess']['lang']
			);

//print"<pre>";print_r($list);

	} elseif ($_SESSION['auth']['access'] == 'moder_old') {
		if ($_SESSION['auth']['access_id'] == 1)
		{	// меню для модератора
			$list = $db->select("SELECT * FROM ?_site WHERE class IN
				('moder_profile','user','invoice','order','comment','rating','chat','motivation','spp2')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		}
		elseif ($_SESSION['auth']['access_id'] == 2)
		{	// меню для СТП 37,38,46,
			$list = $db->select("SELECT * FROM ?_site WHERE class IN
				('moder_profile','act','act1','user','invoice','order','provider','comment','rating','chat','contest','act_img_user','motivation','spp2','ticket','notice','act_funny','act_banner'".
				(in_array($_SESSION['auth']['id'],array(19,37,38,46)) ? ",'gift'": ''). // доступ к модулю gift
				") AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		}
		elseif ($_SESSION['auth']['access_id'] == 3)
		{	// меню для СПП
			$list = $db->select("SELECT * FROM ?_site WHERE class IN
				('moder_profile','act','spp','spp1','comment','rating','chat','motivation','spp2','ticket','notice','act_request','act_stat_cert'".
				(in_array($_SESSION['auth']['id'],array(19)) ? ",'gift'": ''). // доступ к модулю gift
				",'hardware')
				-- ('comment','rating','chat')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		}
		elseif ($_SESSION['auth']['access_id'] == 4)
		{	// меню для менеджера
			$list = $db->select("SELECT * FROM ?_site WHERE class IN
				('moder_profile','act','act1','user','invoice','order','provider','comment','rating','chat','spp','spp1','moder_point','wage','ticket','notice','act_request','act_moder_move','moder')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		}
		elseif ($_SESSION['auth']['access_id'] == 5)
		{	// меню для редактора
			$list = $db->select("SELECT * FROM ?_site WHERE class IN
				('moder_profile','act_editor','notice')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		}


	} elseif ($_SESSION['auth']['access'] == 'prov') {
		// меню для провайдера
		// касса
		if ($_SESSION['auth']['kassa'] == 1)
			$list = $db->select("-- CACHE: 1m
				SELECT id,pid,name
					FROM ?_site
					WHERE class IN ('act_prov','barcode')
					AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
		// администратор
		else
			$list = $db->select("-- CACHE: 1m
				SELECT id,pid,name
					FROM ?_site
					WHERE class IN ('rating_prov','act_prov','provider_profile','ticket','barcode')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);

	} elseif ($_SESSION['auth']['access'] == 'xml') {
		// построение меню для xml партнера
		$list = $db->select("-- CACHE: 1m
				SELECT id,pid,name
					FROM ?_site
					WHERE class IN ('xml_act')
				AND lang_id=?d ORDER BY pid,orderby,name",$_SESSION['sess']['lang']);
	} elseif ($_SESSION['auth']['access'] == 'adm') {
		// построение меню для админа
		$list = $db->select("-- CACHE: 30
			SELECT id,pid,name
				FROM ?_site
				WHERE lang_id=?d
				ORDER BY pid,orderby,name",
				$_SESSION['sess']['lang']);
	}

	// access check -------------------------------------------------------------------------------
	if ($_SESSION['auth']['access'] != 'adm' && count($list)) {
		$access_ids = array('','set');
		foreach ($list as $v) {
			$access_ids[] = $v['id'];
		}
		if (!in_array($_GET['mod'],$access_ids)) {
			//echo "logout";
			$cms->mail_debug('access_denied');
			header ("Location: index.php?logout=2"); exit;
		}
	}
	//-----------------------------------------------------------------------------------------------------
	
		$class = $db->selectCell("-- CACHE: 15m
				SELECT class FROM ?_site WHERE id=?d LIMIT 1",$mod);
		if ($mod == "0" ||
			(isset($cfg['class_list'][$class]['tree']) && $class <> 'site')
		) {
			if ($mod == "0") $class = 'site';
			$smarty->assign('mod_tree',$class);
		}


	// получить список ids
	foreach ($list as $v)
		$ids[] = $v['id'];

	$list2 = array();
	foreach ($list as $i=>$v) {
		$list2[] = $v;
		$pid = $v['pid'];
		while ($pid != 0) {
			// добавить звенья без возможности входить в них
			$m = $db->selectRow("-- CACHE: 15m
					SELECT id,pid,name,'1' as no_url FROM ?_site
					WHERE id=? AND lang_id=?d", $pid,$_SESSION['sess']['lang']);
			if (!in_array($m['id'], $ids)) {
				$list2[] = $m;
			}
			$pid = $m['pid'];
		}
	}
	$list = $list2;

	foreach ($list as $i=>$v) {
		if ($v['no_url'])	$list[$i]['url'] = '';
		else			$list[$i]['url'] = '?mod='.$v['id'];
		$list[$i]['name'] = $cms->print_html($cms->short_name($v['name'],15));
		$list[$i]['title'] = str_replace("'","\\'",$v['name']);
	}

	$smarty->assign('list',$list);
	$smarty->caching = false;
	$smarty->display('menu.tpl');
}
//----------------------------------------------------------------------------------

function insert_mod_tree($a) {
global $cms;
$func = "print_".$a['func']."_tree";
if (method_exists($cms,$func))
	$cms->$func();
else
	echo "no $func";
}

//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------

function insert_status() {
global $_SESSION;

if (count($_SESSION[status]) == 0) return;

$out .= "<p><div>\n<!-- status -->\n\n";

for ($i=0; $i<count($_SESSION[status]); $i++) {

		if ($_SESSION[status][$i][type] == "ok")
	$out .= "<div style='background:green;color:#ffffff;padding:3px;padding-left:10px;padding-right:10px;'>\n";
	else
	$out .= "<div style='background:red;  color:#ffffff;padding:3px;padding-left:10px;padding-right:10px;''>\n";

	$out .= "<b>".$_SESSION[status][$i][text]."</b>\n</div>\n";
}
$out .= "\n\n<!-- /status -->\n</div>\n\n";
unset($_SESSION[status]);

return $out;
}

//------------------------------------------------------------------------------------


function insert_fckeditor($a) {

include("lib/php/fckeditor/fckeditor.php");

$oFCKeditor = new FCKeditor($a[field]);
$oFCKeditor->BasePath = 'lib/php/fckeditor/';
if ($a[toolbarset] == '')
	$oFCKeditor->ToolbarSet = 'Basic';
else
	$oFCKeditor->ToolbarSet = $a[toolbarset];
$oFCKeditor->Value = $a[value];
$oFCKeditor->Height = $a[size];
$oFCKeditor->Create();

}

//-----------------------------------------------------------------------------------

function insert_action_row($a) {
global $smarty;
$smarty->assign('checkbox',$a[checkbox]);
$smarty->assign('id_prefix',$a[id_prefix]);
if ($a[edit_url])
	$smarty->assign('edit_url',$a[edit_url]);
if ($a[del_url]) {
	$smarty->assign('del_url',$a[del_url]);
	if ($a[del_text])
	$smarty->assign('del_text',"<a href='".$a[del_url]."'>Да! Удалить!</a><br>".$a[del_text]);
	else
	$smarty->assign('del_text',"<a href='".$a[del_url]."'>Да! Удалить!</a>");
}
$smarty->caching = false;
$smarty->display('_tools/action_row.tpl');
}

//------------------------------------------------------

function insert_action_multi($a) {
	global $smarty;
	$smarty->assign('colspan',$a[colspan]);
	$smarty->assign('mod',$a[mod]);
	if ($a[mod2])
		$smarty->assign('mod2',$a[mod2]);
	$smarty->assign('button_edit',$a[button_edit]);
	$smarty->assign('button_del',$a[button_del]);
	if ($a[update_ids]) {
		foreach ($a[update_ids] as $k=>$v)
			$ids[] = $a[update_ids][$k][id];
		$smarty->assign('update_ids',implode(",",$ids));
		if ($a[id_prefix])
			$smarty->assign('id_prefix',$a[id_prefix]);
		else
			$smarty->assign('id_prefix',$a[mod]);
	}	
	$smarty->caching = false;
	$smarty->display('_tools/action_multi.tpl');
}


//------------------------------------------------------
function insert_action_pages($a) {
	global $smarty;
	$smarty->assign('colspan',$a[colspan]);
	if ($a[offset])
		$smarty->assign('offset',$a[offset]);
	else
		$smarty->assign('offset',5);
	$smarty->assign('page_url',$a[page_url]);
	if ($a[add_url])
		$smarty->assign('add_url',$a[add_url]);
	$smarty->caching = false;
	$smarty->display('_tools/action_pages.tpl');
}

//-----------------------------------------------------------------------------

function insert_list_sort($a) {
	global $smarty;
	$smarty->assign('index',$a['index']);
	$smarty->assign('url',$a['url']);
	$smarty->assign('dir',$a['dir']);
	$smarty->assign('sort',$_GET['sort']);
	$smarty->assign('text',$a['text']);
	$smarty->caching = false;
	$smarty->display('_tools/list_sort.tpl');
}


?>
