<?


require_once dirname(__FILE__).'/../dbsimple/Generic.php';

$cfg['db_dsn'] =
	$cfg['db_proto'].'://'.
	$cfg['db_user'].':'.
	$cfg['db_pass'].'@'.
	$cfg['db_host'].'/'.
	$cfg['db_base'];


// Код обработчика ошибок SQL.
function dbErrorHandler($message, $info) {
	global $cfg;
	// Если использовалась @, ничего не делать.
	if (!error_reporting()) return;
	// Выводим подробную информацию об ошибке.
	if ($cfg['db_err'] == 'stdout')
	{
		echo "SQL Error: $message<br><pre>"; 
		print_r($info);
		echo "</pre>";
		exit();
	} else {
		echo "[sql error]";
		mail ($cfg['db_err_mail_to'], "sql error | ".$_SERVER['SERVER_NAME'],
			"$message\n\n".
			"code: ".	$info['code']."\n".
			"message: ".	$info['message']."\n".
			"query: ".	$info['query']."\n".
			"context: ".	$info['context']."\n\n".

			"URL: ".	$_SERVER['REQUEST_URI']."\n".
			"REFERER: ".	$_SERVER['HTTP_REFERER']."\n".
			"ADDR: ".	$_SERVER['REMOTE_ADDR']."\n".
			"AGENT: ".	$_SERVER['HTTP_USER_AGENT']."\n\n".

			"GET: ".	print_r($_GET,1)."\n".
			"POST: ".	print_r($_POST,1)."\n".
			"SESSION: ".	print_r($_SESSION,1)."\n".
			"COOKIE: ".	print_r($_COOKIE,1)."\n".
			"SERVER: ".	print_r($_SERVER,1)."\n".

			"GLOBALS: ".	print_r($GLOBALS,1)."\n".
			""
		);
	}
}

//-------------------------------------------------------------------------------

function dbLogger($db, $sql)
{
	// Находим контекст вызова этого запроса.
	$caller = $db->findLibraryCaller();
	$tip = "at ".@$caller['file'].' line '.@$caller['line'];
	// Печатаем запрос (конечно, Debug_HackerConsole лучше).
	echo "<hr>";
	echo "sql debug: $tip<br>$sql";
	echo "<hr>";
}

//-------------------------------------------------------------------------------

function set_sql_codepage($db) {
	global $cfg;

	$cp = $cfg['db_codepage'];

	if ($cfg['db_proto'] = 'mysql') {

		if ($cp == 'win')
		$db->query("SET NAMES cp1251");
		elseif ($cp == 'koi')
		$db->query("SET NAMES koi8r");
		elseif ($cp == 'utf')
		$db->query("SET NAMES utf8");
		elseif ($cp == 'null' || $cp == '')
		return;
		else {
		print "error config: unknown codepage type";
		exit(1);
		}

	} else { //------------------------------------------------

		if ($cp == 'win')
		$db->query("SET client_encoding = 'WIN1251'");
		elseif ($cp == 'koi')
		$db->query("SET client_encoding = 'KOI8'");
		elseif ($cp == 'utf')
		$db->query("SET client_encoding = 'UTF8'");
		elseif ($cp == 'null' || $cp == '')
		return;
		else {
		print "error config: unknown codepage type";
		exit(1);
		}

	}

}

function dbCacher($key, $value=null) {
	global $cfg, $cms, $mc;

	// http://forum.dklab.ru/viewtopic.php?t=24195

	if (count($cfg['memcache'])) {
		$mc = new Memcache;
		foreach ($cfg['memcache'] as $m) {
			@$mc->addServer($m['host'], $m['port']);
		}
	} else {
		return false;
	}

	$log['key'] = $key;
	$log['value'] = $value;

	if ($value === null) {
		$log[] = 'value !=== null';
		$value = $mc->get($key);
		$log[] = 'value get';
		$cms->memcache++;
	} else {
		$log[] = 'value = null';
		if ($mc->set($key, $value, false, 3600)) {
			$log[] = 'set ok';
		} else {
			$log[] = 'set bad';
		}
	}

	$mc->close();
//	print_r($log);
	return $value;
}


?>
