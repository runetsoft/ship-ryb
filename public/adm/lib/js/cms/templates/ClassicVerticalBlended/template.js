CompleteMenuSolution.prototype.theme.ClassicVerticalBlended = {
  /*
  *  List of available transitions
  *
  *  @type array
  */
  menuOptions : {
    'transitions' : {
      'blend' : {
        'start' : 0,
        'end'   : 1,
        'useIeBlendFix' : true
      },
      'windowborderschecker' : {
      }
    },
    'modifiers' : ['activeontop','blendiebugfix']
  }
}