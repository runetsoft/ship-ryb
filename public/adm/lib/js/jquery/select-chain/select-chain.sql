drop table if exists select_chain;
create table select_chain
(
  id int not null auto_increment,
  category char(10) not null,
  label char(20) not null,

  primary key (id),
  index category_id (category)
);

insert into select_chain values (null, 'forms', 'Form');

insert into select_chain values (null, 'form', 'action');
insert into select_chain values (null, 'form', 'method');
insert into select_chain values (null, 'form', 'enctype');
insert into select_chain values (null, 'form', 'accept');
insert into select_chain values (null, 'form', 'name');
insert into select_chain values (null, 'form', 'onsubmit');
insert into select_chain values (null, 'form', 'onreset');
insert into select_chain values (null, 'form', 'accept-charset');

insert into select_chain values (null, 'forms', 'Select');

insert into select_chain values (null, 'select', 'name');
insert into select_chain values (null, 'select', 'size'); 
insert into select_chain values (null, 'select', 'multiple');
insert into select_chain values (null, 'select', 'disabled');
insert into select_chain values (null, 'select', 'tabindex');
insert into select_chain values (null, 'select', 'onfocus');
insert into select_chain values (null, 'select', 'onblur');
insert into select_chain values (null, 'select', 'onchange');

insert into select_chain values (null, 'scripts', 'Script');

insert into select_chain values (null, 'script', 'charset');
insert into select_chain values (null, 'script', 'type');
insert into select_chain values (null, 'script', 'src');
insert into select_chain values (null, 'script', 'defer');

insert into select_chain values (null, 'scripts', 'Noscript');
insert into select_chain values (null, 'noscript', '[none]');