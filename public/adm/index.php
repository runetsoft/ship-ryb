<?
error_reporting(E_ALL & ~E_DEPRECATED);
$time_start = microtime_float();
require dirname(__FILE__).'/../conf/conf.php';
require $cfg['dir_adm'].'/inc/local.php';


if ($cfg['lang_enable']) {
	$smarty->assign('lang_tree',$db->selectCol("SELECT id as ARRAY_KEY,name	FROM ?_lang ORDER BY name"));
}
$smarty->assign('mods',$cms->mods_list());
$smarty->assign('site_domain',$cfg['site_domain']);

if ($cnt = $db->selectCell("-- CACHE: 1m
		SELECT count(*) FROM ?_price WHERE price>0 AND price<500 AND sklad>0")) {
	$smarty->assign('price_low',$cnt);
}

$smarty->caching = false;
$smarty->display('header.tpl');

// menu
$core = $db->selectCell("SELECT class FROM ?_site WHERE id=?d LIMIT 1",
	isset($_GET['mod'])  ? $_GET['mod'] : '');
insert_menu_tree(isset($_GET['mod']) ? $_GET['mod'] : '');


if (!isset($_GET['mod'])) {
	if ($_SESSION['auth']['access'] == 'moder' && $_SESSION['auth']['access_id'] == 33)
	{
		include_once("core/spp2/mod.php");
		mod_list();
	}
	else
	{
	    echo isset($cfg['license']) ? $cms->utf($cfg['license']) : 'Выберите раздел';
	}
} elseif ($_GET['mod'] == '0') {
	include_once("core/_site/mod.php");
} elseif ($core) {
	if (file_exists("core/".$core."/mod.php"))
		include_once("core/".$core."/mod.php");
	else
		echo "модуль отключен";
} else {
/*
	echo "неверный запрос (error 56366, сообщите разработчику)<br>";
	echo "GET ".print_r($_GET,1)."<br>";
	echo "POST ".print_r($_POST,1)."<br>";
	echo "SESSION ".print_r($_SESSION['auth'],1)."<br>";
	echo "REFERER ".$_SERVER['HTTP_REFERER']."<br>";
	echo "URL ".$_SERVER['REQUEST_URI']."<br>";
*/
}



$time_end = microtime_float();
$time = $time_end - $time_start;
$smarty->assign('time_exec',sprintf("%01.3f",$time));
$smarty->assign('sql_count', $db->sql_count);
$smarty->assign('memcache',$cms->memcache);
$smarty->display('footer.tpl');

//print_r($_SESSION['auth']);

ob_end_flush();

function microtime_float() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

?>
