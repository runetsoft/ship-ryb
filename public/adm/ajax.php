<?

require dirname(__FILE__).'/../conf/conf.php';
require $cfg['dir_adm'].'/inc/local.php';

require_once dirname(__FILE__)."/lib/php/JsHttpRequest/JsHttpRequest.php";
$JsHttpRequest = new JsHttpRequest("utf-8");

// news

if ($_REQUEST['do'] == 'good_list') {
	include dirname(__FILE__).'/core/price/ajax.php';
	ajax_good_list();

// image

} elseif ($_REQUEST['do'] == 'imageGetJson') {

	$folders = array(
		'/files/ajax',
		'/files/news/img',
		'/files/news/1',
		'/files/news/2',
	);
	foreach($folders as $folder) {

		$files = scandir($cfg['dir_root'].$folder);
		foreach ($files as $file) {
			if ($file == '.' || $file == '..') continue;
			if (!preg_match('/^.+\.(png|gif|jpe?g)$/i', $file)) continue;
			$a = array('thumb'=>$folder.'/'.$file,'image'=>$folder.'/'.$file,'title'=>$file,'folder'=>$folder);
			$array[] = $a;
		}
	}
	echo stripslashes(json_encode($array));

}

//*******************************************************************************************

