
<!-- action multi -->

{*
параметры (default):

colspan - количество солбцов в таблице выше.
checkbox_name - имя для групповых пометок
update_ids - массив id для групповых пометок
mod - указание режима.
mod2 (false) - указание подрежима.
select - тип, кнопка или выпадающий список.
    submit - кнопка.
    select - выпадающий спискок.

*}
<tr class="grid">
    <td colspan="{$colspan}" align="right">

{if $update_ids}
{literal}
<script language="JavaScript">
<!-- Begin
function checkAll{/literal}{$id_prefix}{literal}(b) {
    var ids = new Array({/literal}{$update_ids}{literal});
    var x = 0;
    for (i in ids) {
	document.getElementById('{/literal}{$id_prefix}{literal}'+ids[x]).checked = b ;
        x++;
    }
}
// End -->
</script>
{/literal}
[
<a href="javascript:void(0)" onclick="checkAll{$id_prefix}(true);" style="text-decoration:none"
    {popup left="10" width="80" text="Выделить все"}>+</a>
<a href="javascript:void(0)" onclick="checkAll{$id_prefix}(false);" style="text-decoration:none"
    {popup left="10" width="150" text="Снять выделение со всех"}>&ndash;</a>
]

{/if}	

        <input type="hidden" name="mod" value="{$mod}">
	{if $mod2}
	    <input type="hidden" name="mod2" value="{$mod2}">
	{/if}
        <input type="hidden" name="do" value="multi">
	{if $button_edit}
    	    <input type="image" src="img/icon/b_edit.png" align="absmiddle"
	    name="multi_edit" style="border:0px"
	    {popup left="10" width="140" text="Изменить выделенное"}>
	{/if}
	{if $button_del}
    	    <input type="image" src="img/icon/b_drop.png" align="absmiddle"
	    name="multi_del" style="border:0px"
	    {popup left="10" width="140" text="Удалить выделенное"}>
	{/if}
    </td>
</tr>

<!-- /action multi -->
