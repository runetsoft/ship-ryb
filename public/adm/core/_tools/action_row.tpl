
<!-- action row -->

{*
параметры (default):

checkbox (false) - показывать чекбокс для мульти команд? параметр - id.
edit_url (false) - показывать ссылку на всплывающее окно редактирования? параметр - url.
target (edit) - имя всплывающего окна.
window_width - ширина всплывающего окна.
window_height - высота всплывающего окна.
del_url (false) - показывать ссылку на удаление? параметр - url.
del_text (Да, удалить!) - текст подтверждения удаления.
if_prefix (empty) - id для групповой пометки

*}
<td align="center" nowrap>
    {if $checkbox}
        <input type="checkbox"
		{if $id_prefix}
			id="{$id_prefix}{$checkbox}" 
		{/if}
			name="update[{$checkbox}]"
			{popup width="50" left="10" text="Выделить"} style="border:0px">
	{/if}
    {if $edit_url}
		<a href="{$edit_url}" style="text-decoration: none;">
		<span {popup width="50" left="10" text="Изменить"}>
		{html_image file="img/icon/b_edit.png" border="0" align="absmiddle" alt=""}</span></a>
    {/if}
    {if $del_url}
		<a href="javascript:void(0)" {popup sticky="true" trigger="onclick" height="10" above="true"
	        caption="Подтверждение удаления" text="$del_text"} style="text-decoration: none;">
		<span {popup width="50" left="10" text="Удалить"}>
		{html_image file="img/icon/b_drop.png" border="0" align="absmiddle" alt=""}</span></a>
    {/if}
</td>

<!-- /action row -->
