
<!-- pages list -->

{*
параметры (default):

offset (5) - сколько сраниц показывать слева и справа.
colspan - количество солбцов в таблице выше.
page_url - url, к которому будет добавлять постраничный вызов.
add_url (false) - показывать ссылку на всплывающее окно добавления? параметр - url.
target (add) - имя всплывающего окна.
window_width - ширина всплывающего окна.
window_height - высота всплывающего окна.

*}
<tr class="grid">
    <td colspan="{$colspan}">
	<table width="100%">
	<tr>
		{if $page_url}
	    <td align="left">Всего записей: <b>{$count}</b></td>
	    <td align="center">Страница:
		{section name=page loop=$pages}
		    {if $page > $offset && $smarty.section.page.first}
			<a href="{$page_url}&page=1">1</a>
		        {if $page-1 <> $offset}...{/if}
		    {/if}
		    {if $smarty.section.page.index+1+$offset > $page &&
			$smarty.section.page.index+1-$offset < $page}
   			        {if $page == $smarty.section.page.index+1}
			    	    <b>{$smarty.section.page.index+1}</b>
			{else}
		    	    <a href="{$page_url}&page={$smarty.section.page.index+1}">
			    {$smarty.section.page.index+1}</a>
			{/if}
		    {/if}
		    {if $page < $smarty.section.page.loop+1-$offset && $smarty.section.page.last}
			{if $page <> $smarty.section.page.loop-$offset}...{/if}
		        <a href="{$page_url}&page={$smarty.section.page.loop}">{$smarty.section.page.loop}</a>
		    {/if}
		{sectionelse}
		    <b>0</b>
		{/section}
	    </td>
	    <td align="center">
		Строк на странице:
		<span {popup width="60" text="Строк на странице"}>
		    {if $elements == 20}<b>20</b>{else}<a href="cmd.php?mod=set&elements=20">20</a>{/if}
		    {if $elements == 50}<b>50</b>{else}<a href="cmd.php?mod=set&elements=50">50</a>{/if}
		    {if $elements == 300}<b>300</b>{else}<a href="cmd.php?mod=set&elements=300">300</a>{/if}
		</span>
	    </td>
		{/if}
	    {if $add_url}
			<td align="right">
		    	<a href="{$add_url}">
    		    <span {popup width="50" left="10" text="Добавить"}>
    		    {html_image file="img/icon/b_insrow.png" border="0" align="absmiddle" alt=""}</span></a>
	        </td>
	    {/if}
	</tr>
	</table>
    </td>
</tr>

<!-- /pages list -->
