<!-- sort -->
{*
параметры

index - номер сортировки
url - url при переходах
dir - направление
    up - сортировка по возрастанию
    down - сортировка по убыванию
*}

{if $sort == $index}
    <span {popup width="100" text="отсортировано по возрастанию"}>{html_image file="img/icon/s_desc.png"}</span><a href="{$url}&sort={$index+1}"
    {popup width="100" text="сортировать по убыванию"}><i>{$text}</i></a>
{elseif $sort == $index+1}
    <span {popup width="100" text="отсортировано по убыванию"}>{html_image file="img/icon/s_asc.png"}</span>
    <a href="{$url}&sort={$index}" {popup width="100" text="сортировать по возрастанию"}><i>{$text}</i></a>
{else}
    <a href="{$url}&sort={$index}" {popup width="100" text="сортировать по возрастанию"}>{$text}</a>
{/if}

<!-- /sort -->
