<?

function mod_list () {
	global $cfg, $db, $smarty, $cms;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = $cms->print_html($find['text']);
		$s = addslashes($find['text']);
		$WHERE_OR[] = "provider like '%$s%'";
	}

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY provider",		#0
		"ORDER BY provider DESC",
		"ORDER BY price_cnt",		#2
		"ORDER BY price_cnt DESC",
		"ORDER BY price_full_cnt",		#4
		"ORDER BY price_full_cnt DESC",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 5;

	// page
	$count = $db->selectCell("-- CACHE: 1
					SELECT count(*)
					FROM ?_provider p
					$WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("-- CACHE: 1
					SELECT p.*,c.city,g.provider_group,
					(SELECT count(*) FROM ?_price WHERE provider_id=p.id) as price_cnt,
					(SELECT count(*) FROM ?_price WHERE provider_id=p.id AND sklad!=0 AND price!=0) as price_full_cnt
					FROM ?_provider p
					LEFT JOIN ?_city c ON c.id=p.city_id
					LEFT JOIN ?_provider_group g ON g.id=p.provider_group_id
					$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i => $v) {
		foreach (array('provider','org_name') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		foreach (array('dt_price_load') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
		
	}
	$smarty->assign('list',$list);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);	
	$smarty->assign('page_title','Список поставщиков');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}

//----------------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	if ($form = $db->selectRow("SELECT * FROM ?_provider WHERE id=?d LIMIT 1", intval($_GET['id']))) {
		$page_title = 'Изменить';
		$do_add = 0;
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['status'] = 1;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('provider','org_name') as $v)
		if (isset($form[$v])) $form[$v] = $cms->print_html($form[$v]);

	$r = $db->select("SELECT id,city FROM ?_city ORDER BY city");
	foreach($r as $v)
		$form['city_tree'][$v['id']] = $v['city'];
	
	$r = $db->select("SELECT id,provider_group FROM ?_provider_group ORDER BY provider_group");
	foreach($r as $v)
		$form['group_tree'][$v['id']] = $v['provider_group'];
	

	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' запись');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

}

//------------------------------------------------------------------------------------

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['provider'])
		$bad[] = 'Пустой поставщик';
	elseif ($db->selectRow("SELECT id FROM ?_provider WHERE provider=? {AND id!=?d}", 
			$_POST['form']['provider'],
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id']))
		$bad[] = 'Такой поставщик уже существует';
	if (!$_SESSION['form']['city_id'])
		$bad[] = 'Не выбран город';
	if (!$_SESSION['form']['provider_group_id'])
		$bad[] = 'Не выбрана группа';

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else					$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if ($_POST['do_add']) {
		$id = $db->query("INSERT INTO ?_provider
					(dt_insert,city_id,provider_group_id,provider,org_name,status,uuid)
					VALUES (now(),?d,?d,?,?,?d,uuid())",
			$_SESSION['form']['city_id'],
			$_SESSION['form']['provider_group_id'],
			$_SESSION['form']['provider'],
			$_SESSION['form']['org_name'],
			$_SESSION['form']['status']
		);

		// добавить ко всем магазинам
		$shops = $db->selectCol("SELECT id FROM ?_shop");
		foreach ($shops as $shop_id) {
			$db->query("INSERT IGNORE INTO ?_shop_provider (dt_insert,shop_id,provider_id) VALUES (now(),?d,?d)",$shop_id,$id);
		}

		$cms->set_status('ok',"Добавлена запись");
		$cms->mail_debug('Добавлен поставщик');
	} else {

		$db->query("UPDATE ?_provider SET dt_update=now(),
			city_id=?d,provider_group_id=?d,provider=?,org_name=?,status=?d WHERE id=?d",
			$_SESSION['form']['city_id'],
			$_SESSION['form']['provider_group_id'],
			$_SESSION['form']['provider'],
			$_SESSION['form']['org_name'],
			$_SESSION['form']['status'],
			$_POST['id']);
		$cms->set_status('ok',"Изменена запись");
		$cms->mail_debug('Изменен поставщик');
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//-------------------------------------------------------------------------------------

function cmd_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_provider WHERE id=?d LIMIT 1", intval($_GET['id']));

	if ($_SESSION['auth']['id'] != 1) {
		$cms->set_status('bad',"Не надо удалять. Ну если надо, попроси...");
		$cms->mail_debug('Попытка удалить провайдера',print_r($r,1));
	} else {
		$db->query("DELETE FROM ?_provider WHERE id=?d LIMIT 1", $r['id']);
		$db->query("DELETE FROM ?_shop_provider WHERE provider_id=?d", $r['id']);
		
		$ids = $db->selectCol("SELECT id FROM ?_price WHERE provider_id=?d",$r['id']);
		foreach ($ids as $id) {
			$db->query("DELETE FROM ?_price WHERE id=?d LIMIT 1",$id);
			$db->query("DELETE FROM ?_good_price WHERE price_id=?d LIMIT 1",$id);
		}

		$cms->set_status('ok',"Удален поставщик");
		$cms->set_status('ok',"Удалено позиций прайса с соответсвиями: ".count($ids));
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

#################################################################################################

function cmd_del2() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_provider WHERE id=?d LIMIT 1", intval($_GET['id']));
	
	if ($db->select("SELECT * from ?_price WHERE provider_id=?d",$r['id']))
	$bad[] = 'Нельзя удалять провайдеров с непустыми прайсами';
	
	if ($db->select("SELECT * from ?_shop_provider WHERE provider_id=?d",$r['id']))
	$bad[] = 'Провайдер привязан к магазину, сначала нужно отцепить';
	
	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	$db->query("DELETE FROM ?_provider WHERE id=?d LIMIT 1", $r['id']);	
	$cms->set_status('ok',"Удален поставщик");
	
	/*
	if ($_SESSION['auth']['id'] != 1) {
		$cms->set_status('bad',"Не надо удалять. Ну если надо, попроси...");
		$cms->mail_debug('Попытка удалить провайдера',print_r($r,1));
	} else {
		$db->query("DELETE FROM ?_provider WHERE id=?d LIMIT 1", $r['id']);
		$db->query("DELETE FROM ?_shop_provider WHERE provider_id=?d", $r['id']);
		
		$ids = $db->selectCol("SELECT id FROM ?_price WHERE provider_id=?d",$r['id']);
		foreach ($ids as $id) {
			$db->query("DELETE FROM ?_price WHERE id=?d LIMIT 1",$id);
			$db->query("DELETE FROM ?_good_price WHERE price_id=?d LIMIT 1",$id);
		}

		$cms->set_status('ok',"Удален поставщик");
		$cms->set_status('ok',"Удалено позиций прайса с соответсвиями: ".count($ids));
	}
	*/

	
	header("Location: ".$_SERVER['HTTP_REFERER']);

}

?>