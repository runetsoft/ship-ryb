{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`"}
{assign var=my_colspan value="10"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td width="90%"><input type="text" name="find[text]" value="{$find.text_html}" style="width:95%"></td>
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="15%"><b>Город</b></td>
	<td width="10%"><b>Группа</b></td>
	<td width="40%"><b>{insert name="list_sort" index="0" url="?$my_url&page=$page" text="Провайдер"}</b></td>
	<td width="25%"><b>Организация</b></td>
	<td width="10%"><b>{insert name="list_sort" index="2" url="?$my_url&page=$page" text="Номенклатура"}</b></td>
	<td width="10%"><b>{insert name="list_sort" index="4" url="?$my_url&page=$page" text="Номенклатура с ценой и складом"}</b></td>
	<td width="15%"><b>Дата загрузки последнего прайса</b></td>
	<td width="5%"><b>Статус</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td>{$i.city}</td>
	<td>{$i.provider_group}</td>
	<td align="left">{$i.provider}</td>
	<td align="left">{$i.org_name}</td>
	<td><a href="?mod={$mods.price}&find[provider_id]={$i.id}">{$i.price_cnt}</a></td>
	<td>{$i.price_full_cnt}</td>
	<td>{$i.dt_price_load}</td>
	<td>
		{if $i.status}	<font color="green">активен</font>
		{else}		-
		{/if}
	</td>
	
	{insert name="action_row"
		edit_url="?$my_url&do=edit&id=`$i.id`"
		del_url="cmd.php?$my_url&do=del&id=`$i.id`"}

</tr>
{/foreach}

{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?$my_url&do=edit"}

</form>
</table>


<!-- /list -->
