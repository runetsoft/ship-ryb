<?

	$scheme_tree = array(
		'csv'			=> 'CSV (название[1];склад[2];цена[3];ЗА[4];МРЦ[5])',
		'automaster'	=> 'Автомастер (название[1-7];склад[12];цена[13])',
		'itr'			=> 'I.T.R. (название[4];склад[6];цена[10-11])',
		'autoexpert'	=> 'АвтоЭксперт (артикул буква название[1];склад[2];цена[3])',
		'discoptim'		=> 'Дископтим (название[1-10];склад[13];цена[15])',
		'atd_len'		=> 'ATD Ленинский (название[1-6];цена[7],склад[8])',
		'i_shin_d'		=> 'I Shin диски (название[1],склад[3],цена[4])',
		'mvo'			=> 'MVO (название[3],бренд[5],склад[9],цена[10])',
		'optshintorg'	=> 'Оптшинторг (название[3],бренд[5],склад[9],цена[10])',
		'fin_shina'		=> 'ФинШина (название[1],название[2],склад[3],цена[4])',
		'kama_spb'		=> 'Кама Санкт-Петербург (название[1],название[2],название[3],склад[4],цена[5])',
	);

function cmd_load() {
	global $cfg, $db, $cms, $scheme_tree;

//	$cms->set_status('ok',print_r($_POST,1));


	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['provider_id'])
		$bad[] = 'Пустой поставщик';
	elseif (!$provider = $db->selectRow("SELECT * FROM ?_provider WHERE id=?d LIMIT 1",$_POST['form']['provider_id']))
		$bad[] = 'Такой поставщик несуществует';

	if (!$scheme_tree[$_POST['form']['scheme_id']])
		$bad[] = 'Такого фильтра нет';

	if (!is_uploaded_file($_FILES['file']['tmp_name']))
		$bad[] = 'Не выбран файл';
	elseif (!$h = fopen($_FILES['file']['tmp_name'], "r"))
		$bad[] = 'Файл не открывается';


	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}
	$log['provider_id'] = $provider_id = $_POST['form']['provider_id'];
	$log['provider'] = $provider;
	$log['scheme'] = $scheme = $_POST['form']['scheme_id'];
	$db->query("UPDATE ?_price SET pass=0 WHERE provider_id=?d",$provider_id);
	$db->query("UPDATE ?_provider SET dt_price_load=now() WHERE id=?d",$provider_id);


	setlocale(LC_ALL, 'ru_RU.CP1251');

	while (($a = fgetcsv($h, 2000, ";")) !== false && $h) {
		$log['line']++;
		$line_ok = 0;

//------------------------------------------------------------------------

		if ($scheme == 'csv') { //--------------------------------------------------		
			if ($a[0] && preg_match('/\d+/', $a[1]) && preg_match('/\d+/', $a[2])) {
				$line_ok = 1;
				$good = trim($a[0]);
				//$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[1];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[2];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
				
				$zarticle = $a[3];

				$rprice = $a[4];
				$rprice = preg_replace('/,/', '.', $rprice);
				$rprice = preg_replace('/[^\d\.]/', '', $rprice);				
			
			
			}
			
		} elseif ($scheme == 'automaster') { //--------------------------------------------------

			if ($a[0] && $a[1] && $a[2] && $a[4] && $a[5] && $a[6] && $a[11] && isset($a[12])) {
				$line_ok = 1;
				$good = trim($a[0]).' '.
					trim($a[1]).' '.
					trim($a[2]).($a[3] ? '/'.$a[3] : '').' '.
					'R'.trim($a[4]).' '.
					trim($a[5]).trim($a[6]);
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = preg_replace('/[^\d\.]/', '', $a[11]);
				$price = preg_replace('/[^\d\.]/', '', $a[12]);
			}

		} elseif ($scheme == 'itr') { //--------------------------------------------------

			if ($a[3] && preg_match('/\d/',$a[5]) && (preg_match('/\d+/',$a[9]) || preg_match('/\d+/',$a[10]))) {
				$line_ok = 1;
				$good = trim($a[3]);
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[5];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[10] ? $a[10] : $a[9];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'autoexpert') { //--------------------------------------------------

			if (preg_match('/^\d+[ ]+.+?(\d.+)$/', $a[0],$a0) && preg_match('/\d+/', $a[1]) && preg_match('/\d+/', $a[2])) {
				$line_ok = 1;
				$good = trim($a0[1]);
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[1];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[2];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'discoptim') { //--------------------------------------------------
			if ($a[0] && $a[2] && $a[3] && $a[4] && $a[5] && $a[7] && $a[8] && $a[9] && preg_match('/\d+/', $a[12]) && preg_match('/\d+/', $a[14])) {
				$line_ok = 1;
				// Replay FD20 5.5x14/4x108 D63.3 ET47.5 S
				$good = $a[0].' '.$a[2].'x'.$a[3].'/'.$a[4].'x'.$a[5].' D'.$a[8].' ET'.$a[7].' '.$a[9];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[12];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[14];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'atd_len') { //--------------------------------------------------
			if ($a[1] && $a[2] && $a[3] && preg_match('/\d+/', $a[6]) && preg_match('/\d+/', $a[7])) {
				$line_ok = 1;
				$good = $a[1].' '.$a[2].' '.$a[3].($a[4] ? ' '.$a[4] : '');
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[7];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[6];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'i_shin_d') { //--------------------------------------------------
			if (preg_match('/^(.+){17,99}$/', $a[0]) && preg_match('/\d+/', $a[2]) && preg_match('/\d+/', $a[3])) {
				$line_ok = 1;
				$good = $a[0];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[2];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[3];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'mvo') { //--------------------------------------------------
			if ($a[2] && $a[4] && preg_match('/\d+/', $a[8]) && preg_match('/\d+/', $a[9])) {
				$line_ok = 1;
				$good = $a[4].' '.$a[2];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[8];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[9];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'optshintorg') { //--------------------------------------------------
			if ($a[1] && $a[3] && $a[4] && preg_match('/\d+/', $a[6]) && preg_match('/\d+/', $a[8])) {
				$line_ok = 1;
				$good = $a[1].' '.$a[3].' '.$a[4];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[6];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[8];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'fin_shina') { //--------------------------------------------------
			if ($a[0] && $a[1] && preg_match('/\d+/', $a[2]) && preg_match('/\d+/', $a[3])) {
				$line_ok = 1;
				$good = $a[0].' '.$a[1];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[2];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[3];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			}

		} elseif ($scheme == 'kama_spb') { //--------------------------------------------------
			if ($a[0] && $a[1] && $a[2] && preg_match('/\d+/', $a[3]) && preg_match('/\d+/', $a[4])) {
				$line_ok = 1;
				$good = $a[0].' '.$a[1].' '.$a[2];
				$good = iconv('windows-1251', 'utf-8', $good);
				$sklad = $a[3];
				$sklad = preg_replace('/,/', '.', $sklad);
				$sklad = preg_replace('/[^\d\.]/', '', $sklad);
				$price = $a[4];
				$price = preg_replace('/,/', '.', $price);
				$price = preg_replace('/[^\d\.]/', '', $price);
			
			}
			
		} // scheme end


if ($_SERVER['REMOTE_ADDR'] == '188.134.39.87-')		$debug = 1;
else												$debug = 0;

if (1 && $debug && $log['line'] == 58) {
    print"<pre>";
    print_r($a);
    print_r(array('good'=>$good,'sklad'=>$sklad,'price'=>$price));
    exit;
}

//-----------------------------------------------------------------------------

		if ($line_ok) {
			$log['line_ok']++;
			if ($r = $db->selectRow("SELECT * FROM ?_price WHERE provider_id=?d AND good=? LIMIT 1",$provider_id,$good)) {
				if ($r['pass'] == 0) {
					$log['good_update']++;
					if ($debug)
						$db->query("UPDATE ?_price SET pass=1 WHERE id=?d LIMIT 1",$sklad,$price,$r['id']);
					else {
						
						if ($zarticle){
							$za_check = $db->selectCell("SELECT zarticle from ?_price WHERE id=?d",$r['id']);
							if ($zarticle==$za_check || $za_check == '' || $_POST['update_za']==1) {$za_ok = 1;} else {$za_ok=0; $cms->set_status('bad','Для товара '.$r['id'].' ЗА не совпадает, не обновили');}
						}
						
					$db->query("UPDATE ?_price SET dt_price=now(),sklad=?d,price=?d,rprice=?d{,zarticle=?},pass=1 WHERE id=?d LIMIT 1",$sklad,$price,$rprice, $za_ok ? $zarticle : DBSIMPLE_SKIP,$r['id']);
					
							//простановка ЗА и МРЦ в товар
							if ($good_price_row = $db->selectRow("SELECT * from ?_good_price WHERE price_id=?d",$r['id'])){
								$good_row = $db->selectRow("SELECT * from ?_good WHERE id=?d LIMIT 1",$good_price_row['good_id']);
								
								//простановка ЗА
								if ($zarticle && !$good_row['zarticle'])
								$db->query("UPDATE ?_good SET zarticle=? WHERE id=?d LIMIT 1",$zarticle,$good_row['id']);
							
								//простановка МРЦ
								if ($rprice>0)
								$db->query("UPDATE ?_good SET rprice=?d WHERE id=?d LIMIT 1",$rprice,$good_row['id']);
							}
					
					}
				} else {
					$log['good_dub']++;
					$cms->set_status('bad',"В строке ".$log['line']." найден дубликат: ".implode(";",$a));
				}

			} else {
				if (!$debug)
					$db->query("INSERT INTO ?_price (dt_insert,dt_update,dt_price,provider_id,good,sklad,price,rprice,zarticle)
						VALUES (now(),now(),now(),?d,?,?d,?d,?d,?)",
						$provider_id, $good, $sklad, $price, intval($rprice), $zarticle ? $zarticle : ''
					);
				
				$cms->set_status('ok',"Добавлен товар ".$good);
				$log['good_new']++;
			}
		} else {
			$log['line_bad']++;
			$cms->set_status('bad',"Строка ".$log['line']." невалидна: ".iconv('windows-1251', 'utf-8', implode(";",$a)));
		}

	} // file

	$db->query("UPDATE ?_price SET sklad=0 WHERE provider_id=?d AND pass=0",$provider_id);

	$cms->set_status('ok',"Всего строк в файле ".intval($log['line']));
	if ($log['line_ok'])		$cms->set_status('ok', "Всего строк загружено ".$log['line_ok']);
	if ($log['line_bad'])		$cms->set_status('bad', "Всего строк незагружено ".$log['line_bad']);
	if ($log['good_new'])		$cms->set_status('ok', "Добавлено новых товаров ".$log['good_new']);
	if ($log['good_update'])	$cms->set_status('ok', "Обновлено товаров ".$log['good_update']);
	if ($log['good_dub'])		$cms->set_status('ok', "Найдено дубликатов ".$log['good_dub']);
	if ($debug)					$cms->set_status('bad', print_r($log,1));

	$cms->mail_debug('price_load',print_r($log,1));

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

function cmd_load2() {
	global $db, $cms;
	
	//$debug = 1;
		
	if ($_GET['site'] == 'bustofinland') {
		//$f = file_get_contents('http://bustofinland.ru/index.php/shoptur/shiny');
		$f = get_f();
		//$f = $cms->win($f);
		//$log['f'] = $f;
		preg_match_all('/<tr style="background-color: #dddddd;" dir="" id="" bgcolor="" height="" lang="" valign="" align="">'.
						'.?<td>.?<p><strong>(.+?)<\/strong>.*?<\/p>.?<\/td>'.
						'.?<td>.?<p>(\d+).*?<\/p>.?<\/td>.?<\/tr>/is', $f, $reg);
		unset($reg[0]);
		$log['reg'] = $reg;
		foreach ($reg[1] as $i=>$v) {
				$g[] = array('good'=>$reg[1][$i],'sklad'=>20,'price'=>$reg[2][$i]);
		}
		$log['g'] = $g;
		$provider_id = 47;
	}

	$provider = $db->selectRow("SELECT * FROM ?_provider WHERE id=?d LIMIT 1",$provider_id);
	$log['provider'] = $provider;
	$db->query("UPDATE ?_price SET pass=0 WHERE provider_id=?d",$provider_id);
	$db->query("UPDATE ?_provider SET dt_price_load=now() WHERE id=?d",$provider_id);
	

	if (count($g)) {
		foreach ($g as $v) {
			$good = $v['good'];
			$sklad = $v['sklad'];
			$price = $v['price'];

			if ($r = $db->selectRow("SELECT * FROM ?_price WHERE provider_id=?d AND good=? LIMIT 1",$provider_id,$good)) {
				if ($r['pass'] == 0) {
					$log['good_update']++;
					if ($debug)
						$db->query("UPDATE ?_price SET pass=1 WHERE id=?d LIMIT 1",$sklad,$price,$r['id']);
					else
						$db->query("UPDATE ?_price SET dt_price=now(),sklad=?d,price=?d,pass=1 WHERE id=?d LIMIT 1",$sklad,$price,$r['id']);
				} else {
					$log['good_dub']++;
					$cms->set_status('bad',"В строке ".$log['line']." найден дубликат: ".implode(";",$a));
				}

			} else {
				if (!$debug)
					$db->query("INSERT INTO ?_price (dt_insert,dt_update,dt_price,provider_id,good,sklad,price)
						VALUES (now(),now(),now(),?d,?,?d,?d)",
						$provider_id, $good, $sklad, $price
					);
				$log['good_new']++;
			}
		}

		$log['line'] = count($reg[1]);
		$cms->set_status('ok',"Всего строк найдено ".intval($log['line']));
		if ($log['good_new'])		$cms->set_status('ok', "Добавлено новых товаров ".$log['good_new']);
		if ($log['good_update'])	$cms->set_status('ok', "Обновлено товаров ".$log['good_update']);
		if ($log['good_dub'])		$cms->set_status('ok', "Найдено дубликатов ".$log['good_dub']);

	} else {
		$cms->set_status('bad', 'Ничего не найдено');
	}
	if ($debug)					$cms->set_status('bad', print_r($log,1));
	header("Location: ".$_SERVER['HTTP_REFERER']);
}


function cmd_load_csv() {
	global $db, $cms, $cfg;
	
	if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {

		
	$price_id = $db->selectCell("SELECT id from ?_price WHERE good=? and provider_id=?d LIMIT 1",iconv('cp1251','utf-8',trim($data[1])),$_POST['form']['provider_id']);
	
	/*
	print $_POST['form']['provider_id'];
	print_r($data); 
	print '<hr>';
	print $price_id;
	exit; 
	*/
	
	
	if ($price_id>0 && $data[0]!=''){
	$db->query("DELETE from ?_good_price WHERE price_id=?d LIMIT 1",$price_id);
	
		$good_id  = $db->selectCell("SELECT id from ?_good WHERE good=? LIMIT 1",iconv('cp1251','utf-8',$data[0]));
		if(!$good_id)
		$good_id = $cms->cmd_good_new(array('good'=>iconv('cp1251','utf-8',$data[0])));
		
		//$good_id = $db->query("INSERT into ?_good (dt_insert,adm_id,good) VALUES (now(),?d,?)",$_SESSION['auth']['id'],iconv('cp1251','utf-8',$data[0]));
		
		$db->query("INSERT into ?_good_price (adm_id,good_id,price_id) VALUES (?d,?d,?d)",$_SESSION['auth']['id'],$good_id,$price_id);

	
	} else {
	$log['bad'][] = 'Для товара '.iconv('cp1251','utf-8',$data[0]).' не найдено совпадений в прайсе поставщика';
	}
	
	/*
	print "<pre>";
	print_r($_POST);
	print_r($_SESSION);
	*/
	
	//print "<hr>";
	
	
    }
    fclose($handle);
	}	
	
	//exit;
	
	$cms->set_status('ok', 'Файл обработан');
	if(count($log['bad'])>0) $cms->set_status('bad',"Лог разбора: ".print_r($log['bad'],1));
	
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

function cmd_load_xls() {
	global $db, $cms, $cfg;

	include $cfg['dir_root'].'/adm/lib/php/PHPExcel/v1.7.9/Classes/PHPExcel/IOFactory.php';

	//~~~~~~~~~~~~~~~~~~~ Загрузка реестра ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	$inputFileName = $_FILES['file']['tmp_name'];
	if (!file_exists($inputFileName)) exit;

	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);	
	$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true); 	

	foreach ($sheetData as $i=>$v){
	
		$price_id = $db->selectCell("SELECT id from ?_price WHERE good=? and provider_id=?d LIMIT 1",$v['B'],$_POST['form']['provider_id']);

		if ($price_id>0 && $v['A']!=''){
		$db->query("DELETE from ?_good_price WHERE price_id=?d LIMIT 1",$price_id);
	
		$good_id  = $db->selectCell("SELECT id from ?_good WHERE good=? LIMIT 1",$v['A']);
		if(!$good_id)
		$good_id = $cms->cmd_good_new(array('good'=>$v['A']));
		
		$db->query("INSERT into ?_good_price (adm_id,good_id,price_id) VALUES (?d,?d,?d)",$_SESSION['auth']['id'],$good_id,$price_id);

	
		} else {
		$log['bad'][] = 'Для товара '.iconv('cp1251','utf-8',$v['A']).' не найдено совпадений в прайсе поставщика';
		}
	
	}
	

	
	
	$cms->set_status('ok', 'Файл обработан');
	header("Location: ".$_SERVER['HTTP_REFERER']);
}


