<?

function mod_list () {
	global $cfg, $db, $smarty, $cms, $scheme_tree;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = urlencode($find['text']);
		$s = addslashes(trim($find['text']));
		//$WHERE_OR[] = "p.article like '%$s%'";
		$WHERE_OR[] = "p.good like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "p.id=$s";
	}

	if ($find['provider_id']) $WHERE_AND[] = "p.provider_id=".intval($find['provider_id']);
	if ($find['adm_id']) $WHERE_AND[] = "a.id=".intval($find['adm_id']);
	if ($find['empty']) $WHERE_AND[] = "gp.price_id IS NULL";

	if (!empty($find['price1'])) $WHERE_AND[] = 'price>='.intval($find['price1']);
	if (!empty($find['price2'])) $WHERE_AND[] = 'price<='.intval($find['price2']);
	
	if (empty($find['sklad']) && $find['sklad']!='0') $find['sklad'] = 8;
	if (!empty($find['sklad'])) $WHERE_AND[] = 'sklad>='.intval($find['sklad']);

	if ($_SESSION['auth']['access_id'] == 3) {
		$provider_list = $db->selectCol("-- CACHE: 1m
			SELECT provider_id FROM ?_adm_provider WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$provider_list[] = 0;
		$WHERE_AND[] = 'p.provider_id IN ('.implode(',', $provider_list).')';
	} else {
		$provider_list[] = 0;
	}
	


	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY good",		#0
		"ORDER BY good DESC",
		"ORDER BY sklad",		#2
		"ORDER BY sklad DESC",
		"ORDER BY price",		#4
		"ORDER BY price DESC",
		"ORDER BY dt_price",		#6
		"ORDER BY dt_price DESC",
		"ORDER BY rprice",			#8
		"ORDER BY rprice DESC",		

	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 5;

	// page
	$count = $db->selectCell("-- CACHE: 10
				SELECT count(*)
				FROM ?_price p
				LEFT JOIN ?_good_price gp	ON gp.price_id=p.id
				LEFT JOIN ?_adm a 		ON gp.adm_id=a.id
				$WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("
				SELECT p.id,p.good,p.sklad,p.price,p.rprice,p.zarticle,date(p.dt_price) as dt_price,
					pro.provider,
					g.good as good_orig, date(gp.dt_insert) as good_orig_insert,
					a.login
				FROM ?_price p
				JOIN ?_provider pro		ON pro.id=p.provider_id
				LEFT JOIN ?_good_price gp	ON gp.price_id=p.id
				LEFT JOIN ?_good g		ON g.id=gp.good_id
				LEFT JOIN ?_adm a 		ON gp.adm_id=a.id
				$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i=>$v) {
		$list[$i]['good_html'] = rawurlencode($list[$i]['good']);

		foreach (array('good_orig_insert','dt_price') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
		foreach (array('article','good','good_orig') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		$list[$i]['price_chunk'] = $cms->print_chunk($v['price']);
		$list[$i]['rprice_chunk'] = $cms->print_chunk($v['rprice']);
		
		
	}

	if (!isset($form)) $form = array();
	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	$r = $db->select("-- CACHE: 1m
			SELECT *
			FROM ?_provider
			WHERE TRUE {AND 1=?d AND id IN (".implode(',', $provider_list).")}
			ORDER BY provider",
			($_SESSION['auth']['access_id'] == 3 ? 1 : DBSIMPLE_SKIP)
	);
	$find['provider_tree'][0] = '-все поставщики-';
	foreach($r as $v)
		$find['provider_tree'][$v['id']] = $cms->print_html($v['provider']);
	foreach($r as $v)
		$form['provider_tree'][$v['id']] = $cms->print_html($v['provider']);

	$r = $db->select("-- CACHE: 1m
			SELECT *
			FROM ?_adm
			ORDER BY login");
	$find['adm_tree'][0] = '-все авторы-';
	foreach($r as $v)
		$find['adm_tree'][$v['id']] = $cms->print_html($v['login']);

	$form['scheme_tree'] = $scheme_tree;
	// defaul
	//$form['provider_id'] = 37;

	
	//print "<pre>";
	//print_r($find);
	
	
	$smarty->assign('form',$form);
	$smarty->assign('list',$list);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);	
	$smarty->assign('page_title','Список прайсов');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
//print_r($provider_list);
}

//----------------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	if ($form = $db->selectRow("
			SELECT p.*,g.good as good_orig
			FROM ?_price p
			LEFT JOIN ?_good_price gp ON p.id=gp.price_id
			LEFT JOIN ?_good g ON g.id=gp.good_id
			WHERE p.id=?d LIMIT 1", intval($_GET['id']))) {
		$form['good_js'] = str_replace("'","\'",$form['good']);
		$page_title = 'Изменить';
		$do_add = 0;
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['status'] = 1;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('good','good_orig') as $v)
		if (isset($form[$v])) $form[$v] = $cms->print_html($form[$v]);

	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' запись');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

}

//------------------------------------------------------------------------------------

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

//print"<pre>";print_r($_POST);;exit;

	if (!$_POST['form']['good_orig'])
		$bad[] = 'Пустое соответсвие';
	elseif (!$good = $db->selectRow("SELECT id FROM ?_good WHERE good=? LIMIT 1",$_POST['form']['good_orig'])) {
		if ($_POST['form']['good_new']) {
			//$good_id = $db->query("INSERT INTO ?_good (dt_insert,good) VALUES (now(),?)",$_POST['form']['good_orig']);
			$good_id = $cms->cmd_good_new(array('good'=>$_POST['form']['good_orig']));
			$good['id'] = $good_id;
		} else
			$bad[] = 'Такого соответсвия нет';
	}

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else					$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	$good_price = $db->selectRow("SELECT * FROM ?_good_price WHERE price_id=?d LIMIT 1", $_POST['id']);

	if ($good['id'] == $good_price['good_id']) {
		$cms->set_status('ok',"Осталось без изменений");
	} elseif (!$good_price['id']) {
		$id = $db->query("INSERT INTO ?_good_price (dt_insert,good_id,price_id,adm_id)
					VALUES (now(),?d,?d,?d)",
			$good['id'],
			$_POST['id'],
			$_SESSION['auth']['id']
		);
		$cms->set_status('ok',"Добавлена запись");
	} else {
		$db->query("UPDATE ?_good_price SET dt_insert=now(),good_id=?d,price_id=?d,adm_id=?d WHERE id=?d LIMIT 1",
			$good['id'],
			$_POST['id'],
			$_SESSION['auth']['id'],
			$good_price['id']
		);
		$cms->set_status('ok',"Изменена запись");
	}
	
	//установка ЗА для товара
	$good = $db->selectRow("SELECT * from ?_good WHERE id=?d LIMIT 1",$good['id']);
	if (!$good['zarticle']){
		$zarticle = $db->selectCell("SELECT zarticle from ?_price p WHERE zarticle<>'' AND id IN (SELECT price_id from ?_good_price WHERE good_id=?d) LIMIT 1",$good['id']);
		if ($zarticle)
		$db->query("UPDATE ?_good SET zarticle=? WHERE id=?d LIMIT 1",$zarticle,$good['id']);
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//-------------------------------------------------------------------------------------

function cmd_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_price WHERE id=?d LIMIT 1", intval($_GET['id']));

	if (0) {
	} else {
		$db->query("DELETE FROM ?_good_price WHERE price_id=?d LIMIT 1", $r['id']);
		$db->query("DELETE FROM ?_price WHERE id=?d LIMIT 1", $r['id']);
		$cms->set_status('ok',"Удалена запись");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

function cmd_orig_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_good_price WHERE price_id=?d LIMIT 1", intval($_GET['id']));

	if ($r['id']) {
	
		$db->query("DELETE FROM ?_good_price WHERE id=?d LIMIT 1", $r['id']);
		$cms->set_status('ok',"Удалено соответствие");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

#################################################################################################

function cmd_price_del2() {
	global $cfg, $db, $cms;

	//print '<pre>';
	//print_r($_POST);
	//exit;
	

		$ids = $db->selectCol("SELECT id FROM ?_price WHERE provider_id=?d",$_POST['provider_id']);
		foreach ($ids as $id) {
			$db->query("DELETE FROM ?_price WHERE id=?d LIMIT 1",$id);
			$db->query("DELETE FROM ?_good_price WHERE price_id=?d LIMIT 1",$id);
		}	
	
	
	$cms->set_status('ok',"Прайсы удалены");
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

#################################################################################################


function cmd_csv () {
	global $cfg, $db, $smarty, $cms, $scheme_tree;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = urlencode($find['text']);
		$s = addslashes(trim($find['text']));
		//$WHERE_OR[] = "p.article like '%$s%'";
		$WHERE_OR[] = "p.good like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "p.id=$s";
	}

	if ($find['provider_id']) $WHERE_AND[] = "p.provider_id=".intval($find['provider_id']);
	if ($find['adm_id']) $WHERE_AND[] = "a.id=".intval($find['adm_id']);
	if ($find['empty']) $WHERE_AND[] = "gp.price_id IS NULL";

	if (!empty($find['price1'])) $WHERE_AND[] = 'price>='.intval($find['price1']);
	if (!empty($find['price2'])) $WHERE_AND[] = 'price<='.intval($find['price2']);
	
	if (empty($find['sklad']) && $find['sklad']!='0') $find['sklad'] = 8;
	if (!empty($find['sklad'])) $WHERE_AND[] = 'sklad>='.intval($find['sklad']);

	if ($_SESSION['auth']['access_id'] == 3) {
		$provider_list = $db->selectCol("-- CACHE: 1m
			SELECT provider_id FROM ?_adm_provider WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$provider_list[] = 0;
		$WHERE_AND[] = 'p.provider_id IN ('.implode(',', $provider_list).')';
	} else {
		$provider_list[] = 0;
	}
	


	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY good",		#0
		"ORDER BY good DESC",
		"ORDER BY sklad",		#2
		"ORDER BY sklad DESC",
		"ORDER BY price",		#4
		"ORDER BY price DESC",
		"ORDER BY dt_price",		#6
		"ORDER BY dt_price DESC",

	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 5;

	
	// list
	$list = $db->select("
				SELECT g.good as good_orig, p.good
					
				FROM ?_price p
				JOIN ?_provider pro		ON pro.id=p.provider_id
				LEFT JOIN ?_good_price gp	ON gp.price_id=p.id
				LEFT JOIN ?_good g		ON g.id=gp.good_id
				LEFT JOIN ?_adm a 		ON gp.adm_id=a.id
				$WHERE
			".$sorts[$_GET['sort']]);

	$fp = fopen($cfg['dir_root']."/files/good.csv","w");
			
	foreach ($list as $i=>$v) {
		foreach (array('good','good_orig') as $v2)
			$list[$i][$v2] = iconv("utf-8","cp1251",$list[$i][$v2]);
	
	
	
	fputcsv($fp,$list[$i],";");
	
	}
	
	fclose($fp);
	
	$cms->set_status('ok',"Ссылка для загрузки файла: <a href='/files/good.csv'><font color='white'>здесь</font></a>");
	header("Location: ".$_SERVER['HTTP_REFERER']);
	

	

}


function cmd_za_clear () {
	global $cfg, $db, $smarty, $cms, $scheme_tree;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = urlencode($find['text']);
		$s = addslashes(trim($find['text']));
		//$WHERE_OR[] = "p.article like '%$s%'";
		$WHERE_OR[] = "p.good like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "p.id=$s";
	}

	if ($find['provider_id']) $WHERE_AND[] = "p.provider_id=".intval($find['provider_id']);
	if ($find['adm_id']) $WHERE_AND[] = "a.id=".intval($find['adm_id']);
	if ($find['empty']) $WHERE_AND[] = "gp.price_id IS NULL";

	if (!empty($find['price1'])) $WHERE_AND[] = 'price>='.intval($find['price1']);
	if (!empty($find['price2'])) $WHERE_AND[] = 'price<='.intval($find['price2']);
	
	if (empty($find['sklad']) && $find['sklad']!='0') $find['sklad'] = 8;
	if (!empty($find['sklad'])) $WHERE_AND[] = 'sklad>='.intval($find['sklad']);

	if ($_SESSION['auth']['access_id'] == 3) {
		$provider_list = $db->selectCol("-- CACHE: 1m
			SELECT provider_id FROM ?_adm_provider WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$provider_list[] = 0;
		$WHERE_AND[] = 'p.provider_id IN ('.implode(',', $provider_list).')';
	} else {
		$provider_list[] = 0;
	}
	


	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	// clear
	$list = $db->select("
				SELECT p.id					
				FROM ?_price p
				JOIN ?_provider pro		ON pro.id=p.provider_id
				LEFT JOIN ?_good_price gp	ON gp.price_id=p.id
				LEFT JOIN ?_good g		ON g.id=gp.good_id
				LEFT JOIN ?_adm a 		ON gp.adm_id=a.id
				$WHERE
			".$sorts[$_GET['sort']]);
			
	foreach ($list as $i=>$v)
	$db->query("UPDATE ?_price SET zarticle='' WHERE id=?d LIMIT 1",$v['id']);

	$cms->set_status('ok',"ЗА очищены");
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

function cmd_rprice_clear () {
	global $cfg, $db, $smarty, $cms, $scheme_tree;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = urlencode($find['text']);
		$s = addslashes(trim($find['text']));
		//$WHERE_OR[] = "p.article like '%$s%'";
		$WHERE_OR[] = "p.good like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "p.id=$s";
	}

	if ($find['provider_id']) $WHERE_AND[] = "p.provider_id=".intval($find['provider_id']);
	if ($find['adm_id']) $WHERE_AND[] = "a.id=".intval($find['adm_id']);
	if ($find['empty']) $WHERE_AND[] = "gp.price_id IS NULL";

	if (!empty($find['price1'])) $WHERE_AND[] = 'price>='.intval($find['price1']);
	if (!empty($find['price2'])) $WHERE_AND[] = 'price<='.intval($find['price2']);
	
	if (empty($find['sklad']) && $find['sklad']!='0') $find['sklad'] = 8;
	if (!empty($find['sklad'])) $WHERE_AND[] = 'sklad>='.intval($find['sklad']);

	if ($_SESSION['auth']['access_id'] == 3) {
		$provider_list = $db->selectCol("-- CACHE: 1m
			SELECT provider_id FROM ?_adm_provider WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$provider_list[] = 0;
		$WHERE_AND[] = 'p.provider_id IN ('.implode(',', $provider_list).')';
	} else {
		$provider_list[] = 0;
	}
	


	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	// clear
	$list = $db->select("
				SELECT p.id					
				FROM ?_price p
				JOIN ?_provider pro		ON pro.id=p.provider_id
				LEFT JOIN ?_good_price gp	ON gp.price_id=p.id
				LEFT JOIN ?_good g		ON g.id=gp.good_id
				LEFT JOIN ?_adm a 		ON gp.adm_id=a.id
				$WHERE
			".$sorts[$_GET['sort']]);
			
	foreach ($list as $i=>$v)
	$db->query("UPDATE ?_price SET price=0 WHERE id=?d LIMIT 1",$v['id']);

	$cms->set_status('ok',"МРЦ очищены");
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

?>