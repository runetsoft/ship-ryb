{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`&find[provider_id]=`$find.provider_id`&find[adm_id]=`$find.adm_id`&find[empty]=`$find.empty`&find[price1]=`$find.price1`&find[price2]=`$find.price2`&find[sklad]=`$find.sklad`"}
{assign var=my_colspan value="12"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td><input type="text" name="find[text]" value="{$find.text}" style="width:95%"></td>
		<td><input type="checkbox" name="find[empty]"{if $find.empty} checked{/if} style="border:0" title="Показать только без соответсвия"></td>
		<td>{html_options name="find[provider_id]" options=$find.provider_tree selected=$find.provider_id}</td>
		<td>Цена от <input type="text" name="find[price1]" value="{$find.price1}" size="6">
			до <input type="text" name="find[price2]" value="{$find.price2}" size="6">
		</td>
		<td>
			Склад от <input type="text" name="find[sklad]" value="{$find.sklad}" size="2">
		</td>
		<td>{html_options name="find[adm_id]" options=$find.adm_tree selected=$find.adm_id}</td>
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="10%"><b>Поставщик</b></td>
	<td width="20%"><b>Соответствие</b></td>
	<td width="30%"><b>{insert name="list_sort" index="0" url="?$my_url&page=$page" text="Название поставщика"}</b></td>
	<td width="5%"><b>ЗА</b></td>
	<td width="5%"><b>{insert name="list_sort" index="2" url="?$my_url&page=$page" text="Склад"}</b></td>
	<td width="5%"><b>{insert name="list_sort" index="4" url="?$my_url&page=$page" text="Цена"}</b></td>
	<td width="5%"><b>{insert name="list_sort" index="8" url="?$my_url&page=$page" text="МРЦ"}</b></td>	
	<td width="5%"><b>{insert name="list_sort" index="6" url="?$my_url&page=$page" text="Обновление цены"}</b></td>
	<td width="5%"><b>Автор</b></td>
	<td width="5%"><b>Время соответствия</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td align="left">{$i.provider}</td>
	<td align="left">
		{$i.good_orig}
		{if $i.good_orig}
			<span style="cursor:pointer"
			{popup text="Удалить соответсвие для этой позиции в прайсе<br><a href='cmd.php?mod=`$smarty.get.mod`&do=orig_del&id=`$i.id`'>Да!</a>" sticky="true" trigger="onclick" above="true" left="1" caption="Удаление"}
			>[x]</span>
		{/if}
	</td>
	<td align="left">{$i.good}</td>
	<td align="left">{$i.zarticle}</td>	
	<td>{$i.sklad}</td>
	<td>{$i.price_chunk}</td>
	<td>{$i.rprice_chunk}</td>
	<td title="Последняя загрузка цены">{$i.dt_price}</td>
	<td title="Кто поставил соответсвие">{$i.login}</td>
	<td title="Когда поставил соответсвие">{$i.good_orig_insert}</td>
	
	{insert name="action_row"
		edit_url="?mod=`$smarty.get.mod`&do=edit&id=`$i.id`"
		del_url="cmd.php?mod=`$smarty.get.mod`&do=del&id=`$i.id`"}

</tr>
{/foreach}

{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url_="?mod=`$smarty.get.mod`&do=edit"}

	<tr class="grid1">
	<td colspan="{$my_colspan}" align="right"><a href="javascript:void(0);" onclick="return overlib('<a href=\'cmd.php?{$my_url}&do=rprice_clear\'>Да! Очистить!</a>',STICKY,HEIGHT,10,ABOVE,CAPTION,'Подтверждение очистки');">Очистить МРЦ</a> | <a href="javascript:void(0);" onclick="return overlib('<a href=\'cmd.php?{$my_url}&do=za_clear\'>Да! Очистить!</a>',STICKY,HEIGHT,10,ABOVE,CAPTION,'Подтверждение очистки');">Очистить ЗА</a> | <a href="cmd.php?{$my_url}&do=csv">Выгрузка в CSV</a></td>
	</tr>
	
</form>
</table>

<div onclick="document.getElementById('good').style.visibility='visible'"></div>
<span id="good" style="visibility:hidden;position:fixed;top:30%;left:30%;z-index:300;background-color:white">123</span>

{if $find.provider_id>0}
<p style="color:red">
<b>Удаление прайсов</b>
</p>
<p style="color:red">Будут удалены все строки из прайса выбранного провайдера + все соответствия для этих прайсов.</p>
<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="price_del2">
		<input type="hidden" name="provider_id" value="{$find.provider_id}">
		<input type="submit" style="color:red" value="Удалить все позиции провайдера">
	</td>
</tr>
</table>
</form>
{/if}


<p>
<b>Загрузка прайcов</b>
<p>
<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td>Поставщик</td>
	<td>{html_options name="form[provider_id]" options=$form.provider_tree selected=$form.provider_id}</td>
</tr>
<tr class="grid">
	<td>Фильтр</td>
	<td>{html_options name="form[scheme_id]" options=$form.scheme_tree selected=$form.scheme_id}</td>
</tr>
<tr class="grid">
	<td>Файл CSV (разделитель ";")</td>
	<td>
		<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
		<input name="file" type="file">
	</td>
</tr>
<tr class="grid">
	<td>Обновлять ЗА в любом случае</td>
	<td>
		<input type="checkbox" name="update_za" value="1">
	</td>
</tr>
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="load">
		<input type="submit" value="Загрузить">
	</td>
</tr>
</table>
</form>


<p>
<b>Загрузка соответствий из CSV</b>
<p>
<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td>Поставщик</td>
	<td>{html_options name="form[provider_id]" options=$form.provider_tree selected=$form.provider_id}</td>
</tr>
<tr class="grid">
	<td>Файл CSV (разделитель ";")</td>
	<td>
		<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
		<input name="file" type="file">
	</td>
</tr>
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="load_csv">
		<input type="submit" value="Загрузить">
	</td>
</tr>
</table>
</form>

<p>
<b>Загрузка соответствий из Excel (формат строго Excel 2003 - XLS)</b>
<p>
<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td>Поставщик</td>
	<td>{html_options name="form[provider_id]" options=$form.provider_tree selected=$form.provider_id}</td>
</tr>
<tr class="grid">
	<td>Файл CSV (разделитель ";")</td>
	<td>
		<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
		<input name="file" type="file">
	</td>
</tr>
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="load_xls">
		<input type="submit" value="Загрузить">
	</td>
</tr>
</table>
</form>




<!-- /list -->
