<?

include dirname(__FILE__).'/lib.php';

if (isset($_POST['cancel'])) {
	header("Location: ".(isset($_SESSION['form_sess']['back_url']) ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

//------------------------------------------------------------------------------------

} elseif (isset($cancel_access)) {

	echo "<HTML><BODY ".
		"onload=\"javascript:opener.location.href='".
		$_SESSION['form_sess']['back_url'].
		"';javascript:window.close();\">".
		"</BODY></HTML>";

}
elseif ($_POST['do'] == 'edit')
	cmd_edit();

elseif ($_POST['do'] == 'load')
	cmd_load();
	
elseif ($_POST['do'] == 'price_del2')
	cmd_price_del2();	
	
elseif ($_POST['do'] == 'load_csv')
	cmd_load_csv();	
	
elseif ($_POST['do'] == 'load_xls')
	cmd_load_xls();		

elseif ($_GET['do'] == 'load2')
	cmd_load2();

elseif ($_GET['do'] == 'del')
	cmd_del();

elseif ($_GET['do'] == 'orig_del')
	cmd_orig_del();
	
elseif ($_GET['do'] == 'csv')
	cmd_csv();	
	
elseif ($_GET['do'] == 'za_clear')
	cmd_za_clear();		
	
elseif ($_GET['do'] == 'rprice_clear')
	cmd_rprice_clear();			

else
{

	$cms->set_status('bad',"Системная ошибка");
	$cms->set_status('bad',"GET: ".print_r($_GET,1));
	$cms->set_status('bad',"POST: ".print_r($_POST,1));
	$cms->set_status('bad',"REFERER: ".$_SERVER['HTTP_REFERER']);
	$cms->set_status('bad',"URL: ".$_SERVER['REQUEST_URI']);
	header("Location: ".$_SERVER['HTTP_REFERER']);

}

?>
