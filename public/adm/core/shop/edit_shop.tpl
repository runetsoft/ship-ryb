
<!-- edit -->

<form action="cmd.php" method="post" enctype="multipart/form-data">

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="grid">
	<td class="head" width="20%">Магазинин</td>
	<td><input type="text" name="form[shop]" value="{$form.shop}" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">FTP</td>
	<td>
		ftp://
		<input type="text" name="form[ftp_user]" value="{$form.ftp_user}" style="width:15%">
		:
		<input type="text" name="form[ftp_pass]" value="**********" style="width:15%">
		@
		<input type="text" name="form[ftp_host]" value="{$form.ftp_host}" style="width:15%">
		/
		<input type="text" name="form[ftp_dir]" value="{$form.ftp_dir}" style="width:15%">
		/partner_offers.xml
	</td>
</tr>
<tr class="grid">
	<td class="head">Следующий номер для артикула</td>
	<td><input type="text" name="form[article_last]" value="{$form.article_last}" style="width:15%">
		<br>Номер будет автоматически увеличиваться при авто-создании новых артикулов
	</td>
</tr>
<tr class="grid">
	<td class="head">Кол-во знаков в артикуле</td>
	<td>
		<input type="text" name="form[article_width]" value="{$form.article_width}" style="width:15%">
		<br>Если при создании нового артикула, его длина будет меньше этого числа, то слева будут добавлены нули
	</td>
</tr>
<tr class="grid">
	<td class="head">Минимальное кол-во склада для выгрузки</td>
	<td><input type="text" name="form[sklad_min]" value="{$form.sklad_min}" style="width:5%"></td>
</tr>
<tr class="grid">
	<td class="head">Статус</td>
	<td><label name="form[status]"><input type="checkbox" name="form[status]"{if $form.status} checked{/if}> активен</label></td>
</tr>

</table>

<p>
<center>
<input type="hidden" name="mod" value="{$smarty.get.mod}">
<input type="hidden" name="do" value="{$smarty.get.do}">
<input type="hidden" name="do_add" value="{$do_add}">
{if $do_add}
	<input type="submit" value="Добавить">
{else}
	<input type="hidden" name="id" value="{$smarty.get.id}">
	<input type="submit" value="Изменить">
{/if}
<input type="submit" name="cancel" value="Отмена">
</center>
<p>

</form>

<!-- /edit -->
