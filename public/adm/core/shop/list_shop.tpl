{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`"}
{assign var=my_colspan value="9"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td width="90%"><input type="text" name="find[text]" value="{$find.text_html}" style="width:95%"></td>
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="40%"><b>{insert name="list_sort" index="0" url="?$my_url&page=$page" text="Магазин"}</b></td>
	<td width="25%"><b>Выгрузка XML</b></td>
	<td width="15%"><b>Последняя выгрузка на ftp</b></td>
	<td width="10%"><b>Поставщики</b></td>
	<td width="10%"><b>Города</b></td>
	<td width="10%"><b>Мин. склад</b></td>
	<td width="5%"><b>Статус</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td align="left">{$i.shop}</td>
	<td>
		<a href="cmd.php?mod={$smarty.get.mod}&do=xml&file=1&id={$i.id}">скачать файл</a>
		|
		<a href="cmd.php?mod={$smarty.get.mod}&do=xml&ftp=1&id={$i.id}">выгрузить на ftp</a>
	</td>
	<td>{$i.ftp_dt}</td>
	<td><a href="?mod={$smarty.get.mod}&provider_list=1&shop_id={$i.id}">{$i.provider_cnt}</a></td>
	<td></td>
	<td>{$i.sklad_min}</td>
	<td>
		{if $i.status}	<font color="green">активен</font>
		{else}		-
		{/if}
	</td>
	
	{insert name="action_row"
		edit_url="?mod=`$smarty.get.mod`&do=edit&id=`$i.id`"
		del_url="cmd.php?mod=`$smarty.get.mod`do=del&id=`$i.id`"}

</tr>
{/foreach}

{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?mod=`$smarty.get.mod`&do=edit"}

</form>
</table>


<!-- /list -->
