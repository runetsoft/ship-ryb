<?

function mod_provider_list() {
	global $cfg, $db, $smarty, $cms;

	if ($_SESSION['auth']['access_id'] == 3) {
		$provider_list = $db->selectCol("-- CACHE: 1m
			SELECT provider_id FROM ?_adm_provider WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$provider_list[] = 0;
	}

	// list
	$list = $db->select("
					SELECT p.id,p.provider,p.status,
						sp.id as link,pg.provider_group
					FROM ?_provider p
					JOIN ?_provider_group pg ON pg.id=p.provider_group_id
					LEFT JOIN ?_shop_provider sp ON p.id=sp.provider_id AND sp.shop_id=?d
					WHERE TRUE {AND 1=?d AND p.id IN (".implode(",", $provider_list).")}
					ORDER BY p.provider",
		intval($_GET['shop_id']),
		$_SESSION['auth']['access_id'] == 3 ? 1 : DBSIMPLE_SKIP
	);

	$r = $db->selectRow("SELECT * FROM ?_shop WHERE id=? LIMIT 1",intval($_GET['shop_id']));
	
	$smarty->assign('list',$list);
	$smarty->assign('page_title',"Список поставщиков для выгрузки в магазин ".
					"'<a href='?mod=".$_GET['mod']."'>".
					$cms->print_html($r['shop']).
					"</a>'");
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list_provider.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);

}

function cmd_provider_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

//print"<pre>";print_r($_POST);exit;

	if (!$_POST['shop_id'])
		$bad[] = 'Не указан магазин';
	elseif (!$db->selectRow("SELECT id FROM ?_shop WHERE id=?d LIMIT 1",intval($_POST['shop_id'])))
		$bad[] = 'Такой магазин не найден';

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else								$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	$log['links'] = $links = array_keys($_POST['form']['link']);

	// получаем список старых
	$r = $db->selectCol("SELECT provider_id FROM ?_shop_provider WHERE shop_id=?d",$_POST['shop_id']);
	$log['exist'] = $r;

	// проверяем и удаляем лишние
	foreach ($r as $v) {
		if (!in_array($v,$links)) {
			$db->query("DELETE FROM ?_shop_provider WHERE shop_id=?d AND provider_id=?d LIMIT 1",$_POST['shop_id'],$v);
			$log['delete'][] = $v;
		}
	}
	// проверяем и добавляем новые
	foreach ($links as $v) {
		if (!in_array($v,$r)) {
			$db->query("INSERT IGNORE INTO ?_shop_provider (dt_insert,shop_id,provider_id) VALUES (now(),?d,?d)",$_POST['shop_id'],$v);
			$log['insert'][] = $v;
		}
	}
	
	//$cms->set_status('ok',print_r($log,1));
	$cms->mail_debug('shop_provider_edit',print_r($log,1));
	$cms->set_status('ok',"Изменена запись");

	header("Location: ".$_SERVER['HTTP_REFERER']);
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}
