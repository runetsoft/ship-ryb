<?

function mod_list () {
	global $cfg, $db, $smarty, $cms;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = $cms->print_html($find['text']);
		$s = addslashes($find['text']);
		$WHERE_OR[] = "shop like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "s.id=$s";
	}

	if ($_SESSION['auth']['access_id'] == 3) {
		$shop_list = $db->selectCol("-- CACHE: 1m
			SELECT shop_id FROM ?_adm_shop WHERE adm_id=?d",
			$_SESSION['auth']['id']);
		$shop_list[] = 0;
		$WHERE_AND[] = 's.id IN ('.implode(',', $shop_list).')';
	}
	
	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY shop",		#0
		"ORDER BY shop DESC",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 0;

	// page
	$count = $db->selectCell("-- CACHE: 1m
		SELECT count(*)
		FROM ?_shop s
		$WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("
			SELECT s.*,
			(SELECT count(*) FROM ?_shop_provider WHERE shop_id=s.id) as provider_cnt
			FROM ?_shop s
			$WHERE
		".$sorts[$_GET['sort']]." LIMIT ?d,?d",
		$page * $elements - $elements, $elements);

	foreach ($list as $i => $v) {
		foreach (array('shop') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		foreach (array('ftp_dt') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
		
	}
	$smarty->assign('list',$list);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);	
	$smarty->assign('page_title','Список магазинов');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list_shop.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);

}

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	if ($form = $db->selectRow("SELECT * FROM ?_shop WHERE id=?d LIMIT 1", intval($_GET['id']))) {
		$page_title = 'Изменить';
		$do_add = 0;
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['status'] = 1;
		$form['article_width'] = 6;
		$form['sklad_min'] = 4;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('shop','ftp_user','ftp_pass','ftp_host','ft_dir') as $v)
		if (isset($form[$v])) $form[$v] = $cms->print_html($form[$v]);

	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' запись');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit_shop.tpl');

	unset($_SESSION['form']);

}

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['shop'])
		$bad[] = 'Пустое название';
	elseif ($db->selectRow("SELECT id FROM ?_shop WHERE shop=? {AND id!=?d}", 
			$_POST['form']['shop'],
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id']))
		$bad[] = 'Такой магазин уже существует';

	if (!$_POST['form']['article_last'])
		$bad[] = 'Не указан номер следующего артикула';

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else					$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if ($_POST['do_add']) {
		$id = $db->query("INSERT INTO ?_shop (dt_insert,shop,status,article_last,article_width,
					ftp_user,ftp_pass,ftp_host,ftp_dir,
					sklad_min)
					VALUES (now(),?,?d,?d,?d, ?,?,?,?, ?d)",
			$_SESSION['form']['shop'],
			$_SESSION['form']['status'],
			$_SESSION['form']['article_last'],
			$_SESSION['form']['article_width'],
				$_SESSION['form']['ftp_user'],
				$_SESSION['form']['ftp_pass'],
				$_SESSION['form']['ftp_host'],
				$_SESSION['form']['ftp_dir'],
			$_SESSION['form']['sklad_min']
		);
		
		// добавить всех провайдеров
		$providers = $db->selectCol("SELECT id FROM ?_provider");
		foreach ($providers as $provider_id) {
			//$db->query("INSERT IGNORE INTO ?_shop_provider (dt_insert,shop_id,provider_id) VALUES (now(),?d,?d)",$id,$provider_id);
		}

		$cms->cmd_shop_new(array('shop_id'=>$id));
		$cms->set_status('ok',"Добавлена запись");
		
	} else {

		$db->query("UPDATE ?_shop SET dt_update=now(),shop=?,status=?d,article_last=?d,article_width=?d,
					ftp_user=?,ftp_pass=?,ftp_host=?,ftp_dir=?,
					sklad_min=?d
				WHERE id=?d",
			$_SESSION['form']['shop'],
			$_SESSION['form']['status'],
			$_SESSION['form']['article_last'],
			$_SESSION['form']['article_width'],
				$_SESSION['form']['ftp_user'],
				$_SESSION['form']['ftp_pass'],
				$_SESSION['form']['ftp_host'],
				$_SESSION['form']['ftp_dir'],
			$_SESSION['form']['sklad_min'],
			$_POST['id']);
		$cms->set_status('ok',"Изменена запись");
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

function cmd_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_shop WHERE id=?d LIMIT 1", intval($_GET['id']));

	if (1) {
		$cms->set_status('bad',"Не надо удалять");
	} else {
		$db->query("DELETE FROM ?_shop WHERE id=?d LIMIT 1", $r['id']);
		$db->query("DELETE FROM ?_shop_provider WHERE shop_id=?d", $r['id']);
		$cms->set_status('ok',"Удален магазин");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

function cmd_xml() {
	global $db, $sql, $cms;

	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('display_errors',1);
	$filename = 'partner_offers.xml';

	$sql->time_start();
	$r = $sql->cmd_export(array('shop_id'=>$_GET['id']));
	
	$time = $sql->time_end();

	$xml = $r['xml'];
	$log['xml'] = $r['log'];
	
	if ($_GET['file']) {
	
		header("Content-type: text/xml");
		header("Content-Disposition: attachment; filename='$filename'");
		header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
		header("Pragma: no-cache");
		print $xml;
		exit;

	} elseif ($_GET['ftp']) {
	
		$r = $db->selectRow("SELECT * FROM ?_shop WHERE id=?d", intval($_GET['id']));
		$sql->time_start();

		if (!$h = ftp_connect($r['ftp_host'], 21, 15)) {
			$bad[] = 'Неудалось соедениться с ftp';
		} else {
			if (!ftp_login($h, $r['ftp_user'], $r['ftp_pass'])) {
				$bad[] = 'Неудалось залогиниться на ftp';
			} else {
				if (!ftp_chdir($h, $r['ftp_dir'])) {
					$bad[] = 'Неудалось открыть папку на ftp';
				} else {
					if (!$x = tmpfile()) {
						$bad[] = 'Неудалось открыть временный файл';
					} else {
						fwrite($x, $xml);
						fseek($x,0);
						if (!ftp_fput($h, $filename, $x, FTP_ASCII)) {
							$bad[] = 'Неудалось сохранить файл на ftp';
						} else {
							$ok[] = 'Файл записан на ftp';
							$ok[] = 'Размер xml '.$cms->print_traf(strlen($xml));
							$ok[] = 'Генерация xml '.$time.' sec';
							$ok[] = 'Запись на ftp '.$sql->time_end().' sec';
							$db->query("UPDATE ?_shop SET ftp_dt=now() WHERE id=?d LIMIT 1",intval($_GET['id']));
							
							$ok[] = 'Выгружено поставщиков: '.$log['xml']['provider_cnt'];
							$ok[] = 'Выгружено номенклатуры: '.$log['xml']['price_cnt'];
							//$ok[] = 'log: '.print_r($log,1);
						}
					}
				}
			}
		}

		if (count($bad))
			foreach ($bad as $v)
				$cms->set_status('bad',$v);
		
		if (count($ok))
			foreach ($ok as $v)
				$cms->set_status('ok',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;		
	}	


}

?>