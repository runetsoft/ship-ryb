<?


function mod_access_list () {
	global $cfg, $db, $smarty, $cms;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	$WHERE_AND[] = "site_id=".intval($_GET['site_id']);

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";


	$sorts = array(
		"ORDER BY k1,k2,access,moder",		#0
		"ORDER BY model DESC,id",
		"ORDER BY sn,id",		#2
		"ORDER BY sn DESC,id",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 0;


	// list
	$list = $db->select("SELECT sa.*,ma.access,
					concat(m.fio,' (',m.login,')') as moder,
					concat(a.fio,' (',a.login,')') as adm,
				IF (sa.group_id=0,1,0) as k1,
				IF (sa.moder_id=0,1,0) as k2
				FROM ?_site_access sa
				LEFT JOIN ?_adm_access ma ON ma.id=sa.group_id
				LEFT JOIN ?_adm m ON m.id=sa.moder_id
				LEFT JOIN ?_adm a ON a.id=sa.adm_id
				$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i => $v) {
		foreach (array('dt_insert') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
	}

	$r1 = $db->select("SELECT id,access
			FROM ?_adm_access
			WHERE id NOT IN (SELECT group_id FROM ?_site_access WHERE site_id=?d)
			ORDER BY access",intval($_GET['site_id']));
	foreach ($r1 as $v)
		$form['access_tree']['Группы']['a_'.$v['id']] = $cms->print_html($v['access']);
	$r2 = $db->select("SELECT m.*,a.access
			FROM ?_adm m
			LEFT JOIN ?_adm_access a ON a.id=m.access_id
			WHERE m.id NOT IN (SELECT moder_id FROM ?_site_access WHERE site_id=?d)
			ORDER BY m.fio",intval($_GET['site_id']));
	foreach ($r2 as $v)
		$form['access_tree']['Пользователи']['m_'.$v['id']] =
			$cms->print_html($v['fio']." ".$v['fio2']." (".$v['login'].") [".$v['access']."]");


	$r = $db->selectRow("SELECT * FROM ?_site WHERE id=?d LIMIT 1",intval($_GET['site_id']));

	$smarty->assign('list',$list);
	$smarty->assign('form',$form);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);
	$smarty->assign('page_title',"Список доступа к модулю '<a href='?mod=0'>".$cms->print_html($r['name'])."</a>'");
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list_access.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}

//-------------------------------------------------------------------------------

function cmd_access_edit() {
	global $cfg, $db, $cms;


	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['access']) {
		$bad[] = 'Пустой выбор';
	}
	elseif (preg_match("/^(.)_(\d+)$/", $_POST['form']['access'], $r))
	{
		if ($r[1] == 'a') {
			$group_id = $r[2];
			$moder_id = 0;
		} else {
			$group_id = 0;
			$moder_id = $r[2];
		}
	}
	else
		$bad[] = 'Данные некоректны';

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if (1) {
		$id = $db->query("INSERT INTO ?_site_access (dt_insert,site_id,group_id,moder_id,adm_id)
					VALUES (now(),?d,?d,?d,?d)",
			$_POST['site_id'],
			$group_id,
			$moder_id,
			$_SESSION['auth']['id']
		);
		$cms->set_status('ok',"Добавлена запись");

	} else {

	}

	header("Location: ".$_SERVER['HTTP_REFERER']);
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//---------------------------------------------------------------------


function cmd_access_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_site_access WHERE id=?d LIMIT 1", intval($_GET['id']));

	{
		$db->query("DELETE FROM ?_site_access WHERE id=?d", $r['id']);
		$cms->set_status('ok',"Удалена запись");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}
