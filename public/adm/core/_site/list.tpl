
<!-- site list -->

<!--
<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>
-->
<form action="cmd.php" method="post">

<p>
<b>{$page_title}</b>
<p>
<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="head">
	<td align="center" width="1%"><b>#</b></td>
	<td align="center" width="5%"><b>Расположение</b></td>
	<td align="center" width="40%"><b>Название</b></td>
	<td align="center" width="10%"><b>Модуль</b></td>
	<td align="center" width="10%"><b>Доступ</b></td>
	<td align="center" width="10%"><b>Вес</b></td>
	<td align="center" width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}" align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td align="left">{html_options name="form[`$i.id`][pid]" options=$i.tree selected=$i.pid}</td>
	<td><input type="text" name="form[{$i.id}][name]" value="{$i.name}" style="width:95%"></td>
	<td>{$i.class}</td>
	<td><a href="?mod={$smarty.get.mod}&access_list=1&site_id={$i.id}">{$i.access_cnt}</a></td>
	<td><input type="text" name="form[{$i.id}][orderby]" value="{$i.orderby}" style="width:80%"></td>

	{insert name="action_row"
		checkbox="`$i.id`" id_prefix="site"
		edit_url="?mod=`$smarty.get.mod`&do=edit&id=`$i.id`&pid=`$i.pid`"
		del_url="cmd.php?mod=`$smarty.get.mod`&do=del&id=`$i.id`"}

</tr>
{/foreach}

{if $list}
	{insert name="action_multi" colspan="7" mod="`$smarty.get.mod`" id_prefix="site" update_ids="`$list`"
		button_edit="true" button_del="true"}
{/if}

</table>

<div align="right">
<a href="?mod={$smarty.get.mod}&do=edit&pid={$smarty.get.pid}">
<span {popup width="50" left="10" text="Добавить"}>
{html_image file="img/icon/b_insrow.png" border="0" align="absmiddle" alt=""}</span></a>
</div>

</form>

<!-- /site list -->
