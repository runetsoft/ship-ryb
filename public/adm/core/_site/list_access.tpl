{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`"}
{assign var=my_colspan value="2"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="20%"><b>Группа</b></td>
	<td width="20%"><b>Пользователь</b></td>
	<td width="15%"><b>Когда добавлено</b></td>
	<td width="25%"><b>Кто добавил</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td>{$i.access}</td>
	<td>{$i.moder}</td>
	<td>{$i.dt_insert}</td>
	<td>{$i.adm}</td>

	{insert name="action_row"
		edit_url_="?$my_url&do=edit_hardware&id=`$i.id`&page=$page&sort=`$smarty.get.sort`"
		del_url="cmd.php?$my_url&do=del_access&id=`$i.id`"}

</tr>
{/foreach}

<tr class="grid" align="center">
	<td></td>
	<td colspan="3">
		{html_options name="form[access]" options=$form.access_tree selected=$form.access}
	</td>
	<td>
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="site_id" value="{$smarty.get.site_id}">
		<input type="hidden" name="do" value="edit_access">
		<input type="submit" value="Добавить">
	</td>
	<td></td>
</tr>

{*insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?$my_url&do=edit_hardware&page=$page&sort=`$smarty.get.sort`"*}

</form>
</table>


<!-- /list -->
