<?

function mod_list() {
	global $db, $smarty, $tree, $cms, $cfg;

	$lang_id = $_SESSION['sess']['lang'];

	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$list = $db->select("SELECT *,
			(SELECT count(*) FROM ?_site_access WHERE site_id=s.id) as access_cnt
			FROM ?_site s
			WHERE pid=?d AND lang_id=?d
			ORDER BY orderby,name", $_GET['pid'], $lang_id);

	foreach ($list as $i => $v) {
		$list[$i]['name'] = $cms->print_html($v['name']);

		$list[$i]['tree'] = $cms->get_tree(
			array(	'table'		=> '?_site',
				'field_key'	=> 'id',
				'field_value'	=> 'name',
				'field_pid'	=> 'pid',
				'where'		=> "lang_id=$lang_id",
				'root'		=> '/',
				'order_by'	=> 'pid,orderby,name',
				'exclude'	=> $v['id'],
			)
		);

		$list[$i]['class'] = $cfg['site_adm_list'][$v['class']]['name'];
	}

	$smarty->assign('list',$list);
	$smarty->assign('page_title','Список разделов сайта');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');
}


//---------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $do, $tree, $cfg, $cms;

	if (!isset($_GET['id'])) $_GET['id'] = '';

	if ($form = $db->selectRow("SELECT * FROM ?_site WHERE id=? LIMIT 1", $_GET['id'])) {
		$do_add = 0;
		$page_title = 'Изменить';
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['pid'] = isset($_GET['pid']) ? $_GET['pid'] : 0;
		$form['orderby'] = 0;
		$form['page_type'] = 0;
		$form['hidden'] = 1;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('page_title','page_desc','page_keyword') as $v)
		$form[$v] = isset($form[$v]) ? $cms->print_sql($form[$v]) : '';

	foreach ($cfg['site_adm_list'] as $i=>$v)
		$form['class_tree'][$i] = $v['name'];

	$lang_id = $_SESSION['sess']['lang'];
	if (isset($form['id']))
		$form['pid_tree'] = $cms->get_tree(
			array(	'table'		=> '?_site',
				'field_key'	=> 'id',
				'field_value'	=> 'name',
				'field_pid'	=> 'pid',
				'where'		=> "lang_id=$lang_id",
				'root'		=> '/',
				'order_by'	=> 'pid,orderby,name',
				'length'	=> 0,
				'exclude'	=> $form['id'],
			)
		);
	else
		$form['pid_tree'] = $cms->get_tree(
			array(	'table'		=> '?_site',
				'field_key'	=> 'id',
				'field_value'	=> 'name',
				'field_pid'	=> 'pid',
				'where'		=> "lang_id=$lang_id",
				'root'		=> '/',
				'order_by'	=> 'pid,orderby,name',
				'length'	=> 0,
			)
		);

	$form['img_info'] = $cms->img_info($cfg['files']['site'],
				isset($form['file_name']) ? $form['file_name'] : '',
				isset($form['id']) ? $form['id'] : '');

	$smarty->assign('form',$form);
	$smarty->assign('do',$_GET['do']);
	$smarty->assign('do_add',$do_add);
	if (isset($_GET['id']))
		$smarty->assign('id',$_GET['id']);
	$smarty->assign('mod',$_GET['mod']);
	$smarty->assign('path',$cfg['files']['site']);
	$smarty->assign('page_title',$page_title.' раздел сайта');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

}

//---------------------------------------------------------------------------------

function cmd_edit() {
	global $cfg, $db, $cms, $sql;

//$sql->debug_db(1);

	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['name']) {
		$cms->set_status('bad','Пустое название'); $bad = 1;
	}

//print"<pre>";print_r($_POST);exit;

	foreach (array('hidden') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = 1;
		else					$_SESSION['form'][$v] = 0;

	if (isset($bad)) {
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if ($_POST['do_add']) {
		$db->query("INSERT INTO ?_site (dt_insert,lang_id,pid,name,class,orderby)
				VALUES (now(),?d,?d,?,?,?d)",
				$_SESSION['sess']['lang'],
				$_SESSION['form']['pid'],
				$_SESSION['form']['name'],
					$_SESSION['form']['class'],
					$_SESSION['form']['orderby']
		);
		$cms->set_status('ok',"Добавлена запись");
	} else {
		$db->query("UPDATE ?_site SET dt_update=now(),pid=?d,name=?,class=?,orderby=?d
				WHERE id=?d",
			$_SESSION['form']['pid'],
			$_SESSION['form']['name'],
				$_SESSION['form']['class'],
				$_SESSION['form']['orderby'],
			$_POST['id']);
		$cms->set_status('ok',"Изменена запись");
	}

	header("Location: ".(isset($_SESSION['form_sess']['back_url']) ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//--------------------------------------------------------------------------------------

function cmd_del($id) {

	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_site WHERE lang_id=?d AND id=?d LIMIT 1", $_SESSION['sess']['lang'], $id);

	if ($db->selectRow("SELECT * FROM ?_site WHERE lang_id=?d AND pid=?d LIMIT 1", $_SESSION['sess']['lang'], $id)) {
		$cms->set_status('bad',"Этот раздел имеет вложения (".$cms->print_html($r['name']).")");
	} else {

		$db->query("DELETE FROM ?_site WHERE lang_id=?d AND id=?d", $_SESSION['sess']['lang'], $id);
		$cms->set_status('ok',"Удалена запись");
	}
}

//-------------------------------------------------------------------------------------------

function cmd_multi() {
	global $cfg, $db, $cms;

	if (count($_POST['update'])) {

		if (isset($_POST['multi_edit_x'])) { //---------------------------------------------------------

			foreach ($_POST['update'] as $id => $value) {

				$r = $db->selectRow("SELECT lang_id FROM ?_site WHERE id=?d LIMIT 1",$id);

				{
					$db->query("UPDATE ?_site SET dt_update=now(), pid=?,name=?,orderby=?d WHERE id=?",
						$_POST['form'][$id]['pid'],
						$_POST['form'][$id]['name'],
						$_POST['form'][$id]['orderby'],
						$id);
					$cms->set_status('ok', "Изменена запись (".$_POST['form'][$id]['name'].")");
				}
//-------------------------------------------------------



			}

		} elseif (isset($_POST['multi_del_x'])) { //--------------------------------------------------
			foreach ($_POST['update'] as $id => $value)
				cms_del($id);

		}

	} else
		$cms->set_status('bad','Ничего не выделено');

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

?>