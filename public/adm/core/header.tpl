<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>{$info.site_name} - {$info.cp_text}</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<script type="text/javascript" src="lib/js/jquery/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="lib/js/dtree/dtree.js"></script>
	<script type="text/javascript" src="lib/php/JsHttpRequest/JsHttpRequest.js"></script>
	<link rel="StyleSheet" href="lib/js/dtree/dtree.css" type="text/css" />
	<link rel="stylesheet" href="lib/css/adm.css" type="text/css">
	<!--
	<link rel="stylesheet" type="text/css" href="lib/css/kube/css/kube.css" /> 	
	-->
	

</head>
{popup_init src="lib/js/overlib/Mini/overlib_mini.js"}


<!-- header -->

<table width='100%' style="border-bottom: 1px dashed #808080;">
<tr>
<td><b>{$info.site_name} - {$info.cp_text}</b></td>
{if $lang_tree}
<form method="post" action="cmd.php">
<td>
    <select name="lang" onchange='this.form.submit()'>
    {html_options options=$lang_tree selected=$smarty.session.sess.lang}
    </select>
    <input type="hidden" name="mod" value="set">
</td>
</form>
{/if}
{if $price_low}
<td>Найдено {$price_low} товаров с ценой ниже 500 руб.</td>
{/if}
<td align="right">
    Пользователь: {$auth.login} ({$auth.fio})
    | Уровень доступа: {$auth.access}
    | <a href="?logout=1">Выход</a>
</td>
</tr>
</table>

<!-- /header -->

<table>
<tr valign="top">

