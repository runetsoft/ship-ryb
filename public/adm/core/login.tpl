<html>
<head>
<title>{$info.site_name} - {$info.cp_text}</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="stylesheet" href="lib/css/adm.css" type="text/css">
<link rel="stylesheet" type="text/css" href="lib/js/keyboard/keyboard.css">
<script type="text/javascript" src="lib/js/keyboard/keyboard.js"></script>
</head>
<body>

<!-- login -->

<form method="post"
	{if $smarty.server.SERVER_NAME == "dsms.ru"}
		action="/adm/"
	{/if}
	onsubmit="this.submit.disabled=true">

<table width="100%" height="100%">
<tr>
<td align="center" valign="middle">

	<table class="grid" cellpadding="2" cellspacing="1">
	<tr class="grid">
	<th colspan="2">
		{html_image file="img/login.gif"}
		Авторизация
	</th>
	</tr>
	<tr class="grid">
	<td align="right">Логин/E-mail:</td>
		<td align="center">
			<input type="text" id="login" name="login" value="{$login}" class="keyboardInput">
			{if !$login}
				<script type="text/javascript">
				document.getElementById('login').focus();
				</script>
			{/if}
		</td>
	</tr>
	<tr class="grid">
		<td align="right">Пароль:</td>
		<td align="center">
			<input type="password" id="password" name="password" class="keyboardInput">
			{if $login}
				<script type="text/javascript">
				document.getElementById('password').focus();
				</script>
			{/if}
		</td>
	</tr>
	<tr class="grid">
		<td align="right">На сутки:</td>
		<td align="left"><input type="checkbox" name="time24" style="border:0" checked></td>
	</tr>
	<tr class="grid">
		<td colspan="2" align="center">
			<input type="submit" name="submit" value="Вход">
			<input type="hidden" name="on_submit" value="1">
		</td>
	</tr>
	</table>

{if $smarty.session.auth.badlogin == 1}<p><font size="-1" color="red">Пара логин/пароль не найдена</font>{/if}
{if $smarty.session.auth.badlogin == 2}
	<p><font size="-1" color="red">
		Вы запросили закрытую для вас страницу на сайте<br>Ваша авторизации автоматически снята, из-за нарушения доступа
	</font>
{/if}

</td>
</tr>
</table>

</form>

<!-- /login -->

</body>
</html>
