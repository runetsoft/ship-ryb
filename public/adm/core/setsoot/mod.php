<?

include dirname(__FILE__).'/lib.php';

if (isset($_GET['do']) && strpos($_GET['do'],'edit')!==false) {
	if (!isset($_SESSION['form_sess']['back_url']))
		$_SESSION['form_sess']['back_url'] = $_SERVER['HTTP_REFERER'];
} else
	unset($_SESSION['form_sess']['back_url']);


if (isset($_GET['do']) && $_GET['do'] == 'price_list') {
	
	mod_price_list();
	
} else {
	mod_start();
}

?>