<?


//-----------------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	$form = $db->select("SELECT param,value,name,'text' as type  FROM ?_sysctl
				WHERE	param = 'rate_usd_yesterday'
				OR	param = 'rate_eur_yesterday'
				OR	param = 'rate_usd_today'
				OR	param = 'rate_eur_today'
				OR	param = 'weather_type_tomorrow'
				OR	param = 'weather_type_today'
				OR	param = 'weather_temp_tomorrow'
				OR	param = 'weather_temp_today'
				OR	param = 'weather_img_tomorrow'
				OR	param = 'weather_img_today'
			ORDER BY name
		");

	$r = $db->select("SELECT param,value,name,'select' as type  FROM ?_sysctl WHERE
				param = 'kupon_bottom'");
	$r[0]['tree'] = array('banner'=>'Баннер','funny'=>'Анекдот','info'=>'Информация');

//	$form = array_merge($form, $r);

	$page_title = 'Изменить';
	$do_add = 0;

	//if ($_SESSION['form']) $form = $_SESSION['form'];

	$smarty->assign('form',$form);
	$smarty->assign('do','edit');
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' системные настройки');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

//print"<pre>";print_r($form);
}

//-------------------------------------------------------------------------------------

function cmd_edit() {
	global $db, $cfg, $cms;

	//$_SESSION['form'] = $_POST['form'];

//print"<pre>";print_r($_POST);exit;

	if ($_POST['do_add']) {
	} else {
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='rate_eur_today' LIMIT 1",		$_POST['form']['rate_eur_today']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='rate_eur_yesterday' LIMIT 1",	$_POST['form']['rate_eur_yesterday']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='rate_usd_today' LIMIT 1",		$_POST['form']['rate_usd_today']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='rate_usd_yesterday' LIMIT 1",	$_POST['form']['rate_usd_yesterday']);

		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_img_today' LIMIT 1",		$_POST['form']['weather_img_today']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_img_tomorrow' LIMIT 1",	$_POST['form']['weather_img_tomorrow']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_temp_today' LIMIT 1",		$_POST['form']['weather_temp_today']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_temp_tomorrow' LIMIT 1",	$_POST['form']['weather_temp_tomorrow']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_type_today' LIMIT 1",		$_POST['form']['weather_type_today']);
		$db->query("UPDATE ?_sysctl SET dt_update=now(),value=? WHERE param='weather_type_tomorrow' LIMIT 1",	$_POST['form']['weather_type_tomorrow']);
			
		$cms->set_status('ok',"Изменены переменные");
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//-------------------------------------------------------------------------------------------


?>