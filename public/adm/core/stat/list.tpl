{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`"}
{assign var=my_colspan value="2"}

<!-- list -->


<link rel="stylesheet" type="text/css" media="all" href="lib/js/calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
<script type="text/javascript" src="lib/js/calendar/calendar.js"></script>
<script type="text/javascript" src="lib/js/calendar/lang/calendar-ru-UTF.js"></script>
<script type="text/javascript" src="lib/js/calendar/calendar-setup.js"></script>


<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%" border=0>
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0">
			<tr>
				<td rowspan="2">Период</td>
				<td> с </td>
				<td><input id="f_date_1" type="text" readonly="1" name="find[dt1]" value="{$find.dt1}" size="9" /></td>
				<td><img id="f_trigger_1"
					onmouseout="this.style.background=''"
					onmouseover="this.style.background='red';"
					title="Выбрать дату" style="border: 1px solid red; cursor: pointer;"
					src="lib/js/calendar/img.gif"/>
				{literal}
					<script type="text/javascript">
						Calendar.setup({
						inputField : "f_date_1", // id of the input field
						ifFormat : "%d.%m.%Y", // format of the input field
						button : "f_trigger_1", // trigger for the calendar (button ID)
						//align : "Tl", // alignment (defaults to "Bl")
						singleClick : true,
						//showsTime : true
					});
					</script>
				{/literal}
				</td>
				<td rowspan="2">
					<input type="submit" value="Искать">
					<input type="hidden" name="mod" value="{$smarty.get.mod}">
					<input type="hidden" name="sort" value="{$smarty.get.sort}">
				</td>
			</tr>
			<tr>
				<td> по </td>
				<td><input id="f_date_2" type="text" readonly="1" name="find[dt2]" value="{$find.dt2}" size="9" /></td>
				<td><img id="f_trigger_2"
					onmouseout="this.style.background=''"
					onmouseover="this.style.background='red';"
					title="Выбрать дату" style="border: 1px solid red; cursor: pointer;"
					src="lib/js/calendar/img.gif"/>
				{literal}
					<script type="text/javascript">
						Calendar.setup({
						inputField : "f_date_2", // id of the input field
						ifFormat : "%d.%m.%Y", // format of the input field
						button : "f_trigger_2", // trigger for the calendar (button ID)
						//align : "Tl", // alignment (defaults to "Bl")
						singleClick : true,
						//showsTime : true
					});
					</script>
				{/literal}
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<tr class="head" align="center">
	<td width="50%"><b>Логин</b></td>
	<td width="50%"><b>Число соответвий</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.login}</td>
	<td>{$i.cnt}</td>
	

</tr>
{/foreach}
</table>

<p><b>Детально</b></p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="head">
	<th width="{$width}%">Дата</th>
	{foreach from=$users key=u item=user}
		<th width="{$width}%">{$user}</th>
	{/foreach}
</tr>
{foreach from=$list2 key=k2 item=i2}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">
	<td>{$i2.dt2}</td>
	{foreach from=$users key=u item=user}
		<td>{$i2.$u}</td>
	{/foreach}
	
</tr>
{/foreach}
</table>

<!-- /list -->
