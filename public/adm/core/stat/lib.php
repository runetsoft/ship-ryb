<?

function mod_list () {
	global $cfg, $db, $smarty, $cms;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();
	$find['dt1'] = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/','$3-$2-$1',$find['dt1']);
	$find['dt2'] = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/','$3-$2-$1',$find['dt2']);
	
	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = $cms->print_html($find['text']);
		$s = addslashes($find['text']);
		$WHERE_OR[] = "provider like '%$s%'";
	}
	
	if (!$find['dt1'] || !preg_match('/^\d{4}-\d{2}-\d{2}$/',$find['dt1'])) {
		$find['dt1'] = date("Y-m-d");
	}
	if (!$find['dt2'] || !preg_match('/^\d{4}-\d{2}-\d{2}$/',$find['dt2'])) {
		$find['dt2'] = date("Y-m-d");
	}
	$dt1 = $find['dt1'];
	$dt2 = $find['dt2'];

	if ($dt1) $WHERE_AND[] = "g.dt_insert>='$dt1 00:00:00'";
	if ($dt2) $WHERE_AND[] = "g.dt_insert<='$dt2 23:59:59'";

	$find['dt1'] = $cms->print_dt($find['dt1']);
	$find['dt2'] = $cms->print_dt($find['dt2']);

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY provider",		#0
		"ORDER BY provider DESC",
		"ORDER BY price_cnt",		#2
		"ORDER BY price_cnt DESC",
		"ORDER BY price_full_cnt",		#4
		"ORDER BY price_full_cnt DESC",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 5;

	// list
	$list = $db->select("
		SELECT login, count(*) as cnt
		FROM ?_good_price g
		JOIN ?_adm a ON a.id = g.adm_id
		$WHERE
		GROUP BY login
		ORDER BY cnt DESC");


	foreach ($list as $i => $v) {
		foreach (array('provider','org_name') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		foreach (array('dt_price_load') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
		
	}

	$list2 = $db->select("
		SELECT DISTINCT date(g.dt_insert) as dt
		FROM ?_good_price g
		$WHERE
		ORDER BY dt");
	foreach($list2 as $i=>$v) {
		$r = $db->select("
			SELECT a.id,login, count(*) as cnt
			FROM ?_good_price g
			JOIN ?_adm a ON a.id = g.adm_id
			WHERE date(g.dt_insert)=?
			GROUP BY login
			ORDER BY login",$v['dt']);
		$list2[$i]['users'] = $r;
		foreach ($r as $v2) {
			$list2[$i]['dt2'] = $cms->print_dt($v['dt']);
			$list2[$i][$v2['id']] = $v2['cnt'];
			$users[$v2['id']] = $v2['login'];
		}
	}
    $users = $users ? $users : array();
	asort($users);
	$width = 100/(count($users)+1);
	
	
//	echo "<pre>";print_r($list2);
	
	$smarty->assign('list',$list);
	$smarty->assign('list2',$list2);
	$smarty->assign('users',$users);
	$smarty->assign('width',$width);
	
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);	
	$smarty->assign('page_title','Статистика соответсвий');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}


?>