<?

include dirname(__FILE__).'/lib.php';

if (isset($_GET['do']) && strpos($_GET['do'],'edit')!==false) {
	if (!isset($_SESSION['form_sess']['back_url']))
		$_SESSION['form_sess']['back_url'] = $_SERVER['HTTP_REFERER'];
} else
	unset($_SESSION['form_sess']['back_url']);


if ($_GET['do'] == 'edit')
	mod_edit();

elseif ($_GET['provider_list'])
	mod_provider_list();

elseif ($_GET['shop_list'])
	mod_shop_list();
	
else
	mod_list();


?>