<?

function mod_shop_list() {
	global $cfg, $db, $smarty, $cms;
	
	// list
	$list = $db->select("
			SELECT s.id,s.shop,s.status,
				s2.id as link
			FROM ?_shop s
			LEFT JOIN ?_adm_shop s2 ON s.id=s2.shop_id AND s2.adm_id=?d
			ORDER BY s.shop",
		intval($_GET['adm_id'])
	);

	$r = $db->selectRow("SELECT * FROM ?_adm WHERE id=? LIMIT 1",intval($_GET['adm_id']));
	
	$smarty->assign('list',$list);
	$smarty->assign('page_title',"Список магазинов для пользователя ".
					"'<a href='?mod=".$_GET['mod']."'>".
					$cms->print_html($r['login']).
					"</a>'");
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list_shop.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);

}

function cmd_shop_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

//print"<pre>";print_r($_POST);exit;

	if (!$_POST['adm_id'])
		$bad[] = 'Не указан пользователь';
	elseif (!$db->selectRow("SELECT id FROM ?_adm WHERE id=?d LIMIT 1",intval($_POST['adm_id'])))
		$bad[] = 'Такой пользователь не найден';

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else								$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	$log['links'] = $links = array_keys($_POST['form']['link']);

	// получаем список старых
	$r = $db->selectCol("SELECT shop_id FROM ?_adm_shop WHERE adm_id=?d",$_POST['adm_id']);
	$log['exist'] = $r;

	// проверяем и удаляем лишние
	foreach ($r as $v) {
		if (!in_array($v,$links)) {
			$db->query("DELETE FROM ?_adm_shop WHERE adm_id=?d AND shop_id=?d LIMIT 1",$_POST['adm_id'],$v);
			$log['delete'][] = $v;
		}
	}
	// проверяем и добавляем новые
	foreach ($links as $v) {
		if (!in_array($v,$r)) {
			$db->query("INSERT IGNORE INTO ?_adm_shop (dt_insert,adm_id,shop_id) VALUES (now(),?d,?d)",$_POST['adm_id'],$v);
			$log['insert'][] = $v;
		}
	}
	
	//$cms->set_status('ok',print_r($log,1));
	$cms->mail_debug('adm_shop_edit',print_r($log,1));
	$cms->set_status('ok',"Изменена запись");

	header("Location: ".$_SERVER['HTTP_REFERER']);
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}
