<?

function mod_list () {
	global $cfg, $db, $smarty, $cms;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = $cms->print_html($find['text']);
		$s = addslashes($find['text']);
		$WHERE_OR[] = "login like '%$s%'";
		$WHERE_OR[] = "fio like '%$s%'";
		if (preg_match('/\d+/',$s,$r))
			$WHERE_OR[] = "a.id='$s'";
	}

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY dt_login,id",		#0
		"ORDER BY dt_login DESC,id",
		"ORDER BY cnt_login,id",	#2
		"ORDER BY cnt_login DESC,id",
		"ORDER BY login,id",		#4
		"ORDER BY login DESC,id",
		"ORDER BY fio,id",		#6
		"ORDER BY fio DESC,id",
		"ORDER BY status,id",		#8
		"ORDER BY status DESC,id",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 0;

	// page
	$count = $db->selectCell("-- CACHE: 1m
		SELECT count(*) FROM ?_adm a $WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("
			SELECT a.*,aa.access,
			(SELECT count(*) FROM ?_adm_provider WHERE adm_id=a.id) as provider_cnt,
			(SELECT count(*) FROM ?_adm_shop WHERE adm_id=a.id) as shop_cnt
			FROM ?_adm a
			LEFT JOIN ?_adm_access aa ON aa.id=a.access_id
			$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i => $v) {
		foreach (array('login','fio') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		foreach (array('dt_login') as $v2)
			$list[$i][$v2] = $cms->print_dt($list[$i][$v2]);
	}
	$smarty->assign('list',$list);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('sort',isset($_GET['sort']) ? $_GET['sort'] : '');
	$smarty->assign('find',$find);
	$smarty->assign('mod',isset($_GET['mod']) ? $_GET['mod'] : '');
	$smarty->assign('page_title','Список администраторов');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list_adm.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}

//----------------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	if ($form = $db->selectRow("SELECT * FROM ?_adm WHERE id=?d LIMIT 1", intval($_GET['id']))) {
		$page_title = 'Изменить';
		unset($form['password']);
		$do_add = 0;
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['status'] = 1;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('login','fio') as $v)
		if (isset($form[$v])) $form[$v] = $cms->print_html($form[$v]);
	$r = $db->select("SELECT id,access FROM ?_adm_access ORDER BY access");
	foreach ($r as $v)
		$form['access_tree'][$v['id']] = $v['access'];

	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('do',isset($_GET['do']) ? $_GET['do'] : '');
	$smarty->assign('id',isset($_GET['id']) ? $_GET['id'] : 0);
	$smarty->assign('mod',isset($_GET['mod']) ? $_GET['mod'] : '');
	$smarty->assign('page_title',$page_title.' администратора');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit_adm.tpl');

	unset($_SESSION['form']);

}

//------------------------------------------------------------------------------------

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['login']) {
		$cms->set_status('bad','Пустой логин'); $bad = 1;
	} elseif ($db->selectRow("SELECT id FROM ?_adm WHERE login=? {AND id!=?d}", 
			$_POST['form']['login'],
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id'])) {
		$cms->set_status('bad','Такой логин уже существует'); $bad = 1;
	}
	if ($_POST['do_add'] && !$_POST['form']['password']) {
		$cms->set_status('bad','Пустой пароль'); $bad = 1;
	} elseif ($_POST['form']['password'] && strlen($_POST['form']['password']) < 6) {
		$cms->set_status('bad','Пароль слишком короткий (мин. 6 знаков)'); $bad = 1;
	}
	if (!$_POST['form']['fio']) {
		$cms->set_status('bad','Пустое Ф.И.О.'); $bad = 1;
	}
	$e = $cms->check_email2(array('email'=>$_POST['form']['email'],'level'=>2));
	if ($e['code'] == 500) {
		$cms->set_status('bad',$e['message']); $bad = 1;
	}

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else					$_SESSION['form'][$v] = "0";

	if (isset($bad) && $bad) {
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if ($_POST['do_add']) {
		$id = $db->query("INSERT INTO ?_adm (dt_insert,login,password,fio,email,status,access_id)
					VALUES (now(),?,?,?,?,?d,?d)",
			$_SESSION['form']['login'],
			crypt('adm'.$_SESSION['form']['password']),
			$_SESSION['form']['fio'],
			$_SESSION['form']['email'],
			$_SESSION['form']['status'],
			$_SESSION['form']['access_id']
		);
		$cms->set_status('ok',"Добавлена запись");
		$cms->mail_debug('Добавлен администратор');
	} else {

		$db->query("UPDATE ?_adm SET dt_update=now(),login=?{,password=?},fio=?,email=?,status=?d,access_id=?d WHERE id=?d",
			$_SESSION['form']['login'],
			empty($_POST['form']['password']) ? DBSIMPLE_SKIP : crypt('adm'.$_POST['form']['password']),
			$_SESSION['form']['fio'],
			$_SESSION['form']['email'],
			$_SESSION['form']['status'],
			$_SESSION['form']['access_id'],
			$_POST['id']);
		$cms->set_status('ok',"Изменена запись");
		$cms->mail_debug('Изменен администратор');
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//-------------------------------------------------------------------------------------

function cmd_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_adm WHERE id=?d LIMIT 1", intval($_GET['id']));

	if ($r['id'] == $_SESSION['id'] && $_SESSION['access'] == 'adm') {
		$cms->set_status('bad',"Нельзя удалить самого себя (".$cms->print_html($r['login']).")");
	} else {
		$db->query("DELETE FROM ?_adm WHERE id=?d", $r['id']);
		$cms->set_status('ok',"Удален администратор (".$cms->print_html($r['login']).")");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

#################################################################################################

?>