
<!-- edit -->

<form action="cmd.php" method="post" enctype="multipart/form-data">

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<tr class="grid">
	<td class="head" width="10%">Логин</td>
	<td><input type="text" name="form[login]" value="{$form.login}" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Пароль</td>
	<td {if !$do_add}{popup text="Если не желаете менять пароль, оставьте это поле пустым."}{/if}
	><input type="text" name="form[password]" value="{$form.password}" style="width:50%"></td>
</tr>
<tr class="grid">
	<td class="head">Ф.И.О.</td>
	<td><input type="text" name="form[fio]" value="{$form.fio}" style="width:95%"></td>
</tr>
<tr class="grid">
	<td class="head">Email для связи</td>
	<td><input type="text" name="form[email]" value="{$form.email}" style="width:95%"></td>
</tr>
<tr class="grid">
	<td class="head">Статус</td>
	<td><label name="form[status]"><input type="checkbox" name="form[status]"{if $form.status} checked{/if}> активен</label></td>
</tr>
<tr class="grid">
	<td class="head">Группа</td>
	<td>{html_options name="form[access_id]" options=$form.access_tree selected=$form.access_id}</td>
</tr>
</table>

<p>
<center>
<input type="hidden" name="mod" value="{$mod}">
<input type="hidden" name="do" value="{$do}">
<input type="hidden" name="do_add" value="{$do_add}">
{if $do_add}
	<input type="submit" value="Добавить">
{else}
	<input type="hidden" name="id" value="{$id}">
	<input type="submit" value="Изменить">
{/if}
<input type="submit" name="cancel" value="Отмена">
</center>
<p>

</form>

<!-- /edit -->
