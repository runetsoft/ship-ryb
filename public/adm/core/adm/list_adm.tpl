{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`"}
{assign var=my_colspan value="11"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td width="90%"><input type="text" name="find[text]" value="{$find.text_html}" style="width:95%"></td>
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="20%"><b>{insert name="list_sort" index="0" url="?$my_url&page=$page" text="Послений вход"}</b></td>
	<td width="10%"><b>{insert name="list_sort" index="2" url="?$my_url&page=$page" text="Кол-во входов"}</b></td>
	<td width="10%"><b>{insert name="list_sort" index="4" url="?$my_url&page=$page" text="Логин"}</b></td>
	<td width="30%"><b>{insert name="list_sort" index="6" url="?$my_url&page=$page" text="Ф.И.О."}</b></td>
	<td width="10%"><b>Email</b></td>
	<td width="10%"><b>Группа</b></td>
	<td width="10%"><b>Поставщики</b></td>
	<td width="10%"><b>Магазины</b></td>
	<td width="10%"><b>{insert name="list_sort" index="8" url="?$my_url&page=$page" text="Статус"}</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td>{if $i.dt_login == "00.00.0000 00:00:00"}-{else}{$i.dt_login}{/if}</td>
	<td>{$i.cnt_login}</td>
	<td>{$i.login}</td>
	<td align="left">{$i.fio}</td>
	<td>{$i.email}</td>
	<td>{$i.access}</td>
	<td><a href="?mod={$smarty.get.mod}&provider_list=1&adm_id={$i.id}">{$i.provider_cnt}</a></td>
	<td><a href="?mod={$smarty.get.mod}&shop_list=1&adm_id={$i.id}">{$i.shop_cnt}</a></td>
	<td>
		{if $i.status}	<font color="green">активен</font>
		{else}		-
		{/if}
	</td>
	{insert name="action_row"
	edit_url="?$my_url&do=edit&id=`$i.id`"
	del_url="cmd.php?$my_url&do=del&id=`$i.id`"}

</tr>
{/foreach}

{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?$my_url&do=edit"}

</form>
</table>


<!-- /list -->
