<?

function mod_list() {
	global $cfg, $db, $smarty, $cms, $sql;

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	$WHERE_AND[] = "pid=".intval($_GET['pid']);

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = $cms->print_html($find['text']);
		$s = addslashes($find['text']);
		$WHERE_OR[] = "name like '%$s%'";
		if (preg_match('/^\d+$/',$s,$r))
			$WHERE_OR[] = "id='$s'";
	}

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY orderby DESC,url",	#0
		"ORDER BY orderby,url DESC",
		"ORDER BY name",				#2
		"ORDER BY name DESC",
		"ORDER BY name_ru",				#3
		"ORDER BY name_ru DESC",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 0;

	// page
	$count = $db->selectCell("-- CACHE: 1m
				SELECT count(*)
				FROM ?_site_page
				$WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("
			SELECT *,
				(SELECT count(*) FROM ?_site_page WHERE pid=p.id) as cnt,
				(SELECT count(*) FROM ?_site_page_tag WHERE page_id=p.id) as tag_cnt
				FROM ?_site_page p
				$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i => $v) {
		foreach (array('name') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		
	}

	// вывести дерево
	$pid = intval($_GET['pid']);
	while($pid) {
		$r = $db->selectRow("SELECT * FROM ?_site_page WHERE id=?d LIMIT 1",intval($pid));
		$pid = $r['pid'];
		$path[] =
			($r['hidden'] ? '<span title="скрытая страница">[' : '').
			"<a href='?mod=".$_GET['mod']."&pid=".$r['pid']."'>".
			$cms->print_html($r['url'])."</a>".
			($r['hidden'] ? ']</span>' : '');
	}
	if (count($path)) {
		$path = array_reverse($path);
		$path = ' / '.implode(' / ', $path).' / ';
	}

	$smarty->assign('list',$list);
	$smarty->assign('form',$form);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);
	$smarty->assign('path',$path);
	$smarty->assign('page_title','Страницы сайта');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}

function mod_edit() {
	global $db, $smarty, $tree, $cfg, $cms;

	if ($form = $db->selectRow("SELECT * FROM ?_site_page WHERE id=?d LIMIT 1", intval($_GET['id']))) {
		$page_title = 'Изменить';
		$do_add = 0;
		$form['pid_tree'] = $cms->get_tree(
			array(
				'table'			=> '?_site_page',
				'field_key'		=> 'id',
				'field_value'		=> 'name',
				'field_pid'		=> 'pid',
				'where'			=> "lang_id=".$_SESSION['sess']['lang'],
				'root'			=> '/',
				'order_by'		=> 'pid,orderby,name',
				'length'		=> 0,
				'exclude'		=> $form['id'],
			)
		);
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['orderby'] = 0;
		$form['pid_tree'] = $cms->get_tree(
			array(
				'table'			=> '?_site_page',
				'field_key'		=> 'id',
				'field_value'		=> 'name',
				'field_pid'		=> 'pid',
				'where'			=> "lang_id=".$_SESSION['sess']['lang'],
				'root'			=> '/',
				'order_by'		=> 'pid,orderby,name',
				'length'		=> 0,
			)
		);
		$form['pid'] = $_GET['pid'];
	}
	$form['name'] = $cms->print_html($form['name']);
	$form['info'] = $cms->print_sql($form['info']);
	
	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach ($cfg['site_page_list'] as $i=>$v)
		$form['class_tree'][$i] = $v['name'];
	
	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' страницу');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

}

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

	if (!$_POST['form']['name'])
		$bad[] = 'Пустое название';
	
	if (!$_POST['form']['url'] && $_POST['form']['pid'])
		$bad[] = 'Пустой url может быть только у страницы на корневом уровне';
	elseif (!$_POST['form']['url'] &&
			$db->selectRow("SELECT id FROM ?_site_page WHERE url IS NULL {AND id!=?d} AND pid=0 AND lang_id=?d LIMIT 1", 
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id'],
			$_SESSION['sess']['lang']))
		$bad[] = 'Пустой урл может быть только у одной страницы на сайте';
	elseif ($db->selectRow("SELECT id FROM ?_site_page WHERE url=? {AND id!=?d} AND pid=?d AND lang_id=?d AND url IS NOT NULL LIMIT 1", 
			$_POST['form']['url'],
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id'],
			$_POST['form']['pid'],
			$_SESSION['sess']['lang']))
		$bad[] = 'Такой url уже есть на текущем уровне';
	elseif ($db->selectRow("SELECT id FROM ?_tag WHERE url=? LIMIT 1",
			$_POST['form']['url']))
		$bad[] = 'Такой url уже есть в тэгах';

	foreach (array('hidden') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = 1;
		else								$_SESSION['form'][$v] = 0;

	if (count($bad)) {
//$bad[] = print_r($_POST,1);
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if (!$_SESSION['form']['url']) $_SESSION['form']['url'] = null;
	
	if ($_POST['do_add']) {
		$cms->file_upload_add1('img','file_name');
		$db->query("INSERT INTO ?_site_page (dt_insert,lang_id,pid,name,info,url,
				class,hidden,orderby,file_name,
				page_title,page_desc,page_keyword)
				VALUES (now(),?d,?d,?,?,?, ?,?d,?d,?, ?,?,?)",
				$_SESSION['sess']['lang'],
				$_SESSION['form']['pid'],
				$_SESSION['form']['name'],
				$_SESSION['form']['info'],
				$_SESSION['form']['url'],
					$_SESSION['form']['class'],
					$_SESSION['form']['hidden'],
					$_SESSION['form']['orderby'],
					empty($_SESSION['form']['file_name']) ? '' : $_SESSION['form']['file_name'],
				$_SESSION['form']['page_title'],
				$_SESSION['form']['page_desc'],
				$_SESSION['form']['page_keyword']
		);
		$cms->file_upload_add2('img',$cfg['files']['site'],'file_name');
		$cms->set_status('ok',"Добавлена запись");

	} else {

		$cms->file_upload_edit('img',$cfg['files']['site'],'file_name','site_page',$_POST['id']);
		$db->query("UPDATE ?_site_page SET dt_update=now(),pid=?d,name=?,info=?,url=?,
				class=?,hidden=?d,orderby=?d{,file_name=?},
				page_title=?,page_desc=?,page_keyword=?
				WHERE id=?d",
			$_SESSION['form']['pid'],
			$_SESSION['form']['name'],
			$_SESSION['form']['info'],
			$_SESSION['form']['url'],
				$_SESSION['form']['class'],
				$_SESSION['form']['hidden'],
				$_SESSION['form']['orderby'],
				isset($_SESSION['form']['file_name']) ? $_SESSION['form']['file_name'] : DBSIMPLE_SKIP,
			$_SESSION['form']['page_title'],
			$_SESSION['form']['page_desc'],
			$_SESSION['form']['page_keyword'],
				$_POST['id']);
		$cms->set_status('ok',"Изменена запись");

	}
	
	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

function cmd_del() {

	global $cfg, $db, $cms;
	$r = $db->selectRow("SELECT * FROM ?_site_page WHERE lang_id=?d AND id=?d LIMIT 1", $_SESSION['sess']['lang'], intval($_GET['id']));
	if ($db->selectRow("SELECT * FROM ?_site_page WHERE pid=?d LIMIT 1", $r['id'])) {
		$cms->set_status('bad',"Эта страница имеет вложения");
	} else {
		$db->query("DELETE FROM ?_site_page WHERE id=?d", $r['id']);
		@unlink($cfg['dir_root'].$cfg['files']['site'].$r['id']."_".$r['file_name']);
		$cms->set_status('ok',"Удален запись");
	}
	header("Location: ".$_SERVER['HTTP_REFERER']);

}

?>