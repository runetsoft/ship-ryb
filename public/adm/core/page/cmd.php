<?

include dirname(__FILE__).'/lib.php';

//------------------------------------------------------------------------------------
if (isset($_POST['cancel'])) {
	header("Location: ".(isset($_SESSION['form_sess']['back_url']) ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);
}
elseif ($_POST['do'] == 'edit')
	cmd_edit();

elseif ($_GET['do'] == 'del')
	cmd_del();


else {
	$cms->set_status('bad',"Системная ошибка");
	$cms->set_status('bad',"GET: ".print_r($_GET,1));
	$cms->set_status('bad',"POST: ".print_r($_POST,1));
	$cms->set_status('bad',"REFERER: ".$_SERVER['HTTP_REFERER']);
	$cms->set_status('bad',"URL: ".$_SERVER['REQUEST_URI']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

?>
