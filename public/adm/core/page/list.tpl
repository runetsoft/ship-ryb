{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`&pid=`$smarty.get.pid`"}
{assign var=my_colspan value="9"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

{$path}

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td width="90%"><input type="text" name="find[text]" value="{$find.text_html}" style="width:95%"></td>
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="pid" value="{$smarty.get.pid}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="10%"><b>URL</b></td>
	<td width="10%"><b>Подразделы</b></td>
	<td width="10%"><b>Тэги</b></td>
	<td width="50%"><b>Название</b></td>
	<td width="10%"><b>Модуль</b></td>
	<td width="10%"><b>Скрытая</b></td>
	<td width="10%"><b>Вес</b></td>
	<td width="10%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td align="left">
		<a href="?mod={$smarty.get.mod}&pid={$i.id}">{$i.url}</a>
	</td>
	<td>{$i.cnt}</td>
	<td>{$i.tag_cnt}</td>
	<td align="left">{$i.name}</td>
	<td>{$i.class}</td>
	<td>{if $i.hidden}да{/if}</td>
	<td>{$i.orderby}</td>

	{insert name="action_row"
		checkbox_="`$i.id`" id_prefix="categoty"
		edit_url="?$my_url&do=edit&id=`$i.id`"
		del_url="cmd.php?$my_url&do=del&id=`$i.id`"}

</tr>
{/foreach}


{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?$my_url&do=edit"}

</form>
</table>


<!-- /list -->
