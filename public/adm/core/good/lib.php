<?

function mod_list () {
	global $cfg, $db, $smarty, $cms;

//ini_set('magic_quotes_gpc','1');
//ini_set('magic_quotes_sybase','1');

	$elements = $_SESSION['sess']['elements'];
	$page = (abs(intval(isset($_GET['page']) ? $_GET['page'] : 0))); if (!$page) $page = 1;
	if (!isset($_GET['pid'])) $_GET['pid'] = 0;

	$find = (isset($_GET['find']) && !empty($_GET['find'])) ? $_GET['find'] : array();

	$WHERE_OR = array();
	$WHERE_AND = array();

	if (isset($find['text']) && !empty($find['text'])) {
		$find['text_html'] = urlencode($find['text']);
		$s = addslashes(trim($find['text']));
		$WHERE_OR[] = "good like '%$s%'";
		if (preg_match('/^\d+$/',$s))
			$WHERE_OR[] = "g.id=$s";
	}

	// where
	$WHERE = "WHERE TRUE";
	if (isset($WHERE_OR) && count($WHERE_OR))
		$WHERE .= " AND (".implode(" OR ", $WHERE_OR).")";
	if (isset($WHERE_AND) && count($WHERE_AND))
		$WHERE .= " AND (".implode(" AND ", $WHERE_AND).")";

	$sorts = array(
		"ORDER BY good",		#0
		"ORDER BY good DESC",
		"ORDER BY price_cnt",		#2
		"ORDER BY price_cnt DESC",
	);
	$_GET['sort'] = isset($_GET['sort']) ? abs(intval($_GET['sort'])) : 0;

	// page
	$count = $db->selectCell("-- CACHE: 1m
					SELECT count(*)
					FROM ?_good g
					$WHERE");
	$pages = ceil($count/$elements);
	if ($page > $pages && $pages) $page = $pages;

	// list
	$list = $db->select("
					SELECT g.*,
					(SELECT count(*) FROM ?_good_price WHERE good_id=g.id) as price_cnt
					FROM ?_good g
					$WHERE
			".$sorts[$_GET['sort']]." LIMIT ?d,?d",
			$page * $elements - $elements, $elements);

	foreach ($list as $i=>$v) {
		$list[$i]['good_url'] = rawurlencode($v['good']);
		foreach (array('good') as $v2)
			$list[$i][$v2] = $cms->print_html($list[$i][$v2]);
		
		if ($r = $db->select("
					SELECT p.id,p.good,p.sklad,p.price,provider,p.zarticle,p.rprice
					FROM ?_price p
					JOIN ?_good_price gp ON p.id=gp.price_id
					JOIN ?_provider pro ON pro.id=p.provider_id
					WHERE gp.good_id=?d",
					$v['id'])) {
			
			$info = "<table>";
			$info .= "<tr>";
			$info .= "<td><b>#</b></td>";
			$info .= "<td><b>Поставщик</b></td>";
			$info .= "<td><b>Название</b></td>";
			$info .= "<td><b>Склад</b></td>";
			$info .= "<td><b>Цена</b></td>";
			$info .= "<td><b>ЗА</b></td>";
			$info .= "<td><b>МРЦ</b></td>";
			$info .= "</tr>";
			$mods = $cms->mods_list();
			foreach ($r as $v2) {
				$rep = array('`','&');
				$v2['good'] = htmlspecialchars($v2['good']);
				
				$info .= "<tr>";
				$info .= "<td><a href='?mod=".$mods['price']."&find[text]=".$v2['id']."'>".$v2['id']."</a></td>";
				$info .= "<td nowrap>".$cms->print_html($v2['provider'])."</td>";
				$info .= "<td nowrap>".$v2['good']."</td>";
				//$info .= "<td nowrap>111</td>";
				$info .= "<td>".$v2['sklad']."</td>";
				$info .= "<td>".$v2['price']."</td>";
				$info .= "<td>".$v2['zarticle']."</td>";
				$info .= "<td>".$v2['rprice']."</td>";
				$info .= "</tr>";
			
		
			}
			$info .= "</table>";
			$list[$i]['info'] = $info;
		}
		
	}
	
	//print '<pre>';
	//print_r($list);
	
	$smarty->assign('list',$list);
	$smarty->assign('page',$page);
	$smarty->assign('pages',$pages);
	$smarty->assign('elements',$elements);
	$smarty->assign('count',$count);
	$smarty->assign('find',$find);	
	$smarty->assign('page_title','Список товаров');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/list.tpl');

//print"<pre>";print_r($WHERE);print_r($find);print_r($list);
}

//----------------------------------------------------------------------------------------

function mod_edit() {
	global $db, $smarty, $cfg, $cms;

	if ($form = $db->selectRow("SELECT * FROM ?_good WHERE id=?d LIMIT 1", intval($_GET['id']))) {
		$page_title = 'Изменить';
		$do_add = 0;
	} else {
		$do_add = 1;
		$page_title = 'Добавить';
		$form['status'] = 1;
	}

	if (isset($_SESSION['form'])) $form = array_merge($form, $_SESSION['form']);

	foreach (array('good') as $v)
		if (isset($form[$v])) $form[$v] = $cms->print_html($form[$v]);

	$smarty->assign('form',$form);
	$smarty->assign('do_add',$do_add);
	$smarty->assign('page_title',$page_title.' запись');
	$smarty->caching = false;
	$smarty->display(dirname(__FILE__).'/edit.tpl');

	unset($_SESSION['form']);

}

//------------------------------------------------------------------------------------

function cmd_edit() {
	global $cfg, $db, $cms;

	$_SESSION['form'] = $_POST['form'];

	$_POST['form']['good'] = preg_replace('/\\\/', '/', $_POST['form']['good']);
	if (!$_POST['form']['good'])
		$bad[] = 'Пустое название';
	elseif ($db->selectRow("SELECT id FROM ?_good WHERE good=? {AND id!=?d}", 
			$_POST['form']['good'],
			$_POST['do_add'] ? DBSIMPLE_SKIP : $_POST['id']))
		$bad[] = 'Такое название уже существует';

	foreach (array('status') as $v)
		if ($_SESSION['form'][$v]=="on")	$_SESSION['form'][$v] = "1";
		else					$_SESSION['form'][$v] = "0";

	if (count($bad)) {
		foreach ($bad as $v)
			$cms->set_status('bad',$v);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

	if ($_POST['do_add']) {
		$id = $db->query("INSERT INTO ?_good (dt_insert,good,rprice,zarticle,not_load)
					VALUES (now(),?,?d,?,?d)",
			$_SESSION['form']['good'],
			$_SESSION['form']['rprice'],
			$_SESSION['form']['zarticle'],			
			intval($_SESSION['form']['not_load'])
		);
		$cms->set_status('ok',"Добавлена запись");
	} else {

		$db->query("UPDATE ?_good SET dt_update=now(),good=?,rprice=?d,zarticle=?,not_load=?d WHERE id=?d",
			$_SESSION['form']['good'],
			$_SESSION['form']['rprice'],
			$_SESSION['form']['zarticle'],
			intval($_SESSION['form']['not_load']),
			$_POST['id']);
		$cms->set_status('ok',"Изменена запись");
	}

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}

//-------------------------------------------------------------------------------------

function cmd_load_za_mrc() {
	global $cfg, $db, $cms;


	
	if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

	if ($good_id = $db->selectCell("SELECT id from ?_good WHERE zarticle=? LIMIT 1",$data[0])){
		if (intval($data[1])>0)
			$db->query("UPDATE ?_good SET rprice=?d WHERE id=?d LIMIT 1",$data[1],$good_id);
	}
	
	
    }
    fclose($handle);
	
	}

	
	$cms->set_status('ok',"Файл обработан");

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}
//-------------------------------------------------------------------------------------

function cmd_rprice_clear() {
	global $cfg, $db, $cms;

	$db->query("UPDATE ?_good SET rprice = 0");
	
	$cms->set_status('ok',"МРЦ обнулены");

	header("Location: ".($_SESSION['form_sess']['back_url'] ?
		$_SESSION['form_sess']['back_url'] : $_SERVER['HTTP_REFERER']));
	unset($_SESSION['form']);
	unset($_SESSION['form_sess']);

}


//-------------------------------------------------------------------------------------

function cmd_del() {
	global $cfg, $db, $cms;

	$r = $db->selectRow("SELECT * FROM ?_good WHERE id=?d LIMIT 1", intval($_GET['id']));

	if ($db->selectRow("SELECT id FROM ?_good_price WHERE good_id=?d LIMIT 1",$r['id'])) {
		$cms->set_status('bad',"Нельзя удалить залинкованный товар");
	} else {
		$db->query("DELETE FROM ?_good WHERE id=?d LIMIT 1", $r['id']);
		$db->query("DELETE FROM ?_good_shop WHERE good_id=?d", $r['id']);
		$cms->set_status('ok',"Удалена запись");
	}

	header("Location: ".$_SERVER['HTTP_REFERER']);

}

function cmd_del_clear() {
	global $cfg, $db, $cms;
	
		$ids = $db->selectCol("SELECT id FROM ?_good");
		foreach ($ids as $id) {
		
		if (!$db->select("SELECT * from ?_good_price WHERE good_id=?d",$id)){
		$db->query("DELETE from ?_good WHERE id=?d LIMIT 1",$id);
		$db->query("DELETE from ?_good_shop WHERE good_id=?d",$id);
		}
	
		}
	
	$cms->set_status('ok',"Неиспользованные товары удалены");
	header("Location: ".$_SERVER['HTTP_REFERER']);

}

#################################################################################################

?>