{assign var=my_url value="mod=`$smarty.get.mod`&find[text]=`$find.text_html`&find[nesoza]=`$find.nesoza`"}
{assign var=my_colspan value="8"}

<!-- list -->

<script language="javascript">menu.openTo({$smarty.get.mod}, true);</script>

<p>
<b>{$page_title}</b>
<p>

<table class="grid" cellpadding="2" cellspacing="1" width="100%">
<form action="?" method="get">
<tr class="grid">
	<td colspan="{$my_colspan}">
	<table width="100%">
	<tr>
		<td>Поиск:</td>
		<td width="90%"><input type="text" name="find[text]" value="{$find.text}" style="width:95%"></td>		
		<td>
			<input type="submit" value="Искать">
			<input type="hidden" name="mod" value="{$smarty.get.mod}">
			<input type="hidden" name="sort" value="{$smarty.get.sort}">
		</td>
	</tr>
	</table>
	</td>	
</tr>
</form>
<form action="cmd.php" method="post">
<tr class="head" align="center">
	<td width="1%"><b>#</b></td>
	<td width="20%"><b>{insert name="list_sort" index="0" url="?$my_url&page=$page" text="Название"}</b></td>
	<td width="5%"><b>ЗА</b></td>
	<td width="5%"><b>МРЦ</b></td>	
	<td width="5%"><b>{insert name="list_sort" index="2" url="?$my_url&page=$page" text="Использовано"}</b></td>
	<td width="5%"><b>Маркет</b></td>
	<td width="5%"><b>Действия</b></td>
</tr>

{foreach from=$list key=k item=i}
<tr class="{cycle name="tr" values="grid1,grid2"}"  align="center"
	onMouseOut="this.className='{cycle name="mouse" values="grid1,grid2"}'"
	onMouseOver="this.className='grid3'">

	<td>{$i.id}</td>
	<td align="left">{$i.good}</td>
	<td>{$i.zarticle}</td>
	<td>{$i.rprice}</td>
	<td {if $i.info} style="cursor:pointer"
		{popup text="`$i.info`" sticky="true" trigger="onclick" above="true" left="1" caption="Информация"}
		{/if}>{$i.price_cnt}</td>
	<td><a target="_blank" href="http://market.yandex.ru/search.xml?text={$i.good_url}&cvredirect=2">link</a></td>
	
	{insert name="action_row"
		edit_url="?mod=`$smarty.get.mod`&do=edit&id=`$i.id`"
		del_url="cmd.php?mod=`$smarty.get.mod`&do=del&id=`$i.id`"}

</tr>
{/foreach}

{insert name="action_pages" colspan="$my_colspan"
	page_url="?$my_url&sort=`$smarty.get.sort`"
	add_url="?&mod=`$smarty.get.mod`do=edit"}

</form>
</table>


<!-- /list -->

<p><a href="javascript:void(0);" onclick="return overlib('<a href=\'cmd.php?{$my_url}&do=rprice_clear\'>Да! Очистить!</a>',STICKY,HEIGHT,10,ABOVE,CAPTION,'Подтверждение очистки');">Очистить все МРЦ</a></p>

<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="del_clear">
		<input type="submit" style="color:red" value="Удалить все товары без соответствий (использовано 0)">
	</td>
</tr>
</table>
</form>

<p><b>Загрузка ЗА и МРЦ в товары из СSV</b><br />
Колонки: ЗА;МРЦ<br />При прогрузке файла, ищем первый товар с ЗА, и проставляем ему МРЦ</p>
<form action="cmd.php" method="post" enctype="multipart/form-data">
<table class="grid" cellpadding="2" cellspacing="1">
<tr class="grid">
	<td>Файл CSV (разделитель ";")</td>
	<td>
		<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
		<input name="file" type="file">
	</td>
</tr>
<tr class="grid">
	<td colspan="2" align="center">
		<input type="hidden" name="mod" value="{$smarty.get.mod}">
		<input type="hidden" name="do" value="load_za_mrc">
		<input type="submit" value="Загрузить">
	</td>
</tr>
</table>
</form>
