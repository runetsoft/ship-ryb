<?php
require $cfg['dir_adm'].'/inc/const.php';
require $cfg['dir_adm'].'/lib/php/cms_adm/class.php';
require $cfg['dir_adm'].'/lib/php/cms_sql/class.php';



//===========================================================================

class my_cms_adm extends cms_adm {


	//----------------------------------------------------
	function print_news_tree() {
		global $cfg, $smarty, $db, $cms;
		$list = $db->select("SELECT * FROM ?_news_tree ORDER BY pid,orderby,name");
		foreach ($list as $i=>$v) {
			$list[$i][name] = $cms->short_name($v[name],15);
			$list[$i][title] = $v[name];
		}
		$smarty->assign('list',$list);
		$smarty->caching = false;
		$smarty->display('news/tree.tpl');
	}


	//----------------------------------------------------
	function print_good_tree() {
		global $cfg, $smarty, $db, $cms;
		$list = $db->select("SELECT * FROM ?_good_tree ORDER BY pid,orderby,name");
		foreach ($list as $i=>$v) {
			$list[$i][name] = $cms->short_name($v[name],15);
			$list[$i][title] = $v[name];
		}
		$smarty->assign('list',$list);
		$smarty->caching = false;
		$smarty->display('good/tree.tpl');
	}

}


//===========================================================================

$cms = new my_cms_adm;
$sql = new cms_sql;

if ( version_compare( phpversion(), '5', '<' ) ) {
	//$cms->__construct();
	die("PHP version ".phpversion()." not supported. Need PHP version 5.2.x +");

}
$cms->init_smarty();

$smarty->assign('info',$cfg['info']);
$smarty->assign('mod',(isset($_GET['mod']) && !empty($_GET['mod'])) ? $_GET['mod'] : '');

//-----------------------------------------------------

// check MySQL version
$ver = $db->selectCell("SELECT version()");
$a = explode(".",$ver);
if ($a[0] < '4')			die("[sql error] MySQL version < 4. Need version 4.1+");
if ($a[0] == '4' && $a[1] < '1')	die("[sql error] MySQL version < 4.1. Need version 4.1+");

// check MySQL table
$r = $db->selectCol("show tables");
if (!in_array($cfg['db_prefix']."lang", $r))
	die ("[sql error] table not found, use <a href='install.php'>install.php</a>");

//--------------------------------------------------

if (isset($_GET['logout']))
	$cms->logout();

$ver = $db->selectCell("SELECT version()");
$cms->check_auth();

$smarty->assign('auth',$_SESSION['auth']);
$cms->check_access();

function insert_chat_unread() {
	global $cfg, $db, $smarty;
	$count = $db->selectCell("SELECT count(*) FROM ?_game_chat WHERE user_id!=1 AND is_read=0");
	$smarty->assign('count',$count);
	$smarty->caching = false;
	$smarty->display($cfg['dir_adm'].'/core/game/win_chat_unread.tpl');

}

?>
