<?

$cfg['info']['site_name']	= $cfg['site_name'];
$cfg['info']['cp_text']		= "Панель управления";
$cfg['info']['version']		= "1.0.2 (".date("d.m.Y",filemtime(__FILE__)).")";
$cfg['info']['copyright']	= "Copyright &copy; 2006&mdash;".date("Y",filemtime(__FILE__))." &laquo;АДС Медиа&raquo;<br>E-mail: <a href=\"mailto:info@adsmedia.ru\">info@adsmedia.ru</a><br>Все права защищены";
$cfg['info']['support']		= "Техническая поддержка<br>Тел. +7 (812) 309 3898<br>E-mail: <a href=\"mailto:support@adsmedia.ru?subject=Support/".$cfg['info']['cp_text']."/".$cfg['info']['site_name']."\">support@adsmedia.ru</a>";

$cfg['files'] = array(
	'site'			=> '/files/site/',
	'news'			=> '/files/news/',
);

$cfg['site_adm_list'] = array(
	'adm'		=> array( 'name' => 'Пользователи'),
	'provider'	=> array( 'name' => 'Поставщики'),
	'good'		=> array( 'name' => 'Товары'),
	'price'		=> array( 'name' => 'Прайсы'),
	'shop'		=> array( 'name' => 'Магазины'),
	'stat'		=> array( 'name' => 'Статистика'),
	'nesoza'	=> array( 'name' => 'Несоза'),
	'good2'  	=> array( 'name' => 'Товары НеСоЗа'),
	'setsoot'  	=> array( 'name' => 'Простановка соответствий'),
);

$cfg['site_page_list'] = array(
	'page'		=> array( 'name' => 'Страница сайта'),

);

?>
