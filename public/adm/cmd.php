<?
error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE);

require dirname(__FILE__).'/../conf/conf.php';
require $cfg['dir_adm'].'/inc/local.php';



if (isset($_REQUEST['mod']) && $_REQUEST['mod'] == 'set')
	include_once("core/_set/cmd.php");
elseif (isset($_REQUEST['mod']) && $_REQUEST['mod'] == '0')
	include_once("core/_site/cmd.php");
elseif ($core = $db->selectCell("SELECT class FROM ?_site WHERE id=?d LIMIT 1",$_REQUEST['mod']))
	include_once("core/".$core."/cmd.php");
elseif (empty($_POST) || empty($_GET))
	header("Location: index.php");
else {
	echo "неверный запрос (error 65269, сообщите разработчику)";
	echo "GET ".print_r($_GET,1)."<br>";
	echo "POST ".print_r($_POST,1)."<br>";
	echo "SESSION ".print_r($_SESSION['auth'],1)."<br>";
	echo "REFERER ".$_SERVER['HTTP_REFERER']."<br>";
	echo "URL ".$_SERVER['REQUEST_URI']."<br>";
}

ob_end_flush();
?>
